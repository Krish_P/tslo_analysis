function sacstats = extractsaccades(eyepositiondata, sampletimedata, sacthresh, verbose);
% function sacstats = extractsaccades(eyepositiondata, sampletimedata);
% extractsaccades finds locations and amplitudes of saccades from an eye
% trace

%% defaults
% if verbose is not supplied, set to zero for "silent" operation
if nargin < 4;
    verbose = 1;
end

% set sacthresh to 10 deg/sec as default
if nargin < 3;
    sacthresh(1) = 200;
    sacthresh(2) = 1000;
end
% provide an upper limit for saccade velocity as well.
if numel(sacthresh) == 1;
    sacthresh(2) = 80;
end
% if sampletimedata is not supplied, assume 30hz continuous sampling
if nargin < 2;
    sampletimedata = [1:length(eyepositiondata(:,1))]./30;
end

%% fix the two ends so that the velocity crosses zero
eyepositiondata(2) = eyepositiondata(3) + 1;
eyepositiondata(1) = eyepositiondata(2) - 1;
eyepositiondata(end-1) = eyepositiondata(end-2) + 1;
eyepositiondata(end) = eyepositiondata(end-1) - 1;
eyepositiondata(2,2) = eyepositiondata(3,2) + 1;
eyepositiondata(1,2) = eyepositiondata(2,2) - 1;
eyepositiondata(end-1,2) = eyepositiondata(end-2,2) + 1;
eyepositiondata(end,2) = eyepositiondata(end-1,2) - 1;
%% calculate velocity and find saccades
eyevelx= [0; diff(eyepositiondata(:,1))./diff(sampletimedata(:))];
eyevely= [0; diff(eyepositiondata(:,2))./diff(sampletimedata(:))];
eyevel = [0; sqrt(diff(eyepositiondata(:,1)).^2+diff(eyepositiondata(:,2)).^2)./diff(sampletimedata(:))]; %%% Note, this is speed, not velocity
eyeaccel = [0; sqrt(diff(eyevelx).^2+diff(eyevely).^2)./diff(sampletimedata(:))]; %%% Acceleration should be useful for saccade identification and artifact rejection

sacindices = find((abs(eyevel) > sacthresh(1)) .*( abs(eyevel) < sacthresh(2)));

try
    if ~isempty(sacindices);
        
        %         uniquesacindices = sacindices(find(diff([sacindices; length(eyepositiondata)] )>1));
        uniquesacindices = sacindices(find(diff([-1; sacindices])>1 )); %& diff([-1; sacindices])~=15) & diff([-1; sacindices])~=16 & diff([-1; sacindices])~=15 & diff([-1; sacindices])~=32)
        %           uniquesacindicesnew=uniquesacindices;
        %% filtering that may be used if gaussian filter does not work
        diffindices=diff(uniquesacindices);
        hzartifact=zeros(size(uniquesacindices));
        for i=2:size(uniquesacindices)-1
            %            if abs(eyevel(uniquesacindices(i)-1)) <sacthresh(1); %& diffindices(i)==16
            %                    hzartifact(i)=uniquesacindices(i);
            %            end
            % %                    if diffindices(i)==15
            % %                        hzartifact(i)=uniquesacindices(i);
            % %                    end
            % %                  uniquesacindicesnew(i)=[];
            %            end
            if diffindices(i)<15
                hzartifact(i+1)=uniquesacindices(i+1);
            end
        end
        %            if isempty(hzartifact)
        %                uniquesacindicesnew=uniquesacindices;
        %            else
        uniquesacindicesnew=setdiff(uniquesacindices,hzartifact);
        %                uniquesacindicesnew(1)=uniquesacindices(1);
        %                uniquesacindicesnew(size(uniquesacindices))=uniquesacindices(size(uniquesacindices));
        %            end
        hzartifact;
        %           uniquesacindicesnew(size(uniquesacindices)-1)=uniquesacindices(size(uniquesacindices)-1);
        %         if verbose > 1;
        %             disp('sacindices and unique sacindices');
        %             disp(sacindices);
        %             disp(uniquesacindicesnew);
        %% plot results if desired
        if verbose > 0;
            sampleindices=1:size(eyepositiondata(:,1));
            newhandle=figure(300);hold off;
            plot(eyepositiondata(:,1),'r.-');hold on;
            plot(eyepositiondata(:,2),'b.-');
            plot(sampleindices(uniquesacindicesnew),eyepositiondata(uniquesacindicesnew),'bo');hold off;
            %             plot(sampleindices(uniquesacindicesnew),eyepositiondata(uniquesacindicesnew),'bo');hold off;
            %press return when done selecting your positions
            k=0;
            l=0;
            %            [x,y,button]=ginput;
            x=[];y=[];
            if size(x)>=1 %did you click something?
                for j=1:size(x) %loop through your clicks
                    if button(j)==1 %left click?
                        newindices=round(x(j));
                        l=l+1;
                        uniquesacindicesnew(length(uniquesacindicesnew)+1)=newindices;
                    end
                    if button(j)==3 %right click
                        k=k+1;
                        removeindices=round(x(j));
                        [c index] = min(abs(uniquesacindicesnew-removeindices))
                        uniquesacindicesnew(index)=[];
                    end
                    if button(j)==101
                        uniquesacindicesnew=[];
                    end
                    %                     plot(eyepositiondata(:,1),'r.-');hold on;
                    %                     plot(eyepositiondata(:,2),'b.-');hold on;
                    %                     plot(sampleindices(uniquesacindicesnew),eyepositiondata(uniquesacindicesnew),'bo');hold off;
                end
            end
            positionhandle = figure(301);hold off;
            %press return when done selecting your positions
            velocityhandle = figure(302);hold off;
            directionalhandle=figure(303);hold off;
        end
        %% find start and ends of saccades
        eyeveltemp = eyevel;
        sacstarts = zeros(1,length(uniquesacindicesnew));
        sacends = sacstarts;
        for idx = 1:length(uniquesacindicesnew)
            if eyeveltemp(uniquesacindicesnew(idx)) == 0;
                sacstarts(idx) = uniquesacindicesnew(idx);
                
            else
                temp = find(sign(eyeveltemp(1:uniquesacindicesnew(idx))) ...
                    == -sign(eyeveltemp(uniquesacindicesnew(idx))),1,'last');
                if verbose > 2;
                    disp(['line 84']);disp(temp)
                end
                if isempty(temp)
                    sacstarts(idx) = uniquesacindicesnew(idx);
                else
                    sacstarts(idx) = temp;
                end
                %                 if verbose>1;
                %                     disp('sacstarts');
                %                     disp(sacstarts);
                %                 end
                eyeveltemp(1:sacstarts(idx)) = 0;
            end
            
            if eyeveltemp(uniquesacindicesnew(idx)) == 0;
                
                sacends(idx) = uniquesacindicesnew(idx);
            else
                
                temp = (find(sign(eyeveltemp) == -sign(eyeveltemp(uniquesacindicesnew(idx))),1,'first'));
                if verbose > 2;
                    disp(['line 98']);disp(temp)
                end
                
                if isempty(temp)
                    sacends(idx) = uniquesacindicesnew(idx);
                else
                    sacends(idx) = temp;
                end
                %                 if verbose>1;
                %                     disp('sacends');
                %                     disp(sacends);
                %                 end
                
                eyeveltemp(1:sacends(idx)) = 0;
            end
        end
        sacends = sacends -1; % use the last sample before the velocity turns around
        
        % just fix the saccade end to be three samples (at 120 Hz) past the
        % threshold velocity.
        sacends = min(length(eyepositiondata), uniquesacindicesnew + fix(.03 ./ (sampletimedata(2) - sampletimedata(1))));
        %% find frequency velocity and direction of saccades
        sacfreq = length(uniquesacindicesnew) ./ (sampletimedata(end) - sampletimedata(1));
        %avetimebtwsac=1/sacfreq;
        sacamps = (sqrt(eyepositiondata(sacends,2).^2+eyepositiondata(sacends,1).^2) - sqrt(eyepositiondata(sacstarts,2).^2+eyepositiondata(sacstarts,1).^2));
        sacdirection=zeros(size(sacamps));
        sacvel=zeros(size(sacamps));
        %sactime=zeros(size(sacamps));
        
        for i = 1:size(sacamps)
            %              if verbose>0
            %                  figure(positionhandle); %hold on;
            %                  area(sampletimedata(sacstarts):sampletimedata(sacends),max(eyepositiondata(:,2)));
            % %                 area([sampletimedata(sacstarts(i)):sampletimedata(sacends(i))],[eyepositiondata(sacstarts(i),2):eyepositiondata(sacends(i),2)]);
            %              end
            sacvel(i)=max(abs(eyevel(sacstarts(i):sacends(i))));
            %sactime(i)=(sacends(i)-sacstarts(i));
            sacdirection(i)=atan((eyepositiondata(sacstarts(i),2)-eyepositiondata(transpose(sacends(i)),2))./(eyepositiondata(sacstarts(i),1)-eyepositiondata(transpose(sacends(i)),1)));
        end
        %% find drift amplitude and velocity
        shiftinmoviex=eyepositiondata(end-5,1)-eyepositiondata(5,1);
        shiftinmoviey=eyepositiondata(end-5,2)-eyepositiondata(5,2);
        sacx=eyepositiondata(sacends,1)- eyepositiondata(sacstarts,1);
        sacy=eyepositiondata(sacends,2)- eyepositiondata(sacstarts,2);
        driftx=shiftinmoviex-sum(sacx);
        drifty=shiftinmoviey-sum(sacy);
        driftdirection=atand(drifty/driftx);
        driftamp=sqrt(driftx.^2+drifty.^2);
        eyevelindices=1:size(eyevel,1);
        driftindices=setdiff(eyevelindices,sacindices);
        driftvelocity=mean(abs(eyevel(driftindices)));
        %% make the plots
        if verbose > 0;
            figure(positionhandle);hold on;
            %plot(sampletimedata(uniquesacindicesnew),eyepositiondata(uniquesacindicesnew,1),'go','markersize',14);
            plot(sampletimedata, eyepositiondata(:,1),'r.-')
            plot(sampletimedata,eyepositiondata(:,2),'b.-')
            plot(sampletimedata(sacstarts),eyepositiondata(sacstarts,1),'gs');
            plot(sampletimedata(sacends),eyepositiondata(sacends,1),'ms');%hold off
            plot(sampletimedata(sacstarts),eyepositiondata(sacstarts,2),'gs');
            plot(sampletimedata(sacends),eyepositiondata(sacends,2),'ms'); hold off;
            
            %             plot(sampletimedata(sacstarts),eyepositiondata(sacstarts,2),'gs');
            %             plot(sampletimedata(sacends),eyepositiondata(sacends,2),'ms');
            %             figure(velocityhandle);
            %             plot(sampletimedata(sacstarts),eyevel(sacstarts),'gs');
            %             plot(sampletimedata(sacends),eyevel(sacends),'ms');
            figure(directionalhandle)
            polar(sacdirection(:),sacamps(:),'ro')
            figure(velocityhandle); hold on
            plot(sampletimedata,eyevel,'r.-');
            %hold on;
            %plot(sampletimedata(sacindices),eyevel(sacindices),'bo');
            plot(sampletimedata(uniquesacindicesnew),eyevel(uniquesacindicesnew),'gx'); hold off;
        end
    else
        sacfreq = 0; sacamps = 0; uniquesacindicesnew = 1; sacstarts = 1; sacends = 1;
    end
%     disp('it worked');
catch
    rethrow(lasterror);
    keyboard;
end
% driftstats=[driftamp,driftdirection,driftvelocity];
% sacstats = {size(sacamps,1),mean(abs(sacvel)),max(abs(sacvel)),mean(abs(sacamps)),sacfreq,driftamp,driftdirection,driftvelocity,sacdirection,sacamps,uniquesacindicesnew, sacvel};

%KSP:2020:

% finally check if there are any saccades in the file and if not create
%dummy variables 
if ~exist('sacdirection','var')
    sacdirection=nan;sacamps=nan;sacvel=nan;uniquesacindicesnew=nan;
    sacends=nan;starttime=nan;endtime=nan;
else
    %get saccade start and end time points
    for saccIdx=1:length(uniquesacindicesnew)
        startPos=uniquesacindicesnew(saccIdx,1);
        endPos=sacends(saccIdx,1);
        starttime(saccIdx,1)=sampletimedata(startPos);
        endtime(saccIdx,1)=sampletimedata(endPos);
    end
end

sacstats = [sacdirection,sacamps, sacvel,uniquesacindicesnew,sacends,starttime,endtime];