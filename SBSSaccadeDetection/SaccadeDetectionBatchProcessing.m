%% Microsaccade detection
% This script runs through the entire dataset and identifies saccades in
% each eye position file and writes out the output

%% Directory Setup
%set top level folder here
topLevelFolder='/mnt/hd/ao/vids';
cd (topLevelFolder)
%we use the key file to index into the different folders of the top level
%folder
keyFile=readtable('/mnt/hd/ao/psy_list.csv','Delimiter',',');

%% Main loop
counter=0;
fid=fopen('/mnt/hd/ao/vids/SaccadeFailedFiles.txt','a');
for k = 1 : height(keyFile)
    disp(counter);
    counter=counter+1;
    %first we get the folder path from the key file
    folderName=keyFile.date{k};
    folderPath=[topLevelFolder,'/',folderName];
    if exist(folderPath,'dir') %this ensures that the dir is valid
        cd(folderPath);
        listoffiles=dir(folderPath);
        if isempty(listoffiles)
            continue
        else
            for fileIdx=1:length(listoffiles)
                 filename=listoffiles(fileIdx).name;
                 try
                     if (strfind(filename,'EyePos'))>0
                         eyeposData=readtable(filename);
                         eyepos=[eyeposData.xMA,eyeposData.yMA];
                         timesecs=eyeposData.time;
                         saccstats=extractsaccadesnewv2(eyepos,timesecs,8,0);
                         outputfilename=strcat(filename(1:strfind(filename,'_EyePos')-1),'_SaccStats.csv');
                         csvwrite(outputfilename,saccstats);
                     end
                 catch
                      fprintf(fid,'%s,%s\n',cd,filename);                       
                 end
            end
        end
    end
end