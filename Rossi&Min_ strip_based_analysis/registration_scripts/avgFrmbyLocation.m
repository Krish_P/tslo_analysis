function avgFrmbyLocation(frames,numheight_ignore, strip_offsets,strip_starts, stripheight,tl_row,tl_col, CropImg_h,CropImg_w,strip_th_fac, flag_MOD,step_frms, pname, vid_name)
% align and average frames using offsets generated in registration step.

numstrips = size(strip_starts,1);
frames = frames(numheight_ignore+1:end,:,:);
strip_starts = strip_starts-numheight_ignore;
frameheight = size(frames,1);
framewidth = size(frames,2);
numframes = size(frames,3);
min_offset_x = min(strip_offsets(:,2));
max_offset_x = max(strip_offsets(:,2));
min_offset_y = min(strip_offsets(:,1));
max_offset_y = max(strip_offsets(:,1));
% strip_offsets(strip_offsets(:,3) == 0) = nan;
strip_th = strip_th_fac*mean(strip_offsets(:,3),'omitnan');
% if isempty(dir([pname 'Processed/']))
%     mkdir([pname 'Processed/']);
% end
if flag_MOD == 1% for multi-offset video
    for idx_step = 1:length(step_frms)
        if idx_step ==  length(step_frms)
            pos_frms = step_frms(idx_step):numframes;
        else
            pos_frms =  step_frms(idx_step): step_frms(idx_step+1)-1;
        end
        % average all good strips********************************************
        sum_img = zeros(max_offset_y - min_offset_y + frameheight, max_offset_x-min_offset_x+framewidth);
        count_map = zeros(max_offset_y - min_offset_y + frameheight, max_offset_x-min_offset_x+framewidth);
        for frm_idx = 1:length(pos_frms)
            % for cases that strips have overlapping area, use offset from the strip has the highest NCC
            next_strip_idx = 1;
            for idx = 1:frameheight
                offsets_candidate = [];
                strip_idx = next_strip_idx;
                while strip_idx <= numstrips && idx >= strip_starts(strip_idx) && idx< strip_starts(strip_idx)+stripheight
                    if idx == strip_starts(strip_idx)+ stripheight-1
                        next_strip_idx = next_strip_idx +1;
                    end
                    if strip_offsets((pos_frms(frm_idx) - 1) * numstrips + strip_idx, 3)>= strip_th
                        offsets_candidate = [offsets_candidate; strip_offsets((pos_frms(frm_idx) - 1) * numstrips + strip_idx, :)];
                    end
                    strip_idx = strip_idx+1;
                end
                if ~isempty(offsets_candidate)
                    best_offset = offsets_candidate(offsets_candidate(:,3) == max(offsets_candidate(:,3)),:);
                    cur_strip = frames(idx,:, pos_frms(frm_idx));
                    stb_strip_y = idx + best_offset(1,1)- min_offset_y;% stabilized location y
                    stb_strip_x_start = 1 + best_offset(1,2)- min_offset_x;% stabilized location x
                    count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1) = count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1)+1;
                    tmp_stb_strip = sum_img(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1) + cur_strip;
                    sum_img(stb_strip_y,stb_strip_x_start:stb_strip_x_start+framewidth-1) = tmp_stb_strip;
                end
            end
        end
        % save syn_img as binary data
        [im_h, im_w]= size(sum_img);
        img_data = reshape(sum_img,1,im_h*im_w);
        fileID = fopen([pname '/' vid_name(1:end-4) '_strip_sum_count_img_th' num2str(strip_th,2) '_' num2str(idx_step,'%02d') '.DAT'],'w');
        fwrite(fileID,im_w,'int32');
        fwrite(fileID,im_h,'int32');
        fwrite(fileID,0,'int32');% 1: with full frame registration only. 0: with both frame and strip registration
        fwrite(fileID,1,'int32');% 1: fast scanner on vertical direction, 0: fast scanner on horizontal direction
        fwrite(fileID,img_data,'int32');
        count_map(count_map(:)==0)=1;
        avg_img = sum_img./count_map;
        count_map = uint16(count_map);
        fwrite(fileID,reshape(count_map,1,im_h*im_w),'uint16');
        fclose(fileID);
        %         overlap_th = floor(length(pos_frms)*overlap_th_fac);
        avg_img = avg_img(tl_row:tl_row+CropImg_h-1,tl_col:tl_col+CropImg_w-1);
        %         avg_img = CropImgByOverlapth(count_map, avg_img,CropImg_h, CropImg_w, overlap_th);
        avg_img = uint16(mat2gray(avg_img, [min(avg_img(:)) max(avg_img(:))])*2^16);
        imwrite(avg_img,[pname '/' vid_name(1:end-4) '_strip_crop_avg_img_th' num2str(strip_th,2) '_' num2str(idx_step,'%02d') '.tif'],'Resolution', 96);
        %***************************************************************************
    end
else % for video doesn't have MOD file
    sum_img = zeros(max_offset_y - min_offset_y + frameheight, max_offset_x-min_offset_x+framewidth);
    count_map = zeros(max_offset_y - min_offset_y + frameheight, max_offset_x-min_offset_x+framewidth);
    for frm_idx = 1:size(frames,3)
        next_strip_idx = 1;
        for idx = 1:frameheight
            offsets_candidate = [];
            strip_idx = next_strip_idx;
            while (strip_idx <= numstrips) && (idx >= strip_starts(strip_idx)) && (idx < (strip_starts(strip_idx)+stripheight))
                if idx == strip_starts(strip_idx)+ stripheight-1
                    next_strip_idx = next_strip_idx +1;
                end
                if strip_offsets((frm_idx - 1) * numstrips + strip_idx, 3)>= strip_th
                    offsets_candidate = [offsets_candidate; strip_offsets((frm_idx - 1) * numstrips + strip_idx, :)];
                end
                strip_idx = strip_idx+1;
            end
            if ~isempty(offsets_candidate)
                best_offset = offsets_candidate(offsets_candidate(:,3) == max(offsets_candidate(:,3)),:);
                cur_strip = frames(idx,:, frm_idx);
                stb_strip_y = idx + best_offset(1,1)- min_offset_y;% stabilized location y
                stb_strip_x_start = 1 + best_offset(1,2)- min_offset_x;% stabilized location x
                count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1) = count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1)+1;
                tmp_stb_strip = sum_img(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1) + cur_strip;
                sum_img(stb_strip_y,stb_strip_x_start:stb_strip_x_start+framewidth-1) = tmp_stb_strip;
            end
        end
    end
    % save syn_img as binary data
    [im_h, im_w]= size(sum_img);
    img_data = reshape(sum_img,1,im_h*im_w);
    fileID = fopen([pname '/' vid_name(1:end-4) '_strip_sum_count_img_th' num2str(strip_th,2) '.DAT'],'w');
    fwrite(fileID,im_w,'int32');
    fwrite(fileID,im_h,'int32');
    fwrite(fileID,0,'int32');% 1: with full frame registration only. 0: with both frame and strip registration
    fwrite(fileID,1,'int32');% 1: fast scanner on vertical direction, 0: fast scanner on horizontal direction
    fwrite(fileID,img_data,'int32');
    count_map(count_map(:)== 0) = 1;
    avg_img = sum_img./count_map;
    count_map = uint16(count_map);
    fwrite(fileID,reshape(count_map,1,im_h*im_w),'uint16');
    fclose(fileID);
    %     overlap_th = floor(size(frames,3)*overlap_th_fac);
    %     avg_img = CropImgByOverlapth(count_map, avg_img,CropImg_h, CropImg_w, overlap_th);
    avg_img = avg_img(tl_row:tl_row+CropImg_h-1,tl_col:tl_col+CropImg_w-1);
    avg_img = mat2gray(avg_img, [min(avg_img(:)) max(avg_img(:))]);
    imwrite(avg_img,[pname '/' vid_name(1:end-4) '_strip_crop_avg_img_th' num2str(strip_th,2) '.tif'],'Resolution', 96);
%     imwrite(avg_img,[pname '\Processed\' vid_name(1:end-4) '_strip_crop_avg_img_th' num2str(strip_th,2) '.png']);
end
end

