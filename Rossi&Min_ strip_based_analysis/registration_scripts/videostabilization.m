% function [strip_offsets, strip_starts] = videostabilization(orig_vid_name, frames, numheight_ignore, pname, startblink, endblink ,ref_img, numstrips, stripheight)
function [strip_offsets, strip_starts] = videostabilization(orig_vid_name, frames, numheight_ignore, pname, startblink, endblink ,ref_img, numstrips, stripheight,bstartstrip, bendstrip)
%% % this function register image sequences (TSLO & AOSLO) using frame based and strip based cross correlation
%****************************************************************************************************************************************************************
% *_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*
% * � University of Pittsburgh 2021                                                                                                                   *
% * Author: Min Zhang                                                                                                                                 *
% * this registration software is provided for research purpose only.   
% *****************************************************************************************************************************************************
% * Citation for this algorithm:                                                                                                                      *
% *****************************************************************************************************************************************************
% * M. Zhang, E. Gofas-Salas, B. Leonard, Y. Rui, V. Snyder, H. Reecher, P. Mec�, and E. Rossi,                                                       *                 
% * "Strip-based digital image registration for distortion minimization and robust eye motion measurement from scanned ophthalmic imaging systems,"   *
% * Biomed. Opt. Express 12, 2353-2372 (2021).                                                                                                        * 
% *_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*
%****************************************************************************************************************************************************************

% input parameters:
%****************************************************************************************************************************************************************
    % * orig_vid_name: name of raw video, or name of first image sequence.*
    % * e.g.:RLAB-0224_V006.avi or Frame_000.bmp                          *
    % *********************************************************************
    
    % frames: all frames of raw video. it is a 3D matrix of size [videoheight,videowidth, numframes] 
    % numheight_ignore: how many rows from the top should be excluded for registration;
    % vidObject: provides FrameRate, height, and width information of the raw video
    % pname: path to store the output data
    
    % *** startblink and endblink are output from 'FindBlinksandStrips.m' ***
    % * startblink: vector contains a list of blink start frames            *
    % * endblink: vector contains a list of blink end frames                *
    % ***********************************************************************
    
    % ref_img: manually selected reference image, this can be a frame selectd within the image sequence, or an external image 
    % numstrips: divide each image frame into how many strips
    % stripheight: the height of each strip
%*****************************************************************************************************************************************************************

%*****************************************************************************************************************************************************************
% output:
%*****************************************************************************************************************************************************************
    % * strip_offsets: record offsets of each strip; [offset_y, offset_x, cc_val]
    % *** the results will be saved as .mat, which contains the following information ***
    % * orginal_refImg: original reference image
    % * syntheticImg: synthetic reference image
    % * strip_offsets: offsets of each strip; [offset_y, offset_x, cc_val]
    % * strip_starts: starting location of each strip from original frame
    % * goodframes: indices of tracked frames
    % * orig_vid_name: original video name
    % * strip_starts: starting location of each strip
    % * stripheight: strip size
    % * numstrips: number of strips in each frame
    % * numheight_ignore: how many rows from the top be ingored;
    %****added additional outputs 01/11/2021*********
    % * startblink: frame index of blink starts
    % * endblink: frame index of blink ends
    % * bstartstrip: strip index of blink starts
    % * bendstrip: strip index of blink ends
    
% ******************************************************************************************************************************************************************
% *_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*
% * By Min Zhang, 06/30/2020                                                              *
% * add options to choose numStrips & strip_height                                        *
% * For TSLO and AOSLO data                                                               *
% *_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*
%% read all frames of the video into frames 
blink_frms = [];% a vector contains indices of all frames which are blink frames
for frm_idx = 1:length(startblink)
    blink_frms = [blink_frms,startblink(frm_idx):endblink(frm_idx)];
end
numframes = size(frames,3);
framewidth = size(frames,2); % The width of the video (in pixels)
frameheight = size(frames,1);
%% image pre-processing
frameheight_cropped = frameheight - numheight_ignore;% The height of the video (in pixels) after cropping out the top. 
preprocessed_frames = uint8(zeros(frameheight_cropped,framewidth,numframes));
orginal_refImg = ref_img;% keep a record of the original reference image
ref_img = ref_img(numheight_ignore+1:frameheight,:);
filt_ref =  imgaussfilt(ref_img, 20);
ref_img = im2uint8(mat2gray( ref_img - filt_ref));
ref_img = double(adapthisteq(ref_img,'ClipLimit',0.05));
for frm_idx = 1:numframes
    % ****************** apply gaussian filter to remove noise********************
    %     currentframe = gpuArray(frames(numheight_ignore+1:frameheight,:,frm_idx));
    currentframe = frames(numheight_ignore+1:frameheight,:,frm_idx);
    filtframe = imgaussfilt(currentframe, 20);
    currentframe = currentframe-filtframe;
    newFrame = im2uint8(mat2gray(currentframe));
    % performs contrast-limited adaptive histogram equalization to enhance
    preprocessed_frames(:,:,frm_idx) = adapthisteq(newFrame,'ClipLimit',0.05);
end
preprocessed_frames = double(preprocessed_frames);
%% cross correlation between consecutive frames
ref_w = framewidth ; % The width of the video (in pixels)
ref_h = frameheight_cropped ; % The height of the video (in pixels)
consecutive_frm_offsets = NaN(numframes, 3); %[offset_y, offset_x, cc_val]
for frm_idx = 2:numframes
    if isempty(find( blink_frms == frm_idx))&& isempty(find( blink_frms == frm_idx-1))
        gpu_ref = gpuArray(preprocessed_frames(:,:,frm_idx-1));
        gpu_target_frm = gpuArray(preprocessed_frames(:,:,frm_idx));
        cc = normxcorr2(gpu_target_frm,gpu_ref);
        cc = gather(cc);
        cc_val = max(cc(:));
        [ypeak, xpeak] = find(cc == cc_val);
        offset_y = ypeak - ref_h;
        offset_x = xpeak - ref_w;
        consecutive_frm_offsets(frm_idx,:) = [offset_y, offset_x, cc_val];
    end
end
cc_mean = mean(consecutive_frm_offsets(:,3),'omitnan');
cc_std = std(consecutive_frm_offsets(:,3),'omitnan');
distort_frms = [];
for frm_idx = 2:numframes-1
    if consecutive_frm_offsets(frm_idx,3) < max(cc_mean - 0.8 * cc_std, 0.2) && consecutive_frm_offsets(frm_idx+1,3) < max(cc_mean - 0.8 * cc_std, 0.2)
        distort_frms = [distort_frms;frm_idx];
    end
end
%% call normxcorr2 for cross_correlation at frame level
gpu_ref = gpuArray(ref_img);
frm_offsets = NaN(numframes, 3); %record offsets of each frame;[offset_y, offset_x, cc_val]
bad_frm_offset_th = framewidth/4; % threshold used for passive corner data, in order to drop less bad frames.
goodframes = setdiff(1:numframes, blink_frms);
goodframes = setdiff(goodframes,distort_frms );
for frm_idx = 1:length(goodframes)%1:numframes
    gpu_target_frm = gpuArray(preprocessed_frames(:,:,goodframes(frm_idx)));
    cc = normxcorr2(gpu_target_frm,gpu_ref);
    cc = gather(cc);
    cc_val= max(cc(:));
    [ypeak, xpeak] = find(cc == cc_val);
    offset_y = ypeak - ref_h;
    offset_x = xpeak - ref_w;
    frm_offsets(goodframes(frm_idx),:) = [offset_y,offset_x,cc_val];   
end
%% use cc val as threshold for bad frms;
cc_mean = mean(frm_offsets(goodframes,3),'omitnan');
cc_std = std(frm_offsets(goodframes,3),'omitnan');
bad_frms = find(frm_offsets(:,3) <= (cc_mean  - 0.6*cc_std) | abs(frm_offsets(:,1) )>= bad_frm_offset_th | abs(frm_offsets(:,2))>= bad_frm_offset_th);
goodframes = setdiff(goodframes, bad_frms);

%% synthetic reference frame by averaging all of the good frames
min_offset_x = min(frm_offsets(goodframes,2),[],'omitnan');
max_offset_x = max(frm_offsets(goodframes,2),[],'omitnan');
min_offset_y = min(frm_offsets(goodframes,1),[],'omitnan');
max_offset_y = max(frm_offsets(goodframes,1),[],'omitnan');
syn_ref_frm = zeros(max_offset_y - min_offset_y + frameheight_cropped, max_offset_x-min_offset_x+framewidth);
syntheticImg = syn_ref_frm;
% %************************syn from filtered frames***********
count_map = ones(max_offset_y - min_offset_y + frameheight_cropped, max_offset_x-min_offset_x+framewidth);
for frm_idx = 1:length(goodframes)
    match_loc_top_y = 1-min_offset_y+frm_offsets(goodframes(frm_idx),1);
    match_loc_left_x = 1-min_offset_x+frm_offsets(goodframes(frm_idx),2);
    tmp_frm = syn_ref_frm(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1);
    tmp_frm = tmp_frm./count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1).*(count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1)-1) + preprocessed_frames(:,:,goodframes(frm_idx))./count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1);
    syn_ref_frm(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1) = tmp_frm;
    % generate the original synthetic reference image, no filter applied.
    tmp_frm = syntheticImg(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1);
    tmp_frm = tmp_frm./count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1).*(count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1)-1) + frames(numheight_ignore+1:frameheight,:,goodframes(frm_idx))./count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1);
    syntheticImg(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1) = tmp_frm;
    count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1) = count_map(match_loc_top_y:match_loc_top_y+frameheight_cropped-1, match_loc_left_x:match_loc_left_x+framewidth-1)+1;
end
% %***********************************************************************
filt_ref =  imgaussfilt(syntheticImg, 20);
syn_ref_frm = im2uint8(mat2gray( syntheticImg-filt_ref));
syn_ref_frm = double(adapthisteq(syn_ref_frm,'ClipLimit',0.05));
% figure, imshow(syn_ref_frm,[])
clear frames;

%% run cross correlation for distort or not matched frames using sub_frame
%************************** step_h = stripheight*2, sub_frm_height = stripheight *4***************************************************
bad_frm_offset_th = framewidth/4*3;
bad_frms = union(bad_frms, distort_frms);
gpu_ref = gpuArray(syn_ref_frm);
sub_frm_height = floor(frameheight/4);
sub_frm_width = floor(framewidth/4);
step_h = floor(sub_frm_height/2);
step_w = floor(sub_frm_width/2);
num_sub = floor(frameheight_cropped/step_h);
if ~isempty(bad_frms)
    for frm_idx = length(bad_frms):-1:1
        sub_frm_offsets = zeros(num_sub*2,3);
        % use horizontal strip for best matching
        for idx = 1:num_sub-1        
            sub_frm = preprocessed_frames((idx-1)*step_h + 1:(idx-1)*step_h + sub_frm_height,:,bad_frms(frm_idx));            
            gpu_sub = gpuArray(sub_frm);
            cc = normxcorr2(gpu_sub,gpu_ref);
            cc = gather(cc);
            cc_val = max(cc(:));
            [ypeak, xpeak] = find(cc == cc_val);         
            offset_y = ypeak + min_offset_y - (idx-1)*step_h-sub_frm_height;           
            offset_x = xpeak - ref_w + min_offset_x;
            sub_frm_offsets(idx,:) = [offset_y,offset_x,cc_val];
        end
        %use vertical strip for best matching             
        for idx = 1:num_sub-1
            sub_frm = preprocessed_frames(:,(idx-1)*step_w + 1:(idx-1)*step_w + sub_frm_width,bad_frms(frm_idx));
            gpu_sub = gpuArray(sub_frm);
            cc = normxcorr2(gpu_sub,gpu_ref);
            cc = gather(cc);
            cc_val = max(cc(:));
            [ypeak, xpeak] = find(cc == cc_val);
            offset_y = ypeak - ref_h + min_offset_y;
            offset_x = xpeak - (idx-1)*step_w  - sub_frm_width + min_offset_x;
            sub_frm_offsets(idx+num_sub,:) = [offset_y,offset_x,cc_val];
        end
        [i,~] = find(sub_frm_offsets(:,3) == max(sub_frm_offsets(:,3)));  
        frm_offsets(bad_frms(frm_idx),:) = sub_frm_offsets(i,:);
        if frm_offsets(bad_frms(frm_idx),3) >= cc_mean - 0.6*cc_std && abs(frm_offsets(bad_frms(frm_idx),1)) <= bad_frm_offset_th && abs(frm_offsets(bad_frms(frm_idx),2)) <= bad_frm_offset_th            
            bad_frms(frm_idx) = [];
        end
    end
end
%*******************************************************************************************************
%% strip level cross-correlation for user defined strip height and numstrips
goodframes = setdiff(1:numframes, blink_frms);
goodframes = setdiff(goodframes, bad_frms);
frm_offsets(bad_frms,:) = nan;
strip_offsets = NaN(numframes*numstrips,3); %record offsets of each strip; [offset_y, offset_x, cc_val]
searcharoundheight = stripheight;
diff_offset_th = stripheight;%16;% threshold of the difference between frm_offset and strip_offset; if the difference is greater than offset_th, check the next k largest cc value for the most possible match location
strip_starts = ones(numstrips,1);% starting location of each strips in cropped image
strip_starts(numstrips) = frameheight_cropped - stripheight+1;% starging location of the last strip
for strip_idx = 2:numstrips-1
    strip_starts(strip_idx) = round((frameheight_cropped-stripheight)/(numstrips-1) * (strip_idx-1)) + 1;
end
for frm_idx =  1:length(goodframes)
    for strip_idx = 1:numstrips 
        target_strip = preprocessed_frames(strip_starts(strip_idx):strip_starts(strip_idx)+stripheight-1,:,goodframes(frm_idx));
        ref_strip_y_start = strip_starts(strip_idx) + frm_offsets(goodframes(frm_idx), 1) - searcharoundheight - min_offset_y; 
        ref_strip_y_end = strip_starts(strip_idx) + frm_offsets(goodframes(frm_idx), 1) - min_offset_y + stripheight+ searcharoundheight-1;
        local_shift_y = searcharoundheight;
        if ref_strip_y_start < 1
            local_shift_y = searcharoundheight + ref_strip_y_start-1;
            ref_strip_y_start = 1;
        end
            ref_height = size(syn_ref_frm,1);
            ref_strip_y_end = min(ref_strip_y_end,ref_height);
            ref_strip = syn_ref_frm(ref_strip_y_start:ref_strip_y_end,:);
            [strip_h, strip_w] = size(target_strip);
            if size(ref_strip,1) < strip_h
                strip_offsets((goodframes(frm_idx)-1)*numstrips+strip_idx,:) = frm_offsets(goodframes(frm_idx),:);
            else
                target_strip = gpuArray(target_strip);
                ref_strip = gpuArray(ref_strip);
                cc = normxcorr2(target_strip,ref_strip);
                cc = gather(cc);
                th_cc = ones(size(cc))*-1;
                th_y_start = max(1,strip_h + local_shift_y - diff_offset_th);
                th_y_end = min(size(cc,1),strip_h + local_shift_y + diff_offset_th);
                th_x_start = max(1,strip_w - min_offset_x + frm_offsets(goodframes(frm_idx), 2) - diff_offset_th);
                th_x_end = min(size(cc,2),strip_w - min_offset_x + frm_offsets(goodframes(frm_idx), 2) + diff_offset_th);
                th_cc(th_y_start:th_y_end, th_x_start: th_x_end) = cc(th_y_start:th_y_end, th_x_start: th_x_end);%use the local peak within diff_offset_th range of difference from frame based offset
                [ypeak, xpeak] = find(th_cc == max(th_cc(:)));
                strip_offsets_y = ypeak - strip_h -local_shift_y + frm_offsets(goodframes(frm_idx), 1);
                strip_offsets_x = xpeak - strip_w + min_offset_x;
                if length(ypeak) > 1 % when has more than one peak, use the average of the peaks near the frame based offsets
                    [rows, ~] = find(abs(strip_offsets_y - frm_offsets(goodframes(frm_idx), 1)) + abs(strip_offsets_x - frm_offsets(goodframes(frm_idx), 2)) == min(abs(strip_offsets_y-frm_offsets(goodframes(frm_idx), 1)) + abs(strip_offsets_x-frm_offsets(goodframes(frm_idx), 2))));
                    strip_offset_y = round(mean(strip_offsets_y(rows)));
                    strip_offset_x = round(mean(strip_offsets_x(rows)));                   
                else
                    strip_offset_y = strip_offsets_y;
                    strip_offset_x = strip_offsets_x;
                end
                strip_offsets((goodframes(frm_idx)-1)*numstrips+strip_idx,:) = [strip_offset_y,strip_offset_x, max(th_cc(:))];
            end            
            %*************************************************************************************************************************************************     
    end
end
% record starting location of each strips in original image
strip_starts = strip_starts + numheight_ignore;
timeInfo = datetime();
timeStamp = [num2str(year(timeInfo)) num2str( month(timeInfo)) num2str( day(timeInfo)) num2str(hour(timeInfo)) num2str(minute(timeInfo))];
save([pname '/' orig_vid_name(1:end-4) '_offsetdata_' timeStamp '.mat'], 'orginal_refImg', 'syntheticImg', 'strip_offsets', 'goodframes','orig_vid_name','strip_starts','stripheight','numstrips','numheight_ignore','startblink', 'endblink','bstartstrip', 'bendstrip');
imwrite(uint8(syntheticImg),[pname '/' orig_vid_name(1:end-4) '_synImg_' timeStamp '.tif'],'Resolution', 96);
end

