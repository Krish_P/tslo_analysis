% function [tl_row,tl_col] = calculateCropRegion(frameheight,framewidth,numframes,strip_offsets,CropImg_h,CropImg_w,strip_th_fac, flag_fnumFile,step_frms)
function [tl_row,tl_col,crop_h, crop_w] = calculateCropRegion(frameheight, framewidth, numframes,strip_offsets,numheight_ignore, strip_starts, stripheight,CropImg_h, CropImg_w, strip_th_fac, overlap_th_fac, flag_MOD, step_frms)
%
numstrips = size(strip_starts,1);%number of strips each frame
strip_starts = strip_starts-numheight_ignore;
frameheight = frameheight-numheight_ignore;
min_offset_x = min(strip_offsets(:,2));
max_offset_x = max(strip_offsets(:,2));
min_offset_y = min(strip_offsets(:,1));
max_offset_y = max(strip_offsets(:,1));
overlap_th = floor(overlap_th_fac*numframes);
strip_offsets(strip_offsets(:,3) == 0) = nan;
strip_th = strip_th_fac*mean(strip_offsets(:,3),'omitnan');       
if flag_MOD == 1% for video has fnum file
     count_map = zeros(max_offset_y - min_offset_y + frameheight, max_offset_x-min_offset_x+framewidth);
     for idx_step = 1:length(step_frms)
        if idx_step ==  length(step_frms)
            pos_frms = step_frms(idx_step):numframes;
        else
            pos_frms =  step_frms(idx_step): step_frms(idx_step+1)-1;
        end          
        for frm_idx = 1:length(pos_frms)
            % for cases that strips have overlapping area, use offset from the strip has the highest NCC
            next_strip_idx = 1;
            for idx = 1:frameheight
                offsets_candidate = [];
                strip_idx = next_strip_idx;
                while strip_idx <= numstrips && idx >= strip_starts(strip_idx) && idx< strip_starts(strip_idx)+stripheight
                    if idx == strip_starts(strip_idx)+ stripheight-1
                        next_strip_idx = next_strip_idx +1;
                    end
                    if strip_offsets((pos_frms(frm_idx) - 1) * numstrips + strip_idx, 3)>= strip_th
                        offsets_candidate = [offsets_candidate; strip_offsets((pos_frms(frm_idx) - 1) * numstrips + strip_idx, :)];
                    end
                    strip_idx = strip_idx+1;
                end
                if ~isempty(offsets_candidate)
                    best_offset = offsets_candidate(offsets_candidate(:,3) == max(offsets_candidate(:,3)),:);                    
                    stb_strip_y = idx + best_offset(1,1)- min_offset_y;% stabilized location y
                    stb_strip_x_start = 1 + best_offset(1,2)- min_offset_x;% stabilized location x
                    count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1) = count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1)+1;                   
                end
            end
        end
     end
    [tl_row,tl_col,crop_h, crop_w] = findCropTopleftCorner(count_map, CropImg_h, CropImg_w, overlap_th);
else % for video doesn't have MOD file
    count_map = zeros(max_offset_y - min_offset_y + frameheight, max_offset_x-min_offset_x+framewidth);
    for frm_idx = 1:numframes
        next_strip_idx = 1;
        for idx = 1:frameheight
            offsets_candidate = [];
            strip_idx = next_strip_idx;
            while (strip_idx <= numstrips) && (idx >= strip_starts(strip_idx)) && (idx < (strip_starts(strip_idx)+stripheight))
                if idx == strip_starts(strip_idx)+ stripheight-1
                    next_strip_idx = next_strip_idx +1;
                end
                if strip_offsets((frm_idx - 1) * numstrips + strip_idx, 3)>= strip_th
                    offsets_candidate = [offsets_candidate; strip_offsets((frm_idx - 1) * numstrips + strip_idx, :)];
                end
                strip_idx = strip_idx+1;
            end
            if ~isempty(offsets_candidate)
                best_offset = offsets_candidate(offsets_candidate(:,3) == max(offsets_candidate(:,3)),:);
                stb_strip_y = idx + best_offset(1,1)- min_offset_y;% stabilized location y
                stb_strip_x_start = 1 + best_offset(1,2)- min_offset_x;% stabilized location x
                count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1) = count_map(stb_strip_y, stb_strip_x_start:stb_strip_x_start+framewidth-1)+1;
            end
        end
    end
    [tl_row,tl_col,crop_h, crop_w] = findCropTopleftCorner(count_map, CropImg_h, CropImg_w, overlap_th);
end
end

