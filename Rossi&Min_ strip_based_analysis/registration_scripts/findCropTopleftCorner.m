function [tl_row,tl_col, crop_h, crop_w] = findCropTopleftCorner(imgCntMap, img_h, img_w, overlap_th)
% crop image to size(img_h, img_w) at the most overlapping area
% Detailed explanation goes here
% input: 
% imgCntMap: image count map
% img_h, img_w: minimum cropped image size
% overlap_th: overlap threshod 
% output:
% tl_row: top left row # to start cropping
% tl_col: top left col # to start cropping
% crop_h: height of cropped image
% crop_w: width of cropped image
% Min Zhang 07/15/2020

[r, c] = find( imgCntMap == max(imgCntMap(:)));
r_s = min(r);
r_e = max(r);
c_s = min(c);
c_e = max(c);
while r_e - r_s < img_h-1 || c_e - c_s < img_w-1
    % for row
    if r_e - r_s < img_h-1
        if r_s > 1 && r_e < size(imgCntMap,1)
            if min(imgCntMap(r_s-1,c_s:c_e)) < min(imgCntMap(r_e+1,c_s:c_e))
                r_e = r_e + 1;
            elseif  min(imgCntMap(r_s-1,c_s:c_e)) > min(imgCntMap(r_e+1,c_s:c_e))
                r_s = r_s - 1;
            else
                r_e = r_e + 1;
                r_s = r_s - 1;
            end
        elseif r_s == 1
            r_e = r_e + 1;
        else
            r_s = r_s - 1;
        end
    end
    % for col
    if c_e-c_s < img_w-1
        if c_s > 1 && c_e < size(imgCntMap,2)
            if min(imgCntMap(r_s:r_e,c_s-1)) < min(imgCntMap(r_s:r_e,c_e + 1))
                c_e = c_e + 1;
            elseif min(imgCntMap(r_s:r_e,c_s-1)) > min(imgCntMap(r_s:r_e,c_e + 1))
                c_s = c_s - 1;
            else
                 c_e = c_e + 1;
                 c_s = c_s - 1;
            end
        elseif c_s == 1
            c_e = c_e + 1;
        else
            c_s = c_s - 1;
        end
    end
end
while (r_s > 1 && min(imgCntMap(r_s-1,c_s:c_e)) >= overlap_th)||(r_e < size(imgCntMap,1)&& min(imgCntMap(r_e+1,c_s:c_e)) >= overlap_th)|| (c_s > 1 && min(imgCntMap(r_s:r_e,c_s-1))>= overlap_th)|| (c_e < size(imgCntMap,2)&& min(imgCntMap(r_s:r_e,c_e+1))>=overlap_th)
    if r_s > 1 && min(imgCntMap(r_s-1,c_s:c_e)) >= overlap_th
        r_s = r_s - 1;
    end
    if r_e < size(imgCntMap,1)&& min(imgCntMap(r_e + 1,c_s:c_e)) >= overlap_th
        r_e = r_e + 1;
    end
    if c_s > 1 && min(imgCntMap(r_s:r_e,c_s-1))>= overlap_th
        c_s = c_s - 1;
    end
    if c_e < size(imgCntMap,2)&& min(imgCntMap(r_s:r_e,c_e+1))>= overlap_th
        c_e = c_e + 1;
    end
end 
tl_row = r_s;
tl_col = c_s;
crop_h = r_e-r_s+1;
crop_w = c_e-c_s+1;
end




