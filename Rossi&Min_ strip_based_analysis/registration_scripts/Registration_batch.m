%****************************************************************************************************************************************************************
% *_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*
% * � University of Pittsburgh 2021                                                                                                                   *
% * Author: Min Zhang                                                                                                                                 *
% * this registration software is provided for research purpose only.   
% *****************************************************************************************************************************************************
% * Citation for this algorithm:                                                                                                                      *
% *****************************************************************************************************************************************************
% * M. Zhang, E. Gofas-Salas, B. Leonard, Y. Rui, V. Snyder, H. Reecher, P. Mec�, and E. Rossi,                                                       *                 
% * "Strip-based digital image registration for distortion minimization and robust eye motion measurement from scanned ophthalmic imaging systems,"   *
% * Biomed. Opt. Express 12, 2353-2372 (2021).                                                                                                        * 
% *_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*
%****************************************************************************************************************************************************************
% In order to process the videos automatically, you need to prepare a Excel file containing a list of videos you want to process. Information in the excel file contains:
% vid_info{:,1}: path of the data
% vid_info{:,2}: data type: avi or bmp
% vid_info{:,3}: if the data is video, provide video name here; leave
% blank if the data is image sequence
% vid_info{:,4}: MOD step file name, leave blank if no MOD step info
% vid_info{:,5}: reference frame index, leave blank if use external
% image as reference
% vid_info{:,6}: path and name of reference image if use external image
% as reference
% vid_info{:,7}: how many strips each frame has;
% vid_info{:,8}: height of each strip;
% vid_info{:,9}: how many rows from the top should be ingored;
% vid_info{:,10}: Yes if image sequence contains blink, otherwise No;
%****************************************************************************************************************************************************************
%% 
% please modify here as needed: 
% set systemType = 0 if your data is acquired through TSLO system
% set systemType = 1 if your data is acquired through AOSLO system
% set systemType = 2 if your data is acquired through PSI system
systemType = 0;

%% read in .xlsx file containing video information for registration
[fname, pname] = uigetfile('*.xlsx', 'Select .xlsx file containing video information for registration');
[~,~,vid_info] = xlsread([pname fname]);
strip_th_fac = 0.8; % threshold factor to generate average image. threshold = strip_th_fac * mean(NCC); strip_th_fac is a positive number
overlap_th_fac = 0.05;% threshold factor used to crop out averaged image. minimum overlaping threshold =  overlap_th_fac * total number of frames at that imaging position. overlap_th_fac is within the range (0,1)
% usfac = 100;
%KSP added this 12/9/2021
channelVids=false;
%%
nfiles = size(vid_info,1);
h = waitbar(0,['Processing ' num2str(nfiles-1) ' videos...']);
for idx_vid = 2:nfiles
    try
        dataFolder = vid_info{idx_vid,1};
        % Check to make sure that folder actually exists.  Warn user if it doesn't.
        if ~isfolder(dataFolder)
            errorMessage = sprintf('Error: The following folder does not exist:\n%s\nPlease specify a new folder.', dataFolder);
            uiwait(warndlg(errorMessage));
            dataFolder = uigetdir(); % Ask for a new one.
            if dataFolder == 0
                continue;
            end
        end
        dataType = lower(strtrim(vid_info{idx_vid,2}));
        if isnan(vid_info{idx_vid,4})
            flag_MOD = 0;
        else
            flag_MOD = 1;
        end
        if ~isnan(vid_info{idx_vid,5})
            flag_refImg = 0;
            ref_frm_idx = vid_info{idx_vid,5};
        elseif ~isnan(vid_info{idx_vid,6})
            flag_refImg = 1;
            ref_img = double(imread(vid_info{idx_vid,6}));
        else
            error('Please provide reference image information');
            continue;
        end
        nstrips = vid_info{idx_vid,7};
        stripheight = vid_info{idx_vid,8};
        numheight_ignore = vid_info{idx_vid,9};
        hasBlink = lower(strtrim(vid_info{idx_vid,10}));
        if strcmp(hasBlink,'yes')
            flg_blink = 1;
        else
            flg_blink = 0;
        end
        numframes = 0;
        if flag_MOD == 1 % read in frames from videos has fnum* file
            fileID = fopen([dataFolder '/' vid_info{idx_vid,4} '.txt'],'r');
            step_data = fscanf(fileID,'%f');
            fclose(fileID);
            lastfrm_idx = 1;% index of last useful frame
            % find total useful frames and index of last useful frame
            step_frms = [];% starting frms for each location(1,2,...,n). e.g. the last frm for location k is step_frms(k+1)-1;
            if  flag_refImg == 0
                ref_frm = ref_frm_idx;
            end
            for idx_step = 2:2:length(step_data)-1
                if step_data(idx_step) == 0 || step_data(idx_step+1) == 0
                    break;
                else
                    if flag_refImg == 0 && ref_frm >= step_data(idx_step)
                        if idx_step == 2
                            ref_frm_idx = ref_frm_idx -(step_data(idx_step)-1);
                        else
                            ref_frm_idx = ref_frm_idx -(step_data(idx_step) - step_data(idx_step-1) -1);
                        end
                    end
                    % update starting frms for each location(1,2,...,25). e.g. the last frm for location k is step_frms(k+1)-1;
                    if idx_step == 2
                        step_frms = 1;
                    else
                        step_frms = [step_frms; step_frms(end)+1 + step_data(idx_step-1)-step_data(idx_step-2)];
                    end
                    lastfrm_idx = step_data(idx_step+1);
                    numframes = numframes + step_data(idx_step+1) - step_data(idx_step)+1;
                end
            end
            %% only read in frames within interval of fnum files
            if strcmp(dataType,'avi')% for image sequence saved as avi video
                vidObject = VideoReader([dataFolder '/' vid_info{idx_vid,3} '.avi']);
                cur_vid_name = vidObject.Name;
                vidFormat = vidObject.VideoFormat;
                orig_numframes = floor(vidObject.FrameRate*vidObject.Duration);
                % update lastfrm_idx, numframes and step_frms
                if step_data(idx_step) ~= 0
                    if idx_step < length(step_data) && step_data(idx_step + 1) == 0
                        step_frms = [step_frms; step_frms(end)+1 + step_data(idx_step-1) - step_data(idx_step-2)];
                        lastfrm_idx = min(orig_numframes,step_data(idx_step) + step_data(idx_step-1) - step_data(idx_step-2));
                        numframes = numframes + lastfrm_idx - step_data(idx_step) + 1;
                    elseif idx_step + 2 == length(step_data) && step_data(idx_step + 2) ~= 0
                        idx_step = idx_step + 2;
                        step_frms = [step_frms; step_frms(end)+1 + step_data(idx_step-1) - step_data(idx_step-2)];
                        lastfrm_idx = min(orig_numframes,step_data(idx_step) + step_data(idx_step-1) - step_data(idx_step-2));
                        numframes = numframes + lastfrm_idx - step_data(idx_step) + 1;
                    end
                end
                frameheight = vidObject.Height;
                framewidth = vidObject.Width;
                frames = zeros(vidObject.Height,vidObject.Width,numframes);
                framemeans = zeros(numframes,1);
                cur_idx = 1;
                idx_step = 2;
                for frm_idx = 1:lastfrm_idx %only read in frames which been imaged at 1 of 25 locations.
                    tmp_frm = readFrame(vidObject);
                    if size(tmp_frm, 3)>1
                        tmp_frm = rgb2gray(tmp_frm);
                    end
                    if idx_step < length(step_data) && frm_idx >= step_data(idx_step) && frm_idx <= step_data(idx_step+1)
                        frames(:,:,cur_idx) =  double(tmp_frm);
                        framemeans(cur_idx) = mean2(frames(:,:,cur_idx));
                        cur_idx = cur_idx+1;
                        if frm_idx == step_data(idx_step+1)
                            idx_step = idx_step +2;
                        end
                    elseif (idx_step < length(step_data) && step_data(idx_step) ~= 0&& step_data(idx_step+1) == 0) || (idx_step == length(step_data)&& step_data(idx_step) ~= 0) && frm_idx >= step_data(idx_step)
                        frames(:,:,cur_idx) =  double(tmp_frm);
                        framemeans(cur_idx) = mean2(frames(:,:,cur_idx));
                        cur_idx = cur_idx+1;
                    end
                end
            elseif strcmp(dataType,'bmp')% for image sequence saved as bmp
                dataPattern = fullfile(dataFolder, '*.bmp'); %
                imgSequence = dir(dataPattern);
                orig_numframes = length(imgSequence);
                if step_data(idx_step) ~= 0
                    if idx_step < length(step_data) && step_data(idx_step + 1) == 0
                        step_frms = [step_frms; step_frms(end)+1 + step_data(idx_step-1) - step_data(idx_step-2)];
                        lastfrm_idx = min(orig_numframes,step_data(idx_step) + step_data(idx_step-1) - step_data(idx_step-2));
                        numframes = numframes + lastfrm_idx - step_data(idx_step) + 1;
                    elseif idx_step + 2 == length(step_data) && step_data(idx_step + 2) ~= 0
                        idx_step = idx_step + 2;
                        step_frms = [step_frms; step_frms(end)+1 + step_data(idx_step-1) - step_data(idx_step-2)];
                        lastfrm_idx = min(orig_numframes,step_data(idx_step) + step_data(idx_step-1) - step_data(idx_step-2));
                        numframes = numframes + lastfrm_idx - step_data(idx_step) + 1;
                    end
                end
                img = imread(fullfile(imgSequence(1).folder, imgSequence(1).name));
                cur_vid_name = imgSequence(1).name;
                frameheight = size(img,1);
                framewidth = size(img,2);
                frames = zeros(frameheight,framewidth,numframes);
                framemeans = zeros(numframes,1);
                cur_idx = 1;
                idx_step = 2;
                for frm_idx = 1:lastfrm_idx %only read in frames which been imaged at 1 of 25 locations.
                    if idx_step < length(step_data) && frm_idx >= step_data(idx_step) && frm_idx <= step_data(idx_step+1)
                        tmp = imread(fullfile(imgSequence(frm_idx).folder , imgSequence(frm_idx).name));
                        if size(tmp, 3)>1
                            tmp = rgb2gray(tmp);
                        end
                        frames(:,:,cur_idx) = double(tmp);
                        framemeans(cur_idx) = mean2(frames(:,:,cur_idx));
                        cur_idx = cur_idx+1;
                        if frm_idx == step_data(idx_step+1)
                            idx_step = idx_step + 2;
                        end
                    elseif (idx_step < length(step_data) && step_data(idx_step) ~= 0&& step_data(idx_step+1) == 0) || (idx_step == length(step_data)&& step_data(idx_step) ~= 0) && frm_idx >= step_data(idx_step)
                        tmp = imread(fullfile(imgSequence(frm_idx).folder , imgSequence(frm_idx).name));
                        if size(tmp, 3)>1
                            tmp = rgb2gray(tmp);
                        end
                        frames(:,:,cur_idx) = double(tmp);
                        framemeans(cur_idx) = mean2(frames(:,:,cur_idx));
                        cur_idx = cur_idx+1;
                    end
                end
            end
            if flag_refImg == 0
                ref_img = frames(:,:,ref_frm_idx);
            end
            if flg_blink == 0    %for no blink video
                bstartstrip = [];
                bendstrip = [];
                endblink=[];
                startblink = [];
                %                 save([vid_info{idx_vid,1} '\' cur_vid_name(1:end-4) '_blinktest.mat'], 'bstartstrip', 'bendstrip','startblink', 'endblink');
            else
                [bstartstrip, bendstrip,endblink,startblink] = FindBlinksandStrips(frames,cur_vid_name, framemeans, nstrips, numframes, vid_info{idx_vid,1} );
            end
            % video stabilization           
            if systemType == 0
            % if systemType == 0 || systemType == 1
                [strip_offsets,strip_starts] = videostabilization(cur_vid_name, frames, numheight_ignore, vid_info{idx_vid,1}, startblink, endblink, ref_img, nstrips, stripheight,bstartstrip, bendstrip);
            else
                [strip_offsets,strip_starts] = videostabilization_psi(cur_vid_name, frames, numheight_ignore, vid_info{idx_vid,1}, startblink, endblink, ref_img, nstrips, stripheight,bstartstrip, bendstrip);
            end
            %% generate average image
            [tl_row,tl_col,crop_h,crop_w] = calculateCropRegion(frameheight,framewidth,numframes,strip_offsets,numheight_ignore,strip_starts, stripheight,frameheight, framewidth, strip_th_fac, overlap_th_fac, flag_MOD, step_frms);
            avgFrmbyLocation(frames,numheight_ignore,strip_offsets,strip_starts, stripheight,tl_row,tl_col, crop_h,crop_w,strip_th_fac,flag_MOD,step_frms, dataFolder, cur_vid_name);
            %% generate average images for other chs if has any 
            % check if there are some other channels avi in the same
            % folder. May need to edit this based on how video from
            % different channels are named
            if strcmp(dataType,'avi')
                i = strfind(cur_vid_name,'CH');
                CoRegVids = dir(fullfile(dataFolder, [cur_vid_name(1:i+1) '*' cur_vid_name(i+3:end)]));
                if length(CoRegVids) > 1
                    for CoIdx = 1:length(CoRegVids)
                        if ~strcmp(CoRegVids(CoIdx).name,cur_vid_name)
                            vidObject = VideoReader(fullfile(dataFolder, CoRegVids(CoIdx).name));
                            cur_idx = 1;
                            idx_step = 2;
                            for frm_idx = 1:lastfrm_idx %only read in frames which been imaged at 1 of 25 locations.
                                tmp_frm = readFrame(vidObject);
                                if size(tmp_frm, 3)>1
                                    tmp_frm = rgb2gray(tmp_frm);
                                end
                                if idx_step < length(step_data) && frm_idx >= step_data(idx_step) && frm_idx <= step_data(idx_step+1)
                                    frames(:,:,cur_idx) =  double(tmp_frm);
                                    cur_idx = cur_idx+1;
                                    if frm_idx == step_data(idx_step+1)
                                        idx_step = idx_step +2;
                                    end
                                elseif (idx_step < length(step_data) && step_data(idx_step) ~= 0&& step_data(idx_step+1) == 0) || (idx_step == length(step_data)&& step_data(idx_step) ~= 0) && frm_idx >= step_data(idx_step)
                                    frames(:,:,cur_idx) = double(tmp_frm);
                                    cur_idx = cur_idx+1;
                                end
                            end
                            avgFrmbyLocation(frames,numheight_ignore,strip_offsets,strip_starts, stripheight,tl_row,tl_col, crop_h,crop_w,strip_th_fac,flag_MOD,step_frms, dataFolder, CoRegVids(CoIdx).name);
                        end
                    end
                end
            end
        else % read in all frames from videos without fnum file
            step_frms = [];
            if strcmp(dataType,'avi')% for image sequence saved as avi video
                vidObject = VideoReader([vid_info{idx_vid,1} '/' vid_info{idx_vid,3} '.avi']);
                cur_vid_name = vidObject.Name;
                vidFormat = vidObject.VideoFormat;
                numframes = round(vidObject.FrameRate*vidObject.Duration);
                frameheight = vidObject.Height;
                framewidth = vidObject.Width;
                frames = zeros(vidObject.Height,vidObject.Width,numframes);
                framemeans = zeros(numframes,1);
                for frm_idx = 1:numframes
                    tmp_frm = readFrame(vidObject);
                    if size(tmp_frm, 3)>1
                        tmp_frm = rgb2gray(tmp_frm);
                    end
                    frames(:,:,frm_idx) = double(tmp_frm);
                    framemeans(frm_idx) = mean2(frames(:,:,frm_idx));
                end
            elseif strcmp(dataType,'bmp')
                dataPattern = fullfile(dataFolder, '*.bmp'); %
                imgSequence = dir(dataPattern);
                numframes = length(imgSequence);
                img = imread(fullfile(imgSequence(1).folder, imgSequence(1).name));
                cur_vid_name = imgSequence(1).name;
                frameheight = size(img,1);
                framewidth = size(img,2);
                frames = zeros(frameheight,framewidth,numframes);
                framemeans = zeros(numframes,1);
                for frm_idx = 1:numframes
                    tmp = imread(fullfile(imgSequence(frm_idx).folder , imgSequence(frm_idx).name));
                    if size(tmp, 3)>1
                        tmp = rgb2gray(tmp);
                    end
                    frames(:,:,frm_idx) = double(tmp);
                    framemeans(frm_idx) = mean2(frames(:,:,frm_idx));
                end
            end
            %
            if flag_refImg == 0
                ref_img = frames(:,:,ref_frm_idx);
            end
            % find blinks
            if flg_blink == 0 %for no blink video
                bstartstrip = [];
                bendstrip = [];
                endblink=[];
                startblink = [];
                %  save([vid_info{idx_vid,1} '\' cur_vid_name(1:end-4) '_blinktest.mat'], 'bstartstrip', 'bendstrip','startblink', 'endblink');
            else
                [bstartstrip, bendstrip,endblink,startblink] = FindBlinksandStrips(frames,cur_vid_name, framemeans, nstrips, numframes, vid_info{idx_vid,1} );
            end
            % video stabilization  
            if systemType == 0
%             if systemType == 0 || systemType == 1
                [strip_offsets,strip_starts] = videostabilization(cur_vid_name, frames, numheight_ignore, vid_info{idx_vid,1}, startblink, endblink ,ref_img, nstrips, stripheight,bstartstrip, bendstrip);
            else
                [strip_offsets,strip_starts] = videostabilization_psi(cur_vid_name, frames, numheight_ignore, vid_info{idx_vid,1}, startblink, endblink, ref_img, nstrips, stripheight,bstartstrip, bendstrip);
            end
            %% genereate average image
            [tl_row,tl_col,crop_h,crop_w] = calculateCropRegion(frameheight,framewidth,numframes,strip_offsets,numheight_ignore,strip_starts, stripheight,frameheight, framewidth, strip_th_fac, overlap_th_fac, flag_MOD, step_frms);
            avgFrmbyLocation(frames,numheight_ignore,strip_offsets,strip_starts, stripheight,tl_row,tl_col, crop_h,crop_w,strip_th_fac,flag_MOD,step_frms, dataFolder, cur_vid_name);
             %% generate average images for other chs if has any 
            % check if there are some other channels avi in the same
            % folder. May need to edit this based on how video from
            % different channels are named
            if strcmp(dataType,'avi')
                i = strfind(cur_vid_name,'CH');
                CoRegVids = dir(fullfile(dataFolder, [cur_vid_name(1:i+1) '*' cur_vid_name(i+3:end)]));
                if length(CoRegVids) > 1 && channelVids==true
                    for CoIdx = 1:length(CoRegVids)
                        if ~strcmp(CoRegVids(CoIdx).name,cur_vid_name)
                            vidObject = VideoReader(fullfile(dataFolder, CoRegVids(CoIdx).name));
                            for frm_idx = 1:numframes
                                tmp_frm = readFrame(vidObject);
                                if size(tmp_frm,3)>1
                                    tmp_frm = rgb2gray(tmp_frm);
                                end
                                frames(:,:,frm_idx) = double(tmp_frm);
                            end
                            avgFrmbyLocation(frames,numheight_ignore,strip_offsets,strip_starts, stripheight,tl_row,tl_col, crop_h,crop_w,strip_th_fac,flag_MOD,step_frms, dataFolder, CoRegVids(CoIdx).name);
                        end
                    end
                end
            end
        end
        % record the patient been processed sucessfully
        fileID = fopen([vid_info{idx_vid,1}  '/AOSLO_Log.txt'],'a');
        fprintf(fileID,'%s done\r\n',cur_vid_name);
        fclose(fileID);
        h = waitbar((idx_vid-1)/(nfiles-1),h,['Processing videos.. ' num2str(nfiles-idx_vid) ' videos remaining.']);
    catch ME
        fileID = fopen([vid_info{idx_vid,1} '/AOSLO_ErrorLog.txt'],'a');
        fprintf(fileID,'error subject:%s\r\n',cur_vid_name);
        report = ME.getReport;
        fprintf(fileID,'error details:%s\r\n \r\n \r\n',report);
        fclose(fileID);
        h = waitbar((idx_vid-1)/(nfiles-1),h,['Processing videos.. ' num2str(nfiles-idx_vid) ' videos remaining.']);
        continue;
    end
end
close(h);


