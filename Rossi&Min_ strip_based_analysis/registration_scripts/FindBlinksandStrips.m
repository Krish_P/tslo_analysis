% Bianca Leonard & Ethan Rossi 11.19.2018
% Blink Marking for AOSLO
function [bstartstrip, bendstrip,endblink,startblink] = FindBlinksandStrips(frames,vidname, framemeans, nstrips, numframes, pname)

thresh_level = 3.5;%this is how we set the threshold...  we take the minimum value after scaling and log10 and divide by their range
fnum = 1:1:length(framemeans);
framemeans = scale(framemeans);
framemeans = log10(framemeans);
framemeans(isinf(framemeans)) = NaN; %remove infinite values and replace with NaN 

threshold = min(framemeans,[],'omitnan')./thresh_level;
blinkframes = fnum(framemeans<threshold);
blinkframes = [blinkframes, find(isnan(framemeans))'];


%find start and end strips% Bianca Leonard & Ethan Rossi 11.19.2018
% Blink Marking on TSLO

stripheight = 32;
frameinterval= 2;%number of frames to check around start and end of a given blink interval 
frameheight = 512;
bstartstrip= [];
bendstrip = [];

startblink = [];
endblink = [];

if ~isempty(blinkframes)
    blinkframes = sort(blinkframes);
    if blinkframes(end) == numframes %remove "blinks" at the start
        if blinkframes(1,1) == 1
            i = 2;
            while i <= length(blinkframes) && blinkframes(1,i) - blinkframes(1,i-1)==1
                i = i+1;
            end
            blinkframes(1:i-1) = [];
        end
    end
    
    blinkstarts = blinkframes(1);
    blinkends = [];
    
    for i = 2:length(blinkframes) %finds the blink intervals by comparing frame numbers to see when it jumps
        if  blinkframes(i) - blinkframes(i-1)>1
            blinkstarts = [blinkstarts; blinkframes(i)];
            blinkends = [blinkends; blinkframes(i-1)];
        end
    end
    
    blinkends = [blinkends; blinkframes(length(blinkframes))];
    
    % if length(blinkframes) > 1 %remove "blinks" at the end
    %     if blinkends(end) == numframes
    %         i = length(blinkframes);
    %         %         while i > 0 && blinkframes(i) - blinkframes(i-1)==1
    %         while i > 1 && blinkframes(i) - blinkframes(i-1) == 1
    %             i = i-1;
    %             blinkends(end) = [];
    %             blinkstarts(end) = [];
    %         end
    %         blinkframes(i:length(blinkframes)) = [];
    %     end
    % end
    
    
    
    % avgpxval = mean(framemeans); %whole-recording mean of all of the framemeans
    % sdpxval = std2(framemeans)*0.5;%threshold to check for return to open-eye data
    % minussdpxval = avgpxval-sdpxval;
    nblinks = length(blinkstarts);
%     bstartstrip= [];
%     bendstrip = [];
    startpt = [];
    endpt = [];
    
    
    for i = 1:nblinks % check surrounding frames to get blink starts and ends down to strip number
        
        thisblinks = [blinkstarts(i) - frameinterval:1:blinkstarts(i)];
        thisblinke = [blinkends(i):1:blinkends(i) + frameinterval];
        thisblink = [thisblinks thisblinke];
        for index = 1: length(thisblink)
            if thisblink (index) <= 0
                thisblink(index) = 1;
            end
        end
        for index = 1: length(thisblink)
            if thisblink(index) > numframes
                thisblink(index) = numframes;
            end
        end
        scaleall = [];
        stripmean = zeros(length(nstrips));
        for j= 1:length(thisblink)
            framenum = thisblink(j);
            thisframe = frames(:,:,framenum);
            
            %for stripidx = 1:nstrips
            for stripidx = 1:nstrips
                stripmean(stripidx) = mean2(thisframe((stripidx-1)*stripheight+1:stripidx*stripheight));
            end
            
            snum = 1:1:length(stripmean);
            stripmean = scale(stripmean);
            stripmean = log10(stripmean);
            stripmean(isinf(stripmean)) = NaN; %remove infinite values and replace with NaN
            
            threshold = min(stripmean,[],'omitnan')./thresh_level;
            blinkstrips = fnum(stripmean<threshold);
            stripmean = scale(stripmean);
            stripmean = log10(stripmean);
            
            for stripidx = 1:nstrips
                scaleall = [scaleall; (framenum-1)*nstrips+stripidx, stripmean(stripidx)];
            end
            
            %end
            
            startstrip = find(scaleall(:,2) <= threshold,1); %first point to dip below threshold
            endstrip = find(scaleall(:,2) <= threshold, 1,'last');%last point before data is above threshold
            
        end
        bstartstrip = [bstartstrip;scaleall(startstrip,1)]; %store strips
        bendstrip= [bendstrip; scaleall(endstrip,1)];
        startpt = [startpt; scaleall(startstrip,1), (floor((scaleall(startstrip,1)-1)/nstrips))+1];
        endpt= [endpt; scaleall(endstrip,1), (ceil((scaleall(endstrip,1)-1)/nstrips))+1];
        startblink = startpt(:,2);
        endblink =  endpt(:,2);        
    end
end
% save([fname(1:end-4) '_blinktest.mat'], 'bstartstrip', 'bendstrip','startblink', 'endblink');%Bianca orig code
% save([pname '\' vidname(1:end-4) '_blinktest.mat'], 'bstartstrip', 'bendstrip','startblink', 'endblink'); %edit by Min

%   figure
%      plot(framemeans);hold on;
%      plot(1:numframes,threshold, 'r.-');hold on;
%      plot(1:numframes,minussdpxval, 'b.-');hold on;
%      plot(startpt(:,2),framemeans(startpt(:,2)), 'ko'); hold on;
%      plot(endpt(:,2),framemeans(endpt(:,2)), 'go'); hold on;
 
