%% Purpose: 
%  This script uses the individual reference frames from strip based analysis
%  and runs cross correlation between them to get global eye
%  position measure/offset between reference images for each individual subject

%% Step 1: We run through images in the folder and get the largest one to be
%         the template that we register to

topLevelFolder=uigetdir('C:\TSLOBackup','Select Raw Video folder');
cd (topLevelFolder)

firstImg=true;
listoffiles=dir(topLevelFolder);
for fileIdx=1:length(listoffiles)
    filename=listoffiles(fileIdx).name;
    if endsWith(filename,'.tif')
        ref_image=imread(filename);
        % we get sum of horizontal & vertical pixels
        if firstImg==true
            prev_img_area=size(ref_image,1) * size(ref_image,2);
            % after getting first image we set this to false
            firstImg=false;
        else
            curr_img_area=size(ref_image,1) * size(ref_image,2);
             % we check if this image is bigger than previous one
            if curr_img_area>prev_img_area
                largest_ref_image=imread(filename);
            end 
            % we assign the current one as the previous image before the next
            % iteration
            prev_img_area=curr_img_area;
        end    
    end
end
   
%% Step 2:
%  We run the normalized cross correlation for each image using the largest
%  reference image from earlier as the template
x_shift_array=[];
y_shift_array=[];
filename_array=[];
for fileIdx=1:length(listoffiles)
    filename=listoffiles(fileIdx).name;
    if endsWith(filename,'.tif')
        img_for_crosscorr=imread(filename);
        % we run the cross correlation method
        c=xcorr2(cropped_img,largest_ref_image);
        [ypeak,xpeak] = find(c==max(c(:)));
        yoffSet = ypeak-size(img_for_crosscorr,1);
        xoffSet = xpeak-size(img_for_crosscorr,2);
        % we get file code from filename
        fileCode=filename(1:strfind(filename,"_Video")-1);
        %add these values to an array
        x_shift_array=[x_shift_array,xoffSet];
        y_shift_array=[y_shift_array,yoffSet];
        filename_array=[filename_array,fileCode];
    end
end

% we finally combine these values and add them to an array
x_shift_array=transpose(x_shift_array);y_shift_array=tranpose(y_shift_array);
filename_array=transpose(filename_array);
pix_shift_array=vertcat(x_shift_array,y_shift_array,filename_array);