# Purpose: This function compiles the eye position data from the mat file that we get after running the strip based method
# we then pad the eye position data to make the time stamps more uniform. 
def eyepos_padding(foldername,trialTime):
    trialDuration=int(trialTime)
    # 1. we compile the data into one single array
    foldername=str(foldername)
    file_list=os.listdir(foldername)
    # create empty list and create array on the fly
    tslodata=[]
    for filename in file_list:
        try:
            if '540_hz' in filename:
                # First we load the mat file into jupyter
                mat=scipy.io.loadmat(os.path.join(foldername,filename))
                # we then obtain only the fields that are needed (like the one with eye movement and timestamps)
                eyepos=pd.DataFrame(mat['frameshifts_strips_spline'])
                # we also get the field size from the key_file which is then used to scale the frame shifts
                #from pixels to degrees
                field_size=4.75
                eyepos=eyepos*(field_size/512)
                # we name the first two columns as x and y positions
                eyepos=eyepos.rename(columns={0:'xpos',1:'ypos'})
                time=pd.DataFrame(mat['timeaxis_secs'])
                time=time.rename(columns={0:'time'})
                #we use the eye pos and time fields and combine it to a form a temporary data structure
                data=pd.concat([time,eyepos.iloc[:,:]],axis=1,sort=False)
                # We finally add file details to the data structure which can later be used to 
                # query into the dataframe
                subjfolder=foldername.replace('/home/coateslab/LabData/TSLOVideos/MainExperiment/','')
                data['filename']=filename[0:filename.find('_540')]
                #The temporary data is then appended to the bigger data structure 
                tslodata.append(data)
        except: # catch all errors
            e=sys.exc_info()[0]
            print(e)
    #combine the array
    tslodata=pd.concat(tslodata)
    # 2. We run the padding method to get uniform set of samples
    codeList=tslodata['filename'].unique()
    eyeposMissing=pd.DataFrame({'time':[],'xpos':[],'ypos':[],'filename':[]})
    failedList=[]
    diffArray=[]
    # we loop through each eye position data file within the pandas dataframe
    for acode in codeList:
        mydata=tslodata.query("filename==@acode")
        filename=mydata['filename'][0]
        timeseries=mydata['time']
        # we identify places where the time stamps are inconsistent and pad them with 
        # the required number of dummy samples 
        try:
            for i in range(len(timeseries)):
                if i<len(timeseries)-1:
                    diffVal=(timeseries[i+1]-timeseries[i])
                    if diffVal>0.0020:
                        diffArray.append(diffVal)
                        numPts=int(diffVal/(1/540))
                        startPt=timeseries[i]+(1/540)
                        endPt=timeseries[i+1]-(1/540)
                        timeFillers=np.linspace((startPt),(endPt),numPts)
                        random_matrix=np.random.randn(len(timeFillers),1)
                        insert_df=pd.DataFrame(random_matrix,columns=['time'])
                        insert_df.iloc[:,0]=timeFillers
                        insert_df['xpos']=float("NaN")
                        insert_df['ypos']=float("NaN")
                        insert_df['filename']=filename
                        eyeposMissing=pd.concat([eyeposMissing,insert_df],ignore_index=True)
                else:
                    diffVal=trialDuration-timeseries[i]
                    numPts=int(diffVal/(1/540))
                    startPt=timeseries[i]+(1/540)
                    endPt=trialDuration
                    timeFillers=np.linspace((startPt),(endPt),numPts)
                    random_matrix=np.random.randn(len(timeFillers),1)
                    insert_df=pd.DataFrame(random_matrix,columns=['time'])
                    insert_df.iloc[:,0]=timeFillers
                    insert_df['xpos']=float("NaN")
                    insert_df['ypos']=float("NaN")
                    insert_df['filename']=filename
                    eyeposMissing=pd.concat([eyeposMissing,insert_df],ignore_index=True)
        except ValueError:
            pass 
    eyeposMerge=pd.concat([tslodata,eyeposMissing],ignore_index=True)
    # 3. we finally write the padded eye position data back to a csv 
    expectedNumSamples=trialDuration*540 #currrent sampling rate
    codeList=eyeposMerge['filename'].unique()
    failedList=[]
    for acode in codeList:
        os.chdir(foldername)
        mydata=eyeposMerge.query("filename==@acode")
        print(filename)
        mydata=mydata.sort_values('time')
        if len(mydata)<expectedNumSamples:
            failedList.append(acode)
            outFileName=acode+'_EyePosNaN.csv'
            mydata.to_csv(outFileName)
        else:
            outFileName=acode+'_EyePosNaN.csv'
            mydata.to_csv(outFileName)
			
if __name__ == "__main__":
    import os,sys
    import pandas as pd
    import numpy as np
    import scipy.io
    globals()[sys.argv[1]](sys.argv[2],sys.argv[3]) # Pass in parameters on the command line to this program
    print("Set of files complete!")
