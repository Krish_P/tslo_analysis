function sumcorr = locatestripCUDA(theframe,thestrip)
% corrarray = locatestrip(frame, strip);
% returns a 2D correlation array showing the peak correlation where the
% array STRIP is found in the larger array FRAME
%
%this is a modified version of locatestripNoDLL.m from (c) 2013 SBStevenson@uh.edu Peace*Love*Trees
%edited by Ethan A. Rossi, June 2018

[fm, fn] = size(theframe);
[sm, ~] = size(thestrip);
sumcorr = double(zeros(fm-sm,fn));
%theframe = double(theframe); theframe = theframe - mean(theframe(:));
%thestrip = double(thestrip); thestrip = thestrip - mean(thestrip(:));

%move the data to GPU
theframe = gpuArray(theframe);
thestrip = gpuArray(thestrip);
sumcorr = gpuArray(sumcorr);

for srowdx = 1:sm
    a = fft(theframe(srowdx:fm-sm+srowdx-1,:),[],2);
    b = conj(fft(thestrip(srowdx,:)));  
    onecorr =  ifft(bsxfun(@times,a,b),[],2);
    sumcorr = sumcorr + onecorr;
end

sumcorr = real(fftshift(sumcorr,2));
sumcorr = sumcorr ./ max(sumcorr(:));

sumcorr = gather(sumcorr);