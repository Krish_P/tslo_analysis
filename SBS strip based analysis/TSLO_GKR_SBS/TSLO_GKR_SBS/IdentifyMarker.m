%% Purpose:  This script helps identify markers from the video (excluding blinks)
function [markerData]=IdentifyMarker(filepath,markermessage,threshFactor)
videotoanalyse = filepath;
blinkthreshold = 25;                
minimummeanlevel = 15;  

blinkFrames=getblinkframes(videotoanalyse,blinkthreshold,minimummeanlevel,0);
videoinfo=VideoReader(videotoanalyse);
numFrames=videoinfo.FrameRate*videoinfo.Duration;
messageCounter=1;
blinkCounter=1;
prevMarker=0; % We would set this to be 0 at first and assign the marker frame in the loop
fps=videoinfo.FrameRate;
%Place holder for markerData
markerData=[];
for frameIdx=1:numFrames
    if any(blinkFrames==frameIdx)==1
        frame=frame2im(read(videoinfo,frameIdx,'native'));
        meanLumFrame=mean(frame,(2));
        % A single marker could occassionally be split between the
        % bottom of one frame and the top of the next one. 
        stripLoc=find(meanLumFrame==0);
        stripHt=max(stripLoc)-min(stripLoc);
        % we expect the strip height to be around 8 pixels
        blinkMarker(blinkCounter,1:2)=[frameIdx,min(stripLoc)];
        blinkCounter=blinkCounter+1;
        prevMarker=frameIdx; 
    else
        frame=frame2im(read(videoinfo,frameIdx,'native'));
        meanLumFrame=mean(frame,(2));
        % In this case the marker message is always the same between
        % frames or just the black strip 
        if markermessage==0
            if isempty(find(meanLumFrame<threshFactor, 1))
                continue
            else
                % A single marker could occassionally be split between the
                % bottom of one frame and the top of the next one. 
                if frameIdx-prevMarker>2 % we expect the next event to occur atleast a second from a last one
                    stripLoc=find(meanLumFrame<threshFactor);
                    stripHt=max(stripLoc)-min(stripLoc);
                    % we expect the strip height to be around 8 pixels
                    markerData(messageCounter,1:2)=[frameIdx,min(stripLoc)];
                    messageCounter=messageCounter+1;
                    prevMarker=frameIdx; % would update this each time
                end
            end
        elseif markermessage==1
          % we use only the left half of the frame since the last marker
          % has a slightly bigger gap to the right than the other ones,
          % which makes it harder to detect
          %Likewise omit the first few scanlines that are a part of the
          %flyback
          frame=frame(8:end,1:256);
          % we set the strip counter to be one for each frame
          stripCounter=1;
          if frameIdx-prevMarker>1 % we expect the next event to occur atleast for 30 ms
            for scanIdx=1:size(frame,1)
                scanline=mean(frame(scanIdx,:),2);
                threshold=mean(frame,'all')/threshFactor;
                if scanline<threshold
                    stripLoc(stripCounter)=scanIdx;
                    stripCounter=stripCounter+1;
                end
            end
            if stripCounter>1
                markerData(messageCounter,1:2)=[frameIdx,min(stripLoc)];
                messageCounter=messageCounter+1;
                prevMarker=frameIdx; % would update this each time
            end
          end
        end
    end     
end
end
