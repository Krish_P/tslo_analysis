function [stabilisedvideoname,stabilisedframe,stabilisedframe_full] =...
    makestabilizedvideoandframe(videoname,framestostabilize,frameshifts,...
    peakratios,stripindices,badsamplethreshold,maintaintimerelationships,...
    numlinesperfullframe,blacklineflag,maxsizeincrement,splineflag,verbose)
% makestabilizedvideoandframe.m. This program creates a stabilized movie and frame
% given a raw SLO video and the required frameshifts and other inputs. This help
% text assumes that analysevideo_priorref was used to obtained the shifts
% in the retinal SLO video.
% 
% Usage: [stabilisedvideoname] =
% makestabilizedvideo(videoname,framestostabilize,frameshifts,peakratios,st
% ripindices,[badsamplethreshold],[maintaintimerelationships],[blacklineflag],
% [maxsizeincrement],[verbose])
% videoname             - The name of the raw AOSLO video that needs to be
%                         stabilized.
% framestostabilize     - The framenumbers of the video that will be
%                         stabilized. The frameshifts that are returned by
%                         anaylsevideo_priorref.m do not cover then entire
%                         video range, some frames are dropped. The
%                         variable "framesforqanalyses" saved by
%                         analysevideo_priorref can be used as this input
%                         variable.
% frameshifts           - The shifts extracted from the retinal video. The
%                         variables "frameshifts_strips" / 
%                         "frameshifts_strips_spline" from the matfile
%                         saved by analysedvideo_priorref should be used.
%                         You could pass any shifts to the program, as long
%                         the sign convention is maintained. This matrix as
%                         to have two columns - horizontal and vertical
%                         shifts in the first and second columns
%                         respectively. Shifts towards the right and
%                         downward shifts are positive.
% peakratios            - analysevideo_priorref tags data points that could
%                         be possible bad data by calculating the ratio
%                         between the second and first peaks in the cross
%                         correlation functionand saving it in a variable
%                         called "peakratios_strips". Since we want a high
%                         first peak and a low second peak, low peak ratios
%                         are 'good' and high values need to be considered
%                         as 'bad' data points. This program therefore
%                         finds the low values and fits the spline across
%                         the bad data points.
% stripindices          - analysedvideo_priorref analysed shifts in the
%                         SLO videos by segmenting each frame in the video
%                         into strips centered on specific lines in each
%                         frame. stripindices is a column matrix that
%                         contains these lines that are the center of each
%                         strip.
% badsamplethreshold    - This is the threshold that is peakratios are
%                         compared in order to tag 'bad' data points. The
%                         lower it's value the more conservative is the
%                         comparison. Default value is 0.5.
% maintaintimerelationships - Since we drop unanalysed frames, we have two
%                             options when stabilizing. One leave the frame
%                             out during the stabilization process, leaving
%                             the stabilized with less frames than the raw
%                             video. The second option is to leave in a
%                             blank frame in place of the unanalysed
%                             frames. The second option is too be used if
%                             one is interested in calculating kinematics
%                             of events in the video. If 1 is passed as
%                             this input then the second option is chosen
%                             while 0 chooses the first option. Default is
%                             1.
% blacklineflag         - When the eye moves faster than the scan rate, the
%                         stabilized will have black lines. This is due to
%                         the absence of any data at these locations. If 1
%                         is passed as this input the program interpolates
%                         across these locations in the frame, deleteing
%                         the black line at the expense of anatomical
%                         accuracy. 0 (Default value) keeps the black
%                         lines in the videos.
% maxsizeincrement      - If the video has large shifts, it is quite
%                         possible that the stabilized video will be far
%                         too large. This could also lead to the
%                         possibility of a program crash due to lack of
%                         memory. To overcome this we have to ensure that
%                         the video does not cross a thresold size. If the
%                         value 2 is passed as this input then the max size
%                         of the stabilized video is twice the size of the
%                         raw video. Default value is 2.
% verbose               - If 1 is passed then the program plots each
%                         stabilized frame as it is calculated, if 0 is
%                         passed the user does not get any such feedback.
%
%
% stabilisedvideoname   - The program has an optional output - the name of
%                         the stabilized video (the program adds _stab to
%                         the raw video to form the name of the stabilized
%                         video.
% Note: The length of frameshifts and peakratios has to be equal to the
% length of framestostabilize multiplied by the length of stripindices.



currentdirectory = pwd;

if (nargin < 12) | isempty(verbose)
    verbose = 0;
end

if (nargin < 11) | isempty(splineflag)
    splineflag = 0;
end

if (nargin < 10) | isempty(maxsizeincrement)
    maxsizeincrement = 2;
end

if (nargin < 9) | isempty(blacklineflag)
    blacklineflag = 0;
end

if (nargin < 8) | isempty(numlinesperfullframe)
    numlinesperfullframe = 525;
end

if (nargin < 7) | isempty(maintaintimerelationships)
    maintaintimerelationships = 1;
end

if (nargin < 6) | isempty(badsamplethreshold)
    badsamplethreshold = 0.5;
end

if (nargin < 5) | isempty(stripindices) | isempty(peakratios) |...
        isempty(frameshifts) | isempty(framestostabilize)
    disp('Invalid number of input arguments');
    error('Type ''help makestabilizedvideoandframe'' for usage');
end

if (size(frameshifts,1) ~= size(peakratios,1))
    disp('Shifts and Peakratio matrix sizes are not equal');
    error('Type ''help makestabilizedvideoandframe'' for usage');
end

if isempty(videoname) | ~exist(videoname,'file')
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to stabilize,stopping program');
        error('Type ''help makestabilizedvideoandframe'' for usage');
    else
        videoname = strcat(avipath,avifilename);
    end
end

videoinfo = aviinfo(videoname);
%     KSP Changes 02/07/2020
fileinfo=VideoReader(videoname);
framerate = round(videoinfo.FramesPerSecond);
% numbervideoframes = videoinfo.NumFrames;
numbervideoframes=fileinfo.FrameRate*fileinfo.Duration;
framewidth = videoinfo.Width;
frameheight = videoinfo.Height;
videotype = videoinfo.ImageType;
framesizes = [framewidth,frameheight];
framenumbers = [1:numbervideoframes]';

if strcmp(videotype,'truecolor')
    disp('Video being analyssed is a truecolor video, this program can analyse only 8 bit videos!!');
    warning('Using only the first layer of the video during analyses');
    istruecolor = 1;
else
    istruecolor = 0;
end

if numlinesperfullframe < frameheight
    disp('Number of lines in full frame cannot be smaller than the number of lines in video frame');
    error('Type ''help makestabilizedvideoandframe'' for usage');
end

numanalysedframes = length(framestostabilize);
numsamplesperframe = length(stripindices);
numsamplesintrace = size(frameshifts,1);
if maintaintimerelationships
    numframestostabilize = numbervideoframes;
else
    numframestostabilize = numanalysedframes;
end

if ((numsamplesintrace / numsamplesperframe) ~= numanalysedframes)
    disp('Number of frames supplied does not correspond to size of shift matrix');
    error('Type ''help makestabilizedvideo'' for usage');
end

numindicestoignore = 9;
indexaddition = [0:numindicestoignore - 1] - floor(numindicestoignore / 2);

badmatches_initial = find(peakratios(:) >= badsamplethreshold);
numinitialbadmatches = length(badmatches_initial);

badmatches = repmat(badmatches_initial,1,numindicestoignore) +...
    repmat(indexaddition,numinitialbadmatches,1);
badmatches = unique(max(min(badmatches(:),numsamplesintrace),1));

% goodmatches = setdiff([1:numsamplesintrace]',badmatches);
goodmatches = setdiff([1:numsamplesintrace]',badmatches_initial); %KSP 02/11/2020

if length(goodmatches) == numsamplesintrace
    frameshiftstouse = frameshifts;
else
    frameshiftstouse = zeros(size(frameshifts));
    interp_xaxis = [1:numsamplesintrace];
    sample_xaxis = interp_xaxis(goodmatches);

    for directioncounter = 1:2
        sample_yaxis = frameshifts(goodmatches,directioncounter);
        interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'pchip','extrap');
        frameshiftstouse(:,directioncounter) = interp_yaxis;
    end
end

[sizeincrement,stabilizedsize,imageborders] = getstabilizedparams(frameshiftstouse,framesizes,maxsizeincrement);

sizeincrement = min(sizeincrement,maxsizeincrement);
stabilizedsize = round([framewidth frameheight] * sizeincrement);

imageborders(1) = max(imageborders(1),1);
imageborders(2) = min(imageborders(2),stabilizedsize(1));
imageborders(3) = max(imageborders(3),1);
imageborders(4) = min(imageborders(4),stabilizedsize(2));

stabilisedframe = zeros(stabilizedsize(2),stabilizedsize(1));
sumstabilisedframe = zeros(stabilizedsize(2),stabilizedsize(1));


splineshiftstouse = zeros(frameheight,numframestostabilize,2);

if numsamplesperframe > 1
    if splineflag
        interp_xaxis = [1:frameheight]';
        sample_xaxis = stripindices;
        for directioncounter = 1:2
            tempshifts = frameshiftstouse(:,directioncounter);
            sample_yaxis_full(:,:,directioncounter) = reshape(tempshifts,numsamplesperframe,...
                numanalysedframes);
        end
        for framecounter = 1:numanalysedframes
            if maintaintimerelationships
                indextoputspline = find(framenumbers == framestostabilize(framecounter));
            else
                indextoputspline = framecounter;
            end
            for directioncounter = 1:2
                sample_yaxis = sample_yaxis_full(:,framecounter,directioncounter);
                interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'linear','extrap');
                splineshiftstouse(:,indextoputspline,directioncounter) = interp_yaxis * -1.0;
            end
        end
    else
        interp_xaxis = [0:(numlinesperfullframe * numframestostabilize) - 1];
        interp_xaxis = reshape(interp_xaxis,numlinesperfullframe,numframestostabilize);
        
        if maintaintimerelationships
            sample_axis = interp_xaxis(stripindices,framestostabilize);
        else
            sample_axis = interp_xaxis(stripindices,:);
        end

        interp_xaxis = interp_xaxis(1:frameheight,:);
        interp_xaxis = interp_xaxis(:);
        sample_xaxis = sample_axis(:);
        interp_xaxis = min(interp_xaxis,max(sample_xaxis));

        for directioncounter = 1:2
            sample_yaxis = frameshiftstouse(:,directioncounter);
            interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'linear','extrap');
            splineshiftstouse(:,:,directioncounter) = ...
                reshape(interp_yaxis,frameheight,numframestostabilize) * -1.0;
        end
    end
else
    if maintaintimerelationships
        indicestoputspline = framestostabilize;
    else
        indicestoputspline = [1:numanalysedframes];
    end
    for directioncounter = 1:2
        tempshifts = frameshiftstouse(:,directioncounter) * -1.0;        
        splineshiftstouse(:,indicestoputspline,directioncounter) = repmat(tempshifts(:)',frameheight,1);
    end
end

mymap = repmat([0:255]'/ 256,1,3);
stabilisedvideoname = strcat(videoname(1:end - 4),'_stab.avi');

columnaddition = ceil((stabilizedsize(1) - framewidth) / 2) + [0:framewidth - 1];
rowaddition = ceil((stabilizedsize(2) - frameheight) / 2);
sumrow = ones(1,framewidth);
summatrix = ones(frameheight,framewidth);

if verbose
    namestring = ['Stabilized Figure: 0 frames done, ',num2str(numframestostabilize),...
        ' frames to go'];
    mymap = repmat([0:255]' / 256,1,3);
    
    singlestabfighandle = figure;
    singlestabimghandle = image(zeros(stabilizedsize(2),stabilizedsize(1)));
    colormap(mymap);
    truesize;

    set(singlestabimghandle,'erasemode','none');
    singlestabimgtitlehandle = title('Single Stabilized Frame');
    set(singlestabfighandle,'Name',namestring);
    
    stabfighandle = figure;
    stabimghandle = image(zeros(stabilizedsize(2),stabilizedsize(1)));
    colormap(mymap);
    truesize;

    set(stabimghandle,'erasemode','none');
    stabimgtitlehandle = title('Stabilized Image');
    set(stabfighandle,'Name',namestring);
end

% stabprog = waitbar(0,'Making a Stabilised Video & Frame');

if maintaintimerelationships
    numframesinmovie = numbervideoframes;
else
    numframesinmovie = length(framestostabilize);
end

framerowindices = [1:frameheight]';
stabrowindices = [1:stabilizedsize(2)]';

moviestruct = repmat(struct('cdata',zeros(frameheight,framewidth),'colormap',mymap),numframesinmovie,1);

for framecounter = 1:numbervideoframes
    if ~isempty(find(framecounter == framestostabilize))
        indexintomatrix = find(framecounter == framestostabilize);
        framenumbertoadd = framestostabilize(indexintomatrix);
%         frametoadd = double(frame2im(aviread(videoname,framenumbertoadd)));
        try
            frametoadd = double(frame2im(read(fileinfo,framenumbertoadd,'native'))); %KSP added 
        catch
            frametoadd = double((read(fileinfo,framenumbertoadd,'native')));
        end
        if istruecolor
            frametoadd = frametoadd(:,:,1);
        end
        
        singlestabilisedframe = zeros(stabilizedsize(2),stabilizedsize(1));
        singlesumstabilisedframe = zeros(stabilizedsize(2),stabilizedsize(1));
        
        if maintaintimerelationships
            frameshifttouse = squeeze(splineshiftstouse(:,framecounter,:));
        else
            frameshifttouse = squeeze(splineshiftstouse(:,indexintomatrix,:));
        end
        
        if blacklineflag
            firstrowwithimagedata = max(ceil(frameshifttouse(:,2)) + 1 + rowaddition - 1) + 1;
            lastrowwithimagedata = min(floor(frameshifttouse(:,2)) + frameheight + rowaddition - 1) - 1;
            vertframespan = [firstrowwithimagedata:lastrowwithimagedata]';
        end
        
        for rowcounter = 1:frameheight
            rowtoadd = frametoadd(rowcounter,:);
            shifttouse = round(frameshifttouse(rowcounter,:));

            targetcolumns = round(columnaddition + shifttouse(1));
            targetcolumns = max(targetcolumns,1);
            targetcolumns = min(targetcolumns,stabilizedsize(1));

            targetrow = round(rowaddition + shifttouse(2)) + rowcounter - 1;
            targetrow = max(targetrow,1);
            targetrow = min(targetrow,stabilizedsize(2));

            singlestabilisedframe(targetrow,targetcolumns) = ...
                singlestabilisedframe(targetrow,targetcolumns) + rowtoadd;
            singlesumstabilisedframe(targetrow,targetcolumns) = ...
                singlesumstabilisedframe(targetrow,targetcolumns) + sumrow;
        end
        
        stabilisedframe = stabilisedframe + singlestabilisedframe;
        sumstabilisedframe = sumstabilisedframe + singlesumstabilisedframe;
        
        if verbose
            indiceswithnoimagedata = find(sumstabilisedframe < 1);
            sumstabilisedframe_toimage = sumstabilisedframe;
            sumstabilisedframe_toimage(indiceswithnoimagedata) = 1;
            stabilisedframe_toimage = stabilisedframe ./ sumstabilisedframe;
        end
        
        if blacklineflag
            rowsums = sum(singlestabilisedframe,2);
            blankrows = intersect(vertframespan,find(rowsums == 0));
            nonblankrows = setdiff(vertframespan,blankrows);
            blankrows = blankrows(:);
            numblankrows = length(blankrows);

            if ~isempty(blankrows)
                for blankrowcounter = 1:numblankrows
                    currentblankrow = blankrows(blankrowcounter);
                    if (currentblankrow <= (firstrowwithimagedata + 2)) |...
                            (currentblankrow >= (lastrowwithimagedata - 2))
                        continue
                    end

                    prevnonblankrow = nonblankrows(max(find(nonblankrows < currentblankrow)));
                    if currentblankrow < 1
                        prevnonblankrow = 1;
                    end
                    nextnonblankrow = nonblankrows(min(find(nonblankrows > currentblankrow)));
                    if currentblankrow > max(nonblankrows)
                        nextnonblankrow = max(nonblankrows);
                    end

                    rowtoadd = round((singlestabilisedframe(prevnonblankrow,:) +...
                        singlestabilisedframe(nextnonblankrow,:)) / 2);
                    sumrowtoadd = round((singlesumstabilisedframe(prevnonblankrow,:) +...
                        singlesumstabilisedframe(nextnonblankrow,:)) / 2);
                    indiceswithimagedata = find(rowtoadd >= 1);
                    blanksumrow = zeros(1,stabilizedsize(1));
                    blanksumrow(indiceswithimagedata) = 1;

                    singlestabilisedframe(currentblankrow,:) = rowtoadd;
                    singlesumstabilisedframe(currentblankrow,:) = sumrowtoadd;
                end
            end
        end

        indiceswithnoimagedata = find(singlesumstabilisedframe < 1);

        singlesumstabilisedframe(indiceswithnoimagedata) = 1;
        singlestabilisedframe = singlestabilisedframe ./ singlesumstabilisedframe;

        toscale = 1;
    else
        if maintaintimerelationships
            singlestabilisedframe = ones(stabilizedsize(2),stabilizedsize(1));
        else
            continue
        end

        toscale = 0;
    end
    
    if verbose
        remainingframes = numframestostabilize - framecounter;
        namestring = ['Stabilized Figure: ',num2str(framecounter),' frames done, ',...
            num2str(remainingframes),' frames to go'];
        set(singlestabfighandle,'Name',namestring);
        set(singlestabimghandle,'cdata',singlestabilisedframe);
        
        set(stabfighandle,'Name',namestring);
        set(stabimghandle,'cdata',stabilisedframe_toimage);
    end

    singlestabilisedframe = singlestabilisedframe(imageborders(3):imageborders(4),...
        imageborders(1):imageborders(2),:);

    if maintaintimerelationships
        framenumbertoadd = framecounter;
    else
        framenumbertoadd = find(framecounter == framestostabilize);
    end

    if toscale
        frametoadd = uint8(floor(scale(singlestabilisedframe) * 255) + 1);
    else
        frametoadd = uint8(singlestabilisedframe);
    end
    moviestruct(framenumbertoadd).cdata = frametoadd;
    
    prog = framecounter / numbervideoframes;
%     waitbar(prog,stabprog);
end

if verbose
    close(singlestabfighandle);
    close(stabfighandle);
end

% close(stabprog);

indiceswithimagedata = find(sumstabilisedframe >= 1);
indiceswithnoimagedata = find(sumstabilisedframe < 1);

sumstabilisedframe(indiceswithnoimagedata) = 1;

stabilisedframe = stabilisedframe ./ sumstabilisedframe;

stabilisedframe_withmeanval = stabilisedframe;
stabilisedframe_withmeanval(indiceswithnoimagedata) = mean(stabilisedframe(indiceswithimagedata));

stabilisedframe_withrandvals = stabilisedframe;
randpixelindices = floor(rand(length(indiceswithnoimagedata),1) * length(indiceswithimagedata)) + 1;
randpixelvalues = stabilisedframe(indiceswithimagedata(randpixelindices));
stabilisedframe_withrandvals(indiceswithnoimagedata) = randpixelvalues;

stabilisedframe_withzerovals = stabilisedframe;
stabilisedframe_withzerovals(indiceswithnoimagedata) = 0;

stabilisedframe_full = cat(3,stabilisedframe_withmeanval,stabilisedframe_withrandvals,...
    stabilisedframe_withzerovals);
stabilisedframe = stabilisedframe_full(imageborders(3):imageborders(4),imageborders(1):imageborders(2),:);

movie2avi(moviestruct,stabilisedvideoname,'Compression','None','FPS',framerate);