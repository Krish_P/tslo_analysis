function [stabilisedframe,stabilisedframe_full] = ...
    makestabilizedframe(videoname,framestostabilize,frameshifts,peakratios,...
    stripindices,badsamplethreshold,numlinesperfullframe,maxsizeincrement,splineflag,verbose)

if (nargin < 10) | (isempty(verbose))
    verbose = 0;
end

if (nargin < 9) | (isempty(splineflag))
    splineflag = 0;
end

if (nargin < 8) | (isempty(maxsizeincrement))
    maxsizeincrement = 4;
end

if (nargin < 7) | isempty(numlinesperfullframe)
    numlinesperfullframe = 525;
end

if (nargin < 6) | isempty(badsamplethreshold)
    badsamplethreshold = 0.5;
end

if (nargin < 5) | isempty(stripindices) | isempty(peakratios) |...
        isempty(frameshifts) | isempty(framestostabilize)
    disp('Invalid number of input arguments');
    error('Type ''help makestabilizedframe'' for usage');
end

if (size(frameshifts,1) ~= size(peakratios,1))
    disp('Shifts and Peakratio matrix sizes are not equal');
    error('Type ''help makestabilizedframe'' for usage');
end

if ~exist(videoname,'file')
    disp('Input Video does not exist');
    error('Type ''help makestabilizedframe'' for usage');
end

videoinfo = aviinfo(videoname);
%     KSP Changes 02/07/2020
fileinfo=VideoReader(videoname);
framerate = round(videoinfo.FramesPerSecond);
% numbervideoframes = videoinfo.NumFrames;
numbervideoframes=fileinfo.FrameRate*fileinfo.Duration;%KSP: 02/07/2020
framewidth = videoinfo.Width;
frameheight = videoinfo.Height;
videotype = videoinfo.ImageType;
framesizes = [framewidth,frameheight];

if strcmp(videotype,'truecolor')
    disp('Video being analyssed is a truecolor video, this program can analyse only 8 bit videos!!');
    warning('Using only the first layer of the video during analyses');
    istruecolor = 1;
else
    istruecolor = 0;
end

if numlinesperfullframe < frameheight
    disp('Number of lines in full frame cannot be smaller than the number of lines in video frame');
    error('Type ''help makestabilizedframe'' for usage');
end

numanalysedframes = length(framestostabilize);
numsamplesperframe = length(stripindices);
numsamplesintrace = size(frameshifts,1);
numframestostabilize = numanalysedframes;

if ((numsamplesintrace / numsamplesperframe) ~= numanalysedframes)
    disp('Number of frames supplied does not correspond to size of shift matrix');
    error('Type ''help makestabilizedvideo'' for usage');
end

numindicestoignore = 9;
indexaddition = [0:numindicestoignore - 1] - floor(numindicestoignore / 2);

badmatches_initial = find(peakratios(:) >= badsamplethreshold);
numinitialbadmatches = length(badmatches_initial);

badmatches = repmat(badmatches_initial,1,numindicestoignore) +...
    repmat(indexaddition,numinitialbadmatches,1);
badmatches = unique(max(min(badmatches(:),numsamplesintrace),1));

goodmatches = setdiff([1:numsamplesintrace]',badmatches);
if length(goodmatches) == numsamplesintrace
    frameshiftstouse = frameshifts;
else
    frameshiftstouse = zeros(size(frameshifts));
    interp_xaxis = [1:numsamplesintrace];
    sample_xaxis = interp_xaxis(goodmatches);

    for directioncounter = 1:2
        sample_yaxis = frameshifts(goodmatches,directioncounter);
%         keyboard;
%KSP added comments to silence bugs
%         interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'linear','extrap');
%         frameshiftstouse(:,directioncounter) = interp_yaxis;
    end
end

if max(badmatches(:)) == size(peakratios,1)
    lastgoodsample = max(goodmatches(:));
    for directioncounter = 1:2
        frameshiftstouse(lastgoodsample + 1,directioncounter) =...
            frameshiftstouse(lastgoodsample,directioncounter);
    end
end

if min(badmatches(:)) == 1
    firstgoodsample = min(goodmatches(:));
    for directioncounter = 1:2
        frameshiftstouse(1:firstgoodsample - 1,directioncounter) =...
            frameshiftstouse(firstgoodsample,directioncounter);
    end
end

[sizeincrement,stabilizedsize,imageborders] = getstabilizedparams(frameshiftstouse,framesizes,maxsizeincrement);

sizeincrement = min(sizeincrement,maxsizeincrement);
stabilizedsize = round([framewidth frameheight] * sizeincrement);
imageborders(1) = max(imageborders(1),1);
imageborders(2) = min(imageborders(2),stabilizedsize(1));
imageborders(3) = max(imageborders(3),1);
imageborders(4) = min(imageborders(4),stabilizedsize(2));

splineshiftstouse = zeros(frameheight,numframestostabilize,2);

if numsamplesperframe > 1
    if splineflag
        interp_xaxis = [1:frameheight]';
        sample_xaxis = stripindices;
        for directioncounter = 1:2
            tempshifts = frameshiftstouse(:,directioncounter);
            sample_yaxis_full(:,:,directioncounter) = reshape(tempshifts,numsamplesperframe,numanalysedframes);
        end
        for framecounter = 1:numanalysedframes
            for directioncounter = 1:2
                sample_yaxis = sample_yaxis_full(:,framecounter,directioncounter);
                interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'linear','extrap');
                splineshiftstouse(:,framecounter,directioncounter) = interp_yaxis * -1.0;
            end
        end
    else
        interp_xaxis = [0:(numlinesperfullframe * numframestostabilize) - 1];
        interp_xaxis = reshape(interp_xaxis,numlinesperfullframe,numframestostabilize);
        sample_axis = interp_xaxis(stripindices,:);

        interp_xaxis = interp_xaxis(1:frameheight,:);
        interp_xaxis = interp_xaxis(:);
        sample_xaxis = sample_axis(:);
        interp_xaxis = min(interp_xaxis,max(sample_xaxis));

        for directioncounter = 1:2
            sample_yaxis = frameshiftstouse(:,directioncounter);
            interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'linear','extrap');
            splineshiftstouse(:,:,directioncounter) = ...
                reshape(interp_yaxis,frameheight,numframestostabilize) * -1.0;
        end
    end
else
    for directioncounter = 1:2
        tempshifts = frameshiftstouse(:,directioncounter) * -1.0;
        splineshiftstouse(:,:,directioncounter) = repmat(tempshifts(:)',frameheight,1);
    end
end

stabilisedframe = zeros(stabilizedsize(2),stabilizedsize(1));
sumstabilisedframe = zeros(stabilizedsize(2),stabilizedsize(1));

columnaddition = ceil((stabilizedsize(1) - framewidth) / 2) + [0:framewidth - 1];
rowaddition = ceil((stabilizedsize(2) - frameheight) / 2);
sumrow = ones(1,framewidth);
summatrix = ones(frameheight,framewidth);

if verbose
    mymap = repmat([0:255]' / 256,1,3);
    stabfighandle = figure;
    stabimghandle = image(stabilisedframe);
    colormap(mymap);
    truesize;
    
    set(stabimghandle,'erasemode','none');
    stabimgtitlehandle = title('Stabilized Image');
    
    namestring = ['Stabilized Figure: 0 frames done, ',num2str(numframestostabilize),...
        ' frames to go'];
    set(stabfighandle,'Name',namestring);
end
% stabprog = waitbar(0,'Making a Stabilised Frame');
    
for framecounter = 1:numframestostabilize
    framenumbertoadd = framestostabilize(framecounter);
%     frametoadd = double(frame2im(aviread(videoname,framenumbertoadd)));
    try
        frametoadd = double(frame2im(read(fileinfo,framecounter,'native')));
    catch
        frametoadd = double((read(fileinfo,framecounter,'native')));
    end
    if istruecolor
        frametoadd = frametoadd(:,:,1);
    end
    
    frameshifttouse = squeeze(splineshiftstouse(:,framecounter,:));
    
    for rowcounter = 1:frameheight
        rowtoadd = frametoadd(rowcounter,:);
        shifttouse = round(frameshifttouse(rowcounter,:));

        targetcolumns = round(columnaddition + shifttouse(1));
        targetcolumns = max(targetcolumns,1);
        targetcolumns = min(targetcolumns,stabilizedsize(1));

        targetrow = round(rowaddition + shifttouse(2)) + rowcounter - 1;
        targetrow = max(targetrow,1);
        targetrow = min(targetrow,stabilizedsize(2));

        stabilisedframe(targetrow,targetcolumns) = ...
            stabilisedframe(targetrow,targetcolumns) + rowtoadd;
        sumstabilisedframe(targetrow,targetcolumns) = ...
            sumstabilisedframe(targetrow,targetcolumns) + sumrow;
    end
    
    if verbose
        tempimage = stabilisedframe;
        tempsumimage = sumstabilisedframe;
        indiceswithimagedata = find(tempsumimage >= 1);
        tempimage(indiceswithimagedata) = tempimage(indiceswithimagedata) ./...
            tempsumimage(indiceswithimagedata);
        
        remainingframes = numframestostabilize - framecounter;
        namestring = ['Stabilized Figure: ',num2str(framecounter),' frames done, ',...
            num2str(remainingframes),' frames to go'];
        set(stabfighandle,'Name',namestring);
        set(stabimghandle,'cdata',tempimage);
    end 
    
    prog = framecounter / numframestostabilize;
%     waitbar(prog,stabprog);
end

% close(stabprog);

if verbose
    close(stabfighandle);
end

indiceswithimagedata = find(sumstabilisedframe >= 1);
indiceswithnoimagedata = find(sumstabilisedframe < 1);

sumstabilisedframe(indiceswithnoimagedata) = 1;

stabilisedframe = stabilisedframe ./ sumstabilisedframe;

stabilisedframe_withmeanval = stabilisedframe;
stabilisedframe_withmeanval(indiceswithnoimagedata) = mean(stabilisedframe(indiceswithimagedata));

stabilisedframe_withrandvals = stabilisedframe;
randpixelindices = floor(rand(length(indiceswithnoimagedata),1) * length(indiceswithimagedata)) + 1;
randpixelvalues = stabilisedframe(indiceswithimagedata(randpixelindices));
stabilisedframe_withrandvals(indiceswithnoimagedata) = randpixelvalues;

stabilisedframe_withzerovals = stabilisedframe;
stabilisedframe_withzerovals(indiceswithnoimagedata) = 0;

stabilisedframe_full = cat(3,stabilisedframe_withmeanval,stabilisedframe_withrandvals,...
    stabilisedframe_withzerovals);
stabilisedframe = stabilisedframe_full(imageborders(3):imageborders(4),imageborders(1):imageborders(2),:);