%KSP 02/25/2019
%This scripts analyze TSLO videos with markings on them, created with the
%help of shutter filters that go on and off. 
%first we get the avi files from the dir
% clear all;close all;
[avifile,avipath]=uigetfile('*.avi','Select the AVI file');
cd(avipath)
%%
videoinfo=VideoReader(avifile);
numFrames=videoinfo.FrameRate*videoinfo.Duration;
x_axis=linspace(0,videoinfo.Duration,numFrames);
strips=0;% Set parameter on whether strip based test needs to be used or not
num_strips=18;strip_height=512/num_strips;
locs_array=[];
if strips==0
    for frameIdx=1:numFrames
        frame=frame2im(read(videoinfo,frameIdx,'native'));
        framestrip=frame(305:333,:);
        frameMeanLum(frameIdx)=mean(framestrip,'all');
    end
    figure(2);plot(log10(frameMeanLum));hold on;xlabel('Frame Number');ylabel('Mean Pixel Luminance');
%   find the frame where the luminance drops using 1st differential of
%   mean lum
    frameMeanLumDiff=abs(diff(frameMeanLum));[pks,locs]=findpeaks(frameMeanLumDiff);
    figure(3);plot(frameMeanLumDiff);hold on;xlabel('Time (s)');ylabel('Mean Pixel Luminance');
    real_pks=pks(pks>0.8);
    for i=1:length(real_pks)
    locs_temp(i)=locs(pks==real_pks(i));
    end
%     real_locs=x_axis(locs_temp);timing=diff(real_locs);mean_time=mean(timing);
    LumDropFrameNum=locs(pks==max(pks));
    disp(locs_temp(1));
elseif strips==1
    lower_limit=1;upper_limit=strip_height;%initial values would change in the loop
    for subframeIdx=1:num_strips
        for frameIdx=1:numFrames
            frame=frame2im(read(videoinfo,frameIdx,'native'));
            framestrip=frame(lower_limit:upper_limit,:);
            frameMeanLum(frameIdx)=mean(framestrip,'all');
        end
        frameMeanLumDiff=abs(diff(frameMeanLum(1:50)));[pks,locs]=findpeaks(frameMeanLumDiff);
        locs_array(subframeIdx)=locs(pks==max(pks));
        if upper_limit<512
            lower_limit=upper_limit; %new lower limit
            upper_limit=upper_limit+strip_height;
        else
            lower_limit=1;upper_limit=strip_height;
        end
    end
end


