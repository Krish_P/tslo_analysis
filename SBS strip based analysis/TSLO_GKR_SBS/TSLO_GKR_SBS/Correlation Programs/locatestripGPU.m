% exercise the GPU
% find strip in frame uses a series of one d xcorrs to generate a matrix.
useGPU = 'Yes'
tic;
etimes = [];
searchheight = gpuArray(120);
stripheight = gpuArray(40);
stripwidth = gpuArray(512);
if upper(useGPU(1)) == 'Y'
    frame = gpuArray.randn(stripwidth);
    sumxcorrs = gpuArray.zeros(searchheight, stripwidth);
else
    frame = (randn(512));
    sumxcorrs = (zeros(searchheight, size(strip,2)));
end

strip = frame(10+(1:stripheight),:);
% figure(10);ph = plot(rand(1,512));
etimes = [etimes toc];
for sdx = 1:stripheight;
%     oneline = strip(sdx,:);
    etimes = [etimes toc];

    for jdx = 1:searchheight;
        onexcorr = fftshift(ifft(fft(strip(sdx,:)) .* ...
            conj(fft(frame(jdx + sdx,:)))));
        % set(ph,'ydata',onexcorr);drawnow;
        sumxcorrs(jdx,:) = sumxcorrs(jdx,:) + real(onexcorr);
    end
end
etimes = [etimes toc];
figure(1);mesh(sumxcorrs);
figure(2);plot(etimes)
figure(3);plot(diff(etimes))
