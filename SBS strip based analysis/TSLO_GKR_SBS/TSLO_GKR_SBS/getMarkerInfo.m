% This script runs through the set of video files outputs stabilized 
% SLO movies and eye movement files as CSV
% KSP 2020
%% directory setup
%set top level folder here
topLevelFolder='/home/coateslab/LabData/TSLOVideos/MainExperiment/S02/';
cd (topLevelFolder)
%% Main loop
g=gpuDevice(1); %KSP added this to ensure GPU device is made accessible 
                %to matlab again (following a dismount at the end of each run)

sub_folders=dir(topLevelFolder);
counter=0;

for k = 1 : length(sub_folders)
    folderName=sub_folders(k).name;
    if contains(folderName,'Session')
        folderPath=[topLevelFolder,'/',folderName];
        if exist(folderPath,'dir') %this ensures that the dir is valid
            cd(folderPath);
            listoffiles=dir(folderPath);
            if isempty(listoffiles)
                continue
            else
                parfor fileIdx=1:length(listoffiles)
                    waitbar(k/length(listoffiles));
                    counter=counter+1;
                    filename=listoffiles(fileIdx).name;
                    if endsWith(filename,"_stab.avi")
                        continue
                    elseif endsWith(filename,".avi")                            
                        %Also obtain the marker/stimulus timing
                        %info from the video and store it as a csv
                        outputMarkerFilename=strcat(filename(1:end-4),'_marker.csv');
                        [markerData]=IdentifyMarker(filename);
                        csvwrite(outputMarkerFilename,markerData);
                    end                   
                end
            end
        end
    end
end
% fclose(fileID);