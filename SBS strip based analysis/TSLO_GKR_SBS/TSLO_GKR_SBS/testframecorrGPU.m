imdim = 512;
n = 10000;sd = 4;
gpuFlag = 1
stripheight = 8; numstrips = 128;  verbosity = 0;

xs = rand(n,1)*imdim;
ys = rand(n,1)*imdim;
intensities = sign(randn(n,1));

refframe = blobs(imdim,xs,ys,sd,intensities);
refframe=  single(refframe - mean(refframe(:)));
xs = xs + 2 * sin(8*pi*ys/imdim);
ys = ys + 4 * (sin(4*pi*ys/imdim));
testframe = blobs(imdim,xs,ys,sd,intensities);
testframe = single(testframe - mean(testframe(:)));
figure;imagesc([refframe testframe]);colormap(gray(256));axis off;truesize

striplocs = (0:numstrips-1) *  size(testframe, 1) / numstrips;

coswindow = (.5 - .5 * cos(2* pi * (1:stripheight)'./stripheight) * ones(1, size(testframe,2)));

mask = zeros(size(testframe,1),size(testframe,2),numstrips,'single');
for stripdx = 1:numstrips;
    mask(min(size(mask,1),striplocs(stripdx)+[1:stripheight]),:,stripdx) = coswindow;
end

if gpuFlag == 1;
    refframe = gpuArray(refframe);
    testframe = gpuArray(testframe);
    mask = gpuArray(mask);
end
tic;
framecorrGPU;
% peakvals = framecorrGPU(refframe, testframe, mask, gpuFlag, verbosity);
toc
figure(200);plot(real(peakvals(1:numstrips,:)),'-o');