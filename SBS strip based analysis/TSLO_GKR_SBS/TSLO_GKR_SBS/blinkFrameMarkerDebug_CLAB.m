% Purpose: Can be used to identify markers that might have occured during
% blinks
%get video file
videoFilename=uigetfile('*.avi');
if contains(videoFilename,'meanrem')
    blinkFilename=strcat(videoFilename(1:end-12),'_blinkframes.mat');
else
    blinkFilename=strcat(videoFilename(1:end-4),'_blinkframes.mat');
end
%load dataframes and video
videoInfo=VideoReader(videoFilename);
blinkFrameStuct=load(blinkFilename);
blinkframes=blinkFrameStuct.blinkframes;

%loop through each blinkframe and plot mean luminance profiles
blinkIdx=1;
figure(1);
while 0<blinkIdx && blinkIdx<=length(blinkframesz)
    blinkFrame=blinkframes(blinkIdx);
    if contains(videoFilename,'meanrem')
        frame=(read(videoInfo,blinkFrame));
    else
        frame=frame2im(read(videoInfo,blinkFrame,'native'));
    end
    meanLumFrame=mean(frame,(2));
    plot(meanLumFrame(:,:,1));title(blinkFrame);
    % we then wait for key press 
    keyPressed=getkey();
    if keyPressed==28 % left arrowkey
        blinkIdx=blinkIdx-1;
        continue
    elseif keyPressed==29 %right arrowkey
        blinkIdx=blinkIdx+1;
        continue
    elseif keyPressed==27 %escape key
        close all; %closes all figures
        break
    else
        continue
    end
end