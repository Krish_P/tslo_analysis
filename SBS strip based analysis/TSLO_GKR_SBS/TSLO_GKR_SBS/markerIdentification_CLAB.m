%% Purpose:  This script helps identify markers from the video (excluding blinks)
% directory setup
%set top level folder here
VideoFolder='/home/coateslab/LabData/TSLO_Experiment/CrowdingStream/TSLO_videos/extra/RepeatTrials/S05/Session2/';
behavFolder='/home/coateslab/LabData/TSLO_Experiment/CrowdingStream/TSLO_behav/extra/RepeatTrials/S05/Session2/';
cd (VideoFolder)
numFailed=0;
%% If a particular session folder is given, it does not look for subfolders
if contains(VideoFolder,'S')
    no_sub_folder=true;
    sub_folders={VideoFolder};
else
    no_sub_folder=false;
    sub_folders=dir(VideoFolder);
end
counter=0;
for k = 1 : length(sub_folders)
    counter=counter+1;
    if no_sub_folder
        folderName=VideoFolder;
    else
        folderName=sub_folders(k).name;
    end
    if contains(folderName,'S')
        disp('Yes')
        if no_sub_folder
            folderPath=VideoFolder;
        else
            folderPath=[VideoFolder,'/',folderName];
        end
        if exist(folderPath,'dir') %this ensures that the dir is valid
            cd(folderPath);
            listoffiles=dir(folderPath);
            parfor fileIdx=1:length(listoffiles)
                filename=listoffiles(fileIdx).name;
                if endsWith(filename,'.avi')
                    if contains(filename,'stab')
                        continue
                    else
                        try
                            disp(filename)
                            videotoanalyse=[folderPath,'/', filename];
                            %obtain the marker/stimulus timing
                            %info from the video and store it as a csv
                            outputMarkerFilename=strcat(videotoanalyse(1:end-4),'_marker.csv');
                            % Get the actual number of stimuli from the
                            % behavioral data
                            fileCode=filename(1:strfind(filename,'_Video')-1);
                            behavFiles=dir(behavFolder);
                            for behavIdx=1:length(behavFiles)
                                behavFile=behavFiles(behavIdx).name;
                                if strfind(behavFile,fileCode);
                                    if endsWith(behavFile,'.csv');
                                        numStim=height(readtable(strcat(behavFolder,behavFile)));
                                    end
                                end
                            end
                            numMarker=0; % we would set an arbitary value to start the while loop
                            markerData=[];
                            markerThreshold=6;% Also set the marker threshold to be 6 initially
                            % We would keep running this process till we get
                            % the right number of stimulus marker events
                            numIter=1;
                            while numIter<8
                                [markerData]=IdentifyMarker(videotoanalyse,1,markerThreshold);
                                numMarker=length(markerData);
                                if numMarker==numStim
                                    break
                                elseif numMarker<numStim
                                    markerThreshold=markerThreshold-1;
                                elseif numMarker>numStim
                                    markerThreshold=markerThreshold+1;
                                end
                                numIter=numIter+1;
                            end
                            if numMarker~=numStim
                                numFailed=numFailed+1;
                                % Reset marker threshold to original value and
                                % re-run method
                                markerThreshold=6;
                                [markerData]=IdentifyMarker(videotoanalyse,1,markerThreshold);
                            end
                            csvwrite(outputMarkerFilename,markerData);
                        catch
                            continue
                        end
                    end
                else
                    continue
                end
            end
        end
    end
end
