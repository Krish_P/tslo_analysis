function [datafilename,referenceimage] = makereference(aviname,badframefilename,...
    frameincrement,searchaparamstruct)

rand('state',sum(100 * clock));
randn('state',sum(100 * clock));

if (nargin < 1) | isempty(aviname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stopping program');
        error('Type ''help makereference_priorref'' for usage');
    else
        aviname = strcat(avipath,avifilename);
        cd(avipath);
    end
else
    maxslashindex = 0;
    for charcounter = 1:length(aviname)
        testvariable = strcmp(aviname(charcounter),'\');
        if testvariable
            maxslashindex = charcounter;
        end
    end
    avifilename = aviname(maxslashindex + 1:end);
    avipath = aviname(1:maxslashindex);
    cd(avipath);
end

if (nargin < 2) | isempty(badframefilename)
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
    if fname == 0
        cd(currentdirectory);
        disp('Need reference data,stopping program');
        error('Type ''help makereference_priorref'' for usage');
    else
        badframefilename = strcat(pname,fname);
        toloadframenumbers = 1;
    end
end

if (nargin >= 2) & isstr(badframefilename)
    toloadframenumbers = 1;
    if ~exist(badframefilename,'file')
        warning('Second input string does not point to a valid mat file');
        [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
        if fname == 0
            cd(currentdirectory);
            disp('Need reference data,stopping program');
            error('Type ''help makereference_interactive'' for usage');
        else
            badframefilename = strcat(pname,fname);
        end
    end
end

if (nargin >= 2) & isnumeric(badframefilename)
    toloadframenumbers = 0;
end

if (nargin < 4) | isempty(frameincrement)
    prompt = {'Frame Number Increment'};
    name = 'Frame Input for Reference Program';
    numlines = 1;
    defaultanswer = {'5'};
    inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
    if isempty(inputanswer)
        disp('You have not supplied a frame number increment');
        warning('Using default value of 3');
        frameincrement = 3;
    else
        frameincrement = str2num(inputanswer{1});
    end
end

if nargin < 4 | isempty(searchaparamstruct) | ~isstruct(searchaparamstruct)
    prompt = {'Sample Rate (Hz)','Vertical Search Zone (Pixels)',...
        'Strip Height (Pixels)','Max Correlation Threshold','Bad Strip Correlation Threshold'};

    name = 'Search Parameters Input for Reference Program';
    numlines = 1;
    defaultanswer = {'360','75','13','0.7','0.4'};

    inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
    if isempty(inputanswer)
        inputanswer = {'360','75','13','0.7','0.4'};
    end

    searchaparamstruct = struct('samplerate',str2num(inputanswer{1}),...
        'vertsearchzone',str2num(inputanswer{2}),'stripheight',...
        str2num(inputanswer{3}),'maxcorelthreshold',str2num(inputanswer{4}),...
        'badstripthreshold',str2num(inputanswer{5}));
else
    namesofinputfields = fieldnames(searchaparamstruct);
    if sum(strcmp(namesofinputfields,'samplerate'))
        togetsamplerate = 0;
    else
        togetsamplerate = 1;
    end
    if sum(strcmp(namesofinputfields,'vertsearchzone'))
        togetvertsearchzone = 0;
    else
        togetvertsearchzone = 1;
    end
    if sum(strcmp(namesofinputfields,'stripheight'))
        togetstripheight = 0;
    else
        togetstripheight = 1;
    end
    if sum(strcmp(namesofinputfields,'maxcorelthreshold'))
        togetmaxcorrelthreshold = 0;
    else
        togetmaxcorrelthreshold = 1;
    end
    if sum(strcmp(namesofinputfields,'badstripthreshold'))
        togetbadstripthreshold = 0;
    else
        togetbadstripthreshold = 1;
    end

    if (togetsamplerate + togetvertsearchzone + togetstripheight + togetbadstripthreshold) > 0
        prompt = {};
        defaultanswer = {};
        if togetsamplerate
            prompt{end + 1} = 'Sample Rate (Hz)';
            defaultanswer{end + 1} = '360';
        end
        if togetvertsearchzone
            prompt{end + 1} = 'Vertical Search Zone (Pixels)';
            defaultanswer{end + 1} = '75';
        end
        if togetstripheight
            prompt{end + 1} = 'Strip Height (Pixels)';
            defaultanswer{end + 1} = '13';
        end
        if togetmaxcorrelthreshold
            prompt{end + 1} = 'Max Correlation Threshold';
            defaultanswer{end + 1} = '0.7';
        end
        if togetbadstripthreshold
            prompt{end + 1} = 'Bad Strip Correlation Threshold';
            defaultanswer{end + 1} = '0.4';
        end

        name = 'Search Parameter Input for Reference Program';
        numlines = 1;
        inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
        fieldcounter = 1;
        if togetsamplerate
            searchaparamstruct.samplerate = str2num(inputanswer{fieldcounter});
            fieldcounter = fieldcounter + 1;
        end
        if togetvertsearchzone
            searchaparamstruct.vertsearchzone = str2num(inputanswer{fieldcounter});
            fieldcounter = fieldcounter + 1;
        end
        if togetstripheight
            searchaparamstruct.stripheight = str2num(inputanswer{fieldcounter});
            fieldcounter = fieldcounter + 1;
        end
        if togetmaxcorrelthreshold
            searchaparamstruct.maxcorelthreshold = str2num(inputanswer{fieldcounter});
            fieldcounter = fieldcounter + 1;
        end
        if togetbadstripthreshold
            searchaparamstruct.badstripthreshold = str2num(inputanswer{fieldcounter});
        end
    end
end

fileinfo = aviinfo(aviname); % Get important info of the avifile
framerate = round(fileinfo.FramesPerSecond); % The framerate of the video
numbervideoframes = fileinfo.NumFrames;
aviwidth = fileinfo.Width; % The width of the video (in pixels)
aviheight = fileinfo.Height; % The height of the video (in pixels)
framenumbers = [1:numbervideoframes]';

if toloadframenumbers
    load(badframefilename,'goodframesforrefanalysis','videoname_check');
    if strcmp(videoname_check,avifilename) == 0
        disp('Different Video');
        warning('Bad Frame Information was obtained from different video');
    end

    framesforreference_indices = [1:frameincrement:length(goodframesforrefanalysis)];
    framesforreference = goodframesforrefanalysis(framesforreference_indices);
    framesforreference = framesforreference(:);
else
    framesforreference = badframefilename(:);
end

samplerate = searchaparamstruct.samplerate;
defaultsearchzone_vert_strips = searchaparamstruct.vertsearchzone;
stripheight = searchaparamstruct.stripheight;
maxcorreltreshold = searchaparamstruct.maxcorelthreshold;
badstripthreshold = searchaparamstruct.badstripthreshold;

thumbnailfactor = 10;
numlinesperfullframe = 525;

numstrips = round(samplerate / framerate);
stripseparation = fix(numlinesperfullframe / numstrips);

stripidx(1) = fix(stripseparation / 2); % The location of the first strip
if numstrips > 1
    for stripcounter = 2:numstrips
        stripidx(stripcounter) = stripidx(stripcounter - 1) + stripseparation;
    end
end

stripidx = stripidx(find(stripidx <= aviheight));
stripidx = stripidx(:);
numstrips = length(stripidx);

numframesforreference = length(framesforreference);
matchesthataregood = zeros(numframesforreference,1);
numframesforcurrentlevel = numframesforreference;
badmatchindices = [1:numframesforreference]';

frameshifts_thumbnails_preref = zeros(numframesforreference,2);
maxvals_thumbnails_preref = zeros(numframesforreference,1);

referenceframeindextouse = zeros(numframesforreference,1);

currentrefframeindex = 1;
referenceframenumber = framesforreference(currentrefframeindex);
referenceframe = double(frame2im(aviread(aviname,referenceframenumber)));
referenceframe_thumbnail = downsample(referenceframe,thumbnailfactor,thumbnailfactor);

indicesnotyetassigned = [1:numframesforreference]';
framesnotyetassigned = framesforreference(indicesnotyetassigned);
numframesnotyetassigned = length(framesnotyetassigned);

% analysisprog = waitbar(0,'Inital Thumbnails');
waitbarstring = ['Getting the Appropriate Reference Frame: Cycle 1'];
toexit = 0;
currentcycle = 1;
while ~toexit
%     waitbar(0,analysisprog,waitbarstring);
    tempmaxvals = zeros(numframesnotyetassigned,numframesnotyetassigned);
    for outerframecounter = 1:numframesnotyetassigned
        currentrefframenumber = framesnotyetassigned(outerframecounter);
        currentrefframe = double(frame2im(aviread(aviname,currentrefframenumber)));
        currentrefframe_thumbnail = downsample(currentrefframe,thumbnailfactor,thumbnailfactor);

        for innerframecounter = 1:numframesnotyetassigned
            currenttestframenumber = framesnotyetassigned(innerframecounter);
            currenttestframe = double(frame2im(aviread(aviname,currenttestframenumber)));
            currenttestframe_thumbnail = downsample(currenttestframe,thumbnailfactor,thumbnailfactor);

            [correlation xshift yshift maxval noise secondpeak] =...
                corr2d(currentrefframe_thumbnail,currenttestframe_thumbnail);

            tempmaxvals(outerframecounter,innerframecounter) = maxval;

            prog = (((outerframecounter - 1) * numframesnotyetassigned) +...
                innerframecounter) / (numframesnotyetassigned .^ 2);
%             waitbar(prog,analysisprog);
        end
    end
    tempsum = sum(tempmaxvals >= maxcorreltreshold,2);
    tempbestreferenceindex = min(find(tempsum == max(tempsum)));

    bestreferenceindex = indicesnotyetassigned(tempbestreferenceindex);
    tempindicesassignedincurrentcycle = find(tempmaxvals(tempbestreferenceindex,:) >= maxcorreltreshold);

    indicesassignedincurrentcycle = indicesnotyetassigned(tempindicesassignedincurrentcycle);
    indicesassignedincurrentcycle = indicesassignedincurrentcycle(:);
    referenceframeindextouse(unique([bestreferenceindex;indicesassignedincurrentcycle])) =...
        bestreferenceindex;

    indicesnotyetassigned = setdiff(indicesnotyetassigned,indicesassignedincurrentcycle);
    framesnotyetassigned = framesforreference(indicesnotyetassigned);
    numframesnotyetassigned = length(framesnotyetassigned);

    currentcycle = currentcycle + 1;
    waitbarstring = ['Getting the Appropriate Reference Frame: Cycle ',num2str(currentcycle)];
    if (isempty(tempindicesassignedincurrentcycle)) |...
            (length(tempindicesassignedincurrentcycle) == indicesnotyetassigned) |...
            isempty(indicesnotyetassigned)
        toexit = 1;
    end
end

tempreferenceframesindices = unique(referenceframeindextouse);
numreferenceframesindices = length(tempreferenceframesindices);
unusedframeindices = [];
usedframeindices = [];
for refcounter = 1:numreferenceframesindices
    currentrefframeindex = tempreferenceframesindices(refcounter);
    framesthatusecurrentrefframe = find(referenceframeindextouse == currentrefframeindex);
    numframesthatusecurrentrefframe = length(framesthatusecurrentrefframe);
    if numframesthatusecurrentrefframe < 2
        unusedframeindices = [unusedframeindices;framesthatusecurrentrefframe];
    else
        usedframeindices = [usedframeindices;framesthatusecurrentrefframe];
    end
end
usedframeindices = sort(usedframeindices);
unusedframeindices = sort(unusedframeindices);

framesforreference_prechoice = framesforreference;
referenceframeindextouse_prechoice = referenceframeindextouse;
referenceframetouse_prechoice = framesforreference(referenceframeindextouse);

referenceframetouse = framesforreference(referenceframeindextouse(usedframeindices));
framesnotusedforreference = framesforreference(unusedframeindices);

framesforreference = framesforreference(usedframeindices);
numframesforreference = length(framesforreference);

refframenumbers = unique(referenceframetouse);
numrefsegments = length(refframenumbers);

thumbnailfactor = 5;

frameshifts_thumbnails = zeros(numframesforreference,2);
maxvals_thumbnails = zeros(numframesforreference,1);
secondpeaks_thumbnails = zeros(numframesforreference,1);
noises_thumbnails = zeros(numframesforreference,1);
peakratios_thumbnails = zeros(numframesforreference,1);

frameshifts_strips_unwraped = zeros(numstrips,numframesforreference,2);
maxvals_strips_unwraped = zeros(numstrips,numframesforreference);
secondpeaks_strips_unwraped = zeros(numstrips,numframesforreference);
noises_strips_unwraped = zeros(numstrips,numframesforreference);
peakratios_strips_unwraped = zeros(numstrips,numframesforreference);

xorigin = floor(aviwidth / 2) + 1;
xsearchzone = aviwidth;
ysearchzone = defaultsearchzone_vert_strips;
badmatrixflags_ref = [];
badmatrixflags_test = [];

% waitbar(0,analysisprog,'Strip Analysis');
for framecounter = 1:numframesforreference
    testframenumber = framesforreference(framecounter);
    refframenumber = referenceframetouse(framecounter);

    if (testframenumber == refframenumber)
        prog = framecounter / numframesforreference;
%         waitbar(prog,analysisprog);
        continue
    end

    testframe = double(frame2im(aviread(aviname,testframenumber)));
    referenceframe = double(frame2im(aviread(aviname,refframenumber)));

    testframe_thumbnail = downsample(testframe,thumbnailfactor,thumbnailfactor);
    referenceframe_thumbnail = downsample(referenceframe,thumbnailfactor,thumbnailfactor);

    [correlation xshift yshift maxval noise secondpeak] =...
        corr2d(referenceframe_thumbnail,testframe_thumbnail);

    if noise == 0
        noise = 1;
    end

    peakratio = maxval;

    frameshifts_thumbnails(framecounter,:) = [xshift yshift] * thumbnailfactor;
    maxvals_thumbnails(framecounter) = maxval;
    secondpeaks_thumbnails(framecounter) = secondpeak;
    noises_thumbnails(framecounter) = noise;
    peakratios_thumbnails(framecounter) = peakratio;

    xmovement = round(frameshifts_thumbnails(framecounter,1));
    ymovement = round(frameshifts_thumbnails(framecounter,2));

    refxorigin = xorigin - xmovement;

    for stripcounter = 1:numstrips
        yorigin = stripidx(stripcounter);
        refyorigin = yorigin - ymovement;

        [refmatrix badmatrixflag] = getsubmatrix(referenceframe,refxorigin,refyorigin,...
            xsearchzone,ysearchzone,[1 aviwidth],[1 aviheight],0);
        if any(badmatrixflag)
            badmatrixflags_ref = [badmatrixflags_ref;framecounter,stripcounter,badmatrixflag];
        end

        [teststrip badmatrixflag] = getsubmatrix(testframe,xorigin,yorigin,...
            xsearchzone,stripheight,[1 aviwidth],[1 aviheight]);
        if any(badmatrixflag)
            badmatrixflags_test = [badmatrixflags_test;framecounter,stripcounter,badmatrixflag];
        end

        [correlation xshift yshift maxval noise secondpeak] =....
            findthestrip(refmatrix,teststrip);

        if noise == 0
            noise = 1;
        end

        peakratio = maxval;
        xpixelshift = xmovement + xshift;
        ypixelshift = ymovement + yshift;

        frameshifts_strips_unwraped(stripcounter,framecounter,:) = cat(3,xpixelshift,ypixelshift);
        maxvals_strips_unwraped(stripcounter,framecounter) = maxval;
        secondpeaks_strips_unwraped(stripcounter,framecounter) = secondpeak;
        noises_strips_unwraped(stripcounter,framecounter) = noise;
        peakratios_strips_unwraped(stripcounter,framecounter) = peakratio;
    end

    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

frameshifts_strips_spline_unwraped = zeros(size(frameshifts_strips_unwraped));
framestodrop = [];

interp_xaxis_full = [0:(numlinesperfullframe * numbervideoframes) - 1];
interp_xaxis_full = reshape(interp_xaxis_full,numlinesperfullframe,numbervideoframes);
interp_xaxis_full = interp_xaxis_full(stripidx,framesforreference);

% waitbar(0,analysisprog,'Interpolating Bad Shifts');
for segmentcounter = 1:numrefsegments
    indicesofcurrentsegment = find(referenceframetouse == refframenumbers(segmentcounter));
    numindicesofcurrentsegment = length(indicesofcurrentsegment);
    refframeindex = find(framesforreference(indicesofcurrentsegment) == refframenumbers(segmentcounter));

    peakkratiosofcurrentsegment = peakratios_strips_unwraped(:,indicesofcurrentsegment);
    peakkratiosofcurrentsegment(:,refframeindex) = 2 * badstripthreshold;
    peakkratiosofcurrentsegment = peakkratiosofcurrentsegment(:);
    goodstripindices = find(peakkratiosofcurrentsegment > badstripthreshold);

    if length(goodstripindices) == length(peakkratiosofcurrentsegment)
        shiftstoadd = frameshifts_strips_unwraped(:,indicesofcurrentsegment,:);
    else
        shiftstoadd = zeros(numstrips,numindicesofcurrentsegment,2);
        interp_xaxis = interp_xaxis_full(:,indicesofcurrentsegment);
        interp_xaxis = interp_xaxis(:);
        sample_xaxis = interp_xaxis(goodstripindices);

        for directioncounter = 1:2
            tempshifts = frameshifts_strips_unwraped(:,indicesofcurrentsegment,directioncounter);
            sample_yaxis = tempshifts(:);
            sample_yaxis = sample_yaxis(goodstripindices);

            interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'pchip','extrap');
            shiftstoadd(:,:,directioncounter) = reshape(interp_yaxis,numstrips,numindicesofcurrentsegment);
        end
    end

    frameshifts_strips_spline_unwraped(:,indicesofcurrentsegment,:) = shiftstoadd;

    prog = segmentcounter / numrefsegments;
%     waitbar(prog,analysisprog);
end

frameshifts_strips_unwraped_withfirstframe = frameshifts_strips_unwraped;
frameshifts_strips_spline_unwraped_withfirstframe = frameshifts_strips_spline_unwraped;
firstframeshifts = zeros(size(frameshifts_strips_spline_unwraped_withfirstframe));

% waitbar(0,analysisprog,'Getting First Frame Shifts')
for segmentcounter = 1:numrefsegments
    indicesofcurrentsegment = find(referenceframetouse == refframenumbers(segmentcounter));
    numindicesofcurrentsegment = length(indicesofcurrentsegment);
    refframeindex = find(framesforreference(indicesofcurrentsegment) == refframenumbers(segmentcounter));

    indicestoaverage = setdiff([1:numindicesofcurrentsegment],refframeindex);

    if length(indicesofcurrentsegment) == 1
        frameshifts_strips_unwraped(:,indicesofcurrentsegment,:) =...
            frameshifts_strips_unwraped_withfirstframe(:,indicesofcurrentsegment,:);
        frameshifts_strips_spline_unwraped(:,indicesofcurrentsegment,:) =...
            frameshifts_strips_spline_unwraped_withfirstframe(:,indicesofcurrentsegment,:);
    else
        shifttoaverage = frameshifts_strips_spline_unwraped_withfirstframe(:,indicesofcurrentsegment,:);
        shifttoaverage = shifttoaverage(:,indicestoaverage,:);

        meanshift = mean(shifttoaverage,1);
        shifttoaverage_zeromeanpos = shifttoaverage - repmat(meanshift,[numstrips 1 1]);

        meanstripshift = mean(shifttoaverage_zeromeanpos,2);
        shiftstosubtract = repmat(meanstripshift,[1 numindicesofcurrentsegment 1]);

        frameshifts_strips_unwraped(:,indicesofcurrentsegment,:) =...
            frameshifts_strips_unwraped_withfirstframe(:,indicesofcurrentsegment,:) -...
            shiftstosubtract;

        frameshifts_strips_spline_unwraped(:,indicesofcurrentsegment,:) =...
            frameshifts_strips_spline_unwraped_withfirstframe(:,indicesofcurrentsegment,:) -...
            shiftstosubtract;

        firstframeshifts(:,indicesofcurrentsegment,:) = shiftstosubtract;
    end
    prog = segmentcounter / numrefsegments;
%     waitbar(prog,analysisprog);
end


refframes = cell(numrefsegments,1);
segmentshifts = zeros(numrefsegments,2);
segmentmaxvals = zeros(numrefsegments,1);
segmentnoises = zeros(numrefsegments,1);
segmentsecondpeaks = zeros(numrefsegments,1);
segmentpeakratios = zeros(numrefsegments,1);

interp_xaxis_full = [0:(numlinesperfullframe * numbervideoframes) - 1];
interp_xaxis_full = reshape(interp_xaxis_full,numlinesperfullframe,numbervideoframes);
interp_xaxis_full = interp_xaxis_full(:,framesforreference);
sample_xaxis_full = interp_xaxis_full(stripidx,:);
interp_xaxis_full = interp_xaxis_full(1:aviheight,:);

% waitbar(0,analysisprog,'Making Individual Segment Stabilized Frames')
for segmentcounter = 1:numrefsegments
    indicesofcurrentsegment = find(referenceframetouse == refframenumbers(segmentcounter));
    numindicesofcurrentsegment = length(indicesofcurrentsegment);

    interp_xaxis = interp_xaxis_full(:,indicesofcurrentsegment);
    sample_xaxis = sample_xaxis_full(:,indicesofcurrentsegment);

    interp_xaxis = interp_xaxis(:);
    sample_xaxis = sample_xaxis(:);

    frameshiftsofcurrentsegment = zeros(numindicesofcurrentsegment * numstrips,2);
    emshiftforstabilization = zeros(aviheight,numindicesofcurrentsegment,2);

    for directioncounter = 1:2
        tempshifts = frameshifts_strips_spline_unwraped(:,indicesofcurrentsegment,directioncounter);
        tempshifts = tempshifts(:);

        frameshiftsofcurrentsegment(:,directioncounter) = tempshifts;

        interpedshifts = interp1(sample_xaxis,tempshifts,interp_xaxis,'pchip','extrap') * -1.0;
        emshiftforstabilization(:,:,directioncounter) = reshape(interpedshifts,aviheight,numindicesofcurrentsegment);
    end

    [sizeincrement,stabilizedsize,imageborders] = getstabilizedparams(frameshiftsofcurrentsegment,[aviwidth,aviheight]);

    referenceimage = zeros(stabilizedsize(2),stabilizedsize(1));
    sumreferenceimage = zeros(stabilizedsize(2),stabilizedsize(1));

    columnaddition = ceil((stabilizedsize(1) - aviwidth) / 2) + [0:aviwidth - 1];
    rowaddition = ceil((stabilizedsize(2) - aviheight) / 2);
    sumrow = ones(1,aviwidth);

    for framecounter = 1:numindicesofcurrentsegment
        currentframenumber = framesforreference(indicesofcurrentsegment(framecounter));
        frametoadd = double(frame2im(aviread(aviname,currentframenumber)));

        frameshifttouse = squeeze(emshiftforstabilization(:,framecounter,:));

        for rowcounter = 1:aviheight
            rowtoadd = frametoadd(rowcounter,:);
            shifttouse = round(frameshifttouse(rowcounter,:));

            targetcolumns = round(columnaddition + shifttouse(1));
            targetcolumns = max(targetcolumns,1);
            targetcolumns = min(targetcolumns,stabilizedsize(1));

            targetrow = round(rowaddition + shifttouse(2)) + rowcounter - 1;
            targetrow = max(targetrow,1);
            targetrow = min(targetrow,stabilizedsize(2));

            referenceimage(targetrow,targetcolumns) = ...
                referenceimage(targetrow,targetcolumns) + rowtoadd;
            sumreferenceimage(targetrow,targetcolumns) = ...
                sumreferenceimage(targetrow,targetcolumns) + sumrow;
        end
    end

    indiceswithimagedata = find(sumreferenceimage >= 1);
    indiceswithnoimagedata = find(sumreferenceimage < 1);

    sumreferenceimage(indiceswithnoimagedata) = 1;
    referenceimage = referenceimage ./ sumreferenceimage;

    randpixelindices = floor(rand(length(indiceswithnoimagedata),1) * length(indiceswithimagedata)) + 1;
    randpixelvalues = referenceimage(indiceswithimagedata(randpixelindices));

    referenceimage(indiceswithnoimagedata) = randpixelvalues;
    refframes{segmentcounter} = referenceimage;

    if segmentcounter > 1
        [correlation xshift yshift maxval noise secondpeak] =...
            corr2d(refframes{1},referenceimage);

        if noise ==0
            noise = 1;
        end

        peakratio = maxval;

        segmentshifts(segmentcounter,:) = [xshift yshift];
        segmentmaxvals(segmentcounter) = maxval;
        segmentnoises(segmentcounter) = noise;
        segmentsecondpeaks(segmentcounter) = secondpeak;
        segmentpeakratios(segmentcounter) = peakratio;
    end

    prog = segmentcounter  / numrefsegments;
%     waitbar(prog,analysisprog);
end

frameshifts_strips_unwraped_segments = frameshifts_strips_unwraped;
frameshifts_strips_spline_unwraped_segments = frameshifts_strips_spline_unwraped;

% waitbar(0,analysisprog,'Adding Segment Shifts to Frameshifts');
for framecounter = 1:numframesforreference
    segmenttouse = find(refframenumbers == referenceframetouse(framecounter));

    shiftstoadd = repmat(reshape(segmentshifts(segmenttouse,:),[1 1 2]),...
        [numstrips 1 1]);

    frameshifts_strips_unwraped(:,framecounter,:) =...
        frameshifts_strips_unwraped_segments(:,framecounter,:) +...
        shiftstoadd;

    frameshifts_strips_spline_unwraped(:,framecounter,:) =...
        frameshifts_strips_spline_unwraped_segments(:,framecounter,:) +...
        shiftstoadd;


    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

numsamples = numstrips * numframesforreference;
frameshifts_strips = zeros(numsamples,2);
frameshifts_strips_spline = zeros(numsamples,2);
frameshifts_strips_segments = zeros(numsamples,2);
frameshifts_strips_segments_withfirstframe = zeros(numsamples,2);

for directioncounter = 1:2
    temp = frameshifts_strips_unwraped(:,:,directioncounter);
    frameshifts_strips(:,directioncounter) = temp(:);

    temp = frameshifts_strips_spline_unwraped(:,:,directioncounter);
    frameshifts_strips_spline(:,directioncounter) = temp(:);

    temp = frameshifts_strips_unwraped_segments(:,:,directioncounter);
    frameshifts_strips_segments(:,directioncounter) = temp(:);

    temp = frameshifts_strips_unwraped_withfirstframe(:,:,directioncounter);
    frameshifts_strips_segments_withfirstframe = temp(:);
end

maxvals_strips = maxvals_strips_unwraped(:);
secondpeaks_strips = secondpeaks_strips_unwraped(:);
noises_strips = noises_strips_unwraped(:);
peakratios_strips = peakratios_strips_unwraped(:);

interp_xaxis = [0:(numlinesperfullframe * numbervideoframes) - 1];
interp_xaxis = reshape(interp_xaxis,numlinesperfullframe,numbervideoframes);
sample_xaxis = interp_xaxis(stripidx,framesforreference);
interp_xaxis = interp_xaxis(1:aviheight,framesforreference);

interp_xaxis = interp_xaxis(:);
sample_xaxis = sample_xaxis(:);

emshiftforstabilization = zeros(aviheight,numframesforreference,2);

for directioncounter = 1:2
    sample_yaxis = frameshifts_strips_spline(:,directioncounter);
    interp_yaxis = interp1(sample_xaxis,sample_yaxis,interp_xaxis,'pchip','extrap');
    emshiftforstabilization(:,:,directioncounter) = reshape(interp_yaxis,aviheight,numframesforreference) * -1.0;
end

[sizeincrement,stabilizedsize,imageborders] = getstabilizedparams(frameshifts_strips_spline,[aviwidth,aviheight]);

referenceimage = zeros(stabilizedsize(2),stabilizedsize(1));
sumreferenceimage = zeros(stabilizedsize(2),stabilizedsize(1));

columnaddition = ceil((stabilizedsize(1) - aviwidth) / 2) + [0:aviwidth - 1];
rowaddition = ceil((stabilizedsize(2) - aviheight) / 2);
sumrow = ones(1,aviwidth);

% waitbar(0,analysisprog,'Making the Reference Frame')
for framecounter = 1:numframesforreference
    currentframenumber = framesforreference(framecounter);
    frametoadd = double(frame2im(aviread(aviname,currentframenumber)));

    frameshifttouse = squeeze(emshiftforstabilization(:,framecounter,:));

    for rowcounter = 1:aviheight
        rowtoadd = frametoadd(rowcounter,:);
        shifttouse = round(frameshifttouse(rowcounter,:));

        targetcolumns = round(columnaddition + shifttouse(1));
        targetcolumns = max(targetcolumns,1);
        targetcolumns = min(targetcolumns,stabilizedsize(1));

        targetrow = round(rowaddition + shifttouse(2)) + rowcounter - 1;
        targetrow = max(targetrow,1);
        targetrow = min(targetrow,stabilizedsize(2));

        referenceimage(targetrow,targetcolumns) = ...
            referenceimage(targetrow,targetcolumns) + rowtoadd;
        sumreferenceimage(targetrow,targetcolumns) = ...
            sumreferenceimage(targetrow,targetcolumns) + sumrow;
    end
    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

% close(analysisprog);


indiceswithimagedata = find(sumreferenceimage >= 1);
indiceswithnoimagedata = find(sumreferenceimage < 1);

sumreferenceimage(indiceswithnoimagedata) = 1;
referenceimage = referenceimage ./ sumreferenceimage;

referenceimage_full_norand = referenceimage;

randpixelindices = floor(rand(length(indiceswithnoimagedata),1) * length(indiceswithimagedata)) + 1;
randpixelvalues = referenceimage(indiceswithimagedata(randpixelindices));

referenceimage(indiceswithnoimagedata) = randpixelvalues;
referenceimage_full = referenceimage;
referenceimage = referenceimage_full(imageborders(3):imageborders(4),imageborders(1):imageborders(2));
referenceimage_norand = referenceimage_full_norand(imageborders(3):imageborders(4),imageborders(1):imageborders(2));

timeaxis = [0:(numlinesperfullframe * numbervideoframes) - 1];
timeaxis = reshape(timeaxis,numlinesperfullframe,numbervideoframes);
timeaxis = timeaxis(stripidx,framesforreference);
timeaxis = timeaxis(:);

timeaxis_secs = timeaxis / (30 * numlinesperfullframe);
videoname_check = avifilename;

randstring = num2str(min(ceil(rand(1) * 10000),9999));
sampleratestring = num2str(samplerate);
fullstring = strcat('_refdata_',sampleratestring,'hz_',num2str(frameincrement),'_',randstring,'.mat');
datafilename = strcat(aviname(1:end - 4),fullstring);

save(datafilename,'referenceimage','referenceimage_full','referenceimage_full_norand',...
    'referenceimage_norand','frameshifts_strips','frameshifts_strips_spline',...
    'frameshifts_strips_segments','frameshifts_strips_segments_withfirstframe',...
    'frameshifts_thumbnails','maxvals_strips','maxvals_thumbnails','secondpeaks_strips',...
    'secondpeaks_thumbnails','noises_strips','noises_thumbnails','peakratios_strips',...
    'peakratios_thumbnails','refframes','segmentshifts','segmentmaxvals','segmentnoises',...
    'segmentsecondpeaks','segmentpeakratios','badmatrixflags_ref','badmatrixflags_test',...
    'framesforreference_prechoice','referenceframetouse_prechoice','framesnotusedforreference',...
    'framesforreference','referenceframetouse','samplerate','defaultsearchzone_vert_strips',...
    'stripheight','maxcorreltreshold','badstripthreshold','stripidx','timeaxis',...
    'timeaxis_secs','videoname_check','aviwidth','aviheight');