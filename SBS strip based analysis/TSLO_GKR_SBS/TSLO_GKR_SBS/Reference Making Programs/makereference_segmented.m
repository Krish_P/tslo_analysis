function [datafilename,referenceimage] = makereference_segmented(videoname,...
    badframefilename,searchparamstruct,verbose)

rand('state',sum(100 * clock));
randn('state',sum(100 * clock));

currentdirectory = pwd;

if (nargin < 1) | isempty(videoname)
    [videofilename videopath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if videofilename == 0
        disp('No video to analyse,stopping program');
        error('Type ''help makereference_segmented'' for usage');
    else
        videoname = strcat(videopath,videofilename);
        cd(videopath);
    end
else
    maxslashindex = 0;
    for charcounter = 1:length(videoname)
        testvariable = strcmp(videoname(charcounter),'\');
        if testvariable
            maxslashindex = charcounter;
        end
    end
    videofilename = videoname(maxslashindex + 1:end);
    videopath = videoname(1:maxslashindex);
    cd(videopath);
end

if (nargin < 2) | isempty(badframefilename)
    loadgoodframedata = 1;
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
    if fname == 0
        cd(currentdirectory);
        disp('Need reference data,stopping program');
        error('Type ''help makereference_segmented'' for usage');
    else
        badframefilename = strcat(pname,fname);
    end
end

if isstr(badframefilename)
    loadgoodframedata = 1;
    if ~exist(badframefilename,'file')
        warning('Second input string does not point to a valid mat file');
        [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
        if fname == 0
            cd(currentdirectory);
            disp('Need reference data,stopping program');
            error('Type ''help makereference_coarseframerate'' for usage');
        else
            badframefilename = strcat(pname,fname);
        end
    end
else
    loadgoodframedata = 0;
end

if nargin < 3 | isempty(searchparamstruct) | ~isstruct(searchparamstruct)
    prompt = {'Sample Rate (Hz)','Vertical Search Zone (Pixels)',...
        'Strip Height (Pixels)','Bad Strip Correlation Threshold',...
        'Minimum No. of Frame Per Segment','Number of Lines per Full Video Frame'};
    name = 'Input for Reference Program';
    numlines = 1;
    defaultanswer = {'180','75','11','0.6','5','525'};

    inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
    if isempty(inputanswer)
        disp('You have pressed cancel rather than input any values');
        warning('Using default values in fields in the searchparamstruct structure');
        inputanswer = {'180','75','11','0.6','5','525'};
    end

    togetsamplerate = 0;
    togetvertsearchzone = 0;
    togetstripheight = 0;
    togetbadstripthreshold = 0;
    togetminimumframespersegment = 0;
    togetnumlinesperfullframe = 0;

    searchparamstruct = struct('samplerate',str2num(inputanswer{1}),...
        'vertsearchzone',str2num(inputanswer{2}),'stripheight',...
        str2num(inputanswer{3}),'badstripthreshold',str2num(inputanswer{4}),...
        'minimumframespersegment',str2num(inputanswer{5}),...
        'numlinesperfullframe',str2num(inputanswer{6}));
else
    namesofinputfields = fieldnames(searchparamstruct);
    if sum(strcmp(namesofinputfields,'samplerate'))
        togetsamplerate = 0;
    else
        togetsamplerate = 1;
    end
    if sum(strcmp(namesofinputfields,'vertsearchzone'))
        togetvertsearchzone = 0;
    else
        togetvertsearchzone = 1;
    end
    if sum(strcmp(namesofinputfields,'stripheight'))
        togetstripheight = 0;
    else
        togetstripheight = 1;
    end
    if sum(strcmp(namesofinputfields,'badstripthreshold'))
        togetbadstripthreshold = 0;
    else
        togetbadstripthreshold = 1;
    end
    if sum(strcmp(namesofinputfields,'minimumframespersegment'))
        togetminimumframespersegment = 0;
    else
        togetminimumframespersegment = 1;
    end
    if sum(strcmp(namesofinputfields,'numlinesperfullframe'))
        togetnumlinesperfullframe = 0;
    else
        togetnumlinesperfullframe = 1;
    end
end

if (togetsamplerate + togetvertsearchzone + togetstripheight +...
        togetbadstripthreshold + togetminimumframespersegment + togetnumlinesperfullframe) > 0
    prompt = {};
    defaultanswer = {};
    if togetsamplerate
        prompt{end + 1} = 'Sample Rate (Hz)';
        defaultanswer{end + 1} = '180';
    end
    if togetvertsearchzone
        prompt{end + 1} = 'Vertical Search Zone (Pixels)';
        defaultanswer{end + 1} = '75';
    end
    if togetstripheight
        prompt{end + 1} = 'Strip Height (Pixels)';
        defaultanswer{end + 1} = '11';
    end
    if togetbadstripthreshold
        prompt{end + 1} = 'Bad Strip Correlation Threshold';
        defaultanswer{end + 1} = '0.7';
    end
    if togetminimumframespersegment
        prompt{end + 1} = 'Minimum No. of Frame Per Segment'
        defaultanswer{end + 1} = '0.7';
    end
    if togetnumlinesperfullframe
        prompt{end + 1} = 'Number of Lines per Full Video Frame';
        defaultanswer{end + 1} = '525';
    end

    name = 'Search Parameter Input for Reference Program';
    numlines = 1;
    inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
    fieldcounter = 1;
    if togetsamplerate
        searchparamstruct.samplerate = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetvertsearchzone
        searchparamstruct.vertsearchzone = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetstripheight
        searchparamstruct.stripheight = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetbadstripthreshold
        searchparamstruct.badstripthreshold = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetminimumframespersegment
        searchparamstruct.minimumframespersegment = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetnumlinesperfullframe
        searchparamstruct.numlinesperfullframe = str2num(inputanswer{fieldcounter});
    end
end

if (nargin < 4) | isempty(verbose)
    verbose = 0;
end

fileinfo = aviinfo(videoname); % Get important info of the avifile
videoframerate = round(fileinfo.FramesPerSecond); % The videoframerate of the video
numbervideoframes = fileinfo.NumFrames;
framewidth = fileinfo.Width; % The width of the video (in pixels)
frameheight = fileinfo.Height; % The height of the video (in pixels)
framenumbers = [1:numbervideoframes]';

samplerate = searchparamstruct.samplerate;
defaultsearchzone_vert = searchparamstruct.vertsearchzone;
defaultsearchzone_hori = round(2 * framewidth / 3);
stripheight = searchparamstruct.stripheight;
badstripthreshold = searchparamstruct.badstripthreshold;
minimumframespersegment = searchparamstruct.minimumframespersegment;
numlinesperfullframe = searchparamstruct.numlinesperfullframe;

if loadgoodframedata
    load(badframefilename,'videoname_check','goodframesegmentinfo')
    if exist('videoname_check','var')
        if isempty(videoname_check) | strcmp(videoname_check,videofilename) == 0
            disp('Problem with bad frame mat file');
            warning('Bad frame info was obtained from different video / No video info was in matlab data file');
        end
    else
        disp('Problem with bad frame file');
        warning('No video name was in bad frame datafile');
    end

    if exist('goodframesegmentinfo','var')
        if isempty(goodframesegmentinfo)
            disp('Problem with bad frame mat file');
            error('Segment info not present in the MATLAB data file');
        end
    else
        disp('Problem with bad frame mat file');
        error('Segment info not present in the MATLAB data file');
    end
else
    goodframesegmentinfo =  badframefilename;
end

if samplerate < (3 * videoframerate)
    warning('Too Low a sample sample, increasing to thrice the frame rate of video');
end
if samplerate > (videoframerate * numlinesperfullframe)
    newsampleratestring = [numstr(videoframerate * numlinesperfullframe),' Hz'];
    dispstring = ['Too High a sample rate, decreasing to ',newsampleratestring];
    warning(dispstring);
end
if rem(samplerate,videoframerate) ~= 0
    disp('Sample rate should be multiple of the video frame rate');
    warning('Reducing sample rate to previous multiple of frame rate');
end

if defaultsearchzone_vert > frameheight
    disp('Default Vertical Search Zone is too high');
    warning('Reducing to 10 pixels less than frame height');
    defaultsearchzone_vert = frameheight - 10;
end
if defaultsearchzone_vert < 10
    disp('Default Vertical Search Zone is too low');
    warning('Increasing to 10 pixels');
    defaultsearchzone_vert_strips = 10;
end

if rem(stripheight,1) ~= 0
    disp('Strip height cannot be a decimal');
    warning('Round down...');
    stripheight = floor(stripheight);
end
if stripheight >= defaultsearchzone_vert
    disp('Strip Height cannot be larger than vertical search zone');
    warning('Reducing strip height to one less than vertical search zone')
    stripheight = max(defaultsearchzone_vert_strips - 1,1);
end
if stripheight <= 0
    disp('Strip should have atleast one line');
    warning('Increasing strip height to 1');
    stripheight = 1;
end

if badstripthreshold >= 1
    disp('Bad Strip Threshold is too high')
    warning('Reducing to 0.99')
    badstripthreshold = 0.99;
end
if badstripthreshold <= 0
    disp('Bad Strip Threshold is too low')
    warning('Increasing to 0.01')
    badstripthreshold = 0.01;
end

if minimumframespersegment < 3
    disp('Minimum frames per segment is too low');
    warning('Increasing to 3');
    minimumframespersegment = 3;
end
if minimumframespersegment >15
    disp('Minimum frames per segment is too high');
    warning('Decreasing to 15');
    minimumframespersegment = 15;
end


cd(currentdirectory);

pixelsize_deg = [(2 / 480), (2 /512)];
maxvelthreshold = 8.5;
maxexcursionthreshold = 0.1;
thumbnail_factor = 5;
numstrips = round(samplerate / videoframerate);
stripseparation = round(numlinesperfullframe / numstrips);

stripidx(1) = round(stripseparation / 2); % The location of the first strip
if numstrips > 1
    for stripcounter = 2:numstrips
        stripidx(stripcounter) = stripidx(stripcounter - 1) + stripseparation;
    end
end

stripidx = stripidx(find(stripidx <= frameheight));
stripidx = stripidx(:);

numstrips = length(stripidx);

segmentstartframes = [];
segmentendframes = [];
framesforreference = [];
referencetouse = [];
segmentnumbers = [];

numframesingoodframesegments = goodframesegmentinfo(:,3);
goodsegments = find(numframesingoodframesegments >= minimumframespersegment);
currentsegmentnumber = 1;

for segmentcounter = 1:length(goodsegments)
    numframesincurrentsegment = goodframesegmentinfo(goodsegments(segmentcounter),3);
    firstframeinsegment = goodframesegmentinfo(goodsegments(segmentcounter),1);
    lastframeinsegment = goodframesegmentinfo(goodsegments(segmentcounter),2);

    switch numframesincurrentsegment <= (2 * minimumframespersegment);
        case 1
            framesincurrentsegment = [firstframeinsegment:lastframeinsegment]';
            segmentstartframes = [segmentstartframes;firstframeinsegment];
            segmentendframes = [segmentendframes;lastframeinsegment];
            framesforreference = [framesforreference;framesincurrentsegment];
            referencetouse = [referencetouse;repmat(firstframeinsegment,...
                (lastframeinsegment - firstframeinsegment + 1),1)];
            segmentnumbers = [segmentnumbers;zeros(length(framesincurrentsegment),1) + currentsegmentnumber];
            currentsegmentnumber = currentsegmentnumber + 1;
        case 0
            framesininitalpartofcurrentsegment = [firstframeinsegment:...
                firstframeinsegment + minimumframespersegment - 1]';
            framesinfinalpartofcurrentsegment = [lastframeinsegment - minimumframespersegment + 1:...
                lastframeinsegment]';

            segmentstartframes = [segmentstartframes;firstframeinsegment];
            segmentendframes = [segmentendframes;firstframeinsegment + minimumframespersegment - 1];
            framesforreference = [framesforreference;framesininitalpartofcurrentsegment];
            referencetouse = [referencetouse;repmat(firstframeinsegment,...
                length(framesininitalpartofcurrentsegment),1)];
            segmentnumbers = [segmentnumbers;...
                zeros(length(framesininitalpartofcurrentsegment),1) + currentsegmentnumber];
            currentsegmentnumber = currentsegmentnumber + 1;

            segmentstartframes = [segmentstartframes;lastframeinsegment - minimumframespersegment + 1];
            segmentendframes = [segmentendframes;lastframeinsegment];
            framesforreference = [framesforreference;framesinfinalpartofcurrentsegment];
            referencetouse = [referencetouse;repmat(lastframeinsegment - minimumframespersegment + 1,...
                length(framesinfinalpartofcurrentsegment),1)];
            segmentnumbers = [segmentnumbers;...
                zeros(length(framesinfinalpartofcurrentsegment),1) + currentsegmentnumber];
            currentsegmentnumber = currentsegmentnumber + 1;
    end
end

numframesforreference = length(framesforreference);
numsegments = max(segmentnumbers);

segmentframeshifts_thumbnails = zeros(numframesforreference,2);
segmentmaxvals_thumbnails = ones(numframesforreference,1);
segmentsecondpeaks_thumbnails = ones(numframesforreference,1);
segmentnoises_thumbnails = ones(numframesforreference,1);

indicestodrop = [];
prevrefframenumber = 0;

% analysisprog = waitbar(0,'Thumbnail Analysis');
for framecounter = 1:numframesforreference
    testframenumber = framesforreference(framecounter);
    refframenumber = referencetouse(framecounter);

    if testframenumber == refframenumber
        continue;
    end

    testframe = double(frame2im(aviread(videoname,testframenumber)));
    if prevrefframenumber ~= refframenumber
        refframe = double(frame2im(aviread(videoname,refframenumber)));
        prevrefframenumber = refframenumber;
    end

    testframe_thumbnail = downsample(testframe,thumbnail_factor,thumbnail_factor);
    refframe_thumbnail = downsample(refframe,thumbnail_factor,thumbnail_factor);

    [correlation shifts peaks_noise] = ...
        corr2d(refframe_thumbnail,testframe_thumbnail,1);

    if peaks_noise(3) == 0
        peaks_noise(3) = 1;
    end

    peakratio = peaks_noise(2) / peaks_noise(1);
    xpixelshift = shifts(1) * thumbnail_factor;
    ypixelshift = shifts(2) * thumbnail_factor;

    if peakratio > badstripthreshold
        [correlation tempshifts temppeaks_noise] = ...
            corr2d(refframe,testframe,1);

        if temppeaks_noise(3) == 0
            temppeaks_noise(3) = 1;
        end

        temppeakratio = temppeaks_noise(2) / temppeaks_noise(1);
        if temppeakratio > peakratio
            peakratio = temppeakratio;
            peaks_noise = temppeaks_noise;
            xpixelshift = tempshifts(1);
            ypixelshift = tempshifts(2);
        else
            indicestodrop = [indicestodrop;framecounter];
        end
    end

    segmentframeshifts_thumbnails(framecounter,:) = [xpixelshift ypixelshift];
    segmentmaxvals_thumbnails(framecounter) = peaks_noise(1);
    segmentsecondpeaks_thumbnails(framecounter) = peaks_noise(2);
    segmentnoises_thumbnails(framecounter) = peaks_noise(3);

    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

framesforreference_initial = framesforreference;
if ~isempty(indicestodrop)
    indicestokeep = setdiff([1:numframesforreference]',indicestodrop);

    framesforreference = framesforreference(indicestokeep);
    referencetouse = referencetouse(indicestokeep);
    segmentnumbers = segmentnumbers(indicestokeep);

    segmentframeshifts_thumbnails = segmentframeshifts_thumbnails(indicestokeep,:);
    segmentmaxvals_thumbnails = segmentmaxvals_thumbnails(indicestokeep);
    segmentsecondpeaks_thumbnails = segmentsecondpeaks(indicestokeep);
    segmentnoises_thumbnails = segmentnoises(indicestokeep);
end

numframesforreference = length(framesforreference);
segmentframeshifts_strips_unwraped = zeros(numstrips,numframesforreference,2);
segmentmaxvals_strips_unwraped = ones(numstrips,numframesforreference);
segmentsecondpeaks_strips_unwraped = ones(numstrips,numframesforreference);
segmentnoises_strips_unwraped = ones(numstrips,numframesforreference);

frame_xcentre = floor(framewidth / 2) + 1;
frame_ycentre = floor(frameheight / 2) + 1;

xorigin = frame_xcentre;

xsearchzone = defaultsearchzone_hori;
ysearchzone = defaultsearchzone_vert;

absthumbnailvelocities = abs([0,0;diff(segmentframeshifts_thumbnails,[],1)]);

prevrefframenumber = 0;
wasprevstripagoodmatch = 0;

% waitbar(0,analysisprog,'Higher Rate Analysis');
for framecounter = 1:numframesforreference
    currentframethumbnailvelocity = absthumbnailvelocities(framecounter,:);

    testframenumber = framesforreference(framecounter);
    refframenumber = referencetouse(framecounter);

    if testframenumber == refframenumber
        wasprevstripagoodmatch = 0;
        continue;
    end

    testframe = double(frame2im(aviread(videoname,testframenumber)));
    if prevrefframenumber ~= refframenumber
        refframe = double(frame2im(aviread(videoname,refframenumber)));
        prevrefframenumber = refframenumber;
    end

    if currentframethumbnailvelocity(1) > defaultsearchzone_hori
        framexsearchzonetouse = min(round(2.5 * currentframethumbnailvelocity(1)),...
            framewidth);
    else
        framexsearchzonetouse = xsearchzone;
    end

    if currentframethumbnailvelocity(2) > defaultsearchzone_vert
        frameysearchzonetouse = min(round(2.5 * currentframethumbnailvelocity(2)),...
            round(5 * referencesize_y / 6));
    else
        frameysearchzonetouse = ysearchzone;
    end

    for stripcounter = 1:numstrips
        yorigin = stripidx(stripcounter);
        
        if wasprevstripagoodmatch
            if stripcounter == 1
                stripindexofprevgoodmatch = numstrips;
                frameindexofprevgoodmatch = framecounter - 1;
            else
                stripindexofprevgoodmatch = stripcounter - 1;
                frameindexofprevgoodmatch = framecounter;
            end

            xmovement = round(segmentframeshifts_strips_unwraped(stripindexofprevgoodmatch,...
                frameindexofprevgoodmatch,1));
            ymovement = round(segmentframeshifts_strips_unwraped(stripindexofprevgoodmatch,...
                frameindexofprevgoodmatch,2));

            xsearchzonetouse = max(round(2 * framexsearchzonetouse / 3),round(framewidth / 2));
            ysearchzonetouse = max(round(2 * frameysearchzonetouse / 3),stripheight + 5);
        else
            xmovement = round(segmentframeshifts_thumbnails(framecounter,1));
            ymovement = round(segmentframeshifts_thumbnails(framecounter,2));

            xsearchzonetouse = framexsearchzonetouse;
            ysearchzonetouse = frameysearchzonetouse;
        end

        refxorigin = frame_xcentre - xmovement;
        refyorigin = yorigin - ymovement;

        referencematrix = getsubmatrix(refframe,refxorigin,refyorigin,xsearchzonetouse,...
            ysearchzonetouse,[1 framewidth], [1 frameheight],0,0);
        teststrip = getsubmatrix(testframe,xorigin,yorigin,xsearchzonetouse,...
            stripheight,[1 framewidth], [1 frameheight],0,1);

        [correlation shifts peaks_noise] =....
            findthestrip(referencematrix,teststrip);

        if peaks_noise(3) == 0
            peaks_noise(3) = 1;
        end

        if (peaks_noise(2) ./ peaks_noise(1)) <= badstripthreshold
            wasprevstripagoodmatch = 1;
        else
            wasprevstripagoodmatch = 0;
        end

        xpixelshift = xmovement + shifts(1);
        ypixelshift = ymovement + shifts(2);

        segmentframeshifts_strips_unwraped(stripcounter,framecounter,:) = cat(3,xpixelshift, ypixelshift);
        segmentmaxvals_strips_unwraped(stripcounter,framecounter) = peaks_noise(1);
        segmentsecondpeaks_strips_unwraped(stripcounter,framecounter) = peaks_noise(2);
        segmentnoises_strips_unwraped(stripcounter,framecounter) = peaks_noise(3);
    end
    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

segmentframeshifts_strips_unwraped_withave = segmentframeshifts_strips_unwraped;
segmentmaxvels = zeros(numsegments,1);
segmentmaxexcursions = zeros(numsegments,1);
segmentreferenceframes = cell(numsegments,1);

segmentsdroppedduetohighvel = [];
% waitbar(0,analysisprog,'Registering Individual Segments');
for segmentcounter = 1:numsegments
    indicesofsegmentdata = find(segmentnumbers == segmentcounter);
    numframesinsegment = length(indicesofsegmentdata);

    currentsegmentframenumbers = framesforreference(indicesofsegmentdata);
    currentsegmentframeshifts_withave = segmentframeshifts_strips_unwraped(:,indicesofsegmentdata,:);

    peakratios_unwraped = segmentsecondpeaks_strips_unwraped(:,indicesofsegmentdata) ./...
        segmentmaxvals_strips_unwraped(:,indicesofsegmentdata);
    peakratios = peakratios_unwraped(:);
    peakratios(1:numstrips) = 0;

    badstrips_initial = find(peakratios > badstripthreshold);
    numinitialbadstrips = length(badstrips_initial);
    badstrips = sort(unique(repmat([-2:2],numinitialbadstrips,1) +...
        repmat(badstrips_initial(:),1,5)),'ascend');
    badstrips = max(min(badstrips(:),length(peakratios)),1);
    goodstrips = setdiff([1:length(peakratios)],badstrips);

    frameshiftstouse_unwraped = zeros(size(currentsegmentframeshifts_withave));
    frameshiftstouse = zeros(length(peakratios),2);

    if length(goodstrips) < length(peakratios)
        interp_xaxis = [0:length(peakratios) - 1];
        sample_xaxis = interp_xaxis(goodstrips);

        for directioncounter = 1:2
            sample_yaxis = currentsegmentframeshifts_withave(:,:,directioncounter);
            sample_yaxis = sample_yaxis(:);

            interp_yaxis = interp1(sample_xaxis,sample_yaxis(goodstrips),...
                interp_xaxis,'linear','extrap');
            frameshiftstouse_unwraped(:,:,directioncounter) = reshape(interp_yaxis,numstrips,numframesinsegment);

            if max(badstrips) == length(peakratios)
                lastgoodsample = max(goodstrips(:));
                if directioncounter == 1
                    indicestochange = [lastgoodsample + 1:length(peakratios)];
                    idxtotakevalfrom = lastgoodsample;
                else
                    indicestochange = [length(peakratios) + lastgoodsample:...
                        prod(size(currentsegmentframeshifts_withave))];
                    idxtotakevalfrom = length(peakratios) + lastgoodsample - 1;
                end
                frameshiftstouse_unwraped(indicestochange) =...
                    frameshiftstouse_unwraped(idxtotakevalfrom);
            end
        end
    else
        frameshiftstouse_unwraped = currentsegmentframeshifts_withave;
    end


    tempshifts = frameshiftstouse_unwraped(:,2:end,:);
    meanintraframeshift = mean(tempshifts,1);
    tempshifts = tempshifts - repmat(meanintraframeshift,[numstrips 1 1]);
    firstframeshift = mean(tempshifts,2);

    tempshifts = frameshiftstouse_unwraped;
    frameshiftstouse_unwraped(:,1,:) = firstframeshift;
    frameshiftstouse_unwraped(:,2:end,:) = tempshifts(:,2:end,:) -...
        repmat(firstframeshift,[1 (numframesinsegment - 1) 1]);
    segmentframeshifts_strips_unwraped(:,indicesofsegmentdata,:) = frameshiftstouse_unwraped;

    for directioncounter = 1:2
        tempshifts = frameshiftstouse_unwraped(:,:,directioncounter);
        frameshiftstouse(:,directioncounter) = tempshifts(:);
    end

    timeaxis = [0:(numlinesperfullframe * numframesinsegment) - 1];
    timeaxis = reshape(timeaxis,numlinesperfullframe,numframesinsegment);
    timeaxis = timeaxis(stripidx,:);
    timeaxis = timeaxis(:);
    time_secs = timeaxis / (numlinesperfullframe * videoframerate);
    timediff_secs = diff(time_secs(:),[],1);
    timediff_secs = repmat([timediff_secs(1);timediff_secs],1,2);

    pixelsize_deg_currentseg = repmat(pixelsize_deg,size(frameshiftstouse,1),1);
    frameshifts_deg = frameshiftstouse .* pixelsize_deg_currentseg;
    velocityinseg = diff([0,0;frameshifts_deg],[],1) ./ timediff_secs;

    maxabsvelocityinseg = max(max(abs(velocityinseg),[],1),[],2);
    maxexcursionincurrentseg = max(max(frameshifts_deg,[],1) - min(frameshifts_deg,[],1),[],2);

    segmentmaxvels(segmentcounter) = maxabsvelocityinseg;
    segmentmaxexcursions(segmentcounter) = maxexcursionincurrentseg;

    if (maxabsvelocityinseg > maxvelthreshold) | (maxexcursionincurrentseg > maxexcursionthreshold)
        segmentsdroppedduetohighvel = [segmentsdroppedduetohighvel;segmentcounter];
        continue;
    end

    [stabilisedframe,stabilisedframe_full] = ...
        makestabilizedframe(videoname,currentsegmentframenumbers,frameshiftstouse,peakratios,...
        stripidx,badstripthreshold,2.5,0,0);

    segmentreferenceframes{segmentcounter} = stabilisedframe;

    prog = segmentcounter / numsegments;
%     waitbar(prog,analysisprog);
end

if ~isempty(segmentsdroppedduetohighvel)
    goodstitchablesegments = setdiff([1:numsegments]',segmentsdroppedduetohighvel);
else
    goodstitchablesegments = [1:numsegments]';
end

intersegmentshifts = zeros(numsegments,2);
intersegmentmaxvals = ones(numsegments,1);
intersegmentsecondpeaks = ones(numsegments,1);
intersegmentnoises = ones(numsegments,1);

tempreferenceimage = segmentreferenceframes{min(goodstitchablesegments)};
tempreferenceimage = tempreferenceimage(:,:,2);

currentsegmentnumber = 1;
segmentstostitch = intersect([2:numsegments]',goodstitchablesegments);
segmentsstitchedinthisloop = [];

toexit = 0;
while ~toexit
    if currentsegmentnumber == 1
%         waitbar(0,analysisprog,'Stitching Segments Together');
    end
    indexintomatrix = segmentstostitch(currentsegmentnumber);
    currenttestrefimage = segmentreferenceframes{indexintomatrix};
    currenttestrefimage = currenttestrefimage(:,:,2);

    [correlation shifts peaks_noise] = corr2d(tempreferenceimage,currenttestrefimage,1);

    peakratio = peaks_noise(2) / peaks_noise(1);

    if peakratio <= badstripthreshold
        currenttemprefsize = [size(tempreferenceimage,2),size(tempreferenceimage,1)];
        currenttestrefsize = [size(currenttestrefimage,2),size(currenttestrefimage,1)];

        indicestoputrefmatrix_h = [1:currenttemprefsize(1)];
        indicestoputrefmatrix_v = [1:currenttemprefsize(2)];

        indicestoputtestmatrix_h = (floor((currenttemprefsize(1) - currenttestrefsize(1)) / 2) -...
            round(shifts(1))) + [0:currenttestrefsize(1) - 1];
        if indicestoputtestmatrix_h(1) < 1
            diffinhoriindex = 1 - indicestoputtestmatrix_h(1);
            indicestoputtestmatrix_h = indicestoputtestmatrix_h + diffinhoriindex;
            indicestoputrefmatrix_h = indicestoputrefmatrix_h + diffinhoriindex;
        end
        newtemprefsize_h = max(indicestoputtestmatrix_h(end),indicestoputrefmatrix_h(end));

        indicestoputtestmatrix_v = (floor((currenttemprefsize(2) - currenttestrefsize(2)) / 2) -...
            round(shifts(2))) + [0:currenttestrefsize(2) - 1];
        if indicestoputtestmatrix_v(1) < 1
            diffinvertindex = 1 - indicestoputtestmatrix_v(1);
            indicestoputtestmatrix_v = indicestoputtestmatrix_v + diffinvertindex;
            indicestoputrefmatrix_v = indicestoputrefmatrix_v + diffinvertindex;
        end
        newtemprefsize_v = max(indicestoputtestmatrix_v(end),indicestoputrefmatrix_v(end));

        intersegmentshifts(indexintomatrix,:) = shifts;
        intersegmentmaxvals(indexintomatrix) = peaks_noise(1);
        intersegmentsecondpeaks(indexintomatrix) = peaks_noise(2);
        intersegmentnoises(indexintomatrix) = peaks_noise(3);

        segmentsstitchedinthisloop = [segmentsstitchedinthisloop;segmentstostitch(currentsegmentnumber)];

        newtempreferenceimage = zeros(newtemprefsize_v,newtemprefsize_h);
        refsummatrix = zeros(newtemprefsize_v,newtemprefsize_h);
        testsummatrix = zeros(newtemprefsize_v,newtemprefsize_h);

        newtempreferenceimage(indicestoputrefmatrix_v,indicestoputrefmatrix_h) = tempreferenceimage;
        pixelswithrefimagedata = ones(size(tempreferenceimage));
        pixelswithrefimagedata(tempreferenceimage == 0) = 0;
        refsummatrix(indicestoputrefmatrix_v,indicestoputrefmatrix_h) = pixelswithrefimagedata;

        newtempreferenceimage(indicestoputtestmatrix_v,indicestoputtestmatrix_h) =...
            newtempreferenceimage(indicestoputtestmatrix_v,indicestoputtestmatrix_h) + currenttestrefimage;
        pixelswithtestimagedata = ones(size(currenttestrefimage));
        pixelswithtestimagedata (currenttestrefimage == 0) = 0;
        testsummatrix(indicestoputtestmatrix_v,indicestoputtestmatrix_h) = pixelswithtestimagedata ;

        summatrix = refsummatrix + testsummatrix;
        pixelswithnoimagedata = find(summatrix == 0);
        summatrix(pixelswithnoimagedata) = 1;
        newtempreferenceimage = newtempreferenceimage ./ summatrix;

        tempreferenceimage = newtempreferenceimage;
    end

    currentsegmentnumber = currentsegmentnumber + 1;

    if currentsegmentnumber > length(segmentstostitch)
        currentsegmentnumber = 1;
        segmentstostitch = setdiff(segmentstostitch,segmentsstitchedinthisloop);
        if isempty(segmentstostitch) | isempty(segmentsstitchedinthisloop)
            segmentsnotstitched = segmentstostitch;
            toexit = 1;
            break;
        end

        segmentsstitchedinthisloop = [];
%         waitbar(0,analysisprog,'Temp');
    end

    prog = currentsegmentnumber ./ length(segmentstostitch);
%     waitbar(prog,analysisprog);
end

goodsegments = setdiff([1:numsegments]',segmentsnotstitched);

% close(analysisprog);

for directioncounter = 1:2
    tempshifts = segmentframeshifts_strips_unwraped(:,:,directioncounter);
    segmentframeshifts_strips(:,directioncounter) = tempshifts(:);

    tempshifts = segmentframeshifts_strips_unwraped_withave(:,:,directioncounter);
    segmentframeshifts_strips_withave(:,directioncounter) = tempshifts(:);
end

segmentmaxvals_strips = segmentmaxvals_strips_unwraped(:);
segmentsecondpeaks_strips = segmentsecondpeaks_strips_unwraped(:);
segmentnoises_strips = segmentnoises_strips_unwraped(:);

pixelswithnoimagedata = find(tempreferenceimage == 0);
pixelswithimagedata = find(tempreferenceimage >= 1);

referenceimage_meanvals = tempreferenceimage;
referenceimage_meanvals(pixelswithnoimagedata) = mean(referenceimage_meanvals(pixelswithimagedata));

referenceimage_zeroval = tempreferenceimage;

referenceimage_randvals = tempreferenceimage;
randindices = floor(rand(length(pixelswithnoimagedata),1) * (length(pixelswithimagedata) - 1)) + 1;
randpixelvals = tempreferenceimage(randindices);
referenceimage_randvals(pixelswithnoimagedata) = randpixelvals;

referenceimage = referenceimage_randvals;
referenceimagematrix = cat(3,referenceimage_meanvals,referenceimage_randvals,referenceimage_zeroval);

randstring = num2str(min(ceil(rand(1) * 10000),9999));
sampleratestring = num2str(samplerate);
fullstring = strcat('_segmented_',sampleratestring,'hz_',...
    num2str(minimumframespersegment),'_',randstring,'.mat');
datafilename = strcat(videoname(1:end - 4),fullstring);

analysedframes_initial = framesforreference_initial;
analysedframes = framesforreference;
videoname_check = videofilename;

save(datafilename,'referenceimage','referenceimagematrix',...
    'analysedframes_initial','analysedframes','videoname_check',...
    'goodsegments','segmentframeshifts_strips','segmentframeshifts_strips_withave',...
    'segmentmaxvals_strips','segmentsecondpeaks_strips','segmentnoises_strips',...
    'segmentframeshifts_thumbnails','segmentmaxvals_thumbnails',...
    'segmentsecondpeaks_thumbnails','segmentnoises_thumbnails','referencetouse',...
    'segmentnumbers','intersegmentshifts','intersegmentmaxvals',...
    'intersegmentsecondpeaks','intersegmentnoises','samplerate','defaultsearchzone_vert',...
    'stripheight','badstripthreshold','minimumframespersegment','numlinesperfullframe',...
    'segmentstartframes','segmentendframes','segmentreferenceframes','pixelsize_deg',...
    'maxvelthreshold','segmentmaxvels','maxexcursionthreshold','segmentmaxexcursions',...
    'segmentsdroppedduetohighvel');

if verbose
    mymap = repmat([0:255]' / 256,1,3);
    figure
    image(referenceimage);
    colormap(mymap);
    axis off;
    truesize
end

% for segcounter = 1:numsegments
%     tempimage = segmentreferenceframes{segcounter};
%     if isempty(tempimage)
%         segcounter
%         continue
%     end
%     mymap = repmat([0:255]' / 256,1,3);
%     figure(9);
%     image(tempimage(:,:,2));
%     colormap(mymap);
%     title(num2str(segcounter));
%     axis off;
%     truesize
%     drawnow;
%     pause(2);
% end