function makereference_interactive(aviname,badframefilename)

currentdirectory = pwd;
if (nargin < 1) | isempty(aviname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stopping program');
        error('Type ''help makereference_interactive'' for usage');
    else
        aviname = strcat(avipath,avifilename);
        cd(avipath);
    end
else
    maxslashindex = 0;
    for charcounter = 1:length(aviname)
        testvariable = strcmp(aviname(charcounter),'\');
        if testvariable
            maxslashindex = charcounter;
        end
    end
    avifilename = aviname(maxslashindex + 1:end);
    avipath = aviname(1:maxslashindex);
    cd(avipath);
end

if (nargin < 2) | isempty(badframefilename)
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
    if fname == 0
        cd(currentdirectory);
        disp('Need reference data,stopping program');
        error('Type ''help makereference_interactive'' for usage');
    else
        badframefilename = strcat(pname,fname);
    end
end

if (nargin >= 2)
    if ~exist(badframefilename,'file')
        warning('Second input string does not point to a valid mat file');
        [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
        if fname == 0
            cd(currentdirectory);
            disp('Need reference data,stopping program');
            error('Type ''help makereference_interactive'' for usage');
        else
            badframefilename = strcat(pname,fname);
        end
    end
end


fileinfo = aviinfo(aviname); % Get important info of the avifile
framerate = round(fileinfo.FramesPerSecond); % The framerate of the video
numbervideoframes = fileinfo.NumFrames;
aviwidth = fileinfo.Width; % The width of the video (in pixels)
aviheight = fileinfo.Height; % The height of the video (in pixels)
framenumbers = [1:numbervideoframes]';

if rem(aviwidth,2) == 0
    widthpad = -1;
else
    widthpad = 0;
end 
if rem(aviheight,2) == 0
    heightpad = -1;
else
    heightpad = 0;
end

load(badframefilename);
cd(currentdirectory);

if strcmp(videoname_check,avifilename) == 0
    filenamedlg = warndlg('Bad Frame data was obtained from different video','Different Video','modal');
    uiwait(filenamedlg);
end

segmentstartframes = goodframesegmentinfo(:,1);
segmentendframes = goodframesegmentinfo(:,2);
numframesineachgoodframesegment = goodframesegmentinfo(:,3);

framestoanalyse = goodframesforrefanalysis;
numframestoanalyse = length(goodframesforrefanalysis);
nonanalysedframes = setdiff([1:numframestoanalyse],framestoanalyse);
nonanalysedframes = nonanalysedframes(:);
numnonanalysedframes = length(nonanalysedframes);
numberofframespersegment = 5;

frame_xcentre = floor(aviwidth / 2) + 1;
frame_ycentre = floor(aviheight / 2) + 1;

xorigin_central = frame_xcentre;
yorigin_central = frame_ycentre;

xorigin_strips = frame_xcentre;

defaultsearchzone_hori_central = ceil((2 * aviwidth) / 3);
defaultsearchzone_vert_central = 99;

defaultsearchzone_hori_strips = aviwidth; % The horizontal search zone width in pixels.
defaultsearchzone_vert_strips = 99; % The vertical search zone width in pixels.

thumbnail_factor = 10;
samplerate = 480;
numlinesperfullframe = 525;

stripheight = 9; % The height of a single strip
numstrips = round(samplerate / framerate);
stripseparation = fix(numlinesperfullframe / numstrips);

stripidx(1) = fix(stripseparation / 2); % The location of the first strip
if numstrips > 1
    for stripcounter = 2:numstrips
        stripidx(stripcounter) = stripidx(stripcounter - 1) + stripseparation;
    end
end

stripidx = stripidx(find(stripidx <= aviheight));
numstrips = length(stripidx);

thumbnailestimate_vel = zeros(numbervideoframes,2);

centralestimate_vel = zeros(numbervideoframes,2);
centralestimate = zeros(numbervideoframes,2);
centralpeakdiffratios = ones(numbervideoframes,1);

% analysisprog = waitbar(0,'Thumbnail Analysis');

for framecounter = 2:numframestoanalyse
    testframeindex = goodframesforrefanalysis(framecounter);
    refframeindex = goodframesforrefanalysis(framecounter - 1);

    testframe = double(frame2im(aviread(aviname,testframeindex)));
    testframe_thumbnail = downsample(testframe,thumbnail_factor,thumbnail_factor);

    refframe = double(frame2im(aviread(aviname,refframeindex)));
    refframe_thumbnail = downsample(refframe,thumbnail_factor,thumbnail_factor);

    [correlation xshift yshift maxval noise secondpeak] = corr2d(refframe_thumbnail,testframe_thumbnail);

    thumbnailestimate_vel(testframeindex,:) = [xshift yshift] * thumbnail_factor;

%     waitbar(((framecounter - 1) / (numframestoanalyse - 1)),analysisprog);
end

xorigin = xorigin_central;
yorigin = yorigin_central;

defaultsearchzone_hori = defaultsearchzone_hori_central;
defaultsearchzone_vert = defaultsearchzone_vert_central;

% waitbar(0,analysisprog,'Central Strip Analysis');
    
for framecounter = 2:numframestoanalyse
    testframeindex = goodframesforrefanalysis(framecounter);
    refframeindex = goodframesforrefanalysis(framecounter - 1);
    
    xmovement = thumbnailestimate_vel(testframeindex,1);
    ymovement = thumbnailestimate_vel(testframeindex,2);
    
    refxorigin = round(xorigin - xmovement);
    refyorigin = round(yorigin - ymovement);
    
    xsearchzone = max(ceil(2.5 * abs(thumbnailestimate_vel(testframeindex,1))),defaultsearchzone_hori);
    if rem(xsearchzone,2) == 0
        xsearchzone = xsearchzone + 1;
    end
    if xsearchzone > aviwidth
        xsearchzone = aviwidth + widthpad;
    end
        
    ysearchzone = max(ceil(2.5 * abs(thumbnailestimate_vel(testframeindex,2))),defaultsearchzone_vert);
    if rem(ysearchzone,2) == 0
        ysearchzone = ysearchzone + 1;
    end
    if ysearchzone > aviheight
        ysearchzone = aviheight + heightpad;
    end
    
    testframe = double(frame2im(aviread(aviname,testframeindex)));
    refframe = double(frame2im(aviread(aviname,refframeindex)));
    
    referencematrix = getsubmatrix(refframe,refxorigin,refyorigin,xsearchzone,...
        ysearchzone,[1 aviwidth], [1 aviheight]);
    teststrip = getsubmatrix(testframe,xorigin,yorigin,xsearchzone,...
        stripheight,[1 aviwidth], [1 aviheight]);
    
    [correlation xshift yshift maxval noise secondpeak] = findthestrip(referencematrix,teststrip);
    
    if (maxval - secondpeak) / maxval <= 0.4
        xmovement = xmovement + xshift;
        ymovement = ymovement + yshift;
        
        refxorigin = round(xorigin - xmovement);
        refyorigin = round(yorigin - ymovement);
        
        referencematrix = getsubmatrix(refframe,refxorigin,refyorigin,xsearchzone,...
            ysearchzone,[1 aviwidth], [1 aviheight]);
        
        [correlation xshift yshift maxval noise secondpeak] = findthestrip(referencematrix,teststrip);  
        
        if (maxval - secondpeak) / maxval <= 0.4
            xmovement = 0;
            ymovement = 0;
            
            [correlation xshift yshift maxval noise secondpeak] = corr2d(refframe,testframe); 
        end
    end

    xpixelshift = xmovement + xshift;
    ypixelshift = ymovement + yshift;
    
    centralestimate_vel(testframeindex,:) = [xpixelshift, ypixelshift];
    centralpeakdiffratios(testframeindex) = (maxval - secondpeak) / maxval;
    
%     waitbar(((framecounter - 1) / (numframestoanalyse - 1)),analysisprog);
end


badcorrelframes = find(centralpeakdiffratios <= 0.4);
nonanalysedframes = sort([nonanalysedframes;badcorrelframes(:)]);
numnonanalysedframes = length(nonanalysedframes);
framestoanalyse = setdiff([1:numbervideoframes],nonanalysedframes);
framestoanalyse = framestoanalyse(:);
numframestoanalyse = length(framestoanalyse);

centralestimate = cumsum(centralestimate_vel,1);
centralestimate(nonanalysedframes,:) = 0;

framesinsegment = [];
numusefulframesineachsegment = [];

emtraceplot = figure;
plot(framenumbers,centralestimate(:,1),'b');
hold on;
plot(framenumbers,centralestimate(:,2),'g');
xlabel('Exit');

linehandles = get(gca,'Children');
baselinewidth = get(linehandles(1),'LineWidth');
increasedlinewidth = baselinewidth * 1;
limitsonyaxis = ylim;
minimumyval = limitsonyaxis(1);

tocontinue = 0;
while ~tocontinue
    [x,y] = ginput(1);
    
    if isempty(x) || isempty(y)
        continue;
    end
    
    if y < minimumyval;
        tocontinue = 1;
    end
    
    
    if (y > minimumyval) && (x >= 1) && (x <= numbervideoframes)
        if ~isempty(find(nonanalysedframes == x))
            badframechosen = warndlg('Frame Chosen is probably distorted, choose another','Bad Frame','modal');
        else
            x = round(x);
            segmentofselection = max(find(segmentstartframes < x));
            if numframesineachgoodframesegment(segmentofselection) <= numberofframespersegment
                startframe = segmentstartframes(segmentofselection);
                endframe = segmentendframes(segmentofselection);
            else
                startframe = max((x - round(numberofframespersegment / 2)),...
                    segmentstartframes(segmentofselection));
                endframe = min((startframe + numberofframespersegment - 1),...
                    segmentendframes(segmentofselection));
            end
            framestoadd = [startframe:endframe];
            framestoadd = setdiff(framestoadd,nonanalysedframes);
            framestoadd = max(framestoadd,1);
            framestoadd = min(framestoadd,numbervideoframes);
            framestoadd = sort(unique(framestoadd));
            if length(framestoadd) < 2
                continue
            end
            numusefulframesineachsegment = [numusefulframesineachsegment;length(framestoadd)];
            if length(framestoadd) < numberofframespersegment
                numberofzeros = numberofframespersegment - length(framestoadd);
                framestoadd = [framestoadd,zeros(1,numberofzeros)];
            end
            framesinsegment = [framesinsegment;framestoadd];
            plot([startframe:endframe],centralestimate([startframe:endframe],:),'r','LineWidth',increasedlinewidth);
        end
    end
end

close(emtraceplot);
[tempsort indices] = sort(framesinsegment(:,1));

framesinsegment = framesinsegment(indices,:);
numusefulframesineachsegment = numusefulframesineachsegment(indices);

numreferencesegments = size(framesinsegment,1);
numframestoanalyseforreference = numel(framesinsegment) - numreferencesegments;

refsegmentemtraces = cell(numreferencesegments,1);
refsegmentmaxvals = cell(numreferencesegments,1);
refsegmentpeakratios = cell(numreferencesegments,1);

framesalreadyanalysed = 1;
totalnumberofstripstoanalyse = (numframestoanalyseforreference * numstrips);

xorigin = xorigin_strips;

defaultsearchzone_hori = defaultsearchzone_hori_strips;
defaultsearchzone_vert = defaultsearchzone_vert_strips;

interp_xaxis_full = [0:(numstrips * numberofframespersegment) - 1];

% waitbar(0,analysisprog,'Segmented Analysis');

for segmentcounter = 1:numreferencesegments
    framesinsinglesegment = framesinsegment(segmentcounter,:);
    numusefulframesinsegment = numusefulframesineachsegment(segmentcounter);
    maxusefulindex = (numusefulframesinsegment * numstrips);
    
    singlesegmentshifts_withaver_uncorr = zeros(numstrips * numberofframespersegment,2);
    singlesegmentshifts_withaver = zeros(numstrips * numberofframespersegment,2);
    singlesegmentshifts = zeros(numstrips * numberofframespersegment,2);
    singlesegmentmaxvals = ones(numstrips * numberofframespersegment,1);
    singlesegmentpeakratios = ones(numstrips * numberofframespersegment,1);
    
    refframenumber = framesinsinglesegment(1);
    
    refframe = double(frame2im(aviread(aviname,refframenumber)));
    
    refframemovement_hori = centralestimate(refframenumber,1);
    refframemovement_vert = centralestimate(refframenumber,2);
    
    for framecounter = 2:numusefulframesinsegment
        testframenumber = framesinsinglesegment(framecounter);
        testframe = double(frame2im(aviread(aviname,testframenumber)));
        
        for stripcounter = 1:numstrips
            indexintomatrix = ((framecounter - 1) * numstrips) + stripcounter;
            if singlesegmentpeakratios(indexintomatrix - 1) > 0.4
                xmovement = singlesegmentshifts_withaver_uncorr(indexintomatrix - 1,1);
                ymovement = singlesegmentshifts_withaver_uncorr(indexintomatrix - 1,2);
                
                xsearchzone = round(defaultsearchzone_hori / 2);
                ysearchzone = round(defaultsearchzone_vert / 2);
            else
                xmovement = centralestimate(testframenumber,1) - refframemovement_hori;
                ymovement = centralestimate(testframenumber,2) - refframemovement_vert;
                
                xsearchzone = round(max(abs(xmovement),defaultsearchzone_hori));
                ysearchzone = round(max(abs(ymovement),defaultsearchzone_vert));
            end
            
            yorigin = stripidx(stripcounter);
        
            refxorigin = round(xorigin - xmovement);
            refyorigin = round(yorigin - ymovement);
            
            if rem(xsearchzone,2) == 0
                xsearchzone = xsearchzone + 1;
            end
            if xsearchzone >= aviwidth
                xsearchzone = aviwidth + widthpad;
            end
            
            if rem(ysearchzone,2) == 0
                ysearchzone = ysearchzone + 1;
            end
            if ysearchzone >= aviheight
                ysearchzone = aviheight + heightpad;
            end
            
            refframematrix = getsubmatrix(refframe, refxorigin, refyorigin,xsearchzone,...
                ysearchzone, [1 aviwidth], [1 aviheight]);
            testframestrip = getsubmatrix(testframe, xorigin, yorigin,xsearchzone,...
                stripheight, [1 aviwidth], [1 aviheight]);
            
            [correlation xshift yshift maxval noise secondpeak] = findthestrip(refframematrix,testframestrip);
            
            xpixelshift = xmovement + xshift;
            ypixelshift = ymovement + yshift;
            
            singlesegmentshifts_withaver_uncorr(indexintomatrix,:) = [xpixelshift,ypixelshift];
            singlesegmentmaxvals(indexintomatrix) = maxval;
            singlesegmentpeakratios(indexintomatrix) = (maxval - secondpeak) / maxval;
            
            prog = ((framesalreadyanalysed * numstrips) + stripcounter) / totalnumberofstripstoanalyse;
%             waitbar(prog,analysisprog);
        end
        framesalreadyanalysed = framesalreadyanalysed + 1;
    end

    usefuldatapoints = find(singlesegmentpeakratios(1:maxusefulindex) > 0.4);
    
    interp_xaxis = interp_xaxis_full(1:maxusefulindex);
    sample_xaxis = interp_xaxis(usefuldatapoints);
    
    interp_yaxis = [];
    for directioncounter = 1:2
        if size(usefuldatapoints,1) < size(singlesegmentshifts_withaver_uncorr,1)
            sample_yaxis = singlesegmentshifts_withaver_uncorr(1:maxusefulindex,directioncounter);
            sample_yaxis = sample_yaxis(usefuldatapoints);
            interp_yaxis = spline(sample_xaxis,sample_yaxis,interp_xaxis);
            singlesegmentshifts_withaver(1:maxusefulindex,directioncounter) = interp_yaxis;
        else
            singlesegmentshifts_withaver(:,directioncounter) = ...
                singlesegmentshifts_withaver_uncorr(:,directioncounter);
        end
        tempshifts = reshape(singlesegmentshifts_withaver(:,directioncounter),numstrips,numberofframespersegment);
        meanframemovement = mean(tempshifts,1);
        meanframemovement = repmat(meanframemovement,numstrips,1);
        tempshifts_zeromean = tempshifts - meanframemovement;
        firstframemovement = mean(tempshifts_zeromean(:,2:numusefulframesinsegment),2);
        firstframemovement = repmat(firstframemovement,1,numberofframespersegment);
        tempshift_averrem = tempshifts - firstframemovement;
        tempshift_averrem = tempshift_averrem(:);
        singlesegmentshifts(:,directioncounter) = tempshift_averrem;
    end

    refsegmentemtraces{segmentcounter} = singlesegmentshifts;
    refsegmentmaxvals{segmentcounter} = singlesegmentmaxvals;
    refsegmentpeakratios{segmentcounter} = singlesegmentpeakratios;
end


stabwidth = aviwidth * 2;
stabheight = aviwidth * 2;

segmentstabimages = cell(numreferencesegments,1);

interp_xaxis_full = reshape([0:(numlinesperfullframe * numberofframespersegment) - 1],...
    numlinesperfullframe,numberofframespersegment);
interp_xaxis_full = interp_xaxis_full(1:aviheight,:);
sample_xaxis_full = interp_xaxis_full(stripidx,:);

totalnumberofframes = sum(numusefulframesineachsegment);
framesalreadyanalysed = 0;

% waitbar(0,analysisprog,'Making Stabilzed Frames for each Segment')

for segmentcounter = 1:numreferencesegments
    framesinsinglesegment = framesinsegment(segmentcounter,:);
    numusefulframesinsegment = numusefulframesineachsegment(segmentcounter);
    maxusefulindex = (numusefulframesinsegment * numstrips);
    
    segmentreferenceimage = zeros(stabheight,stabwidth);
    segmentsumimage = zeros(stabheight,stabwidth);
    
    interp_xaxis = interp_xaxis_full(:,1:numusefulframesinsegment);
    interp_xaxis = interp_xaxis(:);
    sample_xaxis = sample_xaxis_full(:,1:numusefulframesinsegment);
    sample_xaxis = sample_xaxis(:);
    segmentemtrace_unwraped = [];
    interp_yaxis_unwraped = [];
    interp_yaxis = [];
    
    segmentemtrace_full = refsegmentemtraces{segmentcounter};
    for directioncounter = 1:2
        segmentemtrace_unwraped = reshape(segmentemtrace_full(:,directioncounter),numstrips,numberofframespersegment);
        segmentemtrace_unwraped = segmentemtrace_unwraped(:,1:numusefulframesinsegment);
        segmentemtrace = segmentemtrace_unwraped(:);
        interp_yaxis(:,directioncounter) = spline(sample_xaxis,segmentemtrace,interp_xaxis);
        interp_yaxis_unwraped(:,:,directioncounter) = reshape(interp_yaxis(:,directioncounter),aviheight,numusefulframesinsegment);
    end
    
    for framecounter = 1:numusefulframesinsegment
        emshiftstouse = interp_yaxis_unwraped(:,framecounter,:);
        emshiftstouse = squeeze(emshiftstouse) * -1.0;
        
        framenumber = framesinsinglesegment(framecounter);
        videoframe = double(frame2im(aviread(aviname,framenumber)));
        for rowcounter = 1:aviheight
            singlerow = videoframe(rowcounter,:);
            targetcols = round(emshiftstouse(rowcounter,1)) + ...
                ceil((stabwidth - aviwidth) / 2) + [0:aviwidth - 1];
            targetcols = max(targetcols,1);
            targetcols = min(targetcols,stabwidth);
            
            targetrow = round(emshiftstouse(rowcounter,2)) + ...
                ceil((stabheight - aviheight) / 2) + rowcounter - 1;
            targetrow = max(targetrow,1);
            targetrow = min(targetrow,stabheight);
            
            segmentreferenceimage(targetrow,targetcols) = ...
                segmentreferenceimage(targetrow,targetcols) + singlerow;
            segmentsumimage(targetrow,targetcols) = ...
                segmentsumimage(targetrow,targetcols) + ones(1,length(singlerow));
        end
        framesalreadyanalysed = framesalreadyanalysed + 1;
        prog = framesalreadyanalysed / totalnumberofframes;
%         waitbar(prog,analysisprog);
    end
    segmentsumimage(find(segmentsumimage == 0)) = 1;
    segmentreferenceimage = segmentreferenceimage ./ segmentsumimage;
    segmentreferenceimage(find(segmentreferenceimage == 0)) = ...
        mean(segmentreferenceimage(find(segmentreferenceimage ~= 0)));
    
    segmentstabimages{segmentcounter} = segmentreferenceimage;
end

firstreference = segmentstabimages{1};
firstreference = downsample(firstreference,thumbnail_factor,thumbnail_factor);
referencenumbers = zeros(numreferencesegments,1);
referencepeakratios = zeros(numreferencesegments,1);
referencenumbers(1) = 1;

% waitbar(0,analysisprog,'Correlating Individual Segment Reference Images (First Pass)');

for segmentcounter = 2:numreferencesegments
    testframe = segmentstabimages{segmentcounter};
    testframe = downsample(testframe,thumbnail_factor,thumbnail_factor);
    
    [correlation xshift yshift maxval noise secondpeak] = corr2d(firstreference,testframe);
    
    peakratio = (maxval - secondpeak) ./ maxval;
    referencepeakratios(segmentcounter) = peakratio;
    
    if peakratio > 0.4
        referencetouse(segmentcounter) = 1;
    end
    
%     waitbar(((segmentcounter - 1) / (numreferencesegments - 1)),analysisprog)
end

badreferences = find(referencepeakratios <= 0.4);

if ~isempty(badreferences)
    numbadreferences = length(badreferences);
    goodreferences = find(referencetouse(2:end) == 1) + 1;
    numgoodreferences = length(goodreferences);
    totalcorrelations = numgoodreferences * numbadreferences;
%     waitbar(0,analysisprog,'Correlating Individual Segment Reference Images (Second Pass)');
    for testcounter = 1:numbadreferences
        testframe = segmentstabimages{badreferences(testcounter)};
        testframe = downsample(testframe,thumbnail_factor,thumbnail_factor);
        
        tempratios = zeros(numgoodreferences,1);
        for refcounter = 1:numgoodreferences
            refframe = segmentstabimages{goodreferences(refcounter)};
            refframe = downsample(refframe,thumbnail_factor,thumbnail_factor);
            
            [correlation xshift yshift maxval noise secondpeak] = corr2d(refframe,testframe);
            
            tempratios(refcounter) = (maxval - secondpeak) ./ maxval;
            
            prog = (((testcounter - 1)* numgoodreferences) + refcounter) / totalcorrelations;
%             waitbar(prog,analysisprog);
        end
        
        maxpeakratio = max(tempratios);
        
        if maxpeakratio > referencepeakratios(badreferences(testcounter));
            maxpeakindex = find(tempratios == maxpeakratio);
            referencetouse(badreferences(testcounter)) = goodreferences(maxpeakindex);
        else
            referencetouse(badreferences(testcounter)) = 1;
        end
    end
end

% waitbar(0,analysisprog,'Correlating Individual Segments (Final Pass)');

segmentrefshifts_uncorr = zeros(numreferencesegments,2);
segmentrefshifts_peakdiffratios_unnormalized = zeros(numreferencesegments,1);

for segmentcounter = 2:numreferencesegments
    reftouse = referencetouse(segmentcounter);
    testtouse = segmentcounter;
    
    refframe = segmentstabimages{reftouse};
    testframe = segmentstabimages{testtouse};
    
    [correlation xshift yshift maxval noise secondpeak] = corr2d(refframe,testframe);
    
    segmentrefshifts_uncorr(segmentcounter,:) = [xshift yshift];
    segmentrefshifts_peakdiffratios_unnormalized(segmentcounter) = (maxval - secondpeak) / maxval;
    
%     waitbar(((segmentcounter - 1) / (numreferencesegments- 1)),analysisprog);
end


maxpeakratio = max(segmentrefshifts_peakdiffratios_unnormalized(2:end));
segmentrefshifts = segmentrefshifts_uncorr(referencetouse,:) + segmentrefshifts_uncorr;
segmentrefshifts_peakdiffratios_normalized = segmentrefshifts_peakdiffratios_unnormalized / maxpeakratio;
segmentrefshifts_peakdiffratios_normalized(1) = 1;

framestogointoreference = [];
referenceemtrace = [];
weightinreference = [];

for segmentcounter = 1:numreferencesegments
    numusefulframesinsegment = numusefulframesineachsegment(segmentcounter);
    tempframestore = framesinsegment(segmentcounter,:);
    tempframestore = tempframestore(1:numusefulframesinsegment);
    tempframestore = tempframestore(:);
    framestogointoreference = [framestogointoreference;tempframestore];
    if segmentcounter == 1
        weightinreference = ones(length(tempframestore),1);
    else
        weightinreference = [weightinreference;repmat(segmentrefshifts_peakdiffratios_normalized(segmentcounter),...
            length(tempframestore),1)];
    end
    singlesegmentemtrace_full = refsegmentemtraces{segmentcounter};
    singlesegmentemtrace = [];
    singlesegmentemtrace_unwraped = [];
    for directioncounter = 1:2
        singlesegmentemtrace_unwraped = reshape(singlesegmentemtrace_full(:,directioncounter),...
            numstrips,numberofframespersegment);
        singlesegmentemtrace_unwraped = singlesegmentemtrace_unwraped(:,1:numusefulframesinsegment);
        singlesegmentemtrace(:,directioncounter) = singlesegmentemtrace_unwraped(:);
    end
    
    numsamples = size(singlesegmentemtrace,1);
    singlesegmentemtrace = singlesegmentemtrace + repmat(segmentrefshifts(segmentcounter,:),numsamples,1);
    referenceemtrace = [referenceemtrace;singlesegmentemtrace];
end

numframesinreference = length(framestogointoreference);

reference_interp_xaxis = [0:(numlinesperfullframe * numframesinreference) - 1];
reference_interp_xaxis = reshape(reference_interp_xaxis,numlinesperfullframe,numframesinreference);
reference_interp_xaxis = reference_interp_xaxis(1:aviheight,:);
reference_sample_xaxis = reference_interp_xaxis(stripidx,:);

reference_interp_xaxis = reference_interp_xaxis(:);
reference_sample_xaxis = reference_sample_xaxis(:);

for directioncounter = 1:2
    reference_interp_yaxis(:,directioncounter) = spline(reference_sample_xaxis,referenceemtrace(:,directioncounter),...
        reference_interp_xaxis) * -1.0;
    reference_interp_yaxis_unwraped(:,:,directioncounter) = reshape(reference_interp_yaxis(:,directioncounter),...
        aviheight,numframesinreference);
end

maxhorishift = max(abs(referenceemtrace(:,1)));
maxvertshift = max(abs(referenceemtrace(:,2)));

if maxhorishift > maxvertshift
    sizeincrement = ceil((aviwidth + (2 * maxhorishift)) / aviwidth);
else
    sizeincrement = ceil((aviheight + (2 * maxvertshift)) / aviheight);
end

stabilizedwidth = aviwidth * sizeincrement;
stabilizedheight = aviheight * sizeincrement;

stabilizedcentre_x = floor(stabilizedwidth / 2) + 1;
stabilizedcentre_y = floor(stabilizedheight / 2) + 1;

leftmovements_indices = find(referenceemtrace(:,1) < 0);
if ~isempty(leftmovements_indices)
    leftmovements = abs(referenceemtrace(leftmovements_indices,1));
    maxleftmovement = max(leftmovements(:));
else
    maxleftmovement = 0;
end
    
rightmovements_indices = find(referenceemtrace(:,1) > 0);
if ~isempty(rightmovements_indices)
    rightmovements = referenceemtrace(rightmovements_indices,1);
    maxrightmovement = max(rightmovements(:));
else
    maxrightmovement = 0;
end

upmovements_indices = find(referenceemtrace(:,2) < 0);
if ~isempty(upmovements_indices)
    upmovements = abs(referenceemtrace(upmovements_indices,2));
    maxupmovement = max(upmovements(:));
else
    maxupmovement = 0;
end
    
downmovements_indices = find(referenceemtrace(:,2) > 0);
if ~isempty(downmovements_indices)
    downmovements = referenceemtrace(downmovements_indices,2);
    maxdownmovement = max(downmovements(:));
else
    maxdownmovement = 0;
end

leftborder = floor(floor(stabilizedwidth / 2) - floor(aviwidth / 2) - maxrightmovement) - 1;
rightborder = ceil(floor(stabilizedwidth / 2) + floor(aviwidth / 2) + maxleftmovement) + 1;

indices_horizontal = [leftborder:rightborder];
indices_horizontal = max(indices_horizontal,1);
indices_horizontal = min(indices_horizontal,stabilizedwidth);

topborder = floor(floor(stabilizedheight / 2) - floor(aviheight / 2) - maxdownmovement) - 1;
bottomborder = ceil(floor(stabilizedheight / 2) + floor(aviheight / 2) + maxupmovement) + 1;

indices_vertical = [topborder:bottomborder];
indices_vertical = max(indices_vertical,1);
indices_vertical = min(indices_vertical,stabilizedheight);

referenceimage = zeros(stabilizedheight,stabilizedwidth);
referencesumimage = zeros(stabilizedheight,stabilizedwidth);

% waitbar(0,analysisprog,'Making the big Reference Image')
for framecounter = 1:numframesinreference
    frametostabilize = double(frame2im(aviread(aviname,framestogointoreference(framecounter)))) * ...
        weightinreference(framecounter);
    emshiftstouse = squeeze(reference_interp_yaxis_unwraped(:,framecounter,:));
    for rowcounter = 1:aviheight
        rowtoadd = frametostabilize(rowcounter,:);
        
        targetcols = round(emshiftstouse(rowcounter,1)) + ...
            ceil((stabilizedwidth - aviwidth) / 2) + [0:aviwidth - 1];
        targetcols = max(targetcols,1);
        targetcols = min(targetcols,stabilizedwidth);
        
        targetrow = round(emshiftstouse(rowcounter,2)) + ...
            ceil((stabilizedheight - aviheight) / 2) + rowcounter - 1;
        targetrow = max(targetrow,1);
        targetrow = min(targetrow,stabilizedheight);
        
        referenceimage(targetrow,targetcols) = ...
            referenceimage(targetrow,targetcols) + rowtoadd;
        referencesumimage(targetrow,targetcols) = ...
            referencesumimage(targetrow,targetcols) + (zeros(1,length(rowtoadd)) + weightinreference(framecounter));
    end
    prog = framecounter / numframesinreference;
%     waitbar(prog,analysisprog);
end

% close (analysisprog);

indiceswherenoimagedatapresent = find(referencesumimage == 0);
indiceswhereimagedatapresent = find(referencesumimage > 0);

referencesumimage(indiceswherenoimagedatapresent) = 1;
referenceimage_full = referenceimage ./ referencesumimage;

referenceimage_full(indiceswherenoimagedatapresent) = ...
    mean(referenceimage_full(indiceswhereimagedatapresent));

referenceimage = referenceimage_full(topborder:bottomborder,leftborder:rightborder);

figure;
imagesc(referenceimage);
colormap(gray(256));
axis off;
truesize;

datafilename = strcat(aviname(1:end - 4),'_refdata.mat');

save(datafilename,'referenceimage','referenceimage_full',...
    'referenceemtrace','framestogointoreference','weightinreference');