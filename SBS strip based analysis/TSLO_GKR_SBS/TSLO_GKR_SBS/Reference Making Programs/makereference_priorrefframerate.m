function [datafilename,referenceimage] = makereference_priorrefframerate(aviname,...
    refframefilename,badframefilename,frameincrement,referencevarname)

rand('state',sum(100 * clock));
randn('state',sum(100 * clock));

currentdirectory = pwd;

if (nargin < 1) | isempty(aviname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stopping program');
        error('Type ''help makereference_priorrefframerate'' for usage');
    else
        aviname = strcat(avipath,avifilename);
        cd(avipath);
    end
else
    maxslashindex = 0;
    for charcounter = 1:length(aviname)
        testvariable = strcmp(aviname(charcounter),'\');
        if testvariable
            maxslashindex = charcounter;
        end
    end
    avifilename = aviname(maxslashindex + 1:end);
    avipath = aviname(1:maxslashindex);
    cd(avipath);
end

if (nargin < 2) | isempty(refframefilename)
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the reference image data');
    if fname == 0
        cd(currentdirectory);
        disp('Need reference data,stopping program');
        error('Type ''help makereference_priorrefframerate'' for usage');
    else
        refframefilename = strcat(pname,fname);
    end
end

if (nargin >= 2)
    if ~exist(refframefilename,'file')
        warning('Second input string does not point to a valid mat file');
        [fname pname] = uigetfile('*.mat','Please enter the matfile with the reference image data');
        if fname == 0
            cd(currentdirectory);
            disp('Need reference data,stopping program');
            error('Type ''help makereference_priorrefframerate'' for usage');
        else
            refframefilename = strcat(pname,fname);
        end
    end
end

if (nargin < 3) | isempty(badframefilename)
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
    if fname == 0
        cd(currentdirectory);
        disp('Need reference data,stopping program');
        error('Type ''help makereference_priorrefframerate'' for usage');
    else
        badframefilename = strcat(pname,fname);
    end
end

if (nargin >= 3)
    if ~exist(badframefilename,'file')
        warning('Second input string does not point to a valid mat file');
        [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
        if fname == 0
            cd(currentdirectory);
            disp('Need reference data,stopping program');
            error('Type ''help makereference_priorrefframerate'' for usage');
        else
            badframefilename = strcat(pname,fname);
        end
    end
end

fileinfo = aviinfo(aviname); % Get important info of the avifile
framerate = round(fileinfo.FramesPerSecond); % The framerate of the video
numbervideoframes = fileinfo.NumFrames;
aviwidth = fileinfo.Width; % The width of the video (in pixels)
aviheight = fileinfo.Height; % The height of the video (in pixels)
framenumbers = [1:numbervideoframes]';

if nargin < 4 | isempty(frameincrement)
    prompt = {'Enter the increment for the frame jump'};
    name = 'Input for makereference_priorrefframerate';
    numlines = 1;
    defaultanswer = {'10'};
    frameincrement = inputdlg(prompt,name,numlines,defaultanswer);
    if isempty(frameincrement)
        frameincrement = 10;
        disp('You have not entered a valid frame increment,using default');
        warning('type ''help makereference_priorrefframerate'' for usage');
    else
        frameincrement = str2num(frameincrement{1});
    end
end

if (frameincrement > (numbervideoframes / 5))
    warning('Frame increment is too high, reducing to 1/5 the number of frames in video');
    frameincrement = numbervideoframes / 5;
end

if (frameincrement < 5)
    warning('Frame increment is too low, increasing to 5');
    frameincrement = 5;
end

if nargin < 4 | isempty(referencevarname) | ~isstr(referencevarname)
    variablesinfile = who('-file',refframefilename);
    [selection,ok] = listdlg('PromptString','Which variable is the reference',...
        'SelectionMode','single','ListString',variablesinfile);

    if ok == 0
        warning('You have not made a valid selection, exiting...');
        return;
    end
    
    referencevarname = variablesinfile{selection};
end
load(refframefilename,referencevarname,'videoname_check');
if strcmp(videoname_check,avifilename) == 0
    disp('Different Video');
    warning('Reference Image was obtained from different video');
end
loadstring = strcat('referenceimage_prior =',referencevarname,';');
eval(loadstring);

load(badframefilename,'goodframesforrefanalysis','videoname_check');
if strcmp(videoname_check,avifilename) == 0
    disp('Different Video');
    warning('Bad Frame Information was obtained from different video');
end

cd(currentdirectory);



badstripthreshold = 0.65;
framesforreference_indices = [1:frameincrement:length(goodframesforrefanalysis)];
framesforreference = goodframesforrefanalysis(framesforreference_indices);
numframesforreference = length(framesforreference);

frameshifts = zeros(numframesforreference,2);
peakratios = zeros(numframesforreference,1);
maxvals = zeros(numframesforreference,1);
secondpeaks = zeros(numframesforreference,1);
noises = zeros(numframesforreference,1);

ironedframes = cell(numframesforreference,1);
framemotions = zeros(aviheight,numframesforreference);

% analysisprog = waitbar(0,'Getting Central Frameshifts');
for framecounter = 1:numframesforreference
    tesframenumber = framesforreference(framecounter);
    testframe = double(frame2im(aviread(aviname,tesframenumber)));
    
    
%     [testframe framemotion] = extractrowmotion(temptestframe);
    
    ironedframes{framecounter} = testframe;
%     framemotions(:,framecounter) = framemotion;
    
    [correlation shifts peaks_noise] = corr2d(referenceimage_prior,testframe);
    
    peakratio = peaks_noise(2) / peaks_noise(1);
    
    xpixelshift = shifts(1);
    ypixelshift = shifts(2);
    
    frameshifts(framecounter,:) = [xpixelshift ypixelshift];
    
    peakratios(framecounter) = peakratio;
    maxvals(framecounter) = peaks_noise(1);
    secondpeaks(framecounter) = peaks_noise(2);
    noises(framecounter) = peaks_noise(3);
    
    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

badmatchindices = find(peakratios >= badstripthreshold);
if (~isempty(badmatchindices)) & (length(badmatchindices) < numframesforreference)
    goodmatchindices = setdiff([1:numframesforreference],badmatchindices);
    framesforreference = framesforreference(goodmatchindices);
    ironedframes = ironedframes(goodmatchindices);
    framemotions = framemotions(:,goodmatchindices);
    frameshifts = frameshifts(goodmatchindices,:);
    peakratios = peakratios(goodmatchindices);
    maxvals = maxvals(goodmatchindices);
    secondpeaks = secondpeaks(goodmatchindices);
    noises = noises(goodmatchindices);
    numframesforreference = length(framesforreference);
end

[sizeincrement,stabilizedsize,imageborders] = getstabilizedparams(frameshifts,...
        [aviwidth,aviheight]);

referenceimage = zeros(stabilizedsize(2),stabilizedsize(1));
sumreferenceimage = zeros(stabilizedsize(2),stabilizedsize(1));

rowaddition = ceil((stabilizedsize(2) - aviheight) / 2) + [0:aviheight - 1];

% waitbar(0,analysisprog,'Making the Big Reference');
for framecounter = 1:numframesforreference
    frametoadd = ironedframes{framecounter};
    framewidth = size(frametoadd,2);
    
    columnaddition = ceil((stabilizedsize(1) - framewidth) / 2) + [0:framewidth - 1];
    summatrix = ones(aviheight,framewidth);

    shifttouse = frameshifts(framecounter,:) * -1.0;
    
    targetcolumns = round(columnaddition + shifttouse(1));
    targetcolumns = max(targetcolumns,1);
    targetcolumns = min(targetcolumns,stabilizedsize(1));
    
    targetrows = round(rowaddition + shifttouse(2));
    targetrows = max(targetrows,1);
    targetrows = min(targetrows,stabilizedsize(2));
    
    referenceimage(targetrows,targetcolumns) = ...
        referenceimage(targetrows,targetcolumns) + frametoadd;
    sumreferenceimage(targetrows,targetcolumns) = ...
        sumreferenceimage(targetrows,targetcolumns) + summatrix;
    
    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

% close(analysisprog);

indiceswithimagedata = find(sumreferenceimage > 0);
indiceswithnoimagedata = find(sumreferenceimage == 0);

randindices = floor(rand(length(indiceswithnoimagedata),1) * (length(indiceswithimagedata) - 1)) + 1;

sumreferenceimage(indiceswithnoimagedata) = 1;
referenceimage = referenceimage ./ sumreferenceimage;
randpixelvalues = referenceimage(indiceswithimagedata(randindices));

referenceimage_norandpixels_full = referenceimage;
referenceimage(indiceswithnoimagedata) = randpixelvalues;
referenceimage_full = referenceimage;

referenceimage = referenceimage_full(imageborders(3):imageborders(4),imageborders(1):imageborders(2));
referenceimage_norandpixels = referenceimage_norandpixels_full(imageborders(3):imageborders(4),imageborders(1):imageborders(2));

randstring = num2str(min(ceil(rand(1) * 10000),9999));
fullstring = strcat('_coarsepriorrefdata','_',randstring,'.mat');
datafilename = strcat(aviname(1:end - 4),fullstring);

stripidx = floor(aviheight / 2) + 1;
videoname_check = avifilename;

save(datafilename,'referenceimage','referenceimage_full','referenceimage_norandpixels',...
    'referenceimage_norandpixels_full','framesforreference','frameshifts','maxvals',...
    'secondpeaks','noises','peakratios','stripidx','videoname_check',...
    'referenceimage_prior','badstripthreshold','framemotions');