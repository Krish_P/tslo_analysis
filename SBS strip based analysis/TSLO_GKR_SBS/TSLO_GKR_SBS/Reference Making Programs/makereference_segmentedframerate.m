function [datafilename,referenceimage] = makereference_segmentedframerate(aviname,badframefilename,...
    minimumframespersegment,badmatchthreshold,verbose)
% makereference_segmentedframerate.m. This is a program that creates a
% reference frame for use by the analysevideo_priorref.m function. It does
% this by first dividing the video into segments that haverelativly low
% amounts of motion, and cross-correlating full frames within each segment
% with the first frame in each segment. After which it registers each
% segment's stabilized frame to obtain the shifts with each segment and
% also to stich the individual stabilized frames to create a larger
% reference frame.
%
% Usage: [datafilename,referenceimage] = 
%           makereference_segmentedframerate(aviname,badframefilename,
%           minimumframespersegment,badmatchthreshold,verbose)
% aviname                   - The name of the video from which a reference
%                             has to be made. This has to be a string. If
%                             this argument is empty or does not point to a 
%                             pre-existing video file, the program will
%                             ask the user to choose a video.
% badframefilename          - The file created by the getbadframes.m
%                             function. This argument can be a string or a
%                             array. If the user  passes a array it has to
%                             be of the same format as the goodframesegmentinfo
%                             array created by the getbadframes.m function,
%                             i.e. the first and second column are the frames
%                             that start and end each column respectively and
%                             the third column is the number of frames in each
%                             segment. Each row in this array represents a new
%                             segment, so if the array as 10 rows then there
%                             are 10 good segments in the video.
% minimumframespersegment   - The minimum number of frames per segment that
%                             will be used to create the reference frame.
%                             Segments with less frames with not be used in
%                             the analysis.
% badmatchthreshold         - The minimum value of the ratio between the
%                             second and first peaks in the
%                             cross-correlation function for the program to
%                             consider the motion match as "good" or valid.
% verbose                   - If this flag is set to 1 the program images
%                             the reference frame in a new figure window.
%                             If set to 0 the user gets no feedback.

rand('state',sum(100 * clock));
randn('state',sum(100 * clock));

currentdirectory = pwd;

if (nargin < 1) | isempty(aviname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stopping program');
        error('Type ''help makereference_segmentedframerate'' for usage');
    else
        aviname = strcat(avipath,avifilename);
        cd(avipath);
    end
else
    maxslashindex = 0;
    for charcounter = 1:length(aviname)
        testvariable = strcmp(aviname(charcounter),'\');
        if testvariable
            maxslashindex = charcounter;
        end
    end
    avifilename = aviname(maxslashindex + 1:end);
    avipath = aviname(1:maxslashindex);
    cd(avipath);
end

if (nargin < 2) | isempty(badframefilename)
    loadgoodframedata = 1;
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
    if fname == 0
        cd(currentdirectory);
        disp('Need bad file data,stopping program');
        error('Type ''help makereference_segmentedframerate'' for usage');
    else
        badframefilename = strcat(pname,fname);
    end
end

if isstr(badframefilename)
    loadgoodframedata = 1;
    if ~exist(badframefilename,'file')
        warning('Second input string does not point to a valid mat file');
        [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
        if fname == 0
            cd(currentdirectory);
            disp('Need reference data,stopping program');
            error('Type ''help makereference_segmentedframerate'' for usage');
        else
            badframefilename = strcat(pname,fname);
        end
    end
else
    loadgoodframedata = 0;
end

if nargin < 3 | isempty(minimumframespersegment)
    togetminimumframespersegment = 1;
else
    togetminimumframespersegment = 0;

end

if nargin < 4 | isempty(badmatchthreshold)
    togetbadmatchthreshold = 1;
else
    togetbadmatchthreshold = 0;
end

if nargin < 5 | isempty(verbose)
    verbose = 0;
end

if togetbadmatchthreshold | togetbadmatchthreshold
    name = 'Input for makereference_segmented.m';
    numlines = 1;
    prompt = {};
    defaultanswer = {};

    if togetminimumframespersegment
        prompt = {'Minimum No. of Frame Per Segment'};
        defaultanswer = {num2str(5)};
    end

    if togetbadmatchthreshold
        prompt{end + 1} = 'Bad Match Threshold';
        defaultanswer{end + 1} = num2str(0.6);
    end

    userresponse = inputdlg(prompt,name,numlines,defaultanswer);

    if isempty(userresponse)
        if togetminimumframespersegment
            warning('Using the default minimum frames per segment (5)');
            minimumframespersegment = 5;
        end

        if togetbadmatchthreshold
            warning('Using default bad match threshold of 0.6');
            badmatchthreshold = 0.6;
        end
    else
        index = 1;
        if togetminimumframespersegment
            minimumframespersegment = str2num(userresponse{index});
            index = index + 1;
        end

        if togetbadmatchthreshold
            badmatchthreshold = str2num(userresponse{index});
        end
    end
end

fileinfo = aviinfo(aviname); % Get important info of the avifile
framerate = round(fileinfo.FramesPerSecond); % The framerate of the video
numbervideoframes = fileinfo.NumFrames;
aviwidth = fileinfo.Width; % The width of the video (in pixels)
aviheight = fileinfo.Height; % The height of the video (in pixels)
framenumbers = [1:numbervideoframes]';

if loadgoodframedata
    load(badframefilename,'goodframesegmentinfo','videoname_check');
    if strcmp(videoname_check,avifilename) == 0
        beep
        filenamedlg = warndlg('Bad Frame data was obtained from different video','Different Video','modal');
        uiwait(filenamedlg);
    end
else
    goodframesegmentinfo = badframefilename;
end

if minimumframespersegment < 3
    disp('Too few frames per segment, increasing to 3');
    minimumframespersegment = 3;
end
if minimumframespersegment > 10
    disp('Too many frames per segment, decreasing to 10');
    minimumframespersegment = 10;
end

cd(currentdirectory);

segmentstartframes = [];
segmentendframes = [];
framesforreference = [];
referencetouse = [];
segmentnumbers = [];

numframesingoodframesegments = goodframesegmentinfo(:,3);
goodsegments = find(numframesingoodframesegments >= minimumframespersegment);
currentsegmentnumber = 1;

for segmentcounter = 1:length(goodsegments)
    numframesincurrentsegment = goodframesegmentinfo(goodsegments(segmentcounter),3);
    firstframeinsegment = goodframesegmentinfo(goodsegments(segmentcounter),1);
    lastframeinsegment = goodframesegmentinfo(goodsegments(segmentcounter),2);

    switch numframesincurrentsegment <= (2 * minimumframespersegment);
        case 1
            framesincurrentsegment = [firstframeinsegment:lastframeinsegment]';
            segmentstartframes = [segmentstartframes;firstframeinsegment];
            segmentendframes = [segmentendframes;lastframeinsegment];
            framesforreference = [framesforreference;framesincurrentsegment];
            referencetouse = [referencetouse;repmat(firstframeinsegment,...
                (lastframeinsegment - firstframeinsegment + 1),1)];
            segmentnumbers = [segmentnumbers;zeros(length(framesincurrentsegment),1) + currentsegmentnumber];
            currentsegmentnumber = currentsegmentnumber + 1;
        case 0
            framesininitalpartofcurrentsegment = [firstframeinsegment:...
                firstframeinsegment + minimumframespersegment - 1]';
            framesinfinalpartofcurrentsegment = [lastframeinsegment - minimumframespersegment + 1:...
                lastframeinsegment]';

            segmentstartframes = [segmentstartframes;firstframeinsegment];
            segmentendframes = [segmentendframes;firstframeinsegment + minimumframespersegment - 1];
            framesforreference = [framesforreference;framesininitalpartofcurrentsegment];
            referencetouse = [referencetouse;repmat(firstframeinsegment,...
                length(framesininitalpartofcurrentsegment),1)];
            segmentnumbers = [segmentnumbers;...
                zeros(length(framesininitalpartofcurrentsegment),1) + currentsegmentnumber];
            currentsegmentnumber = currentsegmentnumber + 1;

            segmentstartframes = [segmentstartframes;lastframeinsegment - minimumframespersegment + 1];
            segmentendframes = [segmentendframes;lastframeinsegment];
            framesforreference = [framesforreference;framesinfinalpartofcurrentsegment];
            referencetouse = [referencetouse;repmat(lastframeinsegment - minimumframespersegment + 1,...
                length(framesinfinalpartofcurrentsegment),1)];
            segmentnumbers = [segmentnumbers;...
                zeros(length(framesinfinalpartofcurrentsegment),1) + currentsegmentnumber];
            currentsegmentnumber = currentsegmentnumber + 1;
    end
end

numframesforreference = length(framesforreference);
numsegments = max(segmentnumbers);

segmentframeshifts = zeros(numframesforreference,2);
segmentmaxvals = ones(numframesforreference,1);
segmentsecondpeaks = ones(numframesforreference,1);
segmentnoises = ones(numframesforreference,1);

indicestodrop = [];
prevrefframenumber = 0;

% analysisprog = waitbar(0,'Getting Individual Segment Frameshifts');
for framecounter = 1:numframesforreference
    testframenumber = framesforreference(framecounter);
    refframenumber = referencetouse(framecounter);

    if testframenumber == refframenumber
        continue;
    end

    testframe = double(frame2im(aviread(aviname,testframenumber)));
    if prevrefframenumber ~= refframenumber
        refframe = double(frame2im(aviread(aviname,refframenumber)));
        prevrefframenumber = refframenumber;
    end

    [correlation shifts peaks_noise] = corr2d(refframe,testframe,1);

    peakratio = peaks_noise(2) / peaks_noise(1);

    if peakratio < badmatchthreshold
        segmentframeshifts(framecounter,:) = shifts;
        segmentmaxvals(framecounter) = peaks_noise(1);
        segmentsecondpeaks(framecounter) = peaks_noise(2);
        segmentnoises(framecounter) = peaks_noise(3);
    else
        indicestodrop = [indicestodrop;framecounter];
    end

    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

if ~isempty(indicestodrop)
    indicestokeep = setdiff([1:numframesforreference]',indicestodrop);

    framesforreference = framesforreference(indicestokeep);
    referencetouse = referencetouse(indicestokeep);
    segmentnumbers = segmentnumbers(indicestokeep);

    segmentframeshifts = segmentframeshifts(indicestokeep,:);
    segmentmaxvals = segmentmaxvals(indicestokeep);
    segmentsecondpeaks = segmentsecondpeaks(indicestokeep);
    segmentnoises = segmentnoises(indicestokeep);
end

numframesforreference = length(framesforreference);
segmentreferenceframes = cell(numsegments,1);

% waitbar(0,analysisprog,'Registering Individual Segments');
for segmentcounter = 1:numsegments
    indicesofsegmentdata = find(segmentnumbers == segmentcounter);
    numframesinsegment = length(indicesofsegmentdata);

    currentsegmentframenumbers = framesforreference(indicesofsegmentdata);
    currentsegmentframeshifts = segmentframeshifts(indicesofsegmentdata,:);

    [sizeincrement,stabilizedsize,imageborders] = getstabilizedparams(currentsegmentframeshifts,...
        [aviwidth,aviheight],2.5);

    rowaddition = ceil((stabilizedsize(2) - aviheight) / 2) + [0:aviheight - 1];
    columnaddition = ceil((stabilizedsize(1) - aviwidth) / 2) + [0:aviwidth - 1];
    summatrix = ones(aviheight,aviwidth);

    referenceimage = zeros(stabilizedsize(2),stabilizedsize(1));
    sumreferenceimage = zeros(stabilizedsize(2),stabilizedsize(1));

    for framecounter = 1:numframesinsegment
        framenumbertoadd = currentsegmentframenumbers(framecounter);
        frametoadd = double(frame2im(aviread(aviname,framenumbertoadd)));

        shifttouse = currentsegmentframeshifts(framecounter,:) * -1.0;

        targetcolumns = round(columnaddition + shifttouse(1));
        targetcolumns = max(targetcolumns,1);
        targetcolumns = min(targetcolumns,stabilizedsize(1));

        targetrows = round(rowaddition + shifttouse(2));
        targetrows = max(targetrows,1);
        targetrows = min(targetrows,stabilizedsize(2));

        referenceimage(targetrows,targetcolumns) = ...
            referenceimage(targetrows,targetcolumns) + frametoadd;
        sumreferenceimage(targetrows,targetcolumns) = ...
            sumreferenceimage(targetrows,targetcolumns) + summatrix;
    end

    indiceswithnoimagedata = find(sumreferenceimage == 0);

    sumreferenceimage(indiceswithnoimagedata) = 1;
    referenceimage = referenceimage ./ sumreferenceimage;
    referenceimage(indiceswithnoimagedata) = 0;
    referenceimage = referenceimage(imageborders(3):imageborders(4),imageborders(1):imageborders(2));

    segmentreferenceframes{segmentcounter} = referenceimage;

    prog = segmentcounter / numsegments;
%     waitbar(prog,analysisprog);
end

intersegmentshifts = zeros(numsegments,2);
intersegmentmaxvals = ones(numsegments,1);
intersegmentsecondpeaks = ones(numsegments,1);
intersegmentnoises = ones(numsegments,1);

tempreferenceimage = segmentreferenceframes{1};

currentsegmentnumber = 1;
segmentstostitch = [2:numsegments]';
segmentsstitchedinthisloop = [];

toexit = 0;
while ~toexit
    if currentsegmentnumber == 1
%         waitbar(0,analysisprog,'Stitching Segments Together');
    end
    indexintomatrix = segmentstostitch(currentsegmentnumber);
    currenttestrefimage = segmentreferenceframes{indexintomatrix};

    [correlation shifts peaks_noise] = corr2d(tempreferenceimage,currenttestrefimage,1);

    peakratio = peaks_noise(2) / peaks_noise(1);

    if peakratio < badmatchthreshold
        currenttemprefsize = [size(tempreferenceimage,2),size(tempreferenceimage,1)];
        currenttestrefsize = [size(currenttestrefimage,2),size(currenttestrefimage,1)];

        indicestoputrefmatrix_h = [1:currenttemprefsize(1)];
        indicestoputrefmatrix_v = [1:currenttemprefsize(2)];

        indicestoputtestmatrix_h = (floor((currenttemprefsize(1) - currenttestrefsize(1)) / 2) -...
            round(shifts(1))) + [0:currenttestrefsize(1) - 1];
        if indicestoputtestmatrix_h(1) < 1
            diffinhoriindex = 1 - indicestoputtestmatrix_h(1);
            indicestoputtestmatrix_h = indicestoputtestmatrix_h + diffinhoriindex;
            indicestoputrefmatrix_h = indicestoputrefmatrix_h + diffinhoriindex;
        end
        newtemprefsize_h = max(indicestoputtestmatrix_h(end),indicestoputrefmatrix_h(end));

        indicestoputtestmatrix_v = (floor((currenttemprefsize(2) - currenttestrefsize(2)) / 2) -...
            round(shifts(2))) + [0:currenttestrefsize(2) - 1];
        if indicestoputtestmatrix_v(1) < 1
            diffinvertindex = 1 - indicestoputtestmatrix_v(1);
            indicestoputtestmatrix_v = indicestoputtestmatrix_v + diffinvertindex;
            indicestoputrefmatrix_v = indicestoputrefmatrix_v + diffinvertindex;
        end
        newtemprefsize_v = max(indicestoputtestmatrix_v(end),indicestoputrefmatrix_v(end));

        intersegmentshifts(indexintomatrix,:) = shifts;
        intersegmentmaxvals(indexintomatrix) = peaks_noise(1);
        intersegmentsecondpeaks(indexintomatrix) = peaks_noise(2);
        intersegmentnoises(indexintomatrix) = peaks_noise(3);

        segmentsstitchedinthisloop = [segmentsstitchedinthisloop;segmentstostitch(currentsegmentnumber)];

        newtempreferenceimage = zeros(newtemprefsize_v,newtemprefsize_h);
        refsummatrix = zeros(newtemprefsize_v,newtemprefsize_h);
        testsummatrix = zeros(newtemprefsize_v,newtemprefsize_h);

        newtempreferenceimage(indicestoputrefmatrix_v,indicestoputrefmatrix_h) = tempreferenceimage;
        pixelswithrefimagedata = ones(size(tempreferenceimage));
        pixelswithrefimagedata(tempreferenceimage == 0) = 0;
        refsummatrix(indicestoputrefmatrix_v,indicestoputrefmatrix_h) = pixelswithrefimagedata;

        newtempreferenceimage(indicestoputtestmatrix_v,indicestoputtestmatrix_h) =...
            newtempreferenceimage(indicestoputtestmatrix_v,indicestoputtestmatrix_h) + currenttestrefimage;
        pixelswithtestimagedata = ones(size(currenttestrefimage));
        pixelswithtestimagedata (currenttestrefimage == 0) = 0;
        testsummatrix(indicestoputtestmatrix_v,indicestoputtestmatrix_h) = pixelswithtestimagedata ;

        summatrix = refsummatrix + testsummatrix;
        pixelswithnoimagedata = find(summatrix == 0);
        summatrix(pixelswithnoimagedata) = 1;
        newtempreferenceimage = newtempreferenceimage ./ summatrix;

        tempreferenceimage = newtempreferenceimage;
    end

    currentsegmentnumber = currentsegmentnumber + 1;

    if currentsegmentnumber > length(segmentstostitch)
        currentsegmentnumber = 1;
        segmentstostitch = setdiff(segmentstostitch,segmentsstitchedinthisloop);
        if isempty(segmentstostitch) | isempty(segmentsstitchedinthisloop)
            segmentsnotstitched = segmentstostitch;
            toexit = 1;
            break;
        end

        segmentsstitchedinthisloop = [];
%         waitbar(0,analysisprog,'Temp');
    end

    prog = currentsegmentnumber ./ length(segmentstostitch);
%     waitbar(prog,analysisprog);
end

goodsegments = setdiff([1:numsegments]',segmentsnotstitched);
framesforreference_initial = framesforreference;
framesforreference = [];

% waitbar(0,analysisprog,'Dropping Bad Matches from Arrays');
for segmentcounter = 1:numsegments
    if ~isempty(find(goodsegments == segmentcounter))
        indicestoget = find(segmentnumbers == segmentcounter);
        numindices = length(indicestoget);
        framesforreference = [framesforreference;framesforreference_initial(indicestoget)];
    end

    prog = segmentcounter / numsegments;
%     waitbar(prog,analysisprog);
end

% close(analysisprog);

pixelswithnoimagedata = find(tempreferenceimage == 0);
pixelswithimagedata = find(tempreferenceimage >= 1);

randindices = floor(rand(length(pixelswithnoimagedata),1) * (length(pixelswithimagedata) - 1)) + 1;

referenceimage_zeroval = tempreferenceimage;
referenceimage_meanval = tempreferenceimage;
referenceimage_meanval(pixelswithnoimagedata) = mean(referenceimage_meanval(pixelswithimagedata));
referenceimage = tempreferenceimage;
randpixelvals = tempreferenceimage(randindices);
referenceimage(pixelswithnoimagedata) = randpixelvals;

referenceimagematrix = cat(3,referenceimage_meanval,referenceimage,referenceimage_zeroval);

randstring = num2str(min(ceil(rand(1) * 10000),9999));
fullstring = strcat('_segframerate','_',randstring,'.mat');
datafilename = strcat(aviname(1:end - 4),fullstring);
videoname_check = avifilename;

save(datafilename,'referenceimage','referenceimagematrix',...
    'framesforreference_initial','framesforreference','videoname_check',...
    'goodsegments','segmentframeshifts','segmentmaxvals','segmentsecondpeaks',...
    'segmentnoises','referencetouse','segmentnumbers','intersegmentshifts',...
    'intersegmentmaxvals','intersegmentsecondpeaks','intersegmentnoises',...
    'minimumframespersegment','badmatchthreshold','segmentstartframes',...
    'segmentendframes','segmentreferenceframes');

if verbose
    mymap = repmat([0:255]' / 256,3,1);
    figure;
    image(referenceimage);
    colormap(mymap)
    axis off
    truesize
end