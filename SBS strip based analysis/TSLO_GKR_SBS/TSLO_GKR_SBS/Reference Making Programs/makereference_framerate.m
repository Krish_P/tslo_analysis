function [datafilename,referenceimage] = makereference_framerate(videoname,badframefilename,...
    frameincrement,badstripthreshold,verbosityarray)

rand('state',sum(100 * clock));
randn('state',sum(100 * clock));

currentdirectory = pwd;
screensize = get(0,'Screensize');

if (nargin < 1) || isempty(videoname)
    togetvideoname = 1;
else
    if ischar(videoname)
        if exist(videoname,'file')
            maxslashindex = 0;
            if ispc
                slash = '\';
            else
                slash = '/';
            end
            maxslashindex = find(slash == videoname, 1, 'last');
            if numel(maxslashindex) > 0
                videopath = videoname(1:maxslashindex);
                videofilename = videoname(maxslashindex + 1:end);
            else
                videopath = pwd;
                videofilename = videoname;
            end
            %             for charcounter = 1:length(videoname)
            %
            %                 testvariable = strcmp(videoname(charcounter),slash);
            %                 if testvariable
            %                     maxslashindex = charcounter;
            %                 end
            %             end
            videofilename = videoname(maxslashindex + 1:end);
            videopath = videoname(1:maxslashindex);
%             keyboard
            cd(videopath);
            togetvideoname = 0;
        else
            disp('Supplied video name does not point to a valid file');
            togetvideoname = 1;
        end
    else
        disp('Supplied video name must be a string');
        togetvideoname = 1;
    end
end

if togetvideoname
    [videofilename videopath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if videofilename == 0
        disp('No video to analyse,stopping program');
        error('Type ''help makereference_framerate'' for usage');
    else
        videoname = strcat(videopath,videofilename);
        cd(videopath);
    end
end

if (nargin < 2) || isempty(badframefilename)
    togetbadframefilename = 1;
else
    if ischar(badframefilename)
        if exist(badframefilename,'file')
            togetbadframefilename = 0;
            toloadframenumbers = 1;
        else
            disp('Third input string does point to a valid mat file');
            togetbadframefilename = 1;
        end
    else
        if isnumeric(badframefilename)
            toloadframenumbers = 0;
            togetbadframefilename = 0;
        else
            disp('Third input argument must be either a string or numeric array');
            togetbadframefilename = 1;
        end
    end
end

if togetbadframefilename
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
    if fname == 0
        cd(currentdirectory);
        disp('Need bad/good frame data,stopping program');
        error('Type ''help makereference_framerate'' for usage');
    else
        badframefilename = strcat(pname,fname);
        toloadframenumbers = 1;
    end
end

if (nargin < 3) || isempty(frameincrement)
    togetframeincrement = 1;
else
    togetframeincrement = 0;
end

if (nargin < 4) || isempty(badstripthreshold)
    togetbadstripthreshold = 1;
else
    togetbadstripthreshold = 0;
end

if (togetframeincrement + togetbadstripthreshold) > 0
    prompt = {};
    defaultanswer = {};
    name = 'Input for makereference_coarseframerate';
    numlines = 1;
    
    if togetframeincrement
        prompt{end + 1} = 'Frame Increment';
        defaultanswer{end + 1} = '10';
    end
    if togetbadstripthreshold
        prompt{end + 1} = 'Bad Strip Threshold';
        defaultanswer{end + 1} = '0.7';
    end
    
    inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
    
    if ~isempty(inputanswer)
        fieldcounter = 1;
        if togetframeincrement
            frameincrement = str2num(inputanswer{fieldcounter});
            fieldcounter = fieldcounter + 1;
        end
        if togetbadstripthreshold
            badstripthreshold = str2num(inputanswer{fieldcounter});
        end
    else
        if togetframeincrement
            disp('You pressed cancel instead of entering the frame increment');
            warning('Using default value of 10 for frame increment');
            frameincrement = 10;
        end
        if togetbadstripthreshold
            if ~togetframeincrement
                disp('You pressed cancel instead of entering the frame increment');
            end
            warning('Using default value of 0.7 for bad strip threshold');
            badstripthreshold = 0.7;
        end
    end
end

if (nargin < 5) || isempty(verbosityarray)
    correlverbose = 0;
    referenceverbose = 0;
    verbosityarray = [0; 0];
    toplotfeedbackfigs = 0;
    disp('No feedback will be provided!');
else
    correlverbose = 0;
    referenceverbose = 0;
    
    if length(verbosityarray) < 2
        disp('Verbose array is too small')
        warning('Unassigned verbose flags set to zero');
    end
    
    switch (length(verbosityarray))
        case 1
            correlverbose = verbosityarray(1);
        case 2
            correlverbose = verbosityarray(1);
            referenceverbose = verbosityarray(2);
    end
end

fileinfo_old = aviinfo(videoname); % Get important info of the avifile
fileinfo=VideoReader(videoname); %KSP: 02/07/2020
% numbervideoframes = fileinfo.NumFrames;
numbervideoframes=fileinfo.FrameRate*fileinfo.Duration;%KSP: 02/07/2020
framewidth = fileinfo.Width; % The width of the video (in pixels)
frameheight = fileinfo.Height; % The height of the video (in pixels)
videotype = fileinfo_old.ImageType;

if strcmp(videotype,'truecolor')
    disp('Video being analyssed is a truecolor video, this program can analyse only 8 bit videos!!');
    warning('Using only the first layer of the video during analyses');
    istruecolor = 1;
else
    istruecolor = 0;
end

if (frameincrement > (numbervideoframes / 5))
    warning('Frame increment is too high, reducing to 1/5 the number of frames in video');
    frameincrement = round(numbervideoframes / 5);
end
if (frameincrement < 5)
    warning('Frame increment is too low, increasing to 5');
    frameincrement = 5;
end

if badstripthreshold >= 1
    disp('Bad Strip Threshold is too high')
    warning('Reducing to 0.99')
    badstripthreshold = 0.99;
end
if badstripthreshold <= 0
    disp('Bad Strip Threshold is too low')
    warning('Increasing to 0.01')
    badstripthreshold = 0.01;
end

if toloadframenumbers
    variablesinfile = who('-file',badframefilename);
    doesfilehavegoodframeinfo = sum(strcmp(variablesinfile,'goodframesforrefanalysis'));
    doesfilehavevideocheckname = sum(strcmp(variablesinfile,'videoname_check'));
    if doesfilehavegoodframeinfo
        load(badframefilename,'goodframesforrefanalysis');
        if doesfilehavevideocheckname
            load(badframefilename,'videoname_check');
        else
            disp('Problem with bad frame file');
            warning('No video name was in bad frame datafile');
        end
    else
        disp('Problem with bad frame file');
        error('MATLAB data file does not have any good frame info');
    end
    
    if (exist('videoname_check','var')) && isempty(videoname_check) ||...
            (strcmp(videoname_check,videofilename) == 0)
        disp('Problem with video name in bad frame MAT file')
        warning('Bad frame info was obtained from different video / Video info was in matlab data file is empty');
    end
    if isempty(goodframesforrefanalysis)
        disp('Problem with good frame info');
        error('MATLAB data file does not have any good frame info');
    end
    
    framesforreference_indices = [1:frameincrement:length(goodframesforrefanalysis)];
    framesforreference = goodframesforrefanalysis(framesforreference_indices);
else
    framesforreference = badframefilename(:);
end


cd(currentdirectory);

stripidx = floor(frameheight / 2) + 1;

numframesforreference = length(framesforreference);
firstframereferencenumber = framesforreference(1);
% referenceimage = double(frame2im(aviread(videoname,firstframereferencenumber)));
try
    referenceimage = double(frame2im(read(fileinfo,firstframereferencenumber,'native')));
catch
    referenceimage = double((read(fileinfo,firstframereferencenumber,'native')));
end
if istruecolor
    referenceimage = referenceimage(:,:,1);
end

matchesthataregood = zeros(numframesforreference,1);
numframesforcurrentlevel = numframesforreference;
badmatches = [1:numframesforreference]';
toexit = 0;

matchesthataregood(1) = 1;

frameshifts = zeros(numframesforreference,2);
peakratios = zeros(numframesforreference,1);
maxvals = zeros(numframesforreference,1);
secondpeaks = zeros(numframesforreference,1);
noises = zeros(numframesforreference,1);

if any(verbosityarray)
    figurewidth = round((screensize(3) - 50) / 3);
    figureheight = round(screensize(4) / 3);
    
    if correlverbose
        correlfig = figure;
        correlaxis = axes;
        newposition = [0,(screensize(4) - figureheight + 1),...
            figurewidth, figureheight];
        set(correlfig,'Position',newposition,'Toolbar','none','Name','Cross-Correlation');
        set(correlaxis,'Zlim',[-1 1]');
        set(get(correlaxis,'XLabel'),'String','Horizontal Pixel Index')
        set(get(correlaxis,'YLabel'),'String','Vertical Pixel Index');
        set(get(correlaxis,'ZLabel'),'String','Correlation Strength');
        plottedcorrelmesh = 0;
    end
end

% analysisprog = waitbar(0,'Setting Up');
% oldwaitbarposition = get(analysisprog,'Position');
% newwaitbarposition = [oldwaitbarposition(1),(oldwaitbarposition(4) + 20),...
%     oldwaitbarposition(3),oldwaitbarposition(4)];
% set(analysisprog,'Position',newwaitbarposition);

while ~toexit
    badmatchesthatcorrelated = [];
%     waitbar(0,analysisprog,'Correlating Frames');
    for framecounter = 1:numframesforcurrentlevel
        indexintomatrices = badmatches(framecounter);
        testframenumber = framesforreference(indexintomatrices);
%         testframe = double(frame2im(aviread(videoname,testframenumber)));
        try
            testframe = double(frame2im(read(fileinfo,testframenumber,'native')));
        catch
            testframe = double((read(fileinfo,testframenumber,'native')));
        end
        if istruecolor
            testframe = testframe(:,:,1);
        end
        
        [correlation shifts peaks_noise] = corr2d(referenceimage,testframe,1);
        
        peakratio = peaks_noise(2) / peaks_noise(1);
        
        if peakratio < badstripthreshold
            badmatchesthatcorrelated = [badmatchesthatcorrelated;indexintomatrices];
            matchesthataregood(indexintomatrices) = 1;
            frameshifts(indexintomatrices,:) = shifts;
            peakratios(indexintomatrices) = peakratio;
            maxvals(indexintomatrices) = peaks_noise(1);
            noises(indexintomatrices) = peaks_noise(3);
            secondpeaks(indexintomatrices) = peaks_noise(2);
        end
        
        if correlverbose
            if ~plottedcorrelmesh
                figure(correlfig);
                correlmeshhandle = mesh(correlaxis,correlation);
                set(correlaxis,'Zlim',[-0.2 1]');
                plottedcorrelmesh = 1;
            else
                figure(correlfig);
                set(correlmeshhandle,'Zdata',correlation);
                set(correlaxis,'Zlim',[-0.2 1]');
            end
        end
        
        prog = (framecounter - 1) / (numframesforcurrentlevel - 1);
%         waitbar(prog,analysisprog);
    end
    
    if isempty(badmatchesthatcorrelated) || (sum(matchesthataregood) == numframesforreference)
        toexit = 1;
        break
    end
    
    badmatchindices = find(matchesthataregood == 0);
    numframesforcurrentlevel = length(badmatchindices);
    badmatches = badmatchindices;
    
    goodmatchindices = find(matchesthataregood == 1);
    numgoodmatches = length(goodmatchindices);
    
    goodframeshifts = frameshifts(goodmatchindices,:);
    goodpeakratios = peakratios(goodmatchindices);
    
%     [~,referencematrix_full] = ...
%         makestabilizedframe(videoname,framesforreference(goodmatchindices),...
%         goodframeshifts,goodpeakratios,stripidx,badstripthreshold,2.5,1,0);
    [~,referencematrix_full] = ...
        makestabilizedframe(videoname,framesforreference(goodmatchindices),...
        goodframeshifts,goodpeakratios,stripidx,badstripthreshold,525,1,0);

    referenceimage = referencematrix_full(:,:,2);
%     waitbar(0,analysisprog,'Temp');
end

% close(analysisprog);

if correlverbose
    close(correlfig);
end

goodmatchindices = find(matchesthataregood == 1);
framesforreference = framesforreference(goodmatchindices);
frameshifts = frameshifts(goodmatchindices,:);
peakratios = peakratios(goodmatchindices);
maxvals = maxvals(goodmatchindices);
noises = noises(goodmatchindices);
secondpeaks = secondpeaks(goodmatchindices);
numframesforreference = length(framesforreference);

[referencematrix,referencematrix_full] = ...
    makestabilizedframe(videoname,framesforreference,frameshifts,peakratios,...
    stripidx,badstripthreshold,frameheight,2.5,1,0);

referenceimage = referencematrix(:,:,2);
analysedframes = framesforreference;
videoname_check = videofilename;

randstring = num2str(min(ceil(rand(1) * 10000),9999));
fullstring = strcat('_coarserefdata_',num2str(frameincrement),'_',randstring,'.mat');
datafilename = strcat(videoname(1:end - 4),fullstring);

%KSP 02/27/2020
% For now replace reference image with the first frame of the video. The
% first frama of the video would have to be valid for this to work. Ensure
% preprocessing is done to ensure that the first frame is valid. 
% try
%      referenceimage = double(frame2im(read(fileinfo,firstframereferencenumber,'native')));
% catch
%      referenceimage = double((read(fileinfo,firstframereferencenumber,'native')));
% end
% if istruecolor
%      referenceimage = referenceimage(:,:,1);
% end


save(datafilename,'referenceimage','referencematrix','referencematrix_full',...
    'analysedframes','frameshifts','maxvals','secondpeaks','noises','peakratios',...
    'stripidx','videoname_check','badstripthreshold','frameincrement','framewidth',...
    'frameheight');

if referenceverbose
    mymap = repmat([0:255]' / 256,1,3);
    titlestring = ['Reference Frame from video ',videofilename];
    figure;
    set(gcf,'Name','Reference Frame');
    image(referenceimage);
    colormap(mymap);
    axis off;
    title(titlestring,'Interpreter','none');
    truesize
end