function [datafilename,referenceimage,timeaxis_secs] = makereference_priorref(videoname,refframefilename,badframefilename,...
    referencevarname,searchparamstruct,verbosityarray)

rand('state',sum(100 * clock));
randn('state',sum(100 * clock));

currentdirectory = pwd;
screensize = get(0,'Screensize');

if (nargin < 1) || isempty(videoname)
    togetvideoname = 1;
else
    if ischar(videoname)
        if exist(videoname,'file')
            maxslashindex = 0;
            if ispc
                slash = '\';
            else
                slash = '/';
            end
            maxslashindex = find(slash == videoname, 1, 'last');
            if numel(maxslashindex) > 0
                videopath = videoname(1:maxslashindex);
                videofilename = videoname(maxslashindex + 1:end);
            else
                videopath = pwd;
                videofilename = videoname;
            end
%             for charcounter = 1:length(videoname)
%                 testvariable = strcmp(videoname(charcounter),'\');
%                 if testvariable
%                     maxslashindex = charcounter;
%                 end
%             end
            videofilename = videoname(maxslashindex + 1:end);
            videopath = videoname(1:maxslashindex);
%             keyboard
            cd(videopath);
            togetvideoname = 0;
        else
            disp('Supplied video name does not point to a valid file');
            togetvideoname = 1;
        end
    else
        disp('Supplied video name must be a string');
        togetvideoname = 1;
    end
end

if togetvideoname
    [videofilename videopath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if videofilename == 0
        disp('No video to analyse,stopping program');
        error('Type ''help makereference_priorref'' for usage');
    else
        videoname = strcat(videopath,videofilename);
        cd(videopath);
    end
end

if (nargin < 2) || isempty(refframefilename)
    togetrefframefilename = 1;
else
    if ischar(refframefilename)
        if exist(refframefilename,'file')
            togetrefframefilename = 0;
            togetreferencevarname = 1;
            toloadreference = 1;
        else
            disp('Second input string does not point to a valid mat file');
            togetrefframefilename = 1;
        end
    else
        if isnumeric(refframefilename)
            togetrefframefilename = 0;
            togetreferencevarname = 0;
            toloadreference = 0;
            referenceimage_prior = refframefilename;
        else
            disp('Second input argument must be a string or 2D double matrix');
            togetrefframefilename = 1;
        end
    end
end

if togetrefframefilename
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the reference image data');
    if fname == 0
        cd(currentdirectory);
        disp('Need reference data,stopping program');
        error('Type ''help makereference_priorref'' for usage');
    else
        refframefilename = strcat(pname,fname);
        togetreferencevarname = 1;
        toloadreference = 1;
    end
end

if (nargin < 3) || isempty(badframefilename)
    togetbadframefilename = 1;
else
    if isstr(badframefilename)
        if exist(badframefilename,'file')
            togetbadframefilename = 0;
            toloadframenumbers = 1;
        else
            disp('Third input string does point to a valid mat file');
            togetbadframefilename = 1;
        end
    else
        if isnumeric(badframefilename)
            toloadframenumbers = 0;
            togetbadframefilename = 0;
        else
            disp('Third input argument must be either a string or numeric array');
            togetbadframefilename = 1;
        end
    end
end

if togetbadframefilename
    [fname pname] = uigetfile('*.mat','Please enter the matfile with the bad frame data');
    if fname == 0
        cd(currentdirectory);
        disp('Need bad/good frame data,stopping program');
        error('Type ''help makereference_priorref'' for usage');
    else
        badframefilename = strcat(pname,fname);
        toloadframenumbers = 1;
    end
end

if (nargin < 4 || isempty(referencevarname) || ~ischar(referencevarname)) && togetreferencevarname
    variablesinfile = who('-file',refframefilename);
    [selection,ok] = listdlg('PromptString','Which variable is the reference',...
        'SelectionMode','single','ListString',variablesinfile);
    if ok == 0
        warning('You have not made a valid selection, exiting...');
        return;
    end
    referencevarname = variablesinfile{selection};
end

if nargin < 5 || isempty(searchparamstruct) || ~isstruct(searchparamstruct)
    prompt = {'Sample Rate (Hz)','Vertical Search Zone (Pixels)',...
        'Strip Height (Pixels)','Bad Strip Correlation Threshold',...
        'Frame Number Increment','Minimum %. of Good Strips Reqd Per Frame',...
        'Number of Lines per Full Frame'};

    name = 'Search Parameters Input for Reference Program';
    numlines = 1;
    defaultanswer = {'720','75','11','0.6','12','0.4','525'};

    inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
    if isempty(inputanswer)
        disp('You have pressed cancel rather than input any values');
        warning('Using default values in fields in the searchparamstruct structure');
        inputanswer = {'720','75','11','0.6','12','0.4','525'};
    end

    togetsamplerate = 0;
    togetvertsearchzone = 0;
    togetstripheight = 0;
    togetbadstripthreshold = 0;
    togetframeincrement = 0;
    togetminpercentofgoodstripsperframe = 0;
    togetnumlinesperfullframe = 0;

    searchparamstruct = struct('samplerate',str2num(inputanswer{1}),...
        'vertsearchzone',str2num(inputanswer{2}),'stripheight',...
        str2num(inputanswer{3}),'badstripthreshold',str2num(inputanswer{4}),...
        'frameincrement',str2num(inputanswer{5}),'minpercentofgoodstripsperframe',...
        str2num(inputanswer{6}),'numlinesperfullframe',str2num(inputanswer{7}));
else
    namesofinputfields = fieldnames(searchparamstruct);
    if sum(strcmp(namesofinputfields,'samplerate'))
        togetsamplerate = 0;
    else
        togetsamplerate = 1;
    end
    if sum(strcmp(namesofinputfields,'vertsearchzone'))
        togetvertsearchzone = 0;
    else
        togetvertsearchzone = 1;
    end
    if sum(strcmp(namesofinputfields,'stripheight'))
        togetstripheight = 0;
    else
        togetstripheight = 1;
    end
    if sum(strcmp(namesofinputfields,'badstripthreshold'))
        togetbadstripthreshold = 0;
    else
        togetbadstripthreshold = 1;
    end
    if sum(strcmp(namesofinputfields,'frameincrement'))
        togetframeincrement = 0;
    else
        togetframeincrement = 1;
    end
    if sum(strcmp(namesofinputfields,'minpercentofgoodstripsperframe'))
        togetminpercentofgoodstripsperframe = 0;
    else
        togetminpercentofgoodstripsperframe = 0;
    end
    if sum(strcmp(namesofinputfields,'numlinesperfullframe'))
        togetnumlinesperfullframe = 0;
    else
        togetnumlinesperfullframe = 1;
    end
end

if (togetsamplerate + togetvertsearchzone + togetstripheight +...
        togetbadstripthreshold + togetminpercentofgoodstripsperframe +...
        togetnumlinesperfullframe) > 0
    prompt = {};
    defaultanswer = {};
    name = 'Search Parameter Input for Reference Program';
    numlines = 1;

    if togetsamplerate
        prompt{end + 1} = 'Sample Rate (Hz)';
        defaultanswer{end + 1} = '720';
    end
    if togetvertsearchzone
        prompt{end + 1} = 'Vertical Search Zone (Pixels)';
        defaultanswer{end + 1} = '75';
    end
    if togetstripheight
        prompt{end + 1} = 'Strip Height (Pixels)';
        defaultanswer{end + 1} = '11';
    end
    if togetbadstripthreshold
        prompt{end + 1} = 'Bad Strip Correlation Threshold';
        defaultanswer{end + 1} = '0.6';
    end
    if togetframeincrement
        prompt{end + 1} = 'Frame Number Increment';
        defaultanswer{end + 1} = '12';
    end
    if togetminpercentofgoodstripsperframe
        prompt{end + 1} = 'Minimum %. of Good Strips Reqd Per Frame';
        defaultanswer{end + 1} = '0.4';
    end
    if togetnumlinesperfullframe
        prompt{end + 1} = 'Number of Lines per Full Frame';
        defaultanswer{end + 1} = '525';
    end

    inputanswer = inputdlg(prompt,name,numlines,defaultanswer);
    fieldcounter = 1;
    if togetsamplerate
        searchparamstruct.samplerate = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetvertsearchzone
        searchparamstruct.vertsearchzone = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetstripheight
        searchparamstruct.stripheight = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetbadstripthreshold
        searchparamstruct.badstripthreshold = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetframeincrement
        searchparamstruct.frameincrement = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetminpercentofgoodstripsperframe
        searchparamstruct.minpercentofgoodstripsperframe = str2num(inputanswer{fieldcounter});
        fieldcounter = fieldcounter + 1;
    end
    if togetnumlinesperfullframe
        searchparamstruct.numlinesperfullframe = str2num(inputanswer{fieldcounter});
    end
end

if (nargin < 6) || isempty(verbosityarray)
    verbosityarray = [ 0; 0; 0; 0];
    correlverbose = 0;
    shiftverbose = 0;
    peakratioverbose = 0;
    referenceverbose = 0;
    toplotfeedbackfigs = 0;
    disp('No feedback will be provided!');
else
    if length(verbosityarray) < 4
        disp('Verbose array is too small')
        warning('Unassigned verbose flags set to zero');
    end

    switch (length(verbosityarray))
        case 1
            correlverbose = verbosityarray(1);
        case 2
            correlverbose = verbosityarray(1);
            shiftverbose = verbosityarray(2);
        case 3
            correlverbose = verbosityarray(1);
            shiftverbose = verbosityarray(2);
            peakratioverbose = verbosityarray(3);
        case 4
            correlverbose = verbosityarray(1);
            shiftverbose = verbosityarray(2);
            peakratioverbose = verbosityarray(3);
            referenceverbose = verbosityarray(4);
    end
end

videoinfo = aviinfo(videoname); % Get important info of the avifile
%     KSP Changes 02/07/2020
fileinfo=VideoReader(videoname);
videoframerate = round(videoinfo.FramesPerSecond); % The videoframerate of the video
% numbervideoframes = videoinfo.NumFrames;
numbervideoframes=fileinfo.FrameRate*fileinfo.Duration;%KSP: 02/07/2020
framewidth = videoinfo.Width; % The width of the video (in pixels)
frameheight = videoinfo.Height; % The height of the video (in pixels)
videotype = videoinfo.ImageType;

if strcmp(videotype,'truecolor')
    disp('Video being analyssed is a truecolor video, this program can analyse only 8 bit videos!!');
    warning('Using only the first layer of the video during analyses');
    istruecolor = 1;
else
    istruecolor = 0;
end

samplerate = searchparamstruct.samplerate;
defaultsearchzone_vert_strips = searchparamstruct.vertsearchzone;
stripheight = searchparamstruct.stripheight;
badstripthreshold = searchparamstruct.badstripthreshold;
frameincrement = searchparamstruct.frameincrement;
minpercentofgoodstripsperframe = searchparamstruct.minpercentofgoodstripsperframe;
numlinesperfullframe = searchparamstruct.numlinesperfullframe;

if rem(samplerate,1)
    disp('Sample rate should be an whole number');
    warning('Rounding off the sample rate');
    samplerate = round(samplerate);
end
if samplerate < (2 * videoframerate)
    warnig('Too Low a sample sample, increasing to twice the frame rate of video');
    samplerate = (2 * videoframerate);
end
if samplerate > (videoframerate * numlinesperfullframe)
    newsampleratestring = [num2str(videoframerate * numlinesperfullframe),' Hz'];
    dispstring = ['Too High a sample rate, decreasing to ',newsampleratestring];
    warning(dispstring);
    samplerate = (videoframerate * numlinesperfullframe);
end
if rem(samplerate,videoframerate) ~= 0
    disp('Sample rate should be multiple of the video frame rate');
    warning('Reducing sample rate to previous multiple of frame rate');
    samplerate = floor(samplerate / videoframerate) * videoframerate;
end

if defaultsearchzone_vert_strips > frameheight
    disp('Default Vertical Search Zone is too high');
    warning('Reducing to 10 pixels less than frame height');
    defaultsearchzone_vert_strips = frameheight - 10;
end
if defaultsearchzone_vert_strips < 10
    disp('Default Vertical Search Zone is too low');
    warning('Increasing to 10 pixels');
    defaultsearchzone_vert_strips = 10;
end

if rem(stripheight,1) ~= 0
    disp('Strip height cannot be a decimal');
    warning('Round down...');
    stripheight = floor(stripheight);
end
if stripheight >= defaultsearchzone_vert_strips
    disp('Strip Height cannot be larger than vertical search zone');
    warning('Reducing strip height to one less than vertical search zone');
    stripheight = max(defaultsearchzone_vert_strips - 1,1);
end
if stripheight <= 0
    disp('Strip should have atleast one line');
    warning('Increasing strip height to 1');
    stripheight = 1;
end

if badstripthreshold >= 1
    disp('Bad Strip Threshold is too high');
    warning('Reducing to 0.99');
    badstripthreshold = 0.99;
end
if badstripthreshold <= 0
    disp('Bad Strip Threshold is too low');
    warnig('Increasing to 0.01')
    badstripthreshold = 0.01;
end

if (frameincrement < 5)
    warning('Frame increment is too low, increasing to 5');
    frameincrement = 5;
end
if (frameincrement > (numbervideoframes / 5))
    warning('Frame increment is too high, reducing to 1/5 the number of frames in video');
    frameincrement = round(numbervideoframes / 5);
end

if minpercentofgoodstripsperframe > 1
    disp('A little difficult to get more than 100% good strips in a frame');
    warning('Reducing minimum percentage of good strips per frame to 99%');
    minpercentofgoodstripsperframe = 0.99;
end
if minpercentofgoodstripsperframe < 0.2
    disp('To few strips per frame')
    warning('Increasing minimum percentage of good strips per frame to 20%');
    minpercentofgoodstripsperframe = 0.2;
end

if numlinesperfullframe < frameheight
    disp('Number of lines in full frame cannot be smaller than the number of lines in video frame');
    error('Type ''help makereference_priorref'' for usage');
end

if toloadreference
    variablesinfile = who('-file',refframefilename);
    doesfilehavereference = sum(strcmp(variablesinfile,referencevarname));
    if doesfilehavereference
        load(refframefilename,referencevarname);
        renamestring = strcat('referenceimage_prior =',referencevarname,';');
        eval(renamestring);
    else
        disp('Problem with file supplied, variable does not exist within the file');
        error('Unable to load reference image, exiting...');
    end
end

if toloadframenumbers
    variablesinfile = who('-file',badframefilename);
    doesfilehavegoodframeinfo = sum(strcmp(variablesinfile,'goodframesforrefanalysis'));
    doesfilehavevideocheckname = sum(strcmp(variablesinfile,'videoname_check'));
    if doesfilehavegoodframeinfo
        load(badframefilename,'goodframesforrefanalysis');
        if doesfilehavevideocheckname
            load(badframefilename,'videoname_check');
        else
            disp('Problem with bad frame file');
            warning('No video name was in bad frame datafile');
        end
    else
        disp('Problem with bad frame file');
        error('MATLAB data file does not have any good frame info');
    end

    if (exist('videoname_check','var')) && isempty(videoname_check) ||...
            (strcmp(videoname_check,videofilename) == 0)
        disp('Problem with video name in bad frame MAT file')
        warning('Bad frame info was obtained from different video / Video info was in matlab data file is empty');
    end
    if isempty(goodframesforrefanalysis)
        disp('Problem with good frame info');
        error('MATLAB data file does not have any good frame info');
    end

    framesforreference_indices = [1:frameincrement:length(goodframesforrefanalysis)];
    framesforreference = goodframesforrefanalysis(framesforreference_indices);
else
    framesforreference = badframefilename(:);
end

if ~isnumeric(referenceimage_prior)
    disp('Reference image has to be a 2D double matrix');
    error('Please supply an appropriate reference image, exiting...');
end
if max(size(size(referenceimage_prior))) > 2
    disp('Reference image cannot have more than one image layer');
    warning('Using only first layer of the reference image');
    referenceimage_prior = referenceimage_prior(:,:,1);
end

if sum(rem(framesforreference,1)) > 0
    disp('Sorry program can analyse frames with decimal indices');
    warning('Rounding off frames numbers');
    framesforreference = unique(round(framesforreference));
end
if min(framesforreference(:)) < 1
    disp('Frame numbers supplied for refererence creation has 0/Neg numbers');
    warning('Deleting the crazy numbers');
    framesforreference = framesforreference(find(framesforreference >= 1));
end
if max(framesforreference(:)) > numbervideoframes
    disp('Frame numbers supplied for refererence creation has number greater than number of frames in video');
    warning('Deleting the crazy numbers');
    framesforreference = framesforreference(find(framesforreference <= numbervideoframes));
end


cd(currentdirectory);

thumbnail_factor = 5;
numframesforreference = length(framesforreference);

referencesize_x = size(referenceimage_prior,2);
referencesize_y = size(referenceimage_prior,1);

if (referencesize_x < framewidth) || (referencesize_y < frameheight)
    disp('Reference image is smaller than video frame')
    warning('Parts of the video might not be registered using current reference image')
end

reference_xcentre = floor(referencesize_x / 2) + 1;
frame_xcentre = floor(framewidth / 2) + 1;

yincrement = floor((referencesize_y - frameheight) / 2);

defaultsearchzone_hori_strips = framewidth;

if rem(defaultsearchzone_vert_strips,2) == 0
    defaultsearchzone_vert_strips = defaultsearchzone_vert_strips + 1;
end

if rem(stripheight,2) == 0
    stripheight = stripheight - 1;
end

numstrips = round(samplerate / videoframerate);
stripseparation = round(numlinesperfullframe / numstrips);

stripidx(1) = round(stripseparation / 2); % The location of the first strip
if numstrips > 1
    for stripcounter = 2:numstrips
        stripidx(stripcounter) = stripidx(stripcounter - 1) + stripseparation;
    end
end

stripidx = stripidx(find(stripidx <= frameheight));
stripidx = stripidx(:);
numstrips = length(stripidx);
mingoodstripsperframes = numstrips * minpercentofgoodstripsperframe;

if mingoodstripsperframes < 2
    disp('Increasing minimum nuumber of good strips to 2');
    mingoodstripsperframes = 2;
end

frameshifts_thumbnails = zeros(numframesforreference,2);
peakratios_thumbnails = ones(numframesforreference,1);
maxvals_thumbnails = ones(numframesforreference,1);
secondpeaks_thumbnails = ones(numframesforreference,1);
noises_thumbnails = ones(numframesforreference,1);

totalnumsamples = numframesforreference * numstrips;

referenceimage_thumbnail = downsample(referenceimage_prior,thumbnail_factor,thumbnail_factor);

xorigin = frame_xcentre;
yorigin = frame_xcentre;

if any(verbosityarray)
    toplotfeedbackfigs = 1;
    figurewidth = round((screensize(3) - 50) / 3);
    figureheight = round(screensize(4) / 3);

    if correlverbose
        correlfig = figure;
        correlaxis = axes;
        newposition = [0,(screensize(4) - figureheight + 1),...
            figurewidth, figureheight];
        set(correlfig,'Position',newposition,'Toolbar','none','Name','Cross-Correlation');
        set(correlaxis,'Zlim',[-1 1]');
        set(get(correlaxis,'XLabel'),'String','Horizontal Pixel Index')
        set(get(correlaxis,'YLabel'),'String','Vertical Pixel Index');
        set(get(correlaxis,'ZLabel'),'String','Correlation Strength');
        plottedcorrelmesh = 0;
    end

    if shiftverbose
        shiftstoplot = zeros(totalnumsamples,2);
        shiftfig = figure;
        shiftaxis = axes;
        shiftplot_hori = plot(shiftstoplot(:,1),'Color',[0 0 1]);
        hold on
        shiftplot_vert = plot(shiftstoplot(:,2),'Color',[0 1 0]);
        hold off;
        shifttitle = title(shiftaxis,'Setting up');
        newposition = [figurewidth + 25,(screensize(4) - figureheight + 1),...
            figurewidth, figureheight];
        set(shiftfig,'Position',newposition,'Toolbar','none','Name','Pixel Shifts');
        set(shiftaxis,'Xlim',[1 totalnumsamples],...
            'YLim',round([(-0.25 * frameheight) (0.25 * frameheight)]));
        set(get(shiftaxis,'XLabel'),'String','Sample No.')
        set(get(shiftaxis,'YLabel'),'String','Shift (Pixels)');
    end

    if peakratioverbose
        peakratstoplot = ones(totalnumsamples,1);
        threshtoplot = repmat(badstripthreshold,totalnumsamples,1);
        peakratfig = figure;
        peakrataxis = axes;
        peakplot = plot(peakratstoplot,'Color',[0 0 1]);
        hold on
        threshplot = plot(threshtoplot,'Color',[0 0 0]);
        hold off
        newposition = [(2 * (figurewidth + 25)),(screensize(4) - figureheight + 1),...
            figurewidth, figureheight];
        set(peakratfig,'Position',newposition,'Toolbar','none','Name','Peak Ratios');
        set(peakrataxis,'Xlim',[1 totalnumsamples],'YLim',[0 1]);
        set(get(peakrataxis,'XLabel'),'String','Sample No.')
        set(get(peakrataxis,'YLabel'),'String','Peak Ratio');
    end

    if shiftverbose || peakratioverbose
        stripindxaddition = [1:numstrips];
    end
else
    toplotfeedbackfigs = 0;
end

% analysisprog = waitbar(0,'Thumbnail Analysis');
% oldwaitbarposition = get(analysisprog,'Position');
% newwaitbarposition = [oldwaitbarposition(1),(oldwaitbarposition(4) + 20),...
%     oldwaitbarposition(3),oldwaitbarposition(4)];
% set(analysisprog,'Position',newwaitbarposition);

for framecounter = 1:numframesforreference
    testframenumber = framesforreference(framecounter);
%     testframe = double(frame2im(aviread(videoname,testframenumber)));
    try
        testframe = double(frame2im(read(fileinfo,testframenumber,'native')));
    catch
        testframe = double((read(fileinfo,testframenumber,'native')));
    end
    if istruecolor
        testframe = testframe(:,:,1);
    end

    testframe_thumbnail = downsample(testframe,thumbnail_factor,thumbnail_factor);

    [correlation shifts peaks_noise] = ...
        corr2d(referenceimage_thumbnail,testframe_thumbnail,1);

    if peaks_noise(3) == 0
        peaks_noise(3) = 1;
    end

    peakratio = peaks_noise(2) / peaks_noise(1);
    xpixelshift = shifts(1) * thumbnail_factor;
    ypixelshift = shifts(2) * thumbnail_factor;

    if peakratio > badstripthreshold
        [correlation tempshifts temppeaks_noise] = ...
            corr2d(referenceimage_prior,testframe,1);

        if temppeaks_noise(3) == 0
            temppeaks_noise(3) = 1;
        end

        temppeakratio = temppeaks_noise(1);
        if temppeakratio <= badstripthreshold
            peakratio = temppeakratio;
            peaks_noise = temppeaks_noise;
            xpixelshift = tempshifts(1);
            ypixelshift = tempshifts(2);
        end
    end

    if toplotfeedbackfigs
        titlestring = ['Frame No.: ',num2str(testframenumber)];
        if shiftverbose | peakratioverbose
            indicesintomatrices = ((framecounter - 1) * numstrips) + stripindxaddition;
        end

        if correlverbose
            if ~plottedcorrelmesh
                figure(correlfig);
                correlmeshhandle = mesh(correlaxis,correlation);
                plottedcorrelmesh = 1;
            else
                figure(correlfig);
                set(correlmeshhandle,'Zdata',correlation);
            end
            title(titlestring)
        end

        if shiftverbose
            figure(shiftfig);
            shiftstoplot(indicesintomatrices,:) =...
                repmat([xpixelshift ypixelshift],numstrips,1);
            set(shiftplot_hori,'YData',shiftstoplot(:,1));
            set(shiftplot_vert,'YData',shiftstoplot(:,2));
            title(titlestring)
        end

        if peakratioverbose
            figure(peakratfig);
            peakratstoplot(indicesintomatrices) =...
                repmat(peakratio,numstrips,1);
            set(peakplot,'YData',peakratstoplot);
            title(titlestring)
        end
        drawnow
    end

    frameshifts_thumbnails(framecounter,:) = [xpixelshift ypixelshift];
    peakratios_thumbnails(framecounter) = peakratio;
    maxvals_thumbnails(framecounter) = peaks_noise(1);
    secondpeaks_thumbnails(framecounter) = peaks_noise(2);
    noises_thumbnails(framecounter) = peaks_noise(3);

    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

goodmatchindices_thumbnails = find(peakratios_thumbnails <= badstripthreshold);

framesforreference_initial = framesforreference;
framesforreference = framesforreference(goodmatchindices_thumbnails);
numframesforreference = length(framesforreference);

frameshifts_strips_unwraped = zeros(numstrips,numframesforreference,2);
peakratios_strips_unwraped = zeros(numstrips,numframesforreference,1);
maxvals_strips_unwraped = ones(numstrips,numframesforreference,1);
secondpeaks_strips_unwraped = ones(numstrips,numframesforreference,1);
noises_strips_unwraped = ones(numstrips,numframesforreference,1);

xsearchzone = defaultsearchzone_hori_strips;
ysearchzone = defaultsearchzone_vert_strips;

absthumbnailvelocities = abs([[0,0];diff(frameshifts_thumbnails,[],1)]);

% waitbar(0,analysisprog,'Higher Rate Analysis');
for framecounter = 1:numframesforreference
    wasprevstripagoodmatch = 0;
    testframenumber = framesforreference(framecounter);
%     testframe = double(frame2im(aviread(videoname,testframenumber)));
    try
        testframe = double(frame2im(read(fileinfo,testframenumber,'native')));
    catch
        testframe = double((read(fileinfo,testframenumber,'native')));
    end
    if istruecolor
        testframe = testframe(:,:,1);
    end

    currentframethumbnailvelocity = absthumbnailvelocities(framecounter,:);

    if currentframethumbnailvelocity(1) > defaultsearchzone_hori_strips
        framexsearchzonetouse = min(round(2.5 * currentframethumbnailvelocity(1)),...
            framewidth);
    else
        framexsearchzonetouse = xsearchzone;
    end

    if currentframethumbnailvelocity(2) > defaultsearchzone_vert_strips
        frameysearchzonetouse = min(round(2.5 * currentframethumbnailvelocity(2)),...
            round(5 * referencesize_y / 6));
    else
        frameysearchzonetouse = ysearchzone;
    end

    for stripcounter = 1:numstrips
        yorigin = stripidx(stripcounter);

        if wasprevstripagoodmatch
            stripindexofprevgoodmatch = stripcounter - 1;
            frameindexofprevgoodmatch = framecounter;

            xmovement = round(frameshifts_strips_unwraped(stripindexofprevgoodmatch,...
                frameindexofprevgoodmatch,1));
            ymovement = round(frameshifts_strips_unwraped(stripindexofprevgoodmatch,...
                frameindexofprevgoodmatch,2));

            xsearchzonetouse = max(round(2 * framexsearchzonetouse / 3),round(framewidth / 2));
            ysearchzonetouse = max(round(2 * frameysearchzonetouse / 3),stripheight + 5);
        else
            xmovement = round(frameshifts_thumbnails(framecounter,1));
            ymovement = round(frameshifts_thumbnails(framecounter,2));

            xsearchzonetouse = framexsearchzonetouse;
            ysearchzonetouse = frameysearchzonetouse;
        end

        refxorigin = reference_xcentre - xmovement;
        refyorigin = yorigin + yincrement - ymovement;

        referencematrix = getsubmatrix(referenceimage_prior,refxorigin,refyorigin,...
            xsearchzonetouse,ysearchzonetouse,[1 referencesize_x], [1 referencesize_y],0,0);
        teststrip = getsubmatrix(testframe,xorigin,yorigin,xsearchzonetouse,...
            stripheight,[1 framewidth], [1 frameheight],0,0);

        [correlation shifts peaks_noise] =....
            findthestrip(referencematrix,teststrip);

        if peaks_noise(3) == 0
            peaks_noise(3) = 1;
        end

        peakratio = peaks_noise(2) /peaks_noise(1);

        xpixelshift = xmovement + shifts(1);
        ypixelshift = ymovement + shifts(2);

        frameshifts_strips_unwraped(stripcounter,framecounter,:) = cat(3,xpixelshift, ypixelshift);
        maxvals_strips_unwraped(stripcounter,framecounter) = peaks_noise(1);
        secondpeaks_strips_unwraped(stripcounter,framecounter) = peaks_noise(2);
        noises_strips_unwraped(stripcounter,framecounter) = peaks_noise(3);
        peakratios_strips_unwraped(stripcounter,framecounter) = peakratio;

        if toplotfeedbackfigs
            titlestring = ['Frame No.: ',num2str(testframenumber),' Strip No.: ',num2str(stripcounter)];
            if shiftverbose | peakratioverbose
                idxintomatrices = ((framecounter - 1) * numstrips) + stripcounter;
            end

            if correlverbose
                figure(correlfig);
                set(correlmeshhandle,'Zdata',correlation);
                set(correlaxis,'Zlim',[-0.2 1]');
                title(titlestring)
            end

            if shiftverbose
                figure(shiftfig);
                shiftstoplot(idxintomatrices,:) = [xpixelshift ypixelshift];
                set(shiftplot_hori,'YData',shiftstoplot(:,1));
                set(shiftplot_vert,'YData',shiftstoplot(:,2));
                title(titlestring)
            end

            if peakratioverbose
                figure(peakratfig);
                peakratstoplot(idxintomatrices) = peakratio;
                set(peakplot,'YData',peakratstoplot);
                title(titlestring)
            end
            drawnow
        end

        if peakratio > badstripthreshold
            wasprevstripagoodmatch = 0;
        else
            wasprevstripagoodmatch = 1;
        end
    end
    prog = framecounter / numframesforreference;
%     waitbar(prog,analysisprog);
end

peakratios_strips = peakratios_strips_unwraped(:);
maxvals_strips = maxvals_strips_unwraped(:);
secondpeaks_strips = secondpeaks_strips_unwraped(:);
noises_strips = noises_strips_unwraped(:);

badmatches_initial = find(peakratios_strips > badstripthreshold);

if ~isempty(badmatches_initial)
    frameshifts_strips_spline_unwraped = zeros(size(frameshifts_strips_unwraped));
    badmatchaddition = [-3:3];
    numbadmatchindicestoadd = length(badmatchaddition);

    badmatches = repmat(badmatches_initial(:),1,numbadmatchindicestoadd) +...
        repmat(badmatchaddition,length(badmatches_initial),1);

    badmatches = sort(unique(min(max(badmatches(:),1),length(peakratios_strips))),'ascend');
    goodmatches = setdiff([1:length(peakratios_strips)]',badmatches);

    if abs((length(badmatches) - length(peakratios_strips))) <= 2
        if abs((length(badmatches_initial) - length(peakratios_strips))) <= 2
            goodmatches = [1:length(peakratios_strips)]';
        else
            goodmatches = setdiff([1:length(peakratios_strips)]',badmatches_initial);
        end
    end

    interp_xaxis = [0:(numlinesperfullframe * numbervideoframes) - 1];
    interp_xaxis = reshape(interp_xaxis,numlinesperfullframe,numbervideoframes);
    interp_xaxis = interp_xaxis(stripidx,framesforreference);
    interp_xaxis = interp_xaxis(:);
    sample_xaxis = interp_xaxis(goodmatches);
    timeaxis = interp_xaxis;

%     waitbar(0,analysisprog,'Interpolating Through Bad Shifts');
    for directioncounter = 1:2
        tempshifts = frameshifts_strips_unwraped(:,:,directioncounter);
        sample_yaxis = tempshifts(:);
        interp_yaxis = interp1(sample_xaxis,sample_yaxis(goodmatches),...
            interp_xaxis,'linear','extrap');
        frameshifts_strips_spline_unwraped(:,:,directioncounter) =...
            reshape(interp_yaxis,numstrips,numframesforreference);

        prog = directioncounter / 2;
%         waitbar(prog,analysisprog);
    end

else
    badmatches = [];
    goodmatches = [1:length(peakratios_strips)]';
    timeaxis = [0:(numlinesperfullframe * numbervideoframes) - 1];
    timeaxis = reshape(timeaxis,numlinesperfullframe,numbervideoframes);
    timeaxis = timeaxis(stripidx,framesforreference);
    timeaxis = timeaxis(:);
    frameshifts_strips_spline_unwraped = frameshifts_strips_unwraped;
end

% close(analysisprog);

if toplotfeedbackfigs
    if correlverbose
        close(correlfig);
    end

    if shiftverbose
        close(shiftfig);
    end

    if peakratioverbose
        close(peakratfig);
    end
end

timeaxis_secs = timeaxis / (numlinesperfullframe * videoframerate);

frameshifts_strips = zeros(numframesforreference * numstrips,2);
frameshifts_strips_spline = zeros(numframesforreference * numstrips,2);

for directioncounter = 1:2
    tempshifts = frameshifts_strips_unwraped(:,:,directioncounter);
    frameshifts_strips(:,directioncounter) = tempshifts(:);

    tempshifts = frameshifts_strips_spline_unwraped(:,:,directioncounter);
    frameshifts_strips_spline(:,directioncounter) = tempshifts(:);
end

todropfirstframe = 0;
todroplastframe = 0;

firstgoodsample = min(goodmatches(:));
lastgoodsample = max(goodmatches(:));

firstgoodframe = ceil(firstgoodsample / numstrips);
lastgoodframe = ceil(lastgoodsample / numstrips);

if firstgoodframe > 1
    todropfirstframe = 1;
end
if lastgoodframe < numframesforreference
    todroplastframe = 1;
end

if max(badmatches(:)) == size(peakratios_strips,1)
    for directioncounter = 1:2
        frameshifts_strips(lastgoodsample + 1:end,directioncounter) =...
            frameshifts_strips(lastgoodsample,directioncounter);

        frameshifts_strips_spline(lastgoodsample + 1:end,directioncounter) =...
            frameshifts_strips_spline(lastgoodsample,directioncounter);
    end
end

if min(badmatches(:)) == 1
    for directioncounter = 1:2
        frameshifts_strips(1:firstgoodsample - 1,directioncounter) =...
            frameshifts_strips(firstgoodsample,directioncounter);

        frameshifts_strips_spline(1:firstgoodsample - 1,directioncounter) =...
            frameshifts_strips_spline(firstgoodsample,directioncounter);
    end
end

isgoodstrip = peakratios_strips_unwraped <= badstripthreshold;
numgoodstripsinframes = sum(isgoodstrip,1);
dropframeflag = find(numgoodstripsinframes < mingoodstripsperframes);
dropframeflag = dropframeflag(:);

if todropfirstframe
    if firstgoodframe > 2
        dropframeflag = [dropframeflag;[1:firstgoodframe - 1]'];
    else
        dropframeflag = [dropframeflag;1];
    end
end
if todroplastframe
    if lastgoodframe < (numframesforreference - 1)
        dropframeflag = [dropframeflag;[lastgoodframe + 1:numframesforreference]'];
    else
        dropframeflag = [dropframeflag;numframesforreference];
    end
end

if isempty(dropframeflag)
    framesthweredropped = [];
    frameshifts_strips_withbadframes = [];
    frameshifts_strips_spline_withbadframes = [];
    maxvals_strips_withbadframes = [];
    secondpeaks_strips_withbadframes = [];
    noises_strips_withbadframes = [];
    peakratios_strips_withbadframes = [];
else
    frameshifts_strips_withbadframes = frameshifts_strips;
    frameshifts_strips_spline_withbadframes = frameshifts_strips_spline;
    maxvals_strips_withbadframes = maxvals_strips;
    secondpeaks_strips_withbadframes = secondpeaks_strips;
    noises_strips_withbadframes = noises_strips;
    peakratios_strips_withbadframes = peakratios_strips;

    indicesdropped = dropframeflag;
    frameindicestouse = setdiff([1:numframesforreference]',indicesdropped(:));
    framesthweredropped = framesforreference(indicesdropped);

    framesforreference = framesforreference(frameindicestouse);

    frameshifts_strips_unwraped = frameshifts_strips_unwraped(:,frameindicestouse,:);
    frameshifts_strips_spline_unwraped = frameshifts_strips_spline_unwraped(:,frameindicestouse,:);
    maxvals_strips_unwraped = maxvals_strips_unwraped(:,frameindicestouse);
    secondpeaks_strips_unwraped = secondpeaks_strips_unwraped(:,frameindicestouse);
    noises_strips_unwraped = noises_strips_unwraped(:,frameindicestouse);
    peakratios_strips_unwraped = peakratios_strips_unwraped(:,frameindicestouse);
end


frameshifts_strips = zeros(length(framesforreference) * numstrips,2);
frameshifts_strips_spline = zeros(length(framesforreference) * numstrips,2);

for directioncounter = 1:2
    tempshifts = frameshifts_strips_unwraped(:,:,directioncounter);
    frameshifts_strips(:,directioncounter) = tempshifts(:);

    tempshifts = frameshifts_strips_spline_unwraped(:,:,directioncounter);
    frameshifts_strips_spline(:,directioncounter) = tempshifts(:);
end

maxvals_strips = maxvals_strips_unwraped(:);
secondpeaks_strips = secondpeaks_strips_unwraped(:);
noises_strips = noises_strips_unwraped(:);
peakratios_strips = peakratios_strips_unwraped(:);


[referencematrix,referencematrix_full] = ...
    makestabilizedframe(videoname,framesforreference,frameshifts_strips_spline,peakratios_strips,...
    stripidx,badstripthreshold,numlinesperfullframe,2.5,1,0);

referenceimage = referencematrix(:,100:end,2);

randstring = num2str(min(ceil(rand(1) * 10000),9999));
sampleratestring = num2str(samplerate);
fullstring = strcat('_priorrefdata_',sampleratestring,'hz_',num2str(frameincrement),'_',randstring,'.mat');
datafilename = strcat(videoname(1:end - 4),fullstring);

analysedframes = framesforreference;
analysedframes_initial = framesforreference_initial;
videoname_check = videofilename;

save(datafilename,'referenceimage','referencematrix','referencematrix_full',...
    'referenceimage_prior','frameshifts_strips','frameshifts_strips_spline',...
    'frameshifts_thumbnails','maxvals_strips','secondpeaks_strips','peakratios_strips',...
    'noises_strips','maxvals_thumbnails','secondpeaks_thumbnails','noises_thumbnails',...
    'peakratios_thumbnails','samplerate','defaultsearchzone_vert_strips','stripheight',...
    'badstripthreshold','numlinesperfullframe','frameincrement','analysedframes','stripidx',...
    'videoname_check','timeaxis','timeaxis_secs','framewidth','frameheight','thumbnail_factor',...
    'goodmatchindices_thumbnails','analysedframes_initial','minpercentofgoodstripsperframe',...
    'framesthweredropped','frameshifts_strips_withbadframes','frameshifts_strips_spline_withbadframes',...
    'maxvals_strips_withbadframes','secondpeaks_strips_withbadframes','noises_strips_withbadframes',...
    'peakratios_strips_withbadframes');

if referenceverbose
    mymap = repmat([0:255]' / 256,1,3);
    titlestring = ['Reference Frame from video ',videofilename];
    figure;
    set(gcf,'Name','Reference Frame');
    image(referenceimage);
    colormap(mymap);
    axis off;
    title(titlestring,'Interpreter','none');
%    truesize;
end