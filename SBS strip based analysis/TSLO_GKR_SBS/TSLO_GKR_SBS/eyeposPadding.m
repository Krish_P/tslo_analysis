function [sortedmat] = eyeposPadding(data,recordingDuration)
%Purpose: This function would load the eye position and time information
%from the mat file and pad them with NaNs wherever necessary to match the
%expected sampling rate
%% Main Code:
samplingRate=540; %sampling rate of eye positions
spacing=1/samplingRate; %time between eye position
missingArray=[];

timeaxis_secs=data(:,1);

%The following is the main loop: We calculate the difference between
%successive time points and compare it to what is expected for the given
%sampling rate. We then add the missing time points and enter the
%corresponding x and y position values as nans. 
for timeIdx=1:length(timeaxis_secs)
    if timeIdx<length(timeaxis_secs)
        diffVal=(timeaxis_secs(timeIdx+1)-timeaxis_secs(timeIdx));
        if diffVal>0.0020
            numPts=diffVal/spacing;
            startPt=timeaxis_secs(timeIdx)+(spacing);
            endPt=timeaxis_secs(timeIdx) - (spacing);
            timeFillers=transpose(linspace(startPt,endPt,numPts));
            missingArray=[missingArray; timeFillers];
        end
%     else
%         diffVal=recordingDuration-timeaxis_secs(timeIdx);
%         numPts=diffVal/(spacing);
%         startPt=timeaxis_secs(timeIdx)+(spacing);
%         endPt=timeaxis_secs(timeIdx) - (spacing);
%         timeFillers=transpose(linspace(startPt,endPt,numPts));
%         missingArray=[missingArray;timeFillers];
    end
end
missingArray(:,2:3)=NaN;
% Finally, we add it to the eyepos data array provided
data_padded=cat(1,data,missingArray);
% We sort the padded data using the first column as the index
[~,idx] = sort(data_padded(:,1));sortedmat = data_padded(idx,:); 
end

