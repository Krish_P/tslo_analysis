% AVIplay.m
% Reads an AVI movie and plays in a figure window
% plays 100 times, tries to get the framerate right
% looks like a mix of authors here
% parts are (c) SBStevenson@uh.edu
[fn pn] = uigetfile('*.avi');

myvid = VideoReader([pn fn]);
FrameRate = myvid.FrameRate;
if FrameRate < 15
    prompt = ['Anomalous FrameRate value: ' num2str(FrameRate) '  ReEnter--> '];
    FrameRate = input(prompt);
end

figure(521);image(255*ones(myvid.Height,myvid.Width));colormap(gray(256));axis off;truesize;
figtitle = ['Playing ' fn ' duration ' num2str(myvid.Duration) ' sec'];
set(521,'name',figtitle);
  currAxes = axes;
  for rep = 1:100
      myvid.CurrentTime = 0;
       while hasFrame(myvid)
            vidFrame = readFrame(myvid);
%             vidFrame = uint8(255*scale(log(double(vidFrame))));
            vidFrame = uint8(255*scale((double(vidFrame))));
            
            image(vidFrame, 'Parent', currAxes);
           
            pause(1/FrameRate);
        end
  end
  
