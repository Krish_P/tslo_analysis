 % Stabilize from raw video
% (c) 2009 SBStevenson@uh.edu and GKR
%
% This script creates a stabilized version of a raw AVI format video.
% It is designed to work with AOSLO retinal images and will not work very
% well with conventional SLO images.
% This script is essentially a macro that calls a series of functions to
% carry out the analysis steps. These functions should be in
% ~\Matlab71\toolbox\AOSLO\
% 08/25/09 - GKR modified file to give user more control of parameters
% passed to the various functions

%% KSP tweaked the original code to work as a function call
function [frameshifts_strips_spline]=stabilizefromraw_function(videotoanalyse)
theinfo = aviinfo(videotoanalyse);
videoframerate = theinfo.FramesPerSecond;

blinkthreshold = 25; % This is a maximum change in mean pixel value between
                        % frames before the function tags the frames as blink
                        % frames.
minimummeanlevel = 15; % This is minimum mean pixel value that a frame has to
                        % have for it to be considered for analyses.
gausslowcutoff = 8; % Low Frequency cutoff  for the gaussbandfilter.m function
smoothsd = 6; % The std. deviation of the gaussian smothing filter that is
              % applied by the gaussbandfilter.m function.
%KSP: Try increasing this parameter
peakratiodiff = 0.5; % Maximum Change in ratio between the secondpeak and firstpeak
                      % between two frames for the frames before the
                      % function tags the frames as "bad".
maxmotionthreshold = 0.2; % Maximum motion between frames, expressed as a percentage
                          % of the frame dimensions, before frames are
                          % tagged as "bad".
coarseframeincrement = 5; % The step size used when choosing a subset of frames
                           % from the frames that are good. This is used by the 
                           % makereference_framerate.m function.
fineframeincrement = 5; % The step size used when choosing a subset of frames
                           % from the frames that are good. This is used by the 
                           % makereference_priorref.m function.
badstripthreshold = .5;  % The ratio used for a cut-off to decide if a correlation 
                            % peak is high enough to trust. The second highest peak
                            % must be this small or smaller relative to the
                            % first peak. This is the same idea as
                            % peakratiodiff above, but here applies to
                            % strips instead of frames.

blinkverbosity = 0; % Set to 1 if you want feedback from the getblinkframes.m
                    % function, otherwise set to 0.
badframeverbosity = 0; % Set to 1 if you want feedback from the getbadframes.m
                    % function, otherwise set to 0.
samplerate = videoframerate * 18; % in Hz. Make it some multiple of the video rate 
                                    % This determines the sample rate of
                                    % your eye position traces The number
                                    % multiplied by videoframerate is how
                                    % many strips will be analyzed in a
                                    % frame. Divide 512 by this to get
                                    % strip separation
stripheight = 11; % normally 11. This determines the number of video lines in each
                    % strip that gets correlated. It has a smoothing
                    % effect, such that if the strip is wider than the strip separation, 
                    % you get overlapping estimates of eye position. When
                    % there is good contrast and resolution, make the strip
                    % narrow so that the correlation is higher. 
vertsearchzone = 24; % in pixels. Bigger is slower but better for jittery video
                        % This number should be bigger that stripheight
horisearchzone = 3 * theinfo.Width / 4; % in pixels. Bigger is slower but better for jittery video
                        % If the eye moves more than this relative to the
                        % reference frame, tracking is lost
startframe = 1;
endframe = theinfo.NumFrames;
badstripthreshold = 0.35; % normally 0.35

tic

% STEP .5 (optional)
% sectrimsqueeze.m dewarps and crops a video, can skip this
%
% STEP 1
bf = getblinkframes(videotoanalyse, blinkthreshold, minimummeanlevel,blinkverbosity);
% blinkframes = getblinkframes(aviname,blinkthreshold, minimummeanlevel, verbose)
% getblinkframes.m creates a list of bad frames that will be skipped.
% blinkthreshold is the difference in mean level from one frame to the next
% minimummeanlevel is the lowest value considered to be a valid frame
% creates a mat file with the name of the video and _blinkframes.mat
% appended
%
% STEP 1.5 (optional)
% gausbandfilter.m(vidname, gausslowcutoff, smoothsd) skippable?
% disp('Step 1 complete');
% STEP 2
[gfsi lmf] = getbadframes(videotoanalyse, [videotoanalyse(1:end-4) '_blinkframes.mat'], peakratiodiff, maxmotionthreshold, badframeverbosity);
% getbadframes.m looks for large movements or bad interframe correlations
% using thumbnail correlations. updates the blinkframes.mat file, including
% frameshifts from the thumbnail correlations.
%
% disp('Step 2 complete');

% STEP 3
[dfn, refim] = makereference_framerate(videotoanalyse,  [videotoanalyse(1:end-4) '_blinkframes.mat'], coarseframeincrement, badstripthreshold, [0 0]);
% makereference_framerate A first pass on making a reference. creates a mat
% file ending in _coarserefdata_####.mat where #### is an arbitrary unique
% identifier. "badframefilename" is still aviname_blinkframes.mat Default
% frameincrement is 15. Note that you must include the path name even if
% you are already in the right directory!
% GKR added a verbosity array to allow the user to get feedback if reqd and also a
% bad strip threshold.
%
% disp('Step 3 complete');

% STEP 4
bffn = [videotoanalyse(1:end-4) '_blinkframes.mat'];
% A struct variable is used to carry the various parameters that control
% the searching. These will determine the quality of the stabilization but
% also the speed of processing.
badstripthreshold = 0.35; % normally 0.35
% startframe = 1;
% endframe = theinfo.NumFrames;
% badstripthreshold = 0.1; % normally 0.35
sps = struct('samplerate', samplerate,'horisearchzone', horisearchzone,...
    'vertsearchzone', vertsearchzone, 'stripheight', stripheight,...
    'endframe', endframe, 'startframe', startframe, 'badstripthreshold',...
    badstripthreshold, 'frameincrement', 14, 'minpercentofgoodstripsperframe', 0.24,...
    'numlinesperfullframe',512);

[dfn, refim] = makereference_priorref(videotoanalyse,dfn ,bffn, 'referenceimage', sps, 1*[0 0 0 0]); % the last arg is verbosity
%
% makereference_priorref a second pass that uses the coarserefdata.mat as well
% as the blinkframes.mat files created previously. Frameincrement defaults to 15
% referencevarname is kind of a mystery: in the mat file there are several variables that
% have referenceimage as part of the name, and they are different sizes and have different properites. So which
% am I supposed to use? The one that is the same size or the "_full" one
% that is bigger? I'll try the "full" one
% searchaparamstruct has fields that
% control the search parameters for finding correlations.
% GKR changed searchaparamstruct with more input arguments (the frame increment
% argument is now part of the structure.
% GKR added a verbosity array to allow the user to get feedback if reqd.
% disp('Step 4 complete');

%
% STEP 5
dfs = analysevideo_priorref(videotoanalyse,dfn ,bffn,'referenceimage', sps, [0 0],1*[0 0 0 0]);
% datafilename = analysevideo_priorref(aviname,refframefilename,badframefilename,...
%    referencevarname,searchparamstruct,todothumbnails, verbosityarray);
% analyzevideo_priorref final pass that makes an average image. Input
% arguments are mostly the same as used in STEP 4, except the last flag
% which is todothumbnails and defaults to 1
% GKR changed searchaparamstruct with more input arguments.
% GKR added a program flag which allows the user to instruct the program to
% do/skip certain parts of the analysis (todothumbails is part of the
% array now)
% GKR added a verbosity array to allow the user to get feedback if reqd.
%
% disp('Step 5 complete');

% STEP 6
matfilename  = dir([videotoanalyse(1:end-4) '_' num2str(samplerate) '_hz_*.*']);
matfilename = matfilename.name;
load(dfs, 'frameshifts_strips_spline', 'peakratios_strips', 'stripidx','analysedframes');
% load the mat file named fn_###Hz_####.avi where fn is the original
% filename, ###Hz is the analysis frequency chosen above in STEP 4 in the
% struct, and the last four digits are some random number.
% then enter
% GKR - The analysevideo_priorref outputs the file name in which it has
% saved the variables, so we can use dfs to load the data. In the
% interest of memory space load only the reqd data rather than the entire
% file

% at this point, "maintaintimerelationships" must be set to 1 or the
% stabilization fails!. 08/23/09  - GK fixed this problem.

% makestabilizedvideo lets you apply frameshifts from another video'sfn
% matfile, using frameshifts_strips
% for comparison, this did NOT work
% svn = makestabilizedvideo([pn fn], framesforanalyses,frameshifts_strips,peakratios_strips, stripidx, 0.5, 1, 2, 0)
% two differences: use of splines, and flag to maintaintimerelationships.
% disp('All done. The entire process lasted ...');
toc
% disp('well not quite...')
% makestabilizedframe similar
% function [stabilisedframe,stabilisedframe_full] = ...
%    makestabilizedframe(videoname,framestostabilize,frameshifts,peakratios,...
%    stripindices,badsamplethreshold,maxsizeincrement,verbose)
%KSP added this to save the stabilized video
[stabilisedvideoname,stabilizedframe,stabilizedframe_full] =...
        makestabilizedvideoandframe(videotoanalyse, analysedframes,...
        frameshifts_strips_spline, peakratios_strips, stripidx,0.6 ,...
        1, 525, 1, 2.5,...
        0, 0); 
%Save reference image
imwrite(stabilizedframe(:,:,1) / 256,[videotoanalyse(1:end-4) '.jpg'],'Quality',100);

%

% disp('Now it is.')
% makeincrementmovie takes in a movie and returns the cumulative sum of
% frames
%
% makesubmovie takes in a movie and returns a subset of frames sampled on a
% square root scale, to show increment effect better
end