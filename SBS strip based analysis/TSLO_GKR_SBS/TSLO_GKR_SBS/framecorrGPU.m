% function peakvals = framecorrGPU(refframe, testframe, mask, gpuFlag, verbosity)
% function peakvals = framecorrGPU(refframe, testframe, mask, gpuFlag, verbosity)
% if nargin < 6;
%     verbosity = 0;
% end
% if nargin < 5;
%     error('not enough arguments for framecorrGPU');
% end

refft = conj(fft2(refframe));
[m n] = size(testframe);
f = 1;
if gpuFlag == 1;
    peakvals = gpuArray(zeros(numstrips,3));
%     coswindow = gpuArray(.5 - .5 * cos(2* pi * (1:stripheight)'./stripheight) * ones(1, size(testframe,2)));
    m = gpuArray(m);
    n = gpuArray(n);
    f = gpuArray(f);
else
    peakvals = (zeros(numstrips,3));
%     coswindow = (.5 - .5 * cos(2* pi * (1:stripheight)'./stripheight) * ones(1, size(testframe,2)));
end

for stripdx = 1:numstrips;
    corrarray = fftshift(ifft2(fft2(mask(:,:,stripdx) .* testframe) .* refft));
    if verbosity > 0;
        figure(103);mesh(corrarray ./ max(corrarray(:))); hold on;
    end
    corrarray = reshape(repmat(reshape(repmat(corrarray(:)',[f 1]),[m * f,n]),[f 1]),f*[m n]);
    [val, loc] = max(corrarray(:));
    [row, col] = ind2sub(size(corrarray), loc);
    snr = val ./ std(corrarray(:));
    peakvals(stripdx,:)= [[row, col]/f - size(corrarray,1)/(2*f), snr];
    if verbosity > 0;
        figure(103);
        plot3(col/f, row/f, 1,'o'); hold off;
        figure(102);imh = imagesc([mask(:,:,stripdx) .* testframe, refframe]);colormap(gray);drawnow;
        if verbosity > 1;
            pause
        end
    end
end

