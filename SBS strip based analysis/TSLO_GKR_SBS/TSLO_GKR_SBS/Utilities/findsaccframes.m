function [saccframes saccindices] = ...
    findsaccframes(rawshifts,numsamplesperframe,numsamplestosmooth,accelthresh,verbose);

if nargin < 5 | isempty(verbose)
    verbose = 1;
end

if nargin < 4 | isempty(accelthresh)
    accelthresh = 2;
end

if nargin < 3 | isempty(numsamplestosmooth)
    numsamplestosmooth = 10;
end

if nargin < 2 | isempty(numsamplesperframe) | isempty(rawshifts)
    disp('Need Raw Shifts and Indcies of Strips');
    error('Exiting...');
end

allsaccframes = [];
saccindices = [];
numsaccs = 0;
saccindex = 0;

totalsamplesinshiftdata = size(rawshifts,1);
if rem(totalsamplesinshiftdata,numsamplesperframe)
    disp('Number of samples per frame does not match the total number of samples in shift data');
    error('type ''help findsaccframes for usage');
end

smoothaddition = [0:numsamplestosmooth - 1] - floor(numsamplestosmooth / 2);

indicestosmoothover = max(repmat([1:totalsamplesinshiftdata]',1,numsamplestosmooth) +...
    repmat(smoothaddition,totalsamplesinshiftdata,1),1);
indicestosmoothover = min(indicestosmoothover,totalsamplesinshiftdata);

rawshifts_smoothed = zeros(totalsamplesinshiftdata,2);

for directioncounter = 1:2
    tempshifts = rawshifts(:,directioncounter);
    tempshifts_smoothed = mean(tempshifts(indicestosmoothover),2);
    rawshifts_smoothed(:,directioncounter) = tempshifts_smoothed(:);
end

accelaration = diff(rawshifts_smoothed,2,1);

saccindices_hori = find(abs(accelaration(:,1)) > accelthresh);
saccindices_vert = find(abs(accelaration(:,2)) > accelthresh);

saccindices = union(saccindices_vert,saccindices_hori);
saccframes = unique(ceil(saccindices / numsamplesperframe));

if verbose
    figure;
    plot(rawshifts);
    hold on;
    plot(saccindices,rawshifts(saccindices,:),'r*');
    hold off;
    
    figure;
    plot(accelaration);
    hold on;
    plot(saccindices,accelaration(saccindices,:),'r*');
    hold off;
end