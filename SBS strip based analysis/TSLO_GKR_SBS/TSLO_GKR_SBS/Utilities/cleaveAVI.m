% cleaveAVI.m is a script to create two AVI movies from one, split down the
% center.
% (c)2013 SBStevenson@uh.edu peace love trees
[origfile origpath] = uigetfile('*.avi','Choose a raw AVI file to cleave');
origOBJ = VideoReader([origpath origfile]);
splitpoint = 256;
leftfile = ['L' origfile];
rightfile = ['R' origfile];
oneframe = read(origOBJ,1);
figure(100);image(oneframe);truesize;
hold on; ph = plot(splitpoint * [1 1],[1 size(oneframe,1)], 'r-');

keepitup = 1;
while keepitup == 1;
    set(100,'name',[leftfile ' ' rightfile]);
    set(ph,'xdata', splitpoint * [1 1]);
    drawnow;
    response = inputdlg({'Left half name', 'Right half name', 'splitpoint'}, ...
        'cleavage parameters', 1, {leftfile, rightfile, num2str(splitpoint)});
    if numel(response) == 0;
        disp('User cancelled. Nothing saved');
        return;
    end
    keepitup = 0;
    if response{1} ~= leftfile
        leftfile = response{1};
        keepitup = 1;
    end
    if response{2} ~= rightfile
        rightfile = response{2};
        keepitup = 1;
    end
    if str2num(response{3}) ~= splitpoint
        splitpoint = str2num(response{3});
        keepitup = 1;
    end
end % while keepitup
leftOBJ = VideoWriter([origpath leftfile]);
open(leftOBJ);
rightOBJ = VideoWriter([origpath rightfile]);
open(rightOBJ);
numframes = origOBJ.NumberOfFrames;
% wbh = waitbar(0,'Cleaving')
for framedx = 1:numframes;
%     waitbar(framedx/numframes);
    oneframe = read(origOBJ,framedx);
    lefthalf = oneframe(:,1:splitpoint,:);
    righthalf = oneframe(:,(splitpoint+1):end,:);
    writeVideo(leftOBJ,lefthalf);
    writeVideo(rightOBJ,righthalf);
end
close(leftOBJ);
close(rightOBJ);
close(wbh);



