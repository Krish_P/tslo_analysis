function [allsaccframes numsaccs saccindex] = ...
    findsaccframes(rawshifts,stripindices,numsamplestosmooth,verbose);

if nargin < 4 | isempty(verbose)
    verbose = 0;
end

if nargin < 2 | isempty(numsamplestosmooth)
    numsamplestosmooth = 15;
end

if nargin < 2 | isempty(stripindices) | isempty(rawshifts)
    disp('Need Raw Shifts and Indcies of Strips');
    error('Exiting...');
end

allsaccframes = [];
numsaccs = 0;
saccindex = 0;

totalsamplesinshiftdata = size(rawshifts,1);
numsamplesperframe = length(stripindices);
numframes = totalsamplesinshiftdata / numsamplesperframe;

smoothaddition = [0:numsamplestosmooth - 1] - floor(numsamplestosmooth / 2);


indicestosmoothover = max(repmat([1:totalsamplesinshiftdata]',1,numsamplestosmooth) +...
    repmat(smoothaddition,totalsamplesinshiftdata,1),1);
indicestosmoothover = min(indicestosmoothover,totalsamplesinshiftdata);

rawshifts_smoothed = zeros(totalsamplesinshiftdata,2);
rawshifts_smoothed_unwraped = zeros(numsamplesperframe,numframes,2);

for directioncounter = 1:2
    tempshifts = rawshifts(:,directioncounter);
    tempshifts_smoothed = mean(tempshifts(indicestosmoothover),2);
    tempshifts_smoothed = tempshifts_smoothed(:);
    rawshifts_smoothed(:,directioncounter) = tempshifts_smoothed;
    rawshifts_smoothed_unwraped(:,:,directioncounter) = ...
        reshape(tempshifts_smoothed,numsamplesperframe,numframes);
end

framevelocity = diff(rawshifts_smoothed_unwraped,1,1);
meanframevelocity = mean(framevelocity,1);
zerovelocityaddition = framevelocity - repmat(meanframevelocity,[(numsamplesperframe  - 1) 1 1]);

rawshifts_smoothed_unwraped_zerovel = [rawshifts_smoothed_unwraped(1,:,:);zerovelocityaddition];
rawshifts_smoothed_unwraped_zerovel = cumsum(rawshifts_smoothed_unwraped_zerovel,1);

stdofshiftsinframe = squeeze(std(rawshifts_smoothed_unwraped_zerovel,0,1));
allsaccframes = [];

for directioncounter = 1:2
    stdsindirection = stdofshiftsinframe(:,directioncounter);
    meanofstdsindirection = mean(stdsindirection);
    stdofstdsindirection = std(stdsindirection);
    
    threshold = meanofstdsindirection + (2.5 * stdofstdsindirection);
    
    lowvelocityframes = find(stdsindirection < threshold);
    
    stdsindirection_lowvelocity = stdsindirection(lowvelocityframes);
    if ~isempty(lowvelocityframes)
        meanofstdsindirection = mean(stdsindirection_lowvelocity);
        stdofstdsindirection = std(stdsindirection_lowvelocity);
        threshold = meanofstdsindirection + (2.5 * stdofstdsindirection);
    end
    
    tempsaccframes = find(stdsindirection > threshold);
    allsaccframes = [allsaccframes;tempsaccframes];
end
   
allsaccframes = sort(unique(allsaccframes));
allsaccframes = allsaccframes(:);

if ~isempty(allsaccframes)
    numtotalsaccframes = length(allsaccframes);

    sacccounter = 1;
    saccindex = sacccounter;
    numsaccs = 1;
    indexcounter = 2;
    
    while indexcounter <= numtotalsaccframes
        if (allsaccframes(indexcounter - 1) + 1) == allsaccframes(indexcounter)
            saccindex = [saccindex;sacccounter];
        else
            sacccounter = sacccounter + 1;
            saccindex = [saccindex;sacccounter];
            numsaccs = numsaccs + 1;
            
        end
        indexcounter = indexcounter + 1;
    end
    
    if verbose
        indexaddition = [0:numsamplesperframe - 1];
        startingindices = (numsamplesperframe * (allsaccframes - 1));
        indicestoplot = repmat(startingindices,1,numsamplesperframe) +...
            repmat(indexaddition,size(allsaccframes,1),1);
        indicestoplot = indicestoplot(:);
        
        figure;
        plot(rawshifts);
        hold on;
        plot(indicestoplot,rawshifts(indicestoplot,:),'r*');
        hold off;
    end
end