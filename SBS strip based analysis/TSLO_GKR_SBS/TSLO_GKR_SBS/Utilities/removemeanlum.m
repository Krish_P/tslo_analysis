function varargout = removemeanlum(aviname,smoothsd,verbose);

if (nargin < 1) | isempty(aviname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stoping program');
        error('Type ''help removemeanlum'' for usage');
    end
    aviname = strcat(avipath,avifilename);
    processfullvideo = 1;
else
    if isstr(aviname)
        processfullvideo = 1;
        if ~exist(aviname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to analyse');
            if avifilename == 0
                disp('No video to analyse,stoping program');
                error('Type ''help removemeanlum'' for usage');
            end
            aviname = strcat(avipathname,avifilename);
        end
    else
        processfullvideo = 0;
        if nargout < 1
            disp('If you do not provide a video name, gaussbandfilter requires an output matrix');
            warning('Type ''help removemeanlum'' for usage');
        end
    end
end

if (nargin < 2) | isempty(smoothsd)
    smoothsd = 10;
end

if (nargin < 3) | isempty(verbose)
    verbose = 1;
end


if processfullvideo
    videoinfo = aviinfo(aviname);
    %     KSP Changes 02/07/2020
    fileinfo=VideoReader(aviname);
    videoframerate = round(videoinfo.FramesPerSecond); % The videoframerate of the video
    numframes=fileinfo.FrameRate*fileinfo.Duration;%KSP: 02/07/2020
    aviwidth = videoinfo.Width;
    aviheight = videoinfo.Height;
%     numframes = videoinfo.NumFrames;
    framerate = round(videoinfo.FramesPerSecond);
    videotype = videoinfo.ImageType;

    if strcmp(videotype,'truecolor')
        disp('Video being analyssed is a truecolor video, this program can analyse only 8 bit videos!!');
        warning('Using only the first layer of the video during analyses');
        istruecolor = 1;
    else
        istruecolor = 0;
    end

    mymap = repmat([0:255]' / 255,1,3);
    newname = strcat(aviname(1:end - 4),'_meanrem.avi');
else
    aviwidth = size(aviname,2);
    aviheight = size(aviname,1);
    numframes = size(aviname,3);

    nomeanframes = zeros(aviheight,aviwidth,numframes);
end

sumframe = zeros(aviheight,aviwidth);

% meanprog = waitbar(0,'Adding Frames');
for framecounter = 1:numframes
%     tempframe = double(frame2im(aviread(aviname,framecounter)));
    tempframe = double(frame2im(read(fileinfo,framecounter,'native')));
    if istruecolor
        tempframe = tempframe(:,:,1);
    end
    sumframe = sumframe + tempframe;

    prog = framecounter / numframes;
%     waitbar(prog,meanprog);
end

aveframe_unsmoothed = sumframe / numframes;
aveframe = smoothimage(aveframe_unsmoothed,smoothsd);

if verbose
    figure;
    image(aveframe_unsmoothed);
    colormap(gray(256));
    axis off;
    truesize;
    title('Unsmoothed Average Frame Luminance');

    figure;
    image(aveframe);
    colormap(gray(256));
    axis off;
    truesize;
    title('Smoothed Average Frame Luminance');
end

aveframe_diff = max(aveframe(:)) - min(aveframe(:));
aveframe = floor(scale(aveframe) * (aveframe_diff - 1)) + 1;

% waitbar(0,meanprog,'Subtracting Mean Frame');
for framecounter = 1:numframes
%     tempframe = double(frame2im(aviread(aviname,framecounter)));
    tempframe = double(frame2im(read(fileinfo,framecounter,'native')));
    if istruecolor
        tempframe = tempframe(:,:,1);
    end
    tempframe_nomeanframe = floor(scale(tempframe - aveframe) * 255) + 1;

    if processfullvideo
        frametoadd = im2frame(uint8(tempframe_nomeanframe),mymap);
        moviestruct(framecounter) = frametoadd;
    else
        nomeanframes(:,:,framecounter) = tempframe_nomeanframe;
    end

    prog = framecounter / numframes;
%     waitbar(prog,meanprog);
end

% close(meanprog);
if processfullvideo
%     movie2avi(moviestruct,newname,'FPS',framerate,'COMPRESSION','None');
    vOut=VideoWriter(newname,'Uncompressed AVI');open(vOut);
    writeVideo(vOut,moviestruct);
else
    varargout{1} = nomeanframes;
end


%--------------------------------------------------------------------------

function newmatrix = scale(oldmatrix);

newmatrix = oldmatrix - min(oldmatrix(:));
newmatrix = newmatrix / max(newmatrix(:));

%--------------------------------------------------------------------------