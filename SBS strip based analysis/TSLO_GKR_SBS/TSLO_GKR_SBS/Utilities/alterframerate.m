function alterframerate(videoname,newframerate)

if (nargin < 1) || isempty(videoname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to shrink');
    if avifilename == 0
        disp('No video to shrink,stoping program');
        error('Type ''help makethumbnailvideo'' for usage');
    end
    videoname = strcat(avipath,avifilename);
else
    if isstr(videoname)
        if ~exist(videoname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to shrink');
            if avifilename == 0
                disp('No video to shrink,stoping program');
                error('Type ''help makethumbnailvideo'' for usage');
            end
            videoname = strcat(avipathname,avifilename);
        end
    else
        disp('alterframerate.m requires a string that points to a video file')
        error('Type ''help alterframerate'' for usage');
    end
end

videoinfo = aviinfo(videoname);
framewidth = videoinfo.Width;
frameheight = videoinfo.Height;
numframes = videoinfo.NumFrames;
videotype = videoinfo.ImageType;
framerate = round(videoinfo.FramesPerSecond);

if strcmp(videotype,'truecolor')
    disp('Video being analysed is a truecolor video, this program can shrink only 8 bit videos!!');
    warning('Using only the first layer of the video during conversion');
    istruecolor = 1;
else
    istruecolor = 0;
end

if (nargin < 2) || isempty(newframerate)
    prompt = {'Enter the required framerate'};
    name = 'Input for alterframerate function';
    numlines = 1;
    defaultanswer = {'20'};
    
    answer = inputdlg(prompt,name,numlines,defaultanswer);
    
    if isempty(answer)
        disp('You need to enter a framerate');
        warning('Using default of half of original framerate');
        newframerate = round(framerate / 2);
    else
        newframerate = str2num(answer{1});
    end
end

moviestruct = aviread(videoname);
newname = strcat(videoname(1:end - 4),'_newfr.avi');
movie2avi(moviestruct,newname,'FPS',newframerate,'COMPRESSION','None');