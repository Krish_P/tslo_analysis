function varargout = gaussbandfilter(videoname,lowfreq,smoothsd);
% gaussbandfilter.m. This is a utility program that is designed to band
% pass filter AOSLO videos/frames. The main requirement for filtering is
% too reduce the high spatial frequency noise in the video/frame (the
% amount of filtering on the high end controlled by the smoothsd) and any
% low spatial frequency luminance artifacts/retinal structures like the
% central fovea which is usually darker than the surrounded retina that
% messes up reference image creation.
%
% Usage: [filteredframes] = gaussbandfilter(videoname,lowfreq,smoothsd);
% videoname           - the name of the video or a collection of frames that
%                     needs to be filtered. If a 3D array of frames is
%                     provided then then output array is a 3D array of same
%                     size with the filtered frames.
% lowfreq           - the lowest frequency in the filtered image in
%                     cycles/image. Default is 5
% smoothsd          - the SD (in pixels) of the guassian function that will
%                     be used to filter (smooth) the video/frame. Default
%                     is 2.
%
% filteredframes    - If a set of frames is given then the user should
%                     provide an output argument to hold the filtered
%                     frames.
%
%
% Program Creator: Girish Kumar
% Date of Completion: 02/15/08




if (nargin < 1) | isempty(videoname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stoping program');
        error('Type ''help gaussbandfilter'' for usage');
    end
    videoname = strcat(avipath,avifilename);
    processfullvideo = 1;
else
    if isstr(videoname)
        processfullvideo = 1;
        if ~exist(videoname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to analyse');
            if avifilename == 0
                disp('No video to analyse,stoping program');
                error('Type ''help gaussbandfilter'' for usage');
            end
            videoname = strcat(avipathname,avifilename);
        end
    else
        processfullvideo = 0;
        if nargout < 1
            disp('If you do not provide a video name, gaussbandfilter requires an output matrix');
            warning('Type ''help gaussbandfilter'' for usage');
        end
    end
end

if (nargin < 3) | isempty(smoothsd)
    smoothsd = 2;
end

if (nargin < 2) | isempty(lowfreq);
    lowfreq = 3;
end

if processfullvideo
    videoinfo = aviinfo(videoname);
    framewidth = videoinfo.Width;
    frameheight = videoinfo.Height;
    numframes = videoinfo.NumFrames;
    videotype = videoinfo.ImageType;
    framerate = round(videoinfo.FramesPerSecond);

    if strcmp(videotype,'truecolor')
        disp('Video being analyssed is a truecolor video, this program can analyse only 8 bit videos!!');
        warning('Using only the first layer of the video during analyses');
        istruecolor = 1;
    else
        istruecolor = 0;
    end

    mymap = repmat([0:255]' / 255,1,3);
    moviestruct = repmat(struct('cdata',zeros(frameheight,framewidth),'colormap',mymap),numframes,1);
    newname = strcat(videoname(1:end - 4),'_bandfilt.avi');
else
    framewidth = size(videoname,2);
    frameheight = size(videoname,1);
    numframes = size(videoname,3);

    filteredframes = zeros(frameheight,framewidth,numframes);
end

numpadpixels = 100;

filterwidth = framewidth + numpadpixels;
filterheight = frameheight + numpadpixels;
imagesize = [filterheight filterwidth];

filtercenter_x = floor(filterwidth / 2) + 1;
filtercenter_y = floor(filterheight / 2) + 1;

xmatrix = [0:filterwidth - 1] - floor(filterwidth / 2);
ymatrix = [0:filterheight - 1]' - floor(filterheight / 2);

indicesofinterest_x = [0:framewidth - 1] + floor((filterwidth - framewidth) / 2);
indicesofinterest_y = [0:frameheight - 1] + floor((filterheight - frameheight) / 2);

leftpadindices = [1:indicesofinterest_x(1) - 1];
rightpadindices = [indicesofinterest_x(end) + 1:filterwidth];

toppadindices = [1:indicesofinterest_y(1) - 1];
bottompadindices = [indicesofinterest_y(end) + 1:filterheight];

numleftpadindices = length(leftpadindices);
numrightpadindices = length(rightpadindices);
numtoppadindices = length(toppadindices);
numbottompadindices = length(bottompadindices);

if smoothsd > 0
    smoothgauss_x = exp(-((xmatrix .^ 2) / (2 * (smoothsd .^ 2))));
    smoothgauss_y = exp(-((ymatrix .^ 2) / (2 * (smoothsd .^ 2))));

    smoothgauss = smoothgauss_y * smoothgauss_x;
    smoothgauss = smoothgauss / sum(smoothgauss(:));
end

lowfreqsd = 1.5;

lowfreqxsign = sign(xmatrix);
lowfreqysign = sign(ymatrix);

lowfreqgauss_x = exp(-((xmatrix .^ 2) / (2 * (lowfreqsd .^ 2))));
lowfreqgauss_y = exp(-((ymatrix .^ 2) / (2 * (lowfreqsd .^ 2))));
lowfreqgauss = lowfreqgauss_y * lowfreqgauss_x;

indicesofzero_h = [-1.0 * lowfreq:lowfreq] + filtercenter_x;
indicesofzero_v = [-1.0 * lowfreq:lowfreq] + filtercenter_y;
indicesofzero = sub2ind(imagesize,repmat(indicesofzero_h,length(indicesofzero_v),1),...
    repmat(indicesofzero_v',1,length(indicesofzero_h)));
indicesofnonzero = setdiff([1:prod(imagesize)]',indicesofzero(:));
indicesofnonzero_max = max(lowfreqgauss(indicesofnonzero));
indicesofnonzero_min = min(lowfreqgauss(indicesofnonzero));

lowfreqgauss(indicesofzero) = 0;
lowfreqgauss(indicesofnonzero) = (lowfreqgauss(indicesofnonzero) - indicesofnonzero_min) ./...
    indicesofnonzero_max;
lowfreqgauss = scale(lowfreqgauss * -1.0);
lowfreqgauss(filtercenter_y,filtercenter_x) = 1;

if smoothsd > 0
    filter_fft = ifftshift(fftshift(fft2(smoothgauss)) .* lowfreqgauss);
else
    filter_fft = ifftshift(lowfreqgauss);
end

% filterprog = waitbar(0,'Filtering');
for framecounter = 1:numframes
    if processfullvideo
        tempframe = double(frame2im(aviread(videoname,framecounter)));
        if istruecolor
            tempframe = tempframe(:,:,1);
        end
    else
        tempframe = videoname(:,:,framecounter);
    end

    fullframe = zeros(filterheight,filterwidth) + mean(tempframe(:));

    fullframe(indicesofinterest_y,leftpadindices) = repmat(tempframe(:,3),1,...
        numleftpadindices);
    fullframe(indicesofinterest_y,rightpadindices) = repmat(tempframe(:,end - 2),1,...
        numrightpadindices);
    fullframe(toppadindices,indicesofinterest_x) = repmat(tempframe(3,:),...
        numtoppadindices,1);
    fullframe(bottompadindices,indicesofinterest_x) = repmat(tempframe(end - 2,:),...
        numbottompadindices,1);

    fullframe(indicesofinterest_y,indicesofinterest_x) = tempframe;

    if smoothsd > 0
        newframe = real(ifftshift(ifft2(fft2(fullframe) .* filter_fft)));
    else
        newframe = real(ifft2(fft2(fullframe) .* filter_fft));
    end

    frametoadd = newframe(indicesofinterest_y,indicesofinterest_x);

    if processfullvideo
        moviestruct(framecounter).cdata = uint8(floor(scale(frametoadd) * 255) + 1);
    else
        filteredframes(:,:,framecounter) = frametoadd;
    end

    prog = framecounter / numframes;
%     waitbar(prog,filterprog);
end
close(filterprog);

if processfullvideo
    movie2avi(moviestruct,newname,'FPS',framerate,'COMPRESSION','None');
else
    varargout{1} = filteredframes;
end


%--------------------------------------------------------------------------

function newmatrix = scale(oldmatrix);

newmatrix = oldmatrix - min(oldmatrix(:));
newmatrix = newmatrix / max(newmatrix(:));

%--------------------------------------------------------------------------