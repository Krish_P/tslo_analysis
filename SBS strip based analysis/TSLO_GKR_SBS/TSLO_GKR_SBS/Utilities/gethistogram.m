function [allhistograms,averagehistogram,bincenters] = gethistogram(videoname,verbose)

if (nargin < 1) || isempty(videoname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to scale');
    if avifilename == 0
        disp('No video to scale,stoping program');
        error('Type ''help gethistogram'' for usage');
    end
    videoname = strcat(avipath,avifilename);
    processfullvideo = 1;
else
    if ischar(videoname)
        processfullvideo = 1;
        if ~exist(videoname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to scale');
            if avifilename == 0
                disp('No video to scale,stoping program');
                error('Type ''help gethistogram'' for usage');
            end
            videoname = strcat(avipathname,avifilename);
        end
    else
        processfullvideo = 0;
    end
end

if (nargin < 2) || isempty(verbose)
    verbose = 1;
end

if processfullvideo
    videoinfo = aviinfo(videoname);
    framewidth = videoinfo.Width;
    frameheight = videoinfo.Height;
    numframes = videoinfo.NumFrames;
    videotype = videoinfo.ImageType;
    framerate = round(videoinfo.FramesPerSecond);

    if strcmp(videotype,'truecolor')
        disp('Video being analysed is a truecolor video, this program can scale only 8 bit videos!!');
        warning('Using only the first layer of the video during scaling');
        istruecolor = 1;
    else
        istruecolor = 0;
    end
else
    istruecolor = 0;
    framewidth = size(videoname,2);
    frameheight = size(videoname,1);
    numframes = size(videoname,3);
end

bincenters = [5:10:260]';
numbins = length(bincenters);

allhistograms = zeros(numbins,numframes,2);
averagehistogram = zeros(numbins,1,2);

% histprog = waitbar(0,'Getting Frame Histograms');
for framecounter = 1:numframes
    if processfullvideo
        currentframe = double(frame2im(aviread(videoname,framecounter)));
        
        if istruecolor
            currentframe = currentframe(:,:,1);
        end
    else
        currentframe = videoname(:,:,framecounter);
    end
    
    framehist = hist(currentframe(:),bincenters);
    allhistograms(:,framecounter,1) =  framehist(:);
    allhistograms(:,framecounter,2) = framehist(:) / numel(currentframe);
    
    prog = framecounter / numframes;
%     waitbar(prog,histprog);
end

close(histprog);

averagehistogram = mean(allhistograms,2);

if verbose
    figure;
    plot(bincenters,allhistograms(:,:,2));
    
    figure
    plot(bincenters,averagehistogram(:,:,2));
    hold on;
    plot(bincenters,averagehistogram(:,:,2),'*');
    hold off;
end