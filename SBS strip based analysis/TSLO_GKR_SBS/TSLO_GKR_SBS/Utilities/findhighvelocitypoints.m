function [highvelocitydatapoints, highvelocityframes,directionaldata] =...
    findhighvelocitypoints(rawvelocity,analysedframes,numstrips,threshold,numsamplestosmooth);

if nargin < 5
    numsamplestosmooth = 15;
end

if nargin < 4
    threshold = 10;
end

if nargin < 3
    disp('Inadequate number of input arguments');
    error('Exiting...');
end

numsamples = size(rawvelocity,1);

smoothaddition = [0:numsamplestosmooth - 1] - floor(numsamplestosmooth / 2);

indicestosmooth = repmat([1:numsamples]',1,numsamplestosmooth) +...
    repmat(smoothaddition,numsamples,1);
indicestosmooth = max(indicestosmooth,1); 
indicestosmooth = min(indicestosmooth,numsamples);

highvelocitydatapoints = [];
highvelocityframes = [];
directionaldata = cell(2,1);

highvelocitydatapoints_directional = cell(2,1);
highvelocityframes_directional = cell(2,1);

for directioncounter = 1:2
    tempvel_unsmoothed = rawvelocity(:,directioncounter);
    tempvel = mean(tempvel_unsmoothed(indicestosmooth),2);
    tempvel = abs(tempvel(:)); 
    temphighvelocitydatapoints = find(tempvel > threshold);
    temphighvelocitydatapoints = temphighvelocitydatapoints(:);
    
    highvelocitydatapoints = [highvelocitydatapoints;temphighvelocitydatapoints];
    highvelocitydatapoints_directional{directioncounter} = temphighvelocitydatapoints;
    highframevelocityindex = unique(ceil(temphighvelocitydatapoints / numstrips));
    highframevelocityindex = highframevelocityindex(:);
    highvelocityframes_directional{directioncounter} = [highframevelocityindex,analysedframes(highframevelocityindex)];
end

highvelocitydatapoints = unique(highvelocitydatapoints);
highframevelocityindex = unique(ceil(highvelocitydatapoints / numstrips));
highframevelocityindex = highframevelocityindex(:);
highvelocityframes = [highframevelocityindex,analysedframes(highframevelocityindex)];
directionaldata{1} = highvelocitydatapoints_directional;
directionaldata{2} = highvelocityframes_directional;