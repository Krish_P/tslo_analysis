function newvideoname = deinterlacevideo(oldvideoname)

if (nargin < 1) || isempty(oldvideoname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to de-interlace');
    if avifilename == 0
        disp('No video to de-interlace,stoping program');
        error('Type ''help deinterlacevideo'' for usage');
    end
    oldvideoname = strcat(avipath,avifilename);
else
    if ischar(oldvideoname)
        if ~exist(oldvideoname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to de-interlace');
            if avifilename == 0
                disp('No video to de-interlace,stoping program');
                error('Type ''help deinterlacevideo'' for usage');
            end
            oldvideoname = strcat(avipathname,avifilename);
        end
    else
        disp('deinterlacevideo.m requires a string that points to a video file')
        error('Type ''help deinterlacevideo'' for usage');
    end
end

videoinfo = aviinfo(oldvideoname);
framewidth = videoinfo.Width;
frameheight = videoinfo.Height;
numframes = videoinfo.NumFrames;
videotype = videoinfo.ImageType;
framerate = round(videoinfo.FramesPerSecond);

if strcmp(videotype,'truecolor')
    disp('Video being analysed is a truecolor video, this program can de-interlace only 8 bit videos!!');
    warning('Using only the first layer of the video during conversion');
    istruecolor = 1;
else
    istruecolor = 0;
end

oddlineindices = [1:2:frameheight];
evenlineindices = [2:2:frameheight];

mymap = repmat([0:255]'/ 256,1,3);
moviestruct = repmat(struct('cdata',zeros(frameheight,framewidth),'colormap',mymap),numframes * 2,1);
newvideoname = strcat(oldvideoname(1:end - 4),'_deinterlaced.avi');

% deinterlaceprog = waitbar(0,'De-interlacing the video');
for framecounter = 1:numframes
    currentframe = double(frame2im(aviread(oldvideoname,framecounter)));

    if istruecolor
        currentframe = currentframe(:,:,1);
    end
       
    oddlineframe = currentframe(oddlineindices,:);
    evenlineframe = currentframe(evenlineindices,:);
    
    oddframenumbertoadd = (2 * (framecounter - 1)) + 1;
    evenframenumbertoadd = 2 * framecounter;
    
    moviestruct(oddframenumbertoadd).cdata = oddlineframe;
    moviestruct(evenframenumbertoadd).cdata = evenlineframe;
    
    prog = framecounter / numframes;
%     waitbar(prog,deinterlaceprog);
end

close(deinterlaceprog);

movie2avi(moviestruct,newvideoname,'Compression','None','FPS',framerate);