clear all;
close all;

[fname pname] = uigetfile('*.mat');
datafiletoload = strcat(pname,fname);

badstripaddition = [-2:2];
numbadstripindices = length(badstripaddition);

load(datafiletoload,'frameshifts_strips','peakdiffs_strips','stripidx','traceformovie');

numsamples = length(frameshifts_strips);
numstrips = length(stripidx);
numframes = numsamples / numstrips;

numframestoinclude = 10;
numsamplestoinclude = 525 * numframestoinclude;
totallength = 525 * numframes;
numsegments = ceil(totallength / numsamplestoinclude);
sizeofbuffer = (numsamplestoinclude * numsegments) - totallength;

if sizeofbuffer ~= 0
    basecos = repmat(scale(cos(2 * pi * 0.25 * [0:sizeofbuffer - 1]' / sizeofbuffer)),1,2);
end

totalsamplestofilter = numsamplestoinclude;
totaltime = totalsamplestofilter / (30 * 525);
filtersd = 1;

fullfiltersize = totalsamplestofilter;
filtercenter = floor(fullfiltersize / 2) + 1;
usablefiltersize = fullfiltersize - filtercenter;
maxtf = usablefiltersize / totaltime;
maxmultiple = floor(maxtf / 30)
filter = zeros(1,fullfiltersize);
nummeans = maxmultiple;
filterxvalues_onehalf = [0:usablefiltersize];
filterxvalues = [fliplr(filterxvalues_onehalf(2:end)),filterxvalues_onehalf];
if length(filterxvalues) < fullfiltersize
    filterxvalues = [usablefiltersize + 1,filterxvalues];
end
for meancounter = 1:nummeans
    filtermean = ((30 * meancounter) / maxtf) * usablefiltersize;
    tempfilter = ifftshift(scale(exp(-(((filterxvalues - filtermean) .^ 2) /...
        (2 * (filtersd .^ 2))))));
    filter = tempfilter + filter;
end
singlefilter = filter';
filter = repmat(singlefilter,1,numsegments);
singlefilter = fftshift(singlefilter');
badstrips_reallybad = find(peakdiffs_strips <= 0.35);
numbadstrips = length(badstrips_reallybad);


badsamples = repmat(badstrips_reallybad(:),1,numbadstripindices) +...
    repmat(badstripaddition,numbadstrips,1);
badsamples = unique(max(min(badsamples(:),numsamples),1));
goodsamples = setdiff([1:numsamples]',badsamples);
numgoodsamples = length(goodsamples);

if length(badsamples) >= 1
    interp_xaxis = [0:numsamples - 1]';
    samples_xaxis = interp_xaxis(goodsamples);
    
    frameshifts_strips_spline = zeros(numsamples,2);
    for directioncounter = 1:2
        sample_yaxis = frameshifts_strips(goodsamples,directioncounter);
        frameshifts_strips_spline(:,directioncounter) =...
            interp1(samples_xaxis,sample_yaxis,interp_xaxis,'linear','extrap');
    end
else
    frameshifts_strips_spline = frameshifts_strips;
end

frameshifts = zeros(numstrips,numframes,2);

for directioncounter = 1:2
    tempshifts = reshape(frameshifts_strips_spline(:,directioncounter),numstrips,numframes);
    frameshifts(:,:,directioncounter) = tempshifts;
end

velocity = diff(frameshifts,1,1);
meanvelocity = repmat(mean(velocity,1),[numstrips - 1,1,1]);
zeroedvelocity = velocity - meanvelocity;
frameshifts_zerovel = cumsum([frameshifts(1,:,:);zeroedvelocity],1);
meanposition = repmat(mean(frameshifts_zerovel,1),[numstrips,1,1]);

frameshifts_zerovel_zeropos = frameshifts_zerovel - meanposition;

positiontoaddback = squeeze(mean(frameshifts_zerovel,1));
velocitytoaddback = squeeze(mean(velocity,1));

newshifts = zeros(numsamples,2);
for directioncounter = 1:2
    tempshifts = frameshifts_zerovel_zeropos(:,:,directioncounter);
    newshifts(:,directioncounter) = tempshifts(:);
end

interp_xaxis = [0:totallength - 1];
interp_xaxis = reshape(interp_xaxis,525,numframes);
sample_xaxis = interp_xaxis(stripidx,:);

interp_xaxis = interp_xaxis(:);
sample_xaxis = sample_xaxis(:);

fullshifts = zeros(totallength,2);

for directioncounter = 1:2
    sample_yaxis = newshifts(:,directioncounter);
    fullshifts(:,directioncounter,:) =...
        interp1(sample_xaxis,sample_yaxis,interp_xaxis,'linear','extrap');
end

if sizeofbuffer ~= 0
    cosbuffer = basecos .* repmat(fullshifts(end,:),sizeofbuffer,1);
else
    cosbuffer = [];
end

shiftstofilter = [fullshifts;cosbuffer];
filteredshifts = zeros(size(shiftstofilter));

for directioncounter = 1:2
    tempshifts = (reshape(shiftstofilter(:,directioncounter),numsamplestoinclude,numsegments));
    filteredtempshifts = (real(ifft((fft(tempshifts,[],1) .* filter),[],1)));
    filteredshifts(:,directioncounter) = filteredtempshifts(:);
end

filteredshifts = filteredshifts(1:totallength,:);

newshifts_zerovel_zeropos = zeros(numstrips,numframes,2);
for directioncounter = 1:2
    tempshifts = reshape(filteredshifts(:,directioncounter),525,numframes);
    newshifts_zerovel_zeropos(:,:,directioncounter) =...
        tempshifts(stripidx,:);
end

newshifts_zerovel = newshifts_zerovel_zeropos + meanposition;
tempvelocity = diff(newshifts_zerovel,1,1);
actualvelocity = tempvelocity + meanvelocity;
frameshifts_strips_dcrem_unwraped = cumsum([newshifts_zerovel(1,:,:);actualvelocity],1);

frameshifts_strips_dcrem = zeros(numsamples,2);
for directioncounter = 1:2
    tempshifts = frameshifts_strips_dcrem_unwraped(:,:,directioncounter);
    frameshifts_strips_dcrem(:,directioncounter) = tempshifts(:);
end

figure;
plot(frameshifts_strips_dcrem);

figure;
plot(frameshifts_strips_spline);