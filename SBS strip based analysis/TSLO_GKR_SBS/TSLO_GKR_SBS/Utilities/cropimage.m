function [croppedimage,varargout] = cropimage(imagetocrop,verbose);
% cropimage.m. This file is a utility program written to crop image to
% desired size.
% Usage: [croppedimage,[imageborders]] = cropimage(imagetocrop,[verbose]);
% imagetocrop   - The original image that needs to be croppped.
% verbose       - If verbose is set to one then the program displays the cropped
%                 image. Default is zero.
%
% croppedimage  - The croppped image.
% imageborders  - This arrays stores the borders of the original image in
%                 case they are needed.
%
%
% Program Creator: Girish Kumar
% Date of Completion: 11/05/07

if nargin < 1
    disp('cropimage.m requires atleast one input argument');
    error('Type ''help filterframe'' for usage');
end


if nargin < 2
    verbose = 0;
end

% Get the size of the original image
origimagesize_hori = size(imagetocrop,2);
origimagesize_vert = size(imagetocrop,1);

% Display the image
imagefigure = figure;
image(imagetocrop);
colormap(gray(256));
truesize;

% Go through s loop that asks thr user to select the appropriate border,
% the name in the figure window as well as the title of the image informs
% the user what is the current border that need to be selected

borders = zeros(4,1);
for bordercounter = 1:4
    switch bordercounter
        case 1
            figure(imagefigure);
            set(imagefigure,'Name','Left Border');
            title('Left Border');
        case 2
            figure(imagefigure);
            set(imagefigure,'Name','Right Border');
            title('Right Border');
        case 3
            figure(imagefigure);
            set(imagefigure,'Name','Top Border');
            title('Top Border');
        case 4
            figure(imagefigure);
            set(imagefigure,'Name','Bottom Border');
            title('Bottom Border');
    end
    
    [x y] = ginput(1);
    if bordercounter <= 2
        x = round(x);
        borders(bordercounter) = x;
    end
    
    if bordercounter > 2
        y = round(y);
        borders(bordercounter) = y;
    end
end

% Error Check the borders to ensure we do look for funny image indices
if borders(1) > borders(2)
    warning('Left Border cannot smaller than Right border, interchanging values');
    temp = borders(1);
    borders(1) = borders(2)
    borders(2) = temp;
end

if borders(3) > borders(4)
    warning('Top Border cannot smaller than Bottom border, interchanging values');
    temp = borders(3);
    borders(3) = borders(4)
    borders(4) = temp;
end

borders(1) = max(borders(1),1);
borders(2) = min(borders(2),origimagesize_hori);

borders(3) = max(borders(3),1);
borders(4) = min(borders(4),origimagesize_vert);

imagesize_hori = borders(2) - borders(1) + 1;
imagesize_vert = borders(4) - borders(3) + 1;

croppedimage = imagetocrop(borders(3):borders(4),borders(1):borders(2));

% Return the borders if asked for
if nargout == 2
    varargout{1} = borders;
end

% Show the original image with the cropped border marked if asked for
% otherwise close the figure window
if verbose
    figure(imagefigure);
    hold on;
    title('Cropped Border');

    plot([borders(1):borders(2)],zeros(imagesize_hori) + borders(3),'r');
    plot([borders(1):borders(2)],zeros(imagesize_hori) + borders(4),'r');
    plot(zeros(imagesize_vert) + borders(1),[borders(3):borders(4)],'r');
    plot(zeros(imagesize_vert) + borders(2),[borders(3):borders(4)],'r');
    hold off;
else
    close(imagefigure);
end