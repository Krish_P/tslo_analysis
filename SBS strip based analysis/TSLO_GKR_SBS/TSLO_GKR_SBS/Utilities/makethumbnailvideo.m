function varargout = makethumbnailvideo(videoname,thumbnailfactor);

if (nargin < 1) | isempty(videoname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to shrink');
    if avifilename == 0
        disp('No video to shrink,stoping program');
        error('Type ''help makethumbnailvideo'' for usage');
    end
    videoname = strcat(avipath,avifilename);
    processfullvideo = 1;
else
    if isstr(videoname)
        processfullvideo = 1;
        if ~exist(videoname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to shrink');
            if avifilename == 0
                disp('No video to shrink,stoping program');
                error('Type ''help makethumbnailvideo'' for usage');
            end
            videoname = strcat(avipathname,avifilename);
        end
    else
        processfullvideo = 0;
        if nargout < 1
            disp('If you do not provide a video name, makethumbnailvideo requires an output matrix');
            warning('Type ''help makethumbnailvideo'' for usage');
        end
    end
end

if processfullvideo
    videoinfo = aviinfo(videoname);
    framewidth = videoinfo.Width;
    frameheight = videoinfo.Height;
    numframes = videoinfo.NumFrames;
    videotype = videoinfo.ImageType;
    framerate = round(videoinfo.FramesPerSecond);

    if strcmp(videotype,'truecolor')
        disp('Video being analysed is a truecolor video, this program can shrink only 8 bit videos!!');
        warning('Using only the first layer of the video during shrinking');
        istruecolor = 1;
    else
        istruecolor = 0;
    end

    mymap = repmat([0:255]' / 255,1,3);
    moviestruct = repmat(struct('cdata',zeros(frameheight,framewidth),'colormap',mymap),numframes,1);
    newname = strcat(videoname(1:end - 4),'_thumb.avi');
else
    framewidth = size(videoname,2);
    frameheight = size(videoname,1);
    numframes = size(videoname,3);

    smallframes = zeros(frameheight,framewidth,numframes);
end

if (nargin < 2) | isempty(thumbnailfactor)
    prompt = {'Enter the required Thumbnail Factor'};
    name = 'Input for makethumbnailvideo function';
    numlines = 1;
    defaultanswer = {'2'};
    
    answer = inputdlg(prompt,name,numlines,defaultanswer);
    
    if isempty(answer)
        disp('You need to enter a thumbnail factor');
        warning('Using default of 2');
        thumbnailfactor = 2;
    else
        thumbnailfactor = str2num(answer{1});
    end
end

% shrinkprog = waitbar(0,'Shrinking');
for framecounter = 1:numframes
    if processfullvideo
        tempframe = double(frame2im(aviread(videoname,framecounter)));
        if istruecolor
            tempframe = tempframe(:,:,1);
        end
    else
        tempframe = videoname(:,:,framecounter);
    end
    
    newframe = downsample(tempframe,thumbnailfactor,thumbnailfactor);
    
    if processfullvideo
        moviestruct(framecounter).cdata = uint8(floor(newframe) + 1);
    else
        smallframes(:,:,framecounter) = newframe;
    end

    prog = framecounter / numframes;
%     waitbar(prog,shrinkprog);
end

close(shrinkprog);

if processfullvideo
    movie2avi(moviestruct,newname,'FPS',framerate,'COMPRESSION','None');
else
    varargout{1} = smallframes;
end


%--------------------------------------------------------------------------

function newmatrix = scale(oldmatrix);

newmatrix = oldmatrix - min(oldmatrix(:));
newmatrix = newmatrix / max(newmatrix(:));

%--------------------------------------------------------------------------