function medianfilter(aviname,windowsize)

if nargin < 2
    windowsize = 5;
end

if nargin < 1 | ~isstr(aviname) | ~exist(aviname,'file')
    [fname pname] = uigetfile('*.avi','Please chose video to filter');
    aviname = strcat(pname,fname);
end

videoinfo = aviinfo(aviname);
aviwidth = videoinfo.Width;
aviheight = videoinfo.Height;
numframes = videoinfo.NumFrames;
framerate = round(videoinfo.FramesPerSecond);

totalwindowpixels = windowsize .^ 2;
startindices_x = repmat([1 floor(aviwidth / 3) floor(2 * aviwidth / 3)],3,1);
endindices_x = [startindices_x(:,2:end) + 1,repmat(aviwidth,3,1)];
startindices_x = startindices_x(:)
endindices_x = endindices_x(:)

startindices_y = [1 floor(aviheight / 3) floor(2 * aviheight / 3)]';
endindices_y = [startindices_y(2:end);aviheight];
startindices_y = repmat(startindices_y,3,1)
endindices_y = repmat(endindices_y,3,1)

mymap = repmat([0:255]' / 255,1,3);
moviestruct = repmat(struct('cdata',zeros(aviheight,aviwidth),'colormap',mymap),numframes,1);
newname = strcat(aviname(1:end - 4),'_medfilt.avi');

totalsegments = 9 * numframes;
% filterprog = waitbar(0,'Applying Median Filter');
for framecounter = 1:numframes
    currentframe = double(frame2im(aviread(aviname,framecounter)));
    newframe = zeros(aviheight,aviwidth);
    
    for segmentcounter = 1:9;
        start_x = startindices_x(segmentcounter);
        end_x = endindices_x(segmentcounter);

        start_y = startindices_y(segmentcounter);
        end_y = endindices_y(segmentcounter);
        
        totalxs = end_x - start_x + 1;
        totalys = end_y - start_y + 1;
        totalpixels = totalxs * totalys;

        baseindices_x = repmat([start_x:end_x],totalys,1);
        baseindices_x = repmat(baseindices_x(:),1,totalwindowpixels);

        addition_x = repmat([0:windowsize - 1] - floor(windowsize / 2),windowsize,1);
        addition_x = repmat(addition_x(:)',totalpixels,1);
        indices_x = min(max((baseindices_x + addition_x),1),aviwidth);

        baseindices_y = repmat(repmat([start_y:end_y]',totalxs,1),1,totalwindowpixels);
        addition_y = repmat(repmat([0:windowsize - 1]' - floor(windowsize / 2),windowsize,1)',...
            totalpixels,1);
        indices_y = min(max((baseindices_y + addition_y),1),aviheight);

        indices = sub2ind([aviheight aviwidth],indices_y,indices_x);
        medianvalues = median(currentframe(indices),2);
        newframe(start_y:end_y,start_x:end_x) = reshape(medianvalues,totalys,totalxs);
        
        prog = (((framecounter - 1) * 9) + segmentcounter) / totalsegments;
%         waitbar(prog,filterprog);
    end
    moviestruct(framecounter).cdata = uint8(floor(scale(newframe) * 255) + 1);
end

close(filterprog);
movie2avi(moviestruct,newname,'FPS',framerate,'COMPRESSION','None');


%--------------------------------------------------------------------------

function newmatrix = scale(oldmatrix)
newmatrix = oldmatrix - min(oldmatrix(:));
newmatrix = newmatrix / max(newmatrix(:));

%--------------------------------------------------------------------------