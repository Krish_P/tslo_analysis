function varargout = removelowfreqs(aviname,lowfreq);
% gaussbandfilter.m. This is a utility program that is designed to band
% pass filter AOSLO videos/frames. The main requirement for filtering is
% too reduce the high spatial frequency noise in the video/frame (the 
% amount of filtering on the high end controlled by the smoothsd) and any
% low spatial frequency luminance artifacts/retinal structures like the
% central fovea which is usually darker than the surrounded retina that
% messes up reference image creation.
% 
% Usage: [filteredframes] = gaussbandfilter(aviname,lowfreq,smoothsd);
% aviname           - the name of the video or a collection of frames that
%                     needs to be filtered. If a 3D array of frames is
%                     provided then then output array is a 3D array of same
%                     size with the filtered frames.
% lowfreq           - the lowest frequency in the filtered image in
%                     cycles/image. Default is 5
%
% filteredframes    - If a set of frames is given then the user should
%                     provide an output argument to hold the filtered
%                     frames.
%
%
% Program Creator: Girish Kumar
% Date of Completion: 02/15/08



if (nargin < 1) | isempty(aviname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stoping program');
        error('Type ''help gaussbandfilter'' for usage');
    end
    aviname = strcat(avipath,avifilename);
    processfullvideo = 1;
else
    if isstr(aviname)
        processfullvideo = 1;
        if ~exist(aviname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to analyse');
            if avifilename == 0
                disp('No video to analyse,stoping program');
                error('Type ''help gaussbandfilter'' for usage');
            end
            aviname = strcat(avipathname,avifilename);
        end
    else
        processfullvideo = 0;
        if nargout < 1
            disp('If you do not provide a video name, gaussbandfilter requires an output matrix');
            warning('Type ''help gaussbandfilter'' for usage');
        end
    end
end

if (nargin < 2) | isempty(lowfreq);
    lowfreq = 2;
end

if processfullvideo
    videoinfo = aviinfo(aviname);
    aviwidth = videoinfo.Width;
    aviheight = videoinfo.Height;
    numframes = videoinfo.NumFrames;
    framerate = round(videoinfo.FramesPerSecond);

    mymap = repmat([0:255]' / 255,1,3);
    moviestruct = repmat(struct('cdata',zeros(aviheight,aviwidth),'colormap',mymap),numframes,1);
    newname = strcat(aviname(1:end - 4),'_nolwfqfilt.avi');
else
    aviwidth = size(aviname,2);
    aviheight = size(aviname,1);
    numframes = size(aviname,3);

    filteredframes = zeros(aviheight,aviwidth,numframes);
end

numpadpixels = 100;

framewidth = aviwidth + numpadpixels;
frameheight = aviheight + numpadpixels;

framecenter_x = floor(framewidth / 2) + 1;
framecenter_y = floor(frameheight / 2) + 1;

xmatrix = [0:framewidth - 1] - floor(framewidth / 2);
ymatrix = [0:frameheight - 1]' - floor(frameheight / 2);

indicesofinterest_x = [0:aviwidth - 1] + floor((framewidth - aviwidth) / 2);
indicesofinterest_y = [0:aviheight - 1] + floor((frameheight - aviheight) / 2);

leftpadindices = [1:indicesofinterest_x(1) - 1];
rightpadindices = [indicesofinterest_x(end) + 1:framewidth];

toppadindices = [1:indicesofinterest_y(1) - 1];
bottompadindices = [indicesofinterest_y(end) + 1:frameheight];

numleftpadindices = length(leftpadindices);
numrightpadindices = length(rightpadindices);
numtoppadindices = length(toppadindices);
numbottompadindices = length(bottompadindices);

lowfreqxsign = sign(xmatrix);
lowfreqysign = sign(ymatrix);

lowfreqxmatrix = (max(abs(xmatrix),lowfreq) - lowfreq) .* lowfreqxsign;
lowfreqymatrix = (max(abs(ymatrix),lowfreq) - lowfreq) .* lowfreqysign;

lowfreqsd = 1.5;

lowfreqgauss_x = exp(-((lowfreqxmatrix .^ 2) / (2 * (lowfreqsd .^ 2))));
lowfreqgauss_y = exp(-((lowfreqymatrix .^ 2) / (2 * (lowfreqsd .^ 2))));
lowfreqgauss = scale((lowfreqgauss_y * lowfreqgauss_x) * -1.0);
lowfreqgauss(framecenter_y,framecenter_x) = 1;

filter_fft = ifftshift(lowfreqgauss);

% filterprog = waitbar(0,'Filtering');
for framecounter = 1:numframes
    if processfullvideo
        tempframe = double(frame2im(aviread(aviname,framecounter)));
    else
        tempframe = aviname(:,:,framecounter);
    end

    fullframe = zeros(frameheight,framewidth) + mean(tempframe(:));

    fullframe(indicesofinterest_y,leftpadindices) = repmat(tempframe(:,3),1,...
        numleftpadindices);
    fullframe(indicesofinterest_y,rightpadindices) = repmat(tempframe(:,end - 2),1,...
        numrightpadindices);
    fullframe(toppadindices,indicesofinterest_x) = repmat(tempframe(3,:),...
        numtoppadindices,1);
    fullframe(bottompadindices,indicesofinterest_x) = repmat(tempframe(end - 2,:),...
        numbottompadindices,1);

    fullframe(indicesofinterest_y,indicesofinterest_x) = tempframe;

    newframe = real(ifft2(fft2(fullframe) .* filter_fft));
    frametoadd = newframe(indicesofinterest_y,indicesofinterest_x);

    if processfullvideo
        moviestruct(framecounter).cdata = uint8(floor(scale(frametoadd) * 255) + 1);
    else
        filteredframes(:,:,framecounter) = frametoadd;
    end

    prog = framecounter / numframes;
%     waitbar(prog,filterprog);
end
close(filterprog);

if processfullvideo
    movie2avi(moviestruct,newname,'FPS',framerate,'COMPRESSION','None');
else
    varargout{1} = filteredframes;
end


%--------------------------------------------------------------------------

function newmatrix = scale(oldmatrix);

newmatrix = oldmatrix - min(oldmatrix(:));
newmatrix = newmatrix / max(newmatrix(:));

%--------------------------------------------------------------------------