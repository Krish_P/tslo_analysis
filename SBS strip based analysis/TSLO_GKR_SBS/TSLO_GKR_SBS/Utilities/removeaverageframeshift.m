function [newshifts runningaverage] = removeaverageframeshift(oldshifts,numframes,...
    numframestoaverage,gausssd,saccframes,accelthresh,meanframevelthresh);

if nargin < 6 | isempty(meanframevelthresh)
    meanframevelthresh = 1;
end

if nargin < 6 | isempty(accelthresh)
    accelthresh = 10;
end

if nargin < 5 | isempty(saccframes)
    saccframes = [];
end
    
if nargin < 4 | isempty(gausssd)
    gausssd = 5;
end

if nargin < 3
    numframestoaverage = 10;
end

if nargin < 2 | isempty(numframes);
    disp('Need number of frames');
    error('Exiting ...');
end

if nargin < 1 | isempty(oldshifts)
    disp('Need old frameshift data');
    error('Exiting ...');
end

numsamplesperframe = length(oldshifts) / numframes;
if rem(numsamplesperframe,1) ~= 0
    disp('Total Number of Samples mismatch with number of frames');
    error('Exiting ...');
end

if ~isempty(saccframes)
    tempsaccframes = saccframes(:);
    numtempsaccframes = length(saccframes);
    
    saccframesaddition = [-1:1];
    numsaccaddition = length(saccframesaddition);
    
    saccframes = repmat(tempsaccframes,1,numsaccaddition) +...
        repmat(saccframesaddition,numtempsaccframes,1);
    saccframes = unique(max(min(saccframes(:),numframes),1));
    
    if length(saccframes) == numframes
        newshifts = oldshifts;
        runningaverage = zeros(size(oldshifts));
        disp('Too Many Saccade frame, Unable to calculate average frameshift');
        return
    end
    
    nonsaccframes = sort(setdiff([1:numframes]',saccframes),1,'ascend');
    
    numsaccframes = length(saccframes);
    numnonsaccframes = length(nonsaccframes);
    
    prevnonsaccframe = [];
    for saccframecounter = 1:numsaccframes
        lastgoodframe = max(find(nonsaccframes < saccframes(saccframecounter)));
        if isempty(lastgoodframe)
            lastgoodframe = nonsaccframes(1);
        end
        prevnonsaccframe = [prevnonsaccframe;lastgoodframe];
    end
    
    nonsaccframes_diff = [1;diff(nonsaccframes)];
    breakpoints = find(nonsaccframes_diff > 1);
    
    nonsaccstartframes = [nonsaccframes(1);nonsaccframes(breakpoints)];
    nonsaccendframes = [nonsaccframes(breakpoints - 1);nonsaccframes(end);];
else
    nonsaccframes = [1:numframes]';
    nonsaccstartframes = 1;
    nonsaccendframes = numframes;
end

numsaccdivisions = length(nonsaccendframes);
numnonsaccframes = length(nonsaccframes);

for directioncounter = 1:2
    oldshifts_unwraped(:,:,directioncounter) = ...
        reshape(oldshifts(:,directioncounter),numsamplesperframe, numframes);
end

oldshifts_unwraped_nonsacc = oldshifts_unwraped(:,nonsaccframes,:);

velocity = diff(oldshifts_unwraped_nonsacc,1,1);
meanframevel = mean(velocity,1);
lowvelindices = find(abs(meanframevel) < meanframevelthresh);
meanframevel(lowvelindices) = 0;
meanframevel = repmat(meanframevel,[(numsamplesperframe - 1) 1 1]);

oldshifts_unwraped_nonsacc_zerovel =...
    cumsum([oldshifts_unwraped_nonsacc(1,:,:);velocity - meanframevel],1);

meanshiftineachframe = mean(oldshifts_unwraped_nonsacc_zerovel,1);
oldshifts_unwraped_nonsacc_zeromean = oldshifts_unwraped_nonsacc_zerovel - ...
    repmat(meanshiftineachframe,[numsamplesperframe 1 1]);

fullshiftstoanalyse = oldshifts_unwraped_nonsacc_zeromean;
runningaverage_unwraped_nonsacc = zeros(numsamplesperframe,numnonsaccframes,2);

totalnumberofsamples = 2 * numnonsaccframes;
if numframestoaverage == 1
    gauss_axis = [0:totalnumberofsamples - 1] - floor(totalnumberofsamples / 2);
else
    numsideelements = ceil((totalnumberofsamples - numframestoaverage) / 2);
    centreofaxis = zeros(1,numframestoaverage);
    leftofaxis = [1:numsideelements];
    if ((2 * numsideelements) + numframestoaverage) > totalnumberofsamples
        gauss_axis = [fliplr(leftofaxis(1:end - 1)),centreofaxis,leftofaxis];
    else
        gauss_axis = [fliplr(leftofaxis),centreofaxis,leftofaxis];
    end
end

if gausssd > 0
    fullgauss = exp(-((gauss_axis .^ 2) / (2 * (gausssd .^ 2))));
else
    fullgauss = zeros(length(gauss_axis));
    indexaddition = ([0:numframestoaverage - 1] - floor(numframestoaverage / 2)) +...
        + floor(totalnumberofsamples / 2) + 1;
    fullgauss(indexaddition) = 1;
end

% frameaveprog = waitbar(0,'Averaging Shifts');
for segmentcounter = 1:numsaccdivisions
    startindex = find(nonsaccframes == nonsaccstartframes(segmentcounter));
    endindex = find(nonsaccframes == nonsaccendframes(segmentcounter));
    shiftstoanalyse = fullshiftstoanalyse(:,startindex:endindex,:);
    
    numsegframes = endindex - startindex + 1;
    
    if numsegframes < numframestoaverage
        averageshiftfromreference = mean(shiftstoanalyse,2);
        segmentrunningaverage_unwraped_nonsacc =...
            repmat(averageshiftfromreference,[1 numsegframes 1]);
    else
        segmentrunningaverage_unwraped_nonsacc = zeros(numsamplesperframe,numsegframes,2);
        gaussindexaddition = fliplr([0:-1:(numsegframes - 1) * -1.0]);
        gaussstartindex = floor(totalnumberofsamples / 2) + numsegframes;

        for framecounter = 1:numsegframes
            lastindexofgauss = gaussstartindex - framecounter + 1;
            gaussindicestouse = lastindexofgauss + gaussindexaddition;
            gaussweight = fullgauss(gaussindicestouse);
            
            for directioncounter = 1:2
                for stripcounter = 1:numsamplesperframe
                    shiftstoaverage = ...
                        shiftstoanalyse(stripcounter,:,directioncounter);
                    masktouse = [1,1,abs((diff(shiftstoaverage,2,2)) < accelthresh)];
                    finalweight = gaussweight .* masktouse;
                    sumfinalweight = sum(finalweight);
                    meanshift = sum(shiftstoaverage .* finalweight) ./ sumfinalweight;
                    segmentrunningaverage_unwraped_nonsacc(stripcounter,framecounter,directioncounter) = ...
                        meanshift;
                end
            end
        end
    end
    runningaverage_unwraped_nonsacc(:,startindex:endindex,:) = segmentrunningaverage_unwraped_nonsacc;
    
    prog = segmentcounter / numsaccdivisions;
%     waitbar(prog,frameaveprog);
end
close(frameaveprog);

% tempshifts = fullshiftstoanalyse(:,nonsaccframes,:);
% meanframeshift = repmat(mean(tempshifts,2),[1 numnonsaccframes 1]);
% runningaverage_unwraped_nonsacc = runningaverage_unwraped_nonsacc + meanframeshift;

runningaverage_unwraped = zeros(numsamplesperframe,numframes,2);
runningaverage_unwraped(:,nonsaccframes,:) = runningaverage_unwraped_nonsacc;
if ~isempty(saccframes)
    runningaverage_unwraped(:,saccframes,:) = runningaverage_unwraped_nonsacc(:,prevnonsaccframe,:);
end

for directioncounter = 1:2
    tempshifts = runningaverage_unwraped(:,:,directioncounter);
    runningaverage(:,directioncounter) = tempshifts(:);
end

newshifts = oldshifts - runningaverage;