function varargout = scalepixels(videoname)

if (nargin < 1) || isempty(videoname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to scale');
    if avifilename == 0
        disp('No video to scale,stoping program');
        error('Type ''help scalepixels'' for usage');
    end
    videoname = strcat(avipath,avifilename);
    processfullvideo = 1;
else
    if ischar(videoname)
        processfullvideo = 1;
        if ~exist(videoname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to scale');
            if avifilename == 0
                disp('No video to analyse,stoping program');
                error('Type ''help scalepixels'' for usage');
            end
            videoname = strcat(avipathname,avifilename);
        end
    else
        processfullvideo = 0;
        if nargout < 1
            disp('If you do not provide a video name, scalepixels requires an output matrix');
            warning('Type ''help scalepixels'' for usage');
        end
    end
end

if processfullvideo
    videoinfo = aviinfo(videoname);
    framewidth = videoinfo.Width;
    frameheight = videoinfo.Height;
    numframes = videoinfo.NumFrames;
    videotype = videoinfo.ImageType;
    framerate = round(videoinfo.FramesPerSecond);

    if strcmp(videotype,'truecolor')
        disp('Video being analysed is a truecolor video, this program can scale only 8 bit videos!!');
        warning('Using only the first layer of the video during scaling');
        istruecolor = 1;
    else
        istruecolor = 0;
    end

    mymap = repmat([0:255]' / 255,1,3);
    moviestruct = repmat(struct('cdata',zeros(frameheight,framewidth),'colormap',mymap),numframes,1);
    newname = strcat(videoname(1:end - 4),'_pscaled.avi');
else
    istruecolor = 0;
    framewidth = size(videoname,2);
    frameheight = size(videoname,1);
    numframes = size(videoname,3);

    scaledframes = zeros(frameheight,framewidth,numframes);
end


scaleprog = waitbar(0,'Scaling');
for framecounter = 1:numframes
    if processfullvideo
        tempframe = double(frame2im(aviread(videoname,framecounter)));
        if istruecolor
            tempframe = tempframe(:,:,1);
        end
    else
        tempframe = videoname(:,:,framecounter);
    end

    if processfullvideo
        moviestruct(framecounter).cdata = uint8(floor(scale(tempframe) * 255) + 1);
    else
        scaledframes(:,:,framecounter) = floor(scale(tempframe) * 255) + 1;
    end

    prog = framecounter / numframes;
    waitbar(prog,scaleprog);

end
close(scaleprog);

if processfullvideo
    movie2avi(moviestruct,newname,'FPS',framerate,'COMPRESSION','None');
else
    varargout{1} = scaledframes;
end


%--------------------------------------------------------------------------

function newmatrix = scale(oldmatrix);

newmatrix = oldmatrix - min(oldmatrix(:));
newmatrix = newmatrix / max(newmatrix(:));

%--------------------------------------------------------------------------