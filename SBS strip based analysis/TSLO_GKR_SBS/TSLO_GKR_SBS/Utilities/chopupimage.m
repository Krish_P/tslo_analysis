function choppedimage = chopupimage(oldimage);

if nargin < 1
    disp('No image provided');
    error('Type ''help chopupimage'' for help');
end


tempimage = oldimage;

imagewidth = size(oldimage,2);
imageheight = size(oldimage,1);
indiceswithnoimagedata = find(oldimage <= 0);
if ~isempty(indiceswithnoimagedata)
    indiceswithnoimagedata = indiceswithnoimagedata(:);
end

mymap = repmat([0:255]' / 256,1,3);

fighandle = figure;
imagehandle = image(tempimage);
colormap(mymap);
axis off
truesize;

toexit = 0;

while ~toexit
    xs = zeros(4,1);
    ys = zeros(4,1);

    figure(fighandle);
    set(gcf,'Name','Click outside the image to exit');
    pause(0.5);

    for pointcounter = 1:4
        switch pointcounter
            case 1
                set(fighandle,'Name','Left Border');
                title('Left Border');
            case 2
                set(fighandle,'Name','Right Border');
                title('Right Border');
            case 3
                set(fighandle,'Name','Top Border');
                title('Top Border');
            case 4
                set(fighandle,'Name','Bottom Border');
                title('Bottom Border');
        end

        [x y] = ginput(1);

        xs(pointcounter) = round(x);
        ys(pointcounter) = round(y);
    end

    x_start = xs(1);
    x_end = xs(2);

    if x_start > x_end
        disp('Left border cannot be greater than right border');
        warning('Interchanging border indices');
        temp = x_start;
        x_start = x_end;
        x_end = temp;
    end

    y_start = ys(3);
    y_end = ys(4);

    if y_start > y_end
        disp('Top border cannot be greater than bottom border');
        warning('Interchanging border indices');
        temp = y_start;
        y_start = y_end;
        y_end = temp;
    end

    if (x_start > 1) & (x_end < imagewidth) &...
            (y_start > 1) & (y_end < imageheight)
        tempimage(y_start:y_end,x_start:x_end) = 0;

        indiceswithnoimagedata = find(tempimage <= 0);
        if ~isempty(indiceswithnoimagedata)
            indiceswithnoimagedata = indiceswithnoimagedata(:);
        end
        set(imagehandle,'CData',tempimage);
    else
        toexit = 1;
    end
end

close(fighandle);

indiceswithimagedata = setdiff([1:numel(oldimage)]',indiceswithnoimagedata);

meanpixelval = mean(oldimage(indiceswithimagedata));

randpixelindices = floor(rand(length(indiceswithnoimagedata),1) * length(indiceswithimagedata)) + 1;
randpixelvalues = oldimage(indiceswithimagedata(randpixelindices));

choppedimage = zeros(imageheight,imagewidth,3);

choppedimage_meanval = oldimage;
choppedimage_meanval(indiceswithnoimagedata) = meanpixelval;

choppedimage_randvals = oldimage;
choppedimage_randvals(indiceswithnoimagedata) = randpixelvalues;

choppedimage_zeroval = tempimage;

choppedimage(:,:,1) = choppedimage_meanval;
choppedimage(:,:,2) = choppedimage_randvals;
choppedimage(:,:,3) = choppedimage_zeroval;