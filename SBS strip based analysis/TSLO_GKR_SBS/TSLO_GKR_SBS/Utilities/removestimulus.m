function removestimulus(videoname,frameswithstim,stimcentres,stimsizes,waitbarhandle)


currentdir = pwd;
if nargin < 5 || isempty(waitbarhandle)
    nowaitbar = 1;
else
    nowaitbar = 0;
end

if nargin < 1 || isempty(videoname)
[fname pname] = uigetfile('*.avi','Please enter name of video from which to remove stimulus');
    if fname == 0
        disp('No video to analyse,stoping program');
        error('Type ''help getandremovestimulus'' for usage');
    end
    cd(pname);
    videoname = strcat(pname,fname);
end

videoinfo = aviinfo(videoname);
numframes = videoinfo.NumFrames;
aviwidth = videoinfo.Width;
aviheight = videoinfo.Height;
xcenter = floor(aviwidth / 2) + 1;
ycenter = floor(aviheight / 2) + 1;
framerate = round(videoinfo.FramesPerSecond);

mymap = repmat([0:255]' / 255,1,3);
newvideoname = strcat(videoname(1:end-4),'_nostim.avi');
newmovie = avifile(newvideoname,'COMPRESSION','None','FPS',framerate);

framenumbers = [1:numframes]';
numframeswithstim = length(frameswithstim);

if ~nowaitbar
    waitbar(0,waitbarhandle,'Removing Stimulus');
else
    removeprog = waitbar(0,'Removing Stimulus');
end

for framecounter = 1:numframes
    if isempty(find(frameswithstim == framenumbers(framecounter)))
        frametoadd = aviread(videoname,framecounter);
        
    else
        videoframe = double(frame2im(aviread(videoname,framecounter)));
        indexinmatrix = framecounter;
        stimcentreinframe = stimcentres(indexinmatrix,:);
        stimsizeinframe = stimsizes(indexinmatrix,:);
        
        leftindex_stim = floor(stimcentreinframe(1) - floor(stimsizeinframe(1) / 2)) + xcenter;
        rightindex_stim = leftindex_stim + stimsizeinframe(1);
        leftindex_stim = max(leftindex_stim,1);
        rightindex_stim = min(rightindex_stim,aviwidth);
        
        topindex_stim = floor(stimcentreinframe(2) - floor(stimsizeinframe(2) / 2)) + ycenter;
        bottomindex_stim = topindex_stim + stimsizeinframe(2);
        topindex_stim = max(topindex_stim,1);
        bottomindex_stim = min(bottomindex_stim,aviheight);
        
        tempstim = videoframe(topindex_stim:bottomindex_stim,leftindex_stim:rightindex_stim);
        thresholdtouse = mean(tempstim(:));
        
        [tempindices leftindices] = find(videoframe(topindex_stim:bottomindex_stim,...
            1:max(leftindex_stim - 5,1)) >= (thresholdtouse + 10));
        if isempty(leftindices)
            leftindex = leftindex_stim - 10;
        else
            leftindex = max(leftindices(:));
        end
        leftindex = max(leftindex,1);
        
        [tempindices rightindices] = find(videoframe(topindex_stim:bottomindex_stim,...
            min(rightindex_stim + 5,aviwidth):aviwidth) >= (thresholdtouse + 10));
        if isempty(rightindices)
            rightindex = rightindex_stim + 10;
        else
            rightindex = min(rightindices(:)) + min(rightindex_stim + 5,aviwidth);
        end
        rightindex = min(rightindex,aviwidth);
        
        [topindices tempindices] = find(videoframe(1:max(topindex_stim - 5,1),...
            leftindex_stim:rightindex_stim) >= (thresholdtouse + 10));
        if isempty(topindices)
            topindex = topindex_stim - 10;
        else
            topindex = max(topindices(:));
        end
        topindex = max(topindex,1);
        
        [bottomindices tempindices] = find(videoframe(min(bottomindex_stim + 5,aviheight):aviheight,...
            leftindex_stim:rightindex_stim) >= (thresholdtouse + 10));
        if isempty(bottomindices)
            bottomindex = bottomindex_stim + 10;
        else
            bottomindex = min(bottomindices(:)) + min(bottomindex_stim + 5,aviheight);
        end
        bottomindex = min(bottomindex,aviheight);
        
%         leftindex = floor(stimcentreinframe(1) - floor(stimsizeinframe(1) / 2)) - 5 + xcenter;
%         leftindex = max(leftindex,1);
%         rightindex = leftindex + stimsizeinframe(1) + 10;
%         rightindex = min(rightindex,aviwidth);
%         
%         topindex = floor(stimcentreinframe(2) - floor(stimsizeinframe(2) / 2)) - 5 + ycenter;
%         topindex = max(topindex,1);
%         bottomindex = topindex + stimsizeinframe(2) + 10;
%         bottomindex = min(bottomindex,aviheight);
        
        colnumbers = [leftindex:rightindex];
        rownumbers = [topindex:bottomindex];
        
        numcols = length(colnumbers);
        numrows = length(rownumbers);
        
        matrixtoadd = zeros(numrows,numcols);
        
        colmatrix = zeros(numrows,numcols);
        rowmatrix = zeros(numrows,numcols);
        
        for colcounter = 1:numcols
            toppixelval = videoframe(topindex,colnumbers(colcounter));
            bottompixelval = videoframe(bottomindex,colnumbers(colcounter));
            linetoadd = linspace(toppixelval,bottompixelval,numrows);
            linetoadd = linetoadd(:);
            colmatrix(:,colcounter) = linetoadd;
        end
        
        for rowcounter = 1:numrows
            leftpixelval = videoframe(rownumbers(rowcounter),leftindex);
            rightpixelval = videoframe(rownumbers(rowcounter),rightindex);
            linetoadd = linspace(leftpixelval,rightpixelval,numcols);
            rowmatrix(rowcounter,:) = linetoadd;
        end
        
        matrixtoadd = (rowmatrix + colmatrix) / 2;

        newvideoframe = videoframe;
        
%         xstimloc = round(stimcentreinframe(1) + xcenter);
%         ystimloc = round(stimcentreinframe(2) + ycenter);
%         newvideoframe(ystimloc,xstimloc) = 255;
        newvideoframe(rownumbers,colnumbers) = matrixtoadd;
        frametoadd = im2frame(uint8(newvideoframe),mymap);
    end
    newmovie = addframe(newmovie,frametoadd);
    if ~nowaitbar
        waitbar((framecounter/ numframes),waitbarhandle);
    else
        waitbar((framecounter/ numframes),removeprog);
    end
end

if nowaitbar
    close(removeprog);
end

newmovie = close(newmovie);
cd(currentdir);