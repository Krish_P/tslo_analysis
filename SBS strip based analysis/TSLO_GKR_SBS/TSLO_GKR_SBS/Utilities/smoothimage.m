function varargout = smoothimage(aviname,smoothsd);
% smoothimage.m. This is a utility program that is designed to band
% pass filter AOSLO videos/frames. The main requirement for filtering is
% too reduce the high spatial frequency noise in the video/frame (the
% amount of filtering on the high end controlled by the smoothsd) and any
% low spatial frequency luminance artifacts/retinal structures like the
% central fovea which is usually darker than the surrounded retina that
% messes up reference image creation.
%
% Usage: [smoothedframes] = smoothimage(aviname,lowfreq,smoothsd);
% aviname           - the name of the video or a collection of frames that
%                     needs to be filtered. If a 3D array of frames is
%                     provided then then output array is a 3D array of same
%                     size with the filtered frames.
% smoothsd          - the SD (in pixels) of the guassian function that will
%                     be used to filter (smooth) the video/frame. Default
%                     is 1.
%
% smoothedframes    - If a set of frames is given then the user should
%                     provide an output argument to hold the filtered
%                     frames.
%
%
% Program Creator: Girish Kumar
% Date of Completion: 02/15/08



if (nargin < 1) | isempty(aviname)
    [avifilename avipath] = uigetfile('*.avi','Please enter filename of video to analyse');
    if avifilename == 0
        disp('No video to analyse,stoping program');
        error('Type ''help smoothimage'' for usage');
    end
    aviname = strcat(avipath,avifilename);
    processfullvideo = 1;
else
    if isstr(aviname)
        processfullvideo = 1;
        if ~exist(aviname,'file')
            warning('Video name does not point to a valid file');
            [avifilename avipathname] = uigetfile('*.avi','Please enter filename of video to analyse');
            if avifilename == 0
                disp('No video to analyse,stoping program');
                error('Type ''help smoothimage'' for usage');
            end
            aviname = strcat(avipathname,avifilename);
        end
    else
        processfullvideo = 0;
        if nargout < 1
            disp('If you do not provide a video name, gaussbandfilter requires an output matrix');
            warning('Type ''help smoothimage'' for usage');
        end
    end
end

if (nargin < 2) | isempty(smoothsd)
    smoothsd = 2;
end

if processfullvideo
    videoinfo = aviinfo(aviname);
    aviwidth = videoinfo.Width;
    aviheight = videoinfo.Height;
    numframes = videoinfo.NumFrames;
    videotype = videoinfo.ImageType;
    framerate = round(videoinfo.FramesPerSecond);

    if strcmp(videotype,'truecolor')
        disp('Video being analyssed is a truecolor video, this program can analyse only 8 bit videos!!');
        warning('Using only the first layer of the video during analyses');
        istruecolor = 1;
    else
        istruecolor = 0;
    end

    mymap = repmat([0:255]' / 255,1,3);
    newname = strcat(aviname(1:end - 4),'_smoothfilt.avi');
else
    aviwidth = size(aviname,2);
    aviheight = size(aviname,1);
    numframes = size(aviname,3);

    filteredframes = zeros(aviheight,aviwidth,numframes);
end

framewidth = aviwidth + 100;
frameheight = aviheight + 100;

framecenter_x = floor(framewidth / 2) + 1;
framecenter_y = floor(frameheight / 2) + 1;

smoothxmatrix = [0:framewidth - 1] - floor(framewidth / 2);
smoothymatrix = [0:frameheight - 1]' - floor(frameheight / 2);

indicesofinterest_x = [0:(aviwidth - 1)] + ((framewidth - aviwidth) / 2);
indicesofinterest_y = [0:(aviheight - 1)] + ((frameheight - aviheight) / 2);

leftpadindices = [1:indicesofinterest_x(1) - 1];
rightpadindices = [indicesofinterest_x(end) + 1:framewidth];

toppadindices = [1:indicesofinterest_y(1) - 1];
bottompadindices = [indicesofinterest_y(end) + 1:frameheight];

numleftpadindices = length(leftpadindices);
numrightpadindices = length(rightpadindices);
numtoppadindices = length(toppadindices);
numbottompadindices = length(bottompadindices);

if smoothsd > 0
    smoothgauss_x = exp(-((smoothxmatrix .^ 2) / (2 * (smoothsd .^ 2))));
    smoothgauss_y = exp(-((smoothymatrix .^ 2) / (2 * (smoothsd .^ 2))));

    smoothgauss = smoothgauss_y * smoothgauss_x;
else
    smoothgauss = ones(filtersize);
end

smoothgauss = smoothgauss / sum(smoothgauss(:));
filter_fft = fft2(smoothgauss);
filter_fft(1) = 1;

filterprog = waitbar(0,'Smoothing');
for framecounter = 1:numframes
    if processfullvideo
        tempframe = double(frame2im(aviread(aviname,framecounter)));
        if istruecolor
            tempframe = tempframe(:,:,1);
        end
    else
        tempframe = aviname(:,:,framecounter);
    end

    bigframe = zeros(frameheight,framewidth) + mean(tempframe(:));

    bigframe(indicesofinterest_y,leftpadindices) = repmat(tempframe(:,3),1,...
        numleftpadindices);
    bigframe(indicesofinterest_y,rightpadindices) = repmat(tempframe(:,end - 2),1,...
        numrightpadindices);
    bigframe(toppadindices,indicesofinterest_x) = repmat(tempframe(3,:),...
        numtoppadindices,1);
    bigframe(bottompadindices,indicesofinterest_x) = repmat(tempframe(end - 2,:),...
        numbottompadindices,1);

    bigframe(indicesofinterest_y,indicesofinterest_x) = tempframe;

    newframe = real(ifftshift(ifft2(fft2(bigframe) .* filter_fft)));
    frametoadd = newframe(indicesofinterest_y,indicesofinterest_x);

    if processfullvideo
        moviestruct(framecounter) = im2frame(uint8(floor(scale(frametoadd) * 255) + 1),mymap);
    else
        filteredframes(:,:,framecounter) = frametoadd;
    end

    prog = framecounter / numframes;
    waitbar(prog,filterprog);
end
close(filterprog);

if processfullvideo
    movie2avi(moviestruct,newname,'FPS',framerate,'COMPRESSION','None');
else
    varargout{1} = filteredframes;
end


%--------------------------------------------------------------------------

function newmatrix = scale(oldmatrix);

newmatrix = oldmatrix - min(oldmatrix(:));
newmatrix = newmatrix / max(newmatrix(:));

%--------------------------------------------------------------------------