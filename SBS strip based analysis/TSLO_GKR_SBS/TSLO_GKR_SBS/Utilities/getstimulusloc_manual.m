[avifilename avipathname] = uigetfile('*.avi','Please enter name of video to analyse');
if avifilename == 0
    disp('No video to analyse,stoping program');
    error('Type ''help getstimulusloc'' for usage');
end
cd(avipathname);
videoname = strcat(avipathname,avifilename);


videoinfo = aviinfo(videoname);
framewidth = videoinfo.Width;
frameheight = videoinfo.Height;
numframes = videoinfo.NumFrames;
videotype = videoinfo.ImageType;

[fname pname] = uigetfile('*.mat','Please enter file name of matfiles with blinkframes');
if fname == 0
    disp('No blink frames,stopping program');
    error('Type ''help getstimulusloc'' for usage');
end
datafilename = strcat(pname,fname);
load(datafilename,'blinkframes');

if strcmp(videotype,'truecolor')
    disp('Video being analysed is a truecolor video, this program can shrink only 8 bit videos!!');
    warning('Using only the first layer of the video during conversion');
    istruecolor = 1;
else
    istruecolor = 0;
end

numlinesperfullframe = 525;
frameswithnostim = blinkframes(:);
stimuluslocs = zeros(numframes,3);
stimsizes =  zeros(numframes,2);

stimulusmotion = questdlg('Does the stimulus move within the frame',...
    'Stimulus Motion','Yes','No','Yes');
stimulusmotion = stimulusmotion(1);

mymap = repmat([0:255]' / 256,1,3);
framefig = figure;

switch stimulusmotion
    case 'Y'
        for framecounter = 1:numframes
            currentframe = double(frame2im(aviread(videoname,framecounter)));
            if istruecolor
                currentframe = currentframe(:,:,1);
            end

            figure(framefig);
            image(currentframe);
            colormap(mymap);
            axis off

            xs = zeros(4,1);
            ys = zeros(4,1);

            for counter = 1:4
                switch counter
                    case 1
                        title('Left Border');
                    case 2
                        title('Right Border');
                    case 3
                        title('Top Border');
                    case 4
                        title('Bottom Border');
                end

                [xs(counter) ys(counter)] = ginput(1);
            end


            if any(xs >= framewidth)
                continue
            else
                xcoord = (xs(1) + xs(2)) / 2;
                ycoord = (ys(3) + ys(4)) / 2;

                linenumber = ((framecounter - 1) * numlinesperfullframe) + ycoord;

                xsize = abs(xs(2) - xs(1)) + 1;
                ysize = abs(ys(4) - ys(3)) + 1;

                stimuluslocs(framecounter,:) = [xcoord,ycoord,linenumber];
                stimsizes(framecounter,:) = [xsize,ysize];
            end
        end
    case 'N'
        currentframe = double(frame2im(aviread(videoname,1)));
        if istruecolor
            currentframe = currentframe(:,:,1);
        end

        figure(framefig);
        image(currentframe);
        colormap(mymap);
        axis off

        xs = zeros(4,1);
        ys = zeros(4,1);

        for counter = 1:4
            switch counter
                case 1
                    title('Left Border');
                case 2
                    title('Right Border');
                case 3
                    title('Top Border');
                case 4
                    title('Bottom Border');
            end

            [xs(counter) ys(counter)] = ginput(1);
        end

        xcoords = repmat((xs(1) + xs(2)) / 2,numframes,1);
        ycoords = repmat((ys(3) + ys(4)) / 2,numframes,1);

        xsizes = repmat(abs(xs(2) - xs(1)) + 1,numframes,1);
        ysizes = repmat(abs(ys(4) - ys(3)) + 1,numframes,1);

        linenumbers = ([0:numframes - 1]' * numlinesperfullframe) + ycoords;

        stimuluslocs = [xcoords,ycoords,linenumbers];
        stimsizes = [xsizes,ysizes];
        
        stimuluslocs(frameswithnostim,:) = 0;
        stimsizes(frameswithnostim,:) = 0;
end

