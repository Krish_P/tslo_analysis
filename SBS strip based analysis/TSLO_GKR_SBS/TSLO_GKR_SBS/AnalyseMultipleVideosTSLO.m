% This script runs through the set of video files outputs stabilized 
% SLO movies and eye movement files as CSV
% KSP 2020
%% directory setup
%set top level folder here
topLevelFolder=uigetdir('/home/coateslab/LabData/TSLO_Experiment/','Select Raw Video folder');
% behavFolder=uigetdir('/home/coateslab/LabData/TSLOTestVideos/PilotTest/VoluntaryMicrosaccade/Trial1/Behav/','Select Behavioral data folder');
cd (topLevelFolder)
%% Protocol parameters
% Here we set the parameters for the different programs/functions called by
% this script
trialDuration=5; % This would be used by the padding protocol
field_size=4.75; % This would be used to scale eye position data
markerThreshold=30; % This would be used to get stimulus onset event markers
%% User input
%Here the user decides on which set of programs or protocols to run
stabilizeFromRaw=true;
markerInfo=true;
crowding_stream=false;
eyeposPadding=true;
saccadeDetection=true;
pre_process=false;
%% Parameters for bandpass filter of videos
filtParams.overwrite = true;
filtParams.smoothing = 1;
filtParams.lowSpatialFrequencyCutoff = 3;
%% Main loop
g=gpuDevice(1); %KSP added this to ensure GPU device is made accessible 
                %to matlab again (following a dismount at the end of each run)
% First we pre-process the videos before running the analysis
if pre_process==true
    listoffiles=dir(topLevelFolder);
    parfor fileIdx=1:length(listoffiles)
        filename=listoffiles(fileIdx).name;
        if endsWith(filename,".avi") && ~contains(filename,'stab') && ~contains(filename,'bandfilt')
            BandpassFilter(filename, filtParams);
        end
    end
end
                
listoffiles=dir(topLevelFolder);
if stabilizeFromRaw==true
    parfor fileIdx=1:length(listoffiles)
        required_filename='stabilized';
        filename=listoffiles(fileIdx).name;
        if endsWith(filename,".avi") %&& contains(filename,'bandfilt')
            if contains(filename,required_filename)             
                continue
            else
                % get GPU temp before initiating the analysis for next file..
                % Basically the program sits here till the temperature drops
                if cuda_get_temp > 75
                    pause(120);
                    cuda_reset;
                else
                    disp(filename)
                    videotoanalyse=[topLevelFolder,'/',filename];
                    j=strfind(videotoanalyse,'.avi');
                    stabFile=[videotoanalyse(1:j-1),'_stab.avi'];
%                     try
%                         % Can come back again and retrieve the
%                         % eyepos files from the mat file
% %                         [frameshifts_strips_spline]=StabilizeSLOMovie(videotoanalyse); 
% %                         [frameshifts_strips_spline]=stabilizefromraw_function(videotoanalyse);
%                     catch
%                         disp('File Failed!');
%                     end
                    %Also obtain the marker/stimulus timing
                    %info from the video and store it as a csv
                    if markerInfo==true
                        if crowding_stream==true
                            %obtain the marker/stimulus timing
                            %info from the video and store it as a csv
                            outputMarkerFilename=strcat(videotoanalyse(1:end-4),'_marker.csv');
                            % Get the actual number of stimuli from the
                            % behavioral data
                            fileCode=filename(1:strfind(filename,'_Video')-1);
                            behavFiles=dir(behavFolder);
                            for behavIdx=1:length(behavFiles)
                                behavFile=behavFiles(behavIdx).name;
                                if strfind(behavFile,fileCode);
                                    if endsWith(behavFile,'.csv');
                                        numStim=height(readtable(strcat(behavFolder,'/',behavFile)));
                                    end
                                end
                            end
                        else
                            outputMarkerFilename=strcat(videotoanalyse(1:end-4),'_marker.csv');
                            numStim=2;
                        end
                            try
                                numMarker=0; % we would set an arbitary value to start the while loop
                                markerThreshold=6;% Also set the marker threshold to be 50 initially
                                % We would keep running this process till we get
                                % the right number of stimulus marker events
                                numIter=1;
                                while numIter<10
                                    if crowding_stream==true
                                        [markerData]=IdentifyMarker(videotoanalyse,1,markerThreshold);
                                    else
                                        [markerData]=IdentifyMarker(videotoanalyse,0,1);
                                    end
                                    numMarker=length(markerData);
                                    if numMarker==numStim
                                        break
                                    elseif numMarker<numStim
                                        markerThreshold=markerThreshold-5;
                                    elseif numMarker>numStim
                                        markerThreshold=markerThreshold+5;
                                    end
                                    numIter=numIter+1;
                                end
                                csvwrite(outputMarkerFilename,markerData);
                            catch
                                disp('no markers found!');
                            end
                    end
                end
            end
        end
    end
end

% we then run the eye position padding protocol from python using the
% system command
% we first set the parameters for the function before passing in the
% command
if eyeposPadding==true
    python_func_path='/home/coateslab/TSLO/Code/tslo_analysis/utils/eyepos_padding.py eyepos_padding';
    commandStr=['python3',' ',python_func_path,' ',topLevelFolder,' ',num2str(trialDuration)];
    % Final send the command across
    system(commandStr)
end

% Finally we run the saccade detection method with the padded eye position
% data
if saccadeDetection==true
    status=SaccadeDetectionMacroCLAB(topLevelFolder);
end