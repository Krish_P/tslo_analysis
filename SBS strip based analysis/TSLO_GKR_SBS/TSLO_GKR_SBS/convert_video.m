%% Convert video: Remove stimulus
% This scripts removes the stimulus from the video and fills it with random
% pixels
[avifile,avipath]=uigetfile('*.avi','Select file');
cd(avipath)
tic;
outputfilename=[avifile(1:end-4),'_rotated.avi'];
v=VideoReader(avifile);vOut=VideoWriter(outputfilename,'Uncompressed AVI');open(vOut);
numFrames=v.FrameRate*v.Duration;

for frameIdxRandPix=1:numFrames
    frame=frame2im(read(v,frameIdxRandPix,'native'));
    newFrame=imrotate(flip(frame,2),90);
    writeVideo(vOut,newFrame)
end
close(vOut);

timeelapsed=toc;
totalstring = ['Total time elapsed ', num2str(timeelapsed),' seconds'];
disp(totalstring);