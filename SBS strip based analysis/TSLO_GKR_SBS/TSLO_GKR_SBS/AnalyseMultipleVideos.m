% This script runs through the set of video files outputs stabilized tslo_analysis/SBS strip based analysis/TSLO_GKR_SBS/TSLO_GKR_SBS
% SLO movies and eye movement files as CSV
% KSP 2020
%% directory setup
%set top level folder here
topLevelFolder='/mnt/hd/ao/vids';
cd (topLevelFolder)
%we use the key file to index into the different folders of the top level
%folder
keyFile=readtable('/mnt/hd/ao/psy_list.csv','Delimiter',',');

%Field ofview is calculated pixel2degree
%pix2deg=3/512; %not being used as yet
%% Parameter setup
%Smoothing parameters are set here
windowWidth=5;
kernel=ones(windowWidth,1)/windowWidth;
% Create an index for writing failed video to an array
vidFailIdx=1;

%% Log file
%Finally we set up a log file 
fileID=fopen('logfile.txt','a');

%% Main loop
for k = 1 : height(keyFile)
    
    g=gpuDevice(1); %KSP added this to ensure GPU device is made accessible 
                    %to matlab again (following a dismount at the end of each run)

    %first we get the folder path from the key file
    folderName=keyFile.date{k};
    folderPath=[topLevelFolder,'/',folderName];
    
    if exist(folderPath,'dir') %this ensures that the dir is valid
        cd(folderPath);
        listoffiles=dir(folderPath);
        time=datestr(clock,'YYYY/mm/dd HH:MM:SS:FFF');
        fprintf(fileID,'%s: %s\n',time,folderPath);
        %second we get the pix2deg conversion using the field size that for the
        %set of videos from the key file
        fieldSize=keyFile.field_size(1);
        pix2deg=fieldSize/512;

        if isempty(listoffiles)
            continue
        else
            for fileIdx=1:length(listoffiles)
                required_filename='stab';
                filename=listoffiles(fileIdx).name;

                if endsWith(filename,".avi")
                    if contains(filename,required_filename)             
                        continue
                    else
                        % get GPU temp before initiating the analysis for next file..
                        % Basically the program sits here till the temperature drops
                        if cuda_get_temp > 75
                            pause(120);
                            cuda_reset;
                        else
%                             disp(filename)
                            videotoanalyse=[folderPath,'/', filename];
                            j=strfind(videotoanalyse,'.avi');
                            stabFile=[videotoanalyse(1:j-1),'_stab.avi'];
                            if isfile(stabFile)
                                disp('File already processed');
                                continue
                            else
                                try
                                    EyePos=[];
                                    [frameshifts_strips_spline,timeaxis_secs]=StabilizeSLOMovie(videotoanalyse);
                                    if length(timeaxis_secs)<length(frameshifts_strips_spline)
                                        EyePos(:,1)=timeaxis_secs;EyePos(:,2:3)=(frameshifts_strips_spline(1:length(timeaxis_secs),:)*pix2deg);
                                    elseif length(timeaxis_secs)>length(frameshifts_strips_spline)
                                        EyePos(:,1)=timeaxis_secs(1:length(frameshifts_strips_spline),:);EyePos(:,2:3)=(frameshifts_strips_spline*pix2deg);
                                    end
                                    smoothedData=filter(kernel,1,EyePos(:,2:3));EyePos(:,2:3)=smoothedData;
                                    outputfilename=strcat(filename(1:end-4),'_EyePos','.csv');
                                    csvwrite(outputfilename,EyePos); 
                                catch
                                    warning('Video Failed!!');
                                    failedVideo{vidFailIdx}=videotoanalyse;
                                    vidFailIdx=vidFailIdx+1;
                                end
                            end
                        end
                    end
                end
            end
        end
    else
        disp('Dir invalid/missing');
    end
end
fclose(fileID);