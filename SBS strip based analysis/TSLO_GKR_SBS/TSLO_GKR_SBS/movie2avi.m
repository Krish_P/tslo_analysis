function movie2avi(MATLAB_movie_struct,filenameforsave,junk1,junk2,junk3,fps);
% function movie2avi(filenameforsave,MATLAB_movie_struct,junk1,junk2,junk3,fps);
% Replaces the deprecated MOVIE2AVI from R2016 and previous
% takes a MATLAB movie struct (.cdata and .colormap fields) and saves it to
% disk as an AVI with no compression. Image should be in range 0..255 and
% should be UINT8
% This is a stripped down version. It expects to be called using the old
% arguments, even though they are not effective. For example
% movie2avi('stabilizedmovie.avi',moviestruct,'compression','none','fps',fps)

% (c)2020 SBStevenson@UH.edu

if nargin < 6
    error('movie2avi needs 6 arguments. check the help')
end
myvid = VideoWriter(filenameforsave);
set(myvid,'FrameRate',fps);
open(myvid);
for framedx = 1:length(MATLAB_movie_struct);
    writeVideo(myvid,MATLAB_movie_struct(framedx));
end
close(myvid);
