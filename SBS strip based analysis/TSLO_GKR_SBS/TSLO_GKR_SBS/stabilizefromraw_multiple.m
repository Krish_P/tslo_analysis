%% Stabilize from raw video
% (c) 2009 SBStevenson@uh.edu and GKR
%
% This script creates a stabilized version of multiple raw AVI format videos.
% It is designed to work with AOSLO retinal images and will not work very
% well with conventional SLO images.
% This script is essentially a macro that calls a series of functions to
% carry out the analysis steps. These functions should be in
% ~\MatlabXXXX\toolbox\AOSLO\


currentdir = pwd;

% Get info from the user regarding the directories where the videos are
% present and then load the video names into a .

prompt={'How many directories are your videos in?'};
name='Directory Query';
numlines=1;
defaultanswer={'1'};

answer = inputdlg(prompt,name,numlines,defaultanswer);
numdirectories = str2num(answer{1});

directorynames = cell(numdirectories,1);
listboxstrings = {};
filelist = {};

prevdir = currentdir;
for directorycounter = 1:numdirectories
    tempdirname = uigetdir(prevdir,'Please choose a single folder with video files');
    if tempdirname == 0
        disp('You pressed cancel instead of choosing a driectory');
        warning('Continuing...');
        continue
    end
    directorynames{directorycounter} = tempdirname;
    dirstucture = dir(tempdirname);
    for structurecounter = 1:length(dirstucture)
        if ~(dirstucture(structurecounter).isdir)
            tempname = dirstucture(structurecounter).name;
            fileextension = upper(tempname(end-2:end));
            if strcmp(fileextension,'AVI')
                filelist{end + 1} = strcat(tempdirname,'\',tempname);
                listboxstrings{end + 1} = tempname;
            end
        end
    end
    prevdir = tempdirname;
    cd(tempdirname);
end

% If the directory has no video, exit
if isempty(listboxstrings)
    error('No AVI videos in selected folders, exiting....');
end

selection = listdlg('ListString',listboxstrings,'InitialValue',[],'Name',...
    'File Select','PromptString','Please select videos to stabilize');

% If the user does not choose any video, exit
if isempty(selection)
    return;
end
numfiletoanalyse = length(selection);

% Get the image format in which to save the stabilised image from the user
formatofstabframe = questdlg('In what image format do you want to save the stabilized frame',...
    'Image Format','JPEG','TIFF','GIF','JPEG');
formatofstabframe = strcat('.',lower(formatofstabframe));

cd(currentdir);

% Set the parameters for the various functions used inthis script
blinkthreshold = 25;                % This is a maximum change in mean pixel value between
                                    % frames before the function tags the frames as blink
                                    % frames.
minimummeanlevel = 15;              % This is minimum mean pixel value that a frame has to
                                    % have for it to be considered for analyses.
tofilter = 0;                       % If the video has luminance gradients or has too much pixel noise
                                    % then it would be wise to filter the video prior to analyses by setting
                                    % this flag to 1. If not set it to 0.
gausslowcutoff = 8;                 % Low Frequency cutoff  for the gaussbandfilter.m function
smoothsd = 6;                       % The std. deviation of the gaussian smothing filter that is
                                    % applied by the gaussbandfilter.m function.
toremmeanlum = 0;                   % If the video has luminance artifacts that has high frequency content
                                    % set this flag to 1 to use the removemeanlum.m function, otherwise
                                    % set to 0;
smoothsdformeanremoval = 12;        % The std. deviation of the smoothing function used by the removemeanlum.m
                                    % function.
                                    % then this program can be used to remove these artifacts.                                    
peakratiodiff = 0.5;               % Maximum Change in ratio between the secondpeak and firstpeak
                                    % between two frames for the frames before the % function tags the frames as "bad".
maxmotionthreshold = 0.2;           % Maximum motion between frames, expressed as a percentage
                                    % of the frame dimensions, before frames are
                                    % tagged as "bad".
coarseframeincrement = 12;          % The step size used when choosing a subset of frames
                                    % from the frames that are good. This is used by the 
                                    % makereference_framerate.m function.
samplerateincrement_priorref = 50;  % The multiple of the framerate that is used
                                    % to obtain the sample rate of the ocular motion
                                    % trace when using the makereference_priorref.m function.
samplerateincrement = 18;           % The multiple of the framerate that is used
                                    % to obtain the sample rate of the ocular motion
                                    % trace when using the analysevideo_priorref.m function.
badsamplethreshold = 0.6;           % The threshold that is used to locate the strips that had
                                    % good correlations during the analysis procedure. The lower
                                    % the number the more samples are discarded as "bad matches".
maintaintimerelationships = 1;      % Certain post analysis questions require the stabilised video
                                    % to reflect accurate time relationships between frames. However
                                    % over the course of the analysis we drop frames that can't be 
                                    % analysed accurately. If the user requires accurate time
                                    % relationships then this flag should be set to one, otherwise
                                    % set it to 0. When this flag is turned on, dropped frames are
                                    % replaced by a blank frame in the stabilised video.
numlinesperfullframe = 525;         % The number of pixel lines that would have been present in a video
                                    % frame if data was collected during the vertical mirror flyback.
blacklineflag = 1;                  % When the eye moves faster than the vertical scan rate black lines are present
                                    % in the stabilised video. This occurs because no image data was collected at
                                    % this location. If you find these lines disconcerting, then set this flag to 1,
                                    % otherwise set to 0. These lines are removed by averaging the image data from
                                    % the lines above and below the black lines.
maxsizeincrement = 2.5;               % When creating stabilised movies and frames, physical memory is a big issue.
                                    % If the maximum motion in the raw video is too high, MATLAB runs out of 
                                    % memory and crashes. To prevent that we have to set a maximum size for the
                                    % stabilised video and frame. The maxsizeincrement sets this limit, as a multiple
                                    % of the raw frame size. The maximum value for this parameter that we have
                                    % tested is 2.5. Any image that is outside is set limit is cropped.
splineflag = 0;                     % When calculating splines during the stabilisation, we could calculate
                                    % splines for individual frames (splineflag = 1), or calculate on spline
                                    % for the video (splineflag = 1).

priorref_inputstruct = struct('samplerate',[],'vertsearchzone',24,...
    'stripheight',11,'badstripthreshold',badsamplethreshold,'frameincrement',6,...
    'minpercentofgoodstripsperframe',0.4,'numlinesperfullframe',...
    769); % Structure that contains input arguments for the makereference_priorref.m function.
analyse_inputstruct = struct('samplerate',[],'vertsearchzone',24,...
    'horisearchzone',[],'startframe',1,'endframe',-1,'stripheight',11,...
    'badstripthreshold',badsamplethreshold,'minpercentofgoodstripsperframe',0.4,'numlinesperfullframe',...
    769); % Structure that contains input arguments for the analysevideo_priorref.m function.
analyse_programflags = [1 0]; % Array with the analysis flags used by the analysevideo_priorref.m function.

% Set the feedback options for the various functions used inthis script
blinkverbosity = 0;                 % Set to 1 if you want feedback from the getblinkframes.m
                                    % function, otherwise set to 0.
meanlumverbosity = 0;               % Set to 1 if you want feedback from the removemeanlum.m
                                    % function, otherwise set to 0.
badframeverbosity = 0;              % Set to 1 if you want feedback from the getbadframes.m
                                    % function, otherwise set to 0.
coarserefverbosity = [0 0];         % The verbose array used by the makereference_framerate.m
                                    % function.
finerefverbosity = [0 0 0 0];       % The verbose array used by the makereference_priorref.m
                                    % function.
analyverbosity = [0 0 0 0];         % The verbose array used by the analysevideo_priorref.m
                                    % function.
stabverbosity = 0;                  % Set to 1 if you want feedback from the makestabilizedvideo.m
                                    % function, otherwise set to 0.


tic
% processprog = waitbar(0,'Processing Videos');
for filecounter = 1:numfiletoanalyse
    videotoanalyse = filelist{selection(filecounter)};

    currentvideoinfo = aviinfo(videotoanalyse);
    frameheight = currentvideoinfo.Height;
    framewidth = currentvideoinfo.Width;
    framerate = round(currentvideoinfo.FramesPerSecond);
    g=gpuDevice(1);%KSP
    priorref_inputstruct.samplerate = framerate * samplerateincrement_priorref;
    analyse_inputstruct.samplerate = framerate * samplerateincrement;
    analyse_inputstruct.horisearchzone = (3 * framewidth) / 4;


    blinkfilename = strcat(videotoanalyse(1:end - 4),'_blinkframes.mat');
    
    if tofilter
        filteredname =  strcat(videotoanalyse(1:end-4),'_bandfilt.avi');
    else
        filteredname = videotoanalyse;
    end
    
    if toremmeanlum
        finalname = strcat(filteredname(1:end - 4),'_meanrem.avi');
    else
        finalname = filteredname;
    end
    
    stabimagename_noext = videotoanalyse(1:end - 4);

    blinkframes = getblinkframes(videotoanalyse, blinkthreshold, minimummeanlevel,blinkverbosity);

    if tofilter
        gaussbandfilter(videotoanalyse, gausslowcutoff, smoothsd);
    end

    if toremmeanlum
        removemeanlum(filteredname,smoothsdformeanremoval,meanlumverbosity);
    end

    [goodframesegmentinfo largemovementframes] = getbadframes(finalname,blinkfilename,...
        peakratiodiff, maxmotionthreshold, badframeverbosity);
    
    [coarsereffilename, coarsereferimage] = makereference_framerate(finalname,...
        blinkfilename, coarseframeincrement, badsamplethreshold, coarserefverbosity);
    
    [finereffilename, finerefimage] = makereference_priorref(finalname,...
        coarsereffilename,blinkfilename, 'referenceimage', priorref_inputstruct,...
        finerefverbosity);
    
    analyseddatafilename = analysevideo_priorref(finalname, finereffilename, blinkfilename,...
        'referenceimage', analyse_inputstruct, analyse_programflags, analyverbosity);
    
    load(analyseddatafilename,'analysedframes','frameshifts_strips_spline','peakratios_strips',...
        'stripidx');
    
    [stabilisedvideoname,stabilizedframe,stabilizedframe_full] =...
        makestabilizedvideoandframe(videotoanalyse, analysedframes,...
        frameshifts_strips_spline, peakratios_strips, stripidx, badsamplethreshold,...
        maintaintimerelationships, numlinesperfullframe, blacklineflag, maxsizeincrement,...
        splineflag, stabverbosity);
    
    save(analyseddatafilename,'blinkframes','goodframesegmentinfo','largemovementframes',...
        'coarsereferimage','finerefimage','stabilizedframe','stabilizedframe_full','-append');

    switch formatofstabframe
        case '.jpeg'
            stabimagename = strcat(stabimagename_noext,formatofstabframe);
            imwrite(stabilizedframe(:,:,1) / 256,stabimagename,'Quality',100);
        case '.tiff'
            stabimagename = strcat(stabimagename_noext,'.tiff');
            imwrite(stabilizedframe(:,:,1) / 256,stabimagename,'Compression','none');
        case '.gif'
            stabimagename = strcat(stabimagename_noext,'.gif');
            imwrite(stabilizedframe(:,:,1) / 256,stabimagename);
    end
    
    prog = filecounter / numfiletoanalyse;
%     waitbar(prog,processprog);
end
cuda_reset %KSP
timeelapsed = toc;
close(processprog);

averagetime = timeelapsed / numfiletoanalyse;
totalstring = ['Total time elapsed ', num2str(timeelapsed),' seconds'];
avestring = ['Average time per video ', num2str(averagetime),' seconds'];

disp(totalstring);
disp(avestring);