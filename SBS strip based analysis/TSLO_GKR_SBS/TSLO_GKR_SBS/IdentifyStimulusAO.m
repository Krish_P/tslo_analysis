%% Identify Stimulus
% This scripts identifies the frame on which the stimulus comes on in the
% video places them in an array



%% directory setup
%set top level folder here
topLevelFolder='/mnt/hd/ao/vids';
cd (topLevelFolder)
%we use the key file to index into the different folders of the top level
%folder
keyFile=readtable('/mnt/hd/ao/psy_list.csv','Delimiter',',');

%% Main loop: Parallelized
tic
counter=1;
stimFrame=[];
stimTime=[];
for k = 1 : height(keyFile)
    disp(k);
    %then we get the folder path from the key file
    folderName=keyFile.date{k};
    folderPath=[topLevelFolder,'/',folderName];
    if exist(folderPath,'dir') %this ensures that the dir is valid
        cd(folderPath);
        listoffiles=dir(folderPath);      
        if isempty(listoffiles)
            continue
        else
            parfor fileIdx=1:length(listoffiles)
                %we use the cross hair to find out when the stimulus comes on as this
                %is constant across videos in size
                params.enableVerbosity = false;
                params.stimuluThickness = 1;
                params.overwrite = true;
                params.stimulus = [];
                params.stimuluSize = 11;
                params.stimuluThickness = 1;
                params.stimuluPolarity = 1;
                
                %Output file:
                fileID=fopen('/mnt/hd/ao/vids/StimulusTiming.txt','a');
                fid=fopen('/mnt/hd/ao/vids/FailedFiles.txt','a');
                
                required_filename='stab';
                filename=listoffiles(fileIdx).name;

                if endsWith(filename,".avi")
                    if contains(filename,required_filename)             
                        continue
                    else
                        try
                            [outputVideo, params, varargout] = RemoveStimuli(filename, params);
                            % we then find frames with correlation values more than 0.6
                            % /frames with cross and we use the first frame where the
                            % stimulus comes on
                            stimFrame=find(params.stimPeakValues>0.6, 1 ); 
                            stimTime=params.timeSec(stimFrame);
                            fprintf(fileID,'%s,%s,%d,%.3f\n',cd,filename,stimFrame,stimTime);
                        catch
                            fprintf(fid,'%s,%s\n',cd,filename);
                        end
                    end
                end
            fclose(fileID);
            fclose(fid);
            end    
        end
    end
end

timelapsed=toc