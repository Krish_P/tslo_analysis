% Purpose: Can be used to filtered eye traces to determine the efficiency
% of eye tracking across the length of the video

% we first get the folder with the data
folderPath=uigetdir('/home/coateslab/LabData//home/coateslab/LabData/TSLO_Experiment/', ...
                    'Select folder with eye movement data');
cd(folderPath)
check_marker=0;
draw_marker=true;
% We currently have output from two registration scripts:
% 1. SBS strip based method ("SBS")
% 2. Rossi strip based method{"RossiLab")
registration_method=2;

if check_marker==1
    %We create an array with marker data file names
    markerFiles=dir(['*','marker','*']);
    %we identify markers with missing events or additional events and print
    %them in the command window
    for markerIdx=1:length(markerFiles)
        markerFile=readtable(markerFiles(markerIdx).name);
        markerLength=height(markerFile);
        if markerLength<50 || markerLength>5
            disp(markerFiles(markerIdx).name);
        end
    end

    %wait for a key press
    disp('Files with missing markers!');
    pause;
end
% We creat an array with eye movement data file names
if registration_method==1
    listoffiles=dir(['*','540_hz','*']);
elseif registration_method==2
    listoffiles=dir(['*','offset','*']);
end
% Create a new list that discards filtered and saccade files
listIdx=1;
% clear(newList)
for itemIdx=1:length(listoffiles)
    filename=listoffiles(itemIdx).name;
    if ~contains(filename,"_Filtered") && ~contains(filename,"_sacsdrifts")
        newList{listIdx,:}=filename;
        listIdx=listIdx+1;
    end
    itemIdx=itemIdx+1;
end
totalFiles=length(newList);
%we start a file counter that would be altered inside a loop
fileCounter=1;

% Parameter setup: for eye movement filter protocol
%First we setup the parameters for the filter function:
filterParams.filter1='medfilt1';
filterParams.filter1Params = 7;
filterParams.filter2 = 'sgolayfilt';
filterParams.filter2Params = [3 21];
filterParams.enableVerbosity=1;
filerParams.overwrite=1;

while 0<fileCounter && fileCounter<=totalFiles
    % we load the data and apply filteration protocol and also plot the
    % same
%     try
        % we would pull in eye positions based on strip method
        if registration_method==1 % SBS method
            Data=load(listoffiles(fileCounter).name);
            % We account for occassional differences in data shapes 
            if length(Data.frameshifts_strips_spline)~=length(Data.timeaxis_secs)
                eyepos=[Data.frameshifts_strips_spline(:,1)*(4.75/512),...
                    Data.frameshifts_strips_spline(:,2)*(4.75/512),Data.timeaxis_secs(1:length(Data.frameshifts_strips_spline))];
            else
                eyepos=[Data.frameshifts_strips_spline(:,1)*(4.75/512),...
                    Data.frameshifts_strips_spline(:,2)*(4.75/512),Data.timeaxis_secs];
            end 
        elseif registration_method==2 % Rossi method
            eyeposFilename=newList{fileCounter};
            if ~contains(eyeposFilename,"_Filtered") && ~contains(eyeposFilename,"_sacsdrifts")
                if draw_marker==true
                    markerFilename=[eyeposFilename(1:strfind(eyeposFilename,'_offset')),'marker.csv'];
                    markerData=csvread(markerFilename);
                    markerData(:,3)=(markerData(:,1)*(1000/30)/1000)+(markerData(:,2)*(1000/16000)/1000);
                end
                Data=load(eyeposFilename);
                strip_offsets=Data.strip_offsets;
                % We get the total time based on number of samples @ 480 Hz
                totalLength=length(strip_offsets)/480;
                timeStamp=transpose(linspace(0,totalLength,length(strip_offsets)));
                % we sort them into eye positions
                eyepos=[Data.strip_offsets(:,2)*(4.75/512),Data.strip_offsets(:,1)*(4.75/512),...
                        timeStamp];
            end
            filename=listoffiles(fileCounter).name;
            eyeposFiltered= FilterEyePosition(eyepos,filterParams);
            if draw_marker==true
                for idx=1:size(markerData,1)
                    xline(markerData(idx,3));
                end
            end
            set(gcf,'Name',eyeposFilename);
            set(gcf,'Units','pixels');
            set(gcf,'Position',[0,200,768,500]);
        end
%     catch
%         disp(eyeposFilename);
%     end
%     saveas(gcf,[filename(1:end-4),'.pdf']);
    % we then wait for key press 
    waitforbuttonpress();
    keyPressed=double(get(gcf,'CurrentCharacter'));
    if keyPressed==28 % left arrowkey
        fileCounter=fileCounter-1;
        continue
    elseif keyPressed==29 %right arrowkey
        fileCounter=fileCounter+1;
        continue
    elseif keyPressed==27 %escape key
        close all; %closes all figures
        break
    else
        continue
    end
end
close all;
