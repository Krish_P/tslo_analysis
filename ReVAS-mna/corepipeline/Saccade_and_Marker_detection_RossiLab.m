% This script runs through the folder containing the processed videos using
% Rossi Lab stabilization software & gets the marker & saccade info
% KSP 2022
%% directory setup
%set top level folder here
topLevelFolder=uigetdir('/home/coateslab/LabData/TSLO_Experiment/','Select Raw Video folder');
cd (topLevelFolder)
%% Protocol parameters
% Here we set the parameters for the different programs/functions called by
% this script
trialDuration=5; % This would be used by the padding protocol
field_size=4.75; % This would be used to scale eye position data
markerDetection=false;
saccadeDetection=true;
%% Main loop
if markerDetection==true
    listoffiles=dir(topLevelFolder);
    for fileIdx=1:length(listoffiles)
        required_filename='stabilized';
        filename=listoffiles(fileIdx).name;
        if endsWith(filename,".avi") %&& contains(filename,'bandfilt')
            if contains(filename,required_filename)             
                continue
            else
                disp(filename)
                videotoanalyse=[topLevelFolder,'/',filename];
                outputMarkerFilename=strcat(videotoanalyse(1:end-4),'_marker.csv');
                 try
                    [markerData]=IdentifyMarker(videotoanalyse,0,1);
                    csvwrite(outputMarkerFilename,markerData);
                catch
                    disp('no markers found!');
                 end
            end
        end
    end
end

if saccadeDetection==true
    status=SaccadeDetectionMacroCLAB(topLevelFolder,'RossiLab',field_size);
end