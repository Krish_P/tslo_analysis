%% Microsaccade detection
% This script runs through the entire dataset and calls the filter traces
% and saccade detection function from ReVAS

%% Directory Setup
%set top level folder here
topLevelFolder='/mnt/hd/ao/vids';
cd (topLevelFolder)
%we use the key file to index into the different folders of the top level
%folder
keyFile=readtable('/mnt/hd/ao/psy_list.csv','Delimiter',',');

%% Parameter setup:
%First we setup the parameters for the filter function:
filterParams.filter1='medfilt1';
filterParams.filter1Params = 7;
filterParams.filter2 = 'sgolayfilt';
filterParams.filter2Params = [3 21];
filterParams.enableVerbosity=0;
filerParams.overwrite=1;

%% Main loop
counter=0;
num_elements=height(keyFile);
fid=fopen('/mnt/hd/ao/vids/ReVASFailedSaccadeList.txt','a');
for k = 1 : height(keyFile)
    waitbar(k/num_elements);
    counter=counter+1;
    %first we get the folder path from the key file
    folderName=keyFile.date{k};
    folderPath=[topLevelFolder,'/',folderName];
    if exist(folderPath,'dir') %this ensures that the dir is valid
        cd(folderPath);
        listoffiles=dir(folderPath);
        if isempty(listoffiles)
            continue
        else
            for fileIdx=1:length(listoffiles)
                 filename=listoffiles(fileIdx).name;
                 try
                     if (strfind(filename,'EyePos'))>0
                         outputFilterName=strcat(filename(1:strfind(filename,'.csv')-1),'_Filtered.mat');
                         outputsacName=strcat(filename(1:strfind(filename,'.csv')-1),'_sacsdrifts.mat');
                         Data=readtable(filename,'Delimiter',',');
                         eyepos=[Data.xpos,Data.ypos,Data.time];
                         eyeposFiltered= FilterEyePosition(eyepos,filterParams);
                         [inputArgument, params] = ...
                            FindSaccadesAndDrifts(eyeposFiltered);
                        save(outputsacName,'params');save(outputFilterName,'eyeposFiltered');
                     end
                 catch
                      fprintf(fid,'%s,%s\n',cd,filename);                       
                 end
            end
        end
    end
end