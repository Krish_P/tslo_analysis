
reRefParams.enableGPU = false;
reRefParams.reRefCorrMethod = 'mex';
reRefParams.enableVerbosity = true;
reRefParams.fixTorsion = false;
reRefParams.tilts = -5:.25:5;
reRefParams.anchorRowNumber = [];
reRefParams.anchorStripHeight = 15;
reRefParams.anchorStripWidth = [];
reRefParams.axesHandles = [];

globalRefImage= imread('D:\KSP\Study\Houston\Projects\TSLOExperiments\AOData\SampleData\AO020R_montage.tif');
reRefParams.globalRefArgument=im2double(globalRefImage(:,:,1));
reRefParams.referenceFrame = rot90(fliplr((stabilizedframe(:,:,1)/256)));

