%% Microsaccade detection
% This script runs through the entire dataset and calls the filter traces
% and saccade detection function from ReVAS
% Analysis Method can be either 'SBS' or 'RossiLab'
function status=SaccadeDetectionMacroCLAB(topLevelFolder,analysisMethod,fieldSize)
    %% Parameter setup:
    %First we setup the parameters for the filter function:
    filterParams.filter1='medfilt1';
    filterParams.filter1Params = 7;
    filterParams.filter2 = 'sgolayfilt';
    filterParams.filter2Params = [3 21];
    filterParams.enableVerbosity=0;
    filerParams.overwrite=1;

    %% Main loop
    counter=0;
    listoffiles=dir(topLevelFolder);
    cd(topLevelFolder)
    for fileIdx=1:length(listoffiles)
         filename=listoffiles(fileIdx).name;
%          try
         if strcmp(analysisMethod,"SBS")==1
             if (strfind(filename,'EyePosNaN'))>0
                 outputFilterName=strcat(filename(1:strfind(filename,'.csv')-1),'_Filtered.mat');
                 outputsacName=strcat(filename(1:strfind(filename,'.csv')-1),'_sacsdrifts.mat');
                 Data=readtable(filename,'Delimiter',',');
                 eyepos=[Data.xpos,Data.ypos,Data.time];
                 eyeposFiltered= FilterEyePosition(eyepos,filterParams);
                 [inputArgument, params] = ...
                    FindSaccadesAndDrifts(eyeposFiltered);
                 save(outputsacName,'params');save(outputFilterName,'eyeposFiltered');
             end
         elseif strcmp(analysisMethod,"RossiLab")==1
             if (strfind(filename,'offsetdata'))>0
                 if ~contains(filename,'Filtered') && ~contains(filename,"sacsdrifts")
                     try
                         outputFilterName=strcat(filename(1:strfind(filename,'.mat')-1),'_Filtered.mat');
                         outputsacName=strcat(filename(1:strfind(filename,'.mat')-1),'_sacsdrifts.mat');
                         Data=load(filename);
                         % We get the total time based on number of samples @ 480 Hz
                        totalLength=length(Data.strip_offsets)/480;
                        timeStamp=flip(rot90(linspace(0,totalLength,length(Data.strip_offsets))));
                        % we sort them into eye positions
                        % we flip both x & y values to get the right
                        % direction in visual field (going from retinal
                        % space)
                        eyepos=[-Data.strip_offsets(:,2)*(fieldSize/512),-Data.strip_offsets(:,1)*(fieldSize/512),...
                        timeStamp];
                        eyeposFiltered= FilterEyePosition(eyepos,filterParams);
                         [inputArgument, params] = ...
                        FindSaccadesAndDrifts(eyeposFiltered);
                        save(outputsacName,'params');save(outputFilterName,'eyeposFiltered');
                     catch
                         filedFiles(counter)=filename;
                         counter=counter+1;
                     end
                 end
             end
         end
 
%          catch
%               disp('File Failed!');                       
%          end
    end
    % when completed we set the status parameter to 1
    status=1;
end