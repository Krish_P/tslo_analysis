%% Microsaccade detection
% This script runs through the entire dataset and calls the filter traces
% and saccade detection function from ReVAS

%% directory setup
%set top level folder here
topLevelFolder='/home/coateslab/LabData/TSLO_Experiment/TSLO_videos/crowding_microsaccade/S01/Session4/';
cd (topLevelFolder)

%% Parameter setup:
%First we setup the parameters for the filter function:
filterParams.filter1='medfilt1';
filterParams.filter1Params = 7;
filterParams.filter2 = 'sgolayfilt';
filterParams.filter2Params = [3 21];
filterParams.enableVerbosity=1;
filerParams.overwrite=1;

%% Main loop

%first we get the folder path from the key file
folderPath=[topLevelFolder];%,'/',folderName];
if exist(folderPath,'dir') %this ensures that the dir is valid
    cd(folderPath);
    listoffiles=dir(folderPath);

    for fileIdx=1:length(listoffiles)
         filename=listoffiles(fileIdx).name;
%          try
             if (strfind(filename,'EyePos'))>0
                 outputFilterName=strcat(filename(1:strfind(filename,'.csv')-1),'_Filtered.mat');
                 outputsacName=strcat(filename(1:strfind(filename,'.csv')-1),'_sacsdrifts.mat');
                 Data=readtable(filename,'Delimiter',',');
                 eyepos=[Data.xpos,Data.ypos,Data.time];
                 eyeposFiltered= FilterEyePosition(eyepos,filterParams);
                 [inputArgument, params] = ...
                    FindSaccadesAndDrifts(eyeposFiltered);
                save(outputsacName,'params');save(outputFilterName,'eyeposFiltered');
             end
%          catch
%               disp('File Failed!');                     
%          end
    end
end



%% Save function when running parallel pool
function parsave(fname,x)
    save(fname,'x')
end