%% AOSLO Example : MNA
% Running pipeline completely in memory. 

% start fresh
clearvars;
close all;
clc;

fprintf('\n\n\n ------------------- DemoScript 2nd Example: AOSLO ----------------------- \n\n\n');

% get video path. The demo videos must be under /demo folder, i.e., already
% added to MATLAB path.
inputVideoPath = ('/home/coateslab/TSLO/tslo_analysis/ReVAS-mna/demo/AO020R_V003.avi'); 

% Read the input video into memory
videoArray = ReadVideoToArray(inputVideoPath);

% for loading default params, use an empty struct
ap = struct;
ap.enableVerbosity = true;

%%%%%%%%%%%%%%%%%%%%%%%
% Run desired modules.
%%%%%%%%%%%%%%%%%%%%%%%

% Find bad frames 
[videoArray, ap] = FindBlinkFrames(videoArray, ap);

% Stimulus removal
% ap.stimulus = imread('cross.png'); % example of using an image file
% videoArray = RemoveStimuli(videoArray, ap);

% Contrast enhancement
[videoArray, ap] = GammaCorrect(videoArray, ap);

% Bandpass filtering
[videoArray, ap] = BandpassFilter(videoArray, ap);

% Extract eye motion
ap.enableVerbosity = 'frame';
[~, ap] = StripAnalysis(videoArray, ap);

% convert position from pixel to degree. 
% note that one does not need to use Pixel2Degree module for this. In this 
% example, we will do the conversion ourselves.
ap.fov = 0.83;
ap.frameWidth = 512;
ap.positionDeg = ap.position * ap.fov / ap.frameWidth;

% eye position filtering
[filteredPositionDegAndTime, ap] = FilterEyePosition([ap.positionDeg ap.timeSec], ap);

% Classify eye motion into events
[~,ap] = FindSaccadesAndDrifts(filteredPositionDegAndTime, ap); %#ok<*ASGLU>

% look at the parameters structure
disp(ap)


